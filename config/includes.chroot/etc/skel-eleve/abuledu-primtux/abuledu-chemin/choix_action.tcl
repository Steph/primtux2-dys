#!/bin/sh
# choix_action.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
#**************************************************************************
#  File  : choix_action.tcl
#  Author  : Andr� Connes <andre.connes@toulouse.iufm.fr>
#  Modifier:
#  Date    : 08/11/2004
#  Licence : GNU/GPL Version 2 ou plus
#
#  Description:
#  ------------
#
#  @version    $Id: choix_action.tcl,v 1.2 2006/03/27 13:13:16 abuledu_andre Exp $
#  @author     Andr� Connes
#  @modifier
#  @project    Le terrier
#  @copyright  Andr� Connes
#
#***********************************************************************

set didacticiel [lindex $argv 0]

source chemin.conf
source msg.tcl
source fonts.tcl
source aider.tcl
source eval.tcl

  #
  # langue par d�faut
  #
  set f [open [file join $glob(home_reglages) lang.conf] "r"]
  gets $f lang
  close $f
  ::msgcat::mclocale $lang
  ::msgcat::mcload [file join [file dirname [info script]] msgs]
  #
  # couleurs des cases
  #
  set f [open [file join $glob(home_reglages) couleurs.conf] "r"]
  gets $f vcouleurs
  close $f
  if { $vcouleurs == "Daltonien" } {
    set glob(celcolor) $glob(celDaltonien)
    set glob(doncolor) $glob(donDaltonien)
    set glob(errcolor) $glob(errDaltonien)
  } else {
    set glob(celcolor) $glob(celNonDaltonien)
    set glob(doncolor) $glob(donNonDaltonien)
    set glob(errcolor) $glob(errNonDaltonien)
  } 
  

wm resizable . 0 0
wm geometry . [expr int([winfo vrootwidth .]*0.89)]x[expr int([winfo vrootheight .]*0.8)]+0+0
. configure -background $glob(bgcolor)
#wm title . "[mc {title_m}]"
wm title . [mc $didacticiel]

frame .frame -width $glob(width) -height $glob(height) -bg $glob(bgcolor)
pack .frame -side top -fill both -expand yes


set c .frame.c
canvas $c -width $glob(width) -height $glob(height) -bg $glob(bgcolor) -highlightbackground $glob(bgcolor)
pack $c -expand true
# pack $c

  button $c.decode \
	-image [image create photo -file [file join sysdata action_decode.png]] \
	-borderwidth 7 -cursor heart \
	-command "exec wish ${didacticiel}_decode.tcl 0"
  grid $c.decode -column 1 -row 1 -padx 7 -pady 25 -sticky e
  button $c.immediat \
	-image [image create photo -file [file join sysdata action_immediat.png]] \
	-borderwidth 7 -cursor heart \
	-command "exec wish ${didacticiel}_code_immediat.tcl 0"
  grid $c.immediat -column 2 -row 1 -padx 7 -pady 25 -sticky e
  button $c.differe \
	-image [image create photo -file [file join sysdata action_differe.png]] \
	-borderwidth 7 -cursor heart \
	-command "exec wish ${didacticiel}_code_differe.tcl 0"
  if { $didacticiel == "chemin_route" } {
	$c.differe configure -state disable
  }
  grid $c.differe -column 3 -row 1 -padx 7 -pady 25 -sticky e
  button $c.cache \
	-image [image create photo -file [file join sysdata action_cache.png]] \
	-borderwidth 7 -cursor heart \
	-command "exec wish ${didacticiel}_cache.tcl 0"
  if { $didacticiel == "chemin_route" } {
	$c.cache configure -state disable
  }
  grid $c.cache -column 4 -row 1 -padx 7 -pady 25 -sticky e

  button $c.un_de \
	-image [image create photo -file [file join sysdata action_un_de.png]] \
	-borderwidth 7 -cursor heart \
	-command "exec wish ${didacticiel}_un_de.tcl 0"
  grid $c.un_de -column 1 -row 2 -padx 7 -pady 25 -sticky e
  button $c.deux_des \
	-image [image create photo -file [file join sysdata action_deux_des.png]] \
	-borderwidth 7 -cursor heart \
	-command "exec wish ${didacticiel}_deux_des.tcl 0"
  grid $c.deux_des -column 2 -row 2 -padx 7 -pady 25 -sticky e
  button $c.chiffre \
	-image [image create photo -file [file join sysdata action_chiffre.png]] \
	-borderwidth 7 -cursor heart \
	-command "exec wish ${didacticiel}_chiffre.tcl 0"
  grid $c.chiffre -column 3 -row 2 -padx 7 -pady 25 -sticky e

  button $c.quitter \
	-image [image create photo -file [file join sysdata quitter_minus.gif]] \
	-borderwidth 5 -cursor heart \
	-command exit
  grid $c.quitter -column 4 -row 2 -sticky e
  
