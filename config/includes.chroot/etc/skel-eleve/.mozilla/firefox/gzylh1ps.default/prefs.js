# Mozilla User Preferences

/* Do not edit this file.
 *
 * If you make changes to this file while the application is running,
 * the changes will be overwritten when the application exits.
 *
 * To make a manual change to preferences, you can visit the URL about:config
 */

user_pref("app.update.lastUpdateTime.addon-background-update-timer", 1457974099);
user_pref("app.update.lastUpdateTime.blocklist-background-update-timer", 1457974219);
user_pref("app.update.lastUpdateTime.browser-cleanup-thumbnails", 1457977956);
user_pref("app.update.lastUpdateTime.experiments-update-timer", 1457973906);
user_pref("app.update.lastUpdateTime.search-engine-update-timer", 1457973662);
user_pref("app.update.lastUpdateTime.xpi-signature-verification", 1487014558);
user_pref("beacon.enabled", false);
user_pref("browser.cache.disk.capacity", 122880);
user_pref("browser.cache.disk.filesystem_reported", 1);
user_pref("browser.cache.disk.smart_size.first_run", false);
user_pref("browser.cache.disk.smart_size.use_old_max", false);
user_pref("browser.cache.frecency_experiment", 1);
user_pref("browser.customizemode.tip0.shown", true);
user_pref("browser.disableResetPrompt", true);
user_pref("browser.download.importedFromSqlite", true);
user_pref("browser.laterrun.bookkeeping.profileCreationTime", 1470736202);
user_pref("browser.laterrun.bookkeeping.sessionCount", 14);
user_pref("browser.migration.version", 40);
user_pref("browser.newtabpage.enhanced", false);
user_pref("browser.newtabpage.storageVersion", 1);
user_pref("browser.pagethumbnails.storage_version", 3);
user_pref("browser.places.importBookmarksHTML", false);
user_pref("browser.places.smartBookmarksVersion", 8);
user_pref("browser.preferences.advanced.selectedTabIndex", 3);
user_pref("browser.rights.3.shown", true);
user_pref("browser.safebrowsing.provider.mozilla.lastupdatetime", "1488653228424");
user_pref("browser.safebrowsing.provider.mozilla.nextupdatetime", "1488656828424");
user_pref("browser.search.countryCode", "FR");
user_pref("browser.search.defaultenginename", "https://www.qwantjunior.com/");
user_pref("browser.search.hiddenOneOffs", "Google,Yahoo,Bing,Amazon.fr,Debian packages,DuckDuckGo,eBay France,Portail Lexical - CNRTL");
user_pref("browser.search.region", "FR");
user_pref("browser.search.useDBForOrder", true);
user_pref("browser.sessionstore.upgradeBackup.latestBuildID", "20170223233003");
user_pref("browser.slowStartup.averageTime", 5623);
user_pref("browser.slowStartup.samples", 4);
user_pref("browser.startup.homepage", "https://www.qwantjunior.com/");
user_pref("browser.startup.homepage_override.buildID", "20170223233003");
user_pref("browser.startup.homepage_override.mstone", "51.0.1");
user_pref("browser.syncPromoViewsLeftMap", "{\"addons\":3}");
user_pref("browser.tabs.warnOnClose", false);
user_pref("browser.uiCustomization.state", "{\"placements\":{\"PanelUI-contents\":[\"edit-controls\",\"zoom-controls\",\"new-window-button\",\"save-page-button\",\"print-button\",\"history-panelmenu\",\"fullscreen-button\",\"find-button\"],\"addon-bar\":[\"addonbar-closebutton\",\"status-bar\"],\"PersonalToolbar\":[\"personal-bookmarks\"],\"nav-bar\":[\"urlbar-container\",\"search-container\",\"tranquility-toolbar-button\",\"bookmarks-menu-button\",\"downloads-button\",\"home-button\",\"loop-button\",\"abp-toolbarbutton\",\"ublock0-button\",\"action-button--easyimageblocker-show-panel\",\"action-button--ebab77cf-e530-46cd-86cc-25353a2ed0e0-lc_button1\",\"action-button--ebab77cf-e530-46cd-86cc-25353a2ed0e0-lc_button2\"],\"TabsToolbar\":[\"tabbrowser-tabs\",\"new-tab-button\",\"alltabs-button\"],\"toolbar-menubar\":[\"menubar-items\"]},\"seen\":[\"abp-toolbarbutton\",\"ublock0-button\",\"action-button--easyimageblocker-show-panel\",\"pocket-button\",\"loop-button\",\"developer-button\",\"action-button--ebab77cf-e530-46cd-86cc-25353a2ed0e0-lc_button1\",\"action-button--ebab77cf-e530-46cd-86cc-25353a2ed0e0-lc_button2\"],\"dirtyAreaCache\":[\"PersonalToolbar\",\"nav-bar\",\"TabsToolbar\",\"toolbar-menubar\",\"PanelUI-contents\",\"addon-bar\"],\"currentVersion\":6,\"newElementCount\":0}");
user_pref("datareporting.policy.dataSubmissionPolicyAcceptedVersion", 2);
user_pref("datareporting.policy.dataSubmissionPolicyNotifiedTime", "1470742290902");
user_pref("datareporting.sessions.current.activeTicks", 2);
user_pref("datareporting.sessions.current.clean", true);
user_pref("datareporting.sessions.current.firstPaint", 3714);
user_pref("datareporting.sessions.current.main", 21);
user_pref("datareporting.sessions.current.sessionRestored", 2983);
user_pref("datareporting.sessions.current.startTime", "1488653321016");
user_pref("datareporting.sessions.current.totalTime", 11);
user_pref("datareporting.sessions.currentIndex", 30);
user_pref("datareporting.sessions.previous.0", "{\"s\":1470736200067,\"a\":10,\"t\":58,\"c\":true,\"m\":20,\"fp\":1059,\"sr\":4572}");
user_pref("datareporting.sessions.previous.1", "{\"s\":1470742275140,\"a\":2,\"t\":24,\"c\":true,\"m\":122,\"fp\":4212,\"sr\":3921}");
user_pref("datareporting.sessions.previous.10", "{\"s\":1471529718727,\"a\":4,\"t\":21,\"c\":true,\"m\":120,\"fp\":3317,\"sr\":1962}");
user_pref("datareporting.sessions.previous.11", "{\"s\":1471529746301,\"a\":6,\"t\":28,\"c\":true,\"m\":21,\"fp\":2600,\"sr\":1209}");
user_pref("datareporting.sessions.previous.12", "{\"s\":1471529780546,\"a\":11,\"t\":111,\"c\":true,\"m\":10,\"fp\":2268,\"sr\":1258}");
user_pref("datareporting.sessions.previous.13", "{\"s\":1477501495865,\"a\":10,\"t\":55,\"c\":true,\"m\":21,\"fp\":1014,\"sr\":4540}");
user_pref("datareporting.sessions.previous.14", "{\"s\":1477501552459,\"a\":1,\"t\":7,\"c\":true,\"m\":310,\"fp\":3355,\"sr\":3561}");
user_pref("datareporting.sessions.previous.15", "{\"s\":1487014435253,\"a\":22,\"t\":142,\"c\":true,\"m\":31,\"fp\":1098,\"sr\":5878}");
user_pref("datareporting.sessions.previous.16", "{\"s\":1487014577589,\"a\":1,\"t\":8,\"c\":true,\"m\":298,\"fp\":4462,\"sr\":4372}");
user_pref("datareporting.sessions.previous.17", "{\"s\":1487014590073,\"a\":2,\"t\":12,\"c\":false,\"m\":10,\"fp\":2818,\"sr\":2707}");
user_pref("datareporting.sessions.previous.18", "{\"s\":1487014714366,\"a\":3,\"t\":15,\"c\":true,\"m\":160,\"fp\":3321,\"sr\":3223}");
user_pref("datareporting.sessions.previous.19", "{\"s\":1487442145657,\"a\":12,\"t\":72,\"c\":true,\"m\":120,\"fp\":4787,\"sr\":4583}");
user_pref("datareporting.sessions.previous.2", "{\"s\":1470753401150,\"a\":5,\"t\":43,\"c\":true,\"m\":133,\"fp\":3233,\"sr\":2335}");
user_pref("datareporting.sessions.previous.20", "{\"s\":1487442217965,\"a\":0,\"t\":4,\"c\":true,\"m\":84,\"fp\":-1,\"sr\":-1}");
user_pref("datareporting.sessions.previous.21", "{\"s\":1487442226930,\"a\":12,\"t\":61,\"c\":true,\"m\":10,\"fp\":2834,\"sr\":2709}");
user_pref("datareporting.sessions.previous.22", "{\"s\":1487442295718,\"a\":2,\"t\":13,\"c\":true,\"m\":30,\"fp\":3554,\"sr\":2891}");
user_pref("datareporting.sessions.previous.23", "{\"s\":1487442313590,\"a\":1,\"t\":6,\"c\":true,\"m\":150,\"fp\":3705,\"sr\":2998}");
user_pref("datareporting.sessions.previous.24", "{\"s\":1488643153170,\"a\":7,\"t\":38,\"c\":true,\"m\":51,\"fp\":5393,\"sr\":4359}");
user_pref("datareporting.sessions.previous.25", "{\"s\":1488643192631,\"a\":2,\"t\":10,\"c\":true,\"m\":46,\"fp\":3493,\"sr\":3276}");
user_pref("datareporting.sessions.previous.26", "{\"s\":1488643207500,\"a\":5,\"t\":28,\"c\":true,\"m\":20,\"fp\":3598,\"sr\":2917}");
user_pref("datareporting.sessions.previous.27", "{\"s\":1488653215003,\"a\":5,\"t\":38,\"c\":true,\"m\":829,\"fp\":11765,\"sr\":10981}");
user_pref("datareporting.sessions.previous.28", "{\"s\":1488653253784,\"a\":7,\"t\":37,\"c\":true,\"m\":490,\"fp\":5379,\"sr\":5172}");
user_pref("datareporting.sessions.previous.29", "{\"s\":1488653292166,\"a\":5,\"t\":28,\"c\":true,\"m\":9,\"fp\":3854,\"sr\":3645}");
user_pref("datareporting.sessions.previous.3", "{\"s\":1470832225274,\"a\":3,\"t\":16,\"c\":true,\"m\":102,\"fp\":3118,\"sr\":2044}");
user_pref("datareporting.sessions.previous.4", "{\"s\":1470832482770,\"a\":2,\"t\":11,\"c\":true,\"m\":10,\"fp\":3026,\"sr\":1343}");
user_pref("datareporting.sessions.previous.5", "{\"s\":1470832744238,\"a\":2,\"t\":12,\"c\":false,\"m\":20,\"fp\":2896,\"sr\":1310}");
user_pref("datareporting.sessions.previous.6", "{\"s\":1470852588594,\"a\":3,\"t\":22,\"c\":true,\"m\":639,\"fp\":7543,\"sr\":7328}");
user_pref("datareporting.sessions.previous.7", "{\"s\":1470852694488,\"a\":4,\"t\":21,\"c\":true,\"m\":121,\"fp\":2583,\"sr\":1561}");
user_pref("datareporting.sessions.previous.8", "{\"s\":1470852721176,\"a\":1,\"t\":7,\"c\":true,\"m\":20,\"fp\":2271,\"sr\":1268}");
user_pref("datareporting.sessions.previous.9", "{\"s\":1471529632139,\"a\":15,\"t\":80,\"c\":true,\"m\":617,\"fp\":7131,\"sr\":7459}");
user_pref("dom.apps.lastUpdate.buildID", "20161002043611");
user_pref("dom.apps.lastUpdate.mstone", "49.0");
user_pref("dom.apps.reset-permissions", true);
user_pref("dom.mozApps.used", true);
user_pref("e10s.rollout.cohort", "unsupportedChannel");
user_pref("experiments.activeExperiment", false);
user_pref("extensions.adbeaver.addonId", "9f497a7d-3b66-44e7-b5e7-78588610abab");
user_pref("extensions.adbeaver.campaign", "publicfox");
user_pref("extensions.adbeaver.foxypid", 1087);
user_pref("extensions.adblockplus.notificationdata", "{\"lastCheck\":1456945141813,\"softExpiration\":1457006004183,\"hardExpiration\":1457105794028,\"data\":{\"notifications\":[],\"version\":\"201603021535\"},\"lastError\":0,\"downloadStatus\":\"synchronize_ok\",\"downloadCount\":1}");
user_pref("extensions.blocklist.pingCountTotal", 3);
user_pref("extensions.blocklist.pingCountVersion", -1);
user_pref("extensions.bootstrappedAddons", "{\"uBlock0@raymondhill.net\":{\"version\":\"1.6.4\",\"type\":\"extension\",\"descriptor\":\"/home/eleve/.mozilla/firefox/gzylh1ps.default/extensions/uBlock0@raymondhill.net.xpi\",\"multiprocessCompatible\":true,\"runInSafeMode\":false,\"dependencies\":[],\"hasEmbeddedWebExtension\":false},\"SilentBlock@schuzak.jp\":{\"version\":\"3.0.0.1\",\"type\":\"extension\",\"descriptor\":\"/home/eleve/.mozilla/firefox/gzylh1ps.default/extensions/SilentBlock@schuzak.jp.xpi\",\"multiprocessCompatible\":true,\"runInSafeMode\":false,\"dependencies\":[],\"hasEmbeddedWebExtension\":false},\"{ebab77cf-e530-46cd-86cc-25353a2ed0e0}\":{\"version\":\"1.2.5\",\"type\":\"extension\",\"descriptor\":\"/home/eleve/.mozilla/firefox/gzylh1ps.default/extensions/{ebab77cf-e530-46cd-86cc-25353a2ed0e0}.xpi\",\"multiprocessCompatible\":false,\"runInSafeMode\":false,\"dependencies\":[],\"hasEmbeddedWebExtension\":false},\"e10srollout@mozilla.org\":{\"version\":\"1.7\",\"type\":\"extension\",\"descriptor\":\"/usr/lib/firefox/browser/features/e10srollout@mozilla.org.xpi\",\"multiprocessCompatible\":true,\"runInSafeMode\":true,\"dependencies\":[],\"hasEmbeddedWebExtension\":false},\"firefox@getpocket.com\":{\"version\":\"1.0.5\",\"type\":\"extension\",\"descriptor\":\"/usr/lib/firefox/browser/features/firefox@getpocket.com.xpi\",\"multiprocessCompatible\":true,\"runInSafeMode\":true,\"dependencies\":[],\"hasEmbeddedWebExtension\":false},\"webcompat@mozilla.org\":{\"version\":\"1.0\",\"type\":\"extension\",\"descriptor\":\"/usr/lib/firefox/browser/features/webcompat@mozilla.org.xpi\",\"multiprocessCompatible\":true,\"runInSafeMode\":true,\"dependencies\":[],\"hasEmbeddedWebExtension\":false},\"aushelper@mozilla.org\":{\"version\":\"1.0\",\"type\":\"extension\",\"descriptor\":\"/usr/lib/firefox/browser/features/aushelper@mozilla.org.xpi\",\"multiprocessCompatible\":true,\"runInSafeMode\":true,\"dependencies\":[],\"hasEmbeddedWebExtension\":false},\"langpack-fr@firefox.mozilla.org\":{\"version\":\"51.0.1\",\"type\":\"locale\",\"descriptor\":\"/usr/lib/firefox/browser/extensions/langpack-fr@firefox.mozilla.org.xpi\",\"multiprocessCompatible\":false,\"runInSafeMode\":false,\"dependencies\":[],\"hasEmbeddedWebExtension\":false}}");
user_pref("extensions.databaseSchema", 19);
user_pref("extensions.dlwatch.aboutconfiglock", true);
user_pref("extensions.dlwatch.addbookmarklock", false);
user_pref("extensions.dlwatch.addonslock", true);
user_pref("extensions.dlwatch.authlastreturn", false);
user_pref("extensions.dlwatch.authopen", false);
user_pref("extensions.dlwatch.blocklinks", false);
user_pref("extensions.dlwatch.bookmarkSidebarLock", false);
user_pref("extensions.dlwatch.customizeToolbarLock", false);
user_pref("extensions.dlwatch.ext", "exe,bat");
user_pref("extensions.dlwatch.hideBookmarksMenu", false);
user_pref("extensions.dlwatch.hideHistoryMenu", false);
user_pref("extensions.dlwatch.hideSafeMode", false);
user_pref("extensions.dlwatch.historylock", false);
user_pref("extensions.dlwatch.libraryLock", false);
user_pref("extensions.dlwatch.lock", false);
user_pref("extensions.dlwatch.optionslock", true);
user_pref("extensions.dlwatch.pass", "0557a1c162b42bf67d4835cfdd4c181b");
user_pref("extensions.dlwatch.sanitizeLock", false);
user_pref("extensions.dlwatch.urlblockenabled", true);
user_pref("extensions.dlwatch.urlblocklisttype", "blacklistRadio");
user_pref("extensions.dlwatch.urlblocklocations", "");
user_pref("extensions.dlwatch.urlblocktempallow", "");
user_pref("extensions.e10s.rollout.hasAddon", false);
user_pref("extensions.e10sBlockedByAddons", true);
user_pref("extensions.enabledAddons", "tranquility%40ushnisha.com:2.0,D_L%40aol.com:1.0.0,%7B972ce4c6-7e08-4474-a285-3208198ce6fd%7D:51.0.1");
user_pref("extensions.getAddons.cache.lastUpdate", 1487014438);
user_pref("extensions.getAddons.databaseSchema", 5);
user_pref("extensions.lastAppVersion", "51.0.1");
user_pref("extensions.lastPlatformVersion", "51.0.1");
user_pref("extensions.pendingOperations", false);
user_pref("extensions.publicfox.lastcheck", "1456932911163");
user_pref("extensions.shownSelectionUI", true);
user_pref("extensions.silentblock.altdir", "");
user_pref("extensions.silentblock.filter.combineThreshold", 64);
user_pref("extensions.silentblock.filter.maxCombinedSize.KiB", 16);
user_pref("extensions.silentblock.filter.maxTotalSize.MiB", 4);
user_pref("extensions.silentblock.logging.enabled", false);
user_pref("extensions.systemAddonSet", "{\"schema\":1,\"addons\":{}}");
user_pref("extensions.tranquility.firstrun", false);
user_pref("extensions.ublock0.cloudStorage.myFiltersPane", "");
user_pref("extensions.ublock0.cloudStorage.myRulesPane", "");
user_pref("extensions.ublock0.cloudStorage.tpFiltersPane", "");
user_pref("extensions.ublock0.cloudStorage.whitelistPane", "");
user_pref("extensions.ui.dictionary.hidden", true);
user_pref("extensions.ui.experiment.hidden", true);
user_pref("extensions.ui.lastCategory", "addons://list/extension");
user_pref("extensions.ui.locale.hidden", false);
user_pref("extensions.xpiState", "{\"app-profile\":{\"SilentBlock@schuzak.jp\":{\"d\":\"/home/eleve/.mozilla/firefox/gzylh1ps.default/extensions/SilentBlock@schuzak.jp.xpi\",\"e\":true,\"v\":\"3.0.0.1\",\"st\":1468418110000},\"uBlock0@raymondhill.net\":{\"d\":\"/home/eleve/.mozilla/firefox/gzylh1ps.default/extensions/uBlock0@raymondhill.net.xpi\",\"e\":true,\"v\":\"1.6.4\",\"st\":1468418110000},\"tranquility@ushnisha.com\":{\"d\":\"/home/eleve/.mozilla/firefox/gzylh1ps.default/extensions/tranquility@ushnisha.com.xpi\",\"e\":true,\"v\":\"2.0\",\"st\":1488643193000},\"{ebab77cf-e530-46cd-86cc-25353a2ed0e0}\":{\"d\":\"/home/eleve/.mozilla/firefox/gzylh1ps.default/extensions/{ebab77cf-e530-46cd-86cc-25353a2ed0e0}.xpi\",\"e\":true,\"v\":\"1.2.5\",\"st\":1487442253000},\"D_L@aol.com\":{\"d\":\"/home/eleve/.mozilla/firefox/gzylh1ps.default/extensions/D_L@aol.com\",\"e\":true,\"v\":\"1.0.0\",\"st\":1488653275000,\"mt\":1488653251000}},\"app-system-defaults\":{\"webcompat@mozilla.org\":{\"d\":\"/usr/lib/firefox/browser/features/webcompat@mozilla.org.xpi\",\"e\":true,\"v\":\"1.0\",\"st\":1487921008000},\"aushelper@mozilla.org\":{\"d\":\"/usr/lib/firefox/browser/features/aushelper@mozilla.org.xpi\",\"e\":true,\"v\":\"1.0\",\"st\":1487921008000},\"e10srollout@mozilla.org\":{\"d\":\"/usr/lib/firefox/browser/features/e10srollout@mozilla.org.xpi\",\"e\":true,\"v\":\"1.7\",\"st\":1487921008000},\"firefox@getpocket.com\":{\"d\":\"/usr/lib/firefox/browser/features/firefox@getpocket.com.xpi\",\"e\":true,\"v\":\"1.0.5\",\"st\":1487921008000}},\"app-global\":{\"langpack-fr@firefox.mozilla.org\":{\"d\":\"/usr/lib/firefox/browser/extensions/langpack-fr@firefox.mozilla.org.xpi\",\"e\":true,\"v\":\"51.0.1\",\"st\":1487917867000},\"{972ce4c6-7e08-4474-a285-3208198ce6fd}\":{\"d\":\"/usr/lib/firefox/browser/extensions/{972ce4c6-7e08-4474-a285-3208198ce6fd}.xpi\",\"e\":true,\"v\":\"51.0.1\",\"st\":1487921008000}}}");
user_pref("extensions.{ebab77cf-e530-46cd-86cc-25353a2ed0e0}.sdk.baseURI", "resource://ebab77cf-e530-46cd-86cc-25353a2ed0e0/");
user_pref("extensions.{ebab77cf-e530-46cd-86cc-25353a2ed0e0}.sdk.domain", "ebab77cf-e530-46cd-86cc-25353a2ed0e0");
user_pref("extensions.{ebab77cf-e530-46cd-86cc-25353a2ed0e0}.sdk.load.reason", "startup");
user_pref("extensions.{ebab77cf-e530-46cd-86cc-25353a2ed0e0}.sdk.rootURI", "jar:file:///home/eleve/.mozilla/firefox/gzylh1ps.default/extensions/%7Bebab77cf-e530-46cd-86cc-25353a2ed0e0%7D.xpi!/");
user_pref("extensions.{ebab77cf-e530-46cd-86cc-25353a2ed0e0}.sdk.version", "1.2.5");
user_pref("foxvox.counter", "13");
user_pref("foxvox.extpath", "/home/eleve/.mozilla/firefox/gzylh1ps.default/extensions/foxvox@wordit.com");
user_pref("foxvox.voice", "fr1");
user_pref("gecko.buildID", "20160803013011");
user_pref("gecko.mstone", "48.0");
user_pref("idle.lastDailyNotification", 1457974362);
user_pref("media.gmp-manager.buildID", "20170205082839");
user_pref("media.gmp-manager.lastCheck", 1487442211);
user_pref("media.gmp.storage.version.observed", 1);
user_pref("network.cookie.prefsMigrated", true);
user_pref("network.http.speculative-parallel-limit", 0);
user_pref("network.predictor.cleaned-up", true);
user_pref("network.prefetch-next", false);
user_pref("pdfjs.migrationVersion", 2);
user_pref("pdfjs.previousHandler.alwaysAskBeforeHandling", true);
user_pref("pdfjs.previousHandler.preferredAction", 4);
user_pref("places.database.lastMaintenance", 1457974362);
user_pref("places.history.expiration.transient_current_max_pages", 52685);
user_pref("plugin.disable_full_page_plugin_for_types", "application/pdf");
user_pref("plugin.importedState", true);
user_pref("pref.downloads.disable_button.edit_actions", false);
user_pref("privacy.cpd.offlineApps", true);
user_pref("privacy.cpd.siteSettings", true);
user_pref("privacy.sanitize.migrateFx3Prefs", true);
user_pref("privacy.sanitize.timeSpan", 0);
user_pref("services.sync.clients.lastSync", "0");
user_pref("services.sync.clients.lastSyncLocal", "0");
user_pref("services.sync.declinedEngines", "");
user_pref("services.sync.globalScore", 0);
user_pref("services.sync.migrated", true);
user_pref("services.sync.nextSync", 0);
user_pref("services.sync.prefs.sync.extensions.ublock0.cloudStorage.myFiltersPane", true);
user_pref("services.sync.prefs.sync.extensions.ublock0.cloudStorage.myRulesPane", true);
user_pref("services.sync.prefs.sync.extensions.ublock0.cloudStorage.tpFiltersPane", true);
user_pref("services.sync.prefs.sync.extensions.ublock0.cloudStorage.whitelistPane", true);
user_pref("services.sync.tabs.lastSync", "0");
user_pref("services.sync.tabs.lastSyncLocal", "0");
user_pref("signon.importedFromSqlite", true);
user_pref("storage.vacuum.last.index", 0);
user_pref("storage.vacuum.last.places.sqlite", 1457974362);
user_pref("toolkit.startup.last_success", 1488653321);
user_pref("toolkit.telemetry.cachedClientID", "a6651808-46c3-40ce-bb40-431571047312");
user_pref("toolkit.telemetry.previousBuildID", "20170223233003");
user_pref("toolkit.telemetry.reportingpolicy.firstRun", false);
user_pref("xpinstall.signatures.required", false);
