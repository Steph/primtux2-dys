﻿MathEOS
=======
_Réalisé par **François Billioud**, avec la participation de **Guillaume Varoqueaux**_

###Logiciel de mathématiques destiné au collège

Ce logiciel a pour vocation à proposer aux élèves une interface unique pour prendre leurs cours de mathématiques. Il permet de créer et gérer un cahier de mathématique virtuel en y incluant des opérations, des graphiques, des tableaux de proportionalités, des formules mathématiques... Tout ce dont un élève du collège peut avoir besoin.

Le logiciel a été développé en partenariat avec des enseignants de mathématiques et des enseignants spécialisés afin de le rendre conforme au besoin du programme de mathématiques, tout en le rendant accessible aux personnes en situation de handicap.

Le logiciel a été réalisé par François Billioud et Guillaume Varoquaux sur une idée originale de Ludovic Faubourg et Frédéric Marinoni

* Site internet :<br>
  http://lecoleopensource.fr/matheos/

* Contact :<br/>
  Pour toute question à propos du logiciel, contactez-nous par mail:
f.billioud@gmail.com
 
 
* Installation :
  - Dézippez MathEOS.zip
  - Double-cliquez sur MathEOS.jar, ou lancez la commande `java -jar chemin/vers/MathEOS.jar`
  - Créez votre profil.
  - Choisissez le répertoire où enregistrer votre fichier.
  - C'est prêt !


* Copyright et licence :<br/>
Le logiciel est distribué sous licence propriétaire. Le contrat de licence est fourni avec le logiciel. Si vous n'avez pas reçu et accepté le contrat de licence utilisateur final, vous ne pouvez pas utiliser le logiciel.

* Configuration :<br/>
Pour l'instant seuls une langue et deux thèmes sont disponibles, mais de nombreux thèmes devraient se développer à l'avenir.

* Mode d'emploi :<br/>
Nous travaillons sur un système de tutoriel qui permettrait aux utilisateurs de se familiariser assez facilement et intuitivement avec le logiciel. En attendant, vous pouvez regarder à cette adresse :
http://lecoleopensource.fr/matheos?part=guide

* Bugs connus :


* Troubleshooting :


* Remerciements :
Guillaume Varoquaux,
Tristan Coulange,
Frédéric Marinoni,
Ludovic Faubourg,
L'association des PEP06,
L'académie de Nice
