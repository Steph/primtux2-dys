folder::fr
matheos file::fichier de profil MathEOS (*.mos, *.mts, *.bmc)
matheos export file::fichier d'export MathEOS (*.mef)
docx file::Microsoft Word file (*.docx)
pdf file::Fichier PDF (*.pdf)
image file::Fichier image (.bmp, .jpg, .png, .gif)
decimal point::,
mark max value::20
measurement system::metric

keyboard<
    a;z;e;r;t;y;u;i;o;p;backspace
    q;s;d;f;g;h;j;k;l;m;enter
    shift;w;x;c;v;b;n;,;:;!;space
>
shifted keyboard<
    A;Z;E;R;T;Y;U;I;O;P;backspace
    Q;S;D;F;G;H;J;K;L;M;enter
    shift;W;X;C;V;B;N;?;.;';space
>

===== MENUS ET BOUTONS PRINCIPAUX =====
-- menu bar --
file::Fichier
edit::Edition
option::Options
help::Aide

- file menu -
new profile::Nouvel utilisateur
open profile::Charger un utilisateur...
save all::Enregistrer
save all description::Enregistrer tous les onglets de cours
save all shortcut::ctrl+S
import::Importer
import description::Importe le contenu d'un traitement de texte depuis un fichier indépendant (.mef)
export::Exporter
export description::Exporte le contenu du traitement de texte dans un fichier indépendant (.mef)
export docx::Générer .docx
export docx description::Exporte le contenu du traitement de texte dans un fichier .docx
export pdf::Générer .pdf
export pdf description::Exporte le contenu du traitement de texte dans un fichier .pdf
import pdf::Import PDF
import pdf description::Importer le Contenu d'un fichier PDF
print::Imprimer...
print shortcut::ctrl+P
layout::Mise en page
quick view::Aperçu avant impression
quit::Quitter

- edition menu -
undo::Annuler
undo description::Annuler
undo shortcut::ctrl+Z
redo::Refaire
redo description::Refaire
redo shortcut::ctrl+Y
cut::Couper
cut description::Couper
cut shortcut::ctrl+X
copy::Copier
copy description::Copier
copy shortcut::ctrl+C
paste::Coller
paste description::Coller 
paste shortcut::ctrl+V
svg rendering::Conversion du SVG
as svg::SVG
as embed svg::HTML
as png::PNG
mathML rendering::Conversion du MathML
as mathML::MathML
as OMML::OMML
as image::Image
as text::Texte

-- option menu --
theme::Thème d'affichage
language::Langue
customize::Polices d'écriture
conference::Mode présentation
conference description::L'éditeur reste toujours blanc
touch mode::Mode tactile
touch mode description::Contrôles au doigt ou au stylet
autosave::Sauvegarde automatique
autosave description::Sauvegarde l'éditeur toutes les 30 secondes
customize font size::Éditer...

- help menu -
help shortcut::F1
context sensitive help::Aide contextuelle
about::A propos de MathEOS
about title::A propos de MathEOS
about message::<html><p><b>A propos de MathEOS : </b></p><br/><p><b>MathEOS</b> v. %s <i>PREMIUM</i></p><br/><p><b>MathEOS</b> - Logiciel de mathématiques proposé par<br/><b>l'Ecole Open Source</b></p><p>Réalisé par <b>François Billioud</b> avec la participation<br/> de <b>Guillaume Varoquaux</b></p></html>
about license::Ce logiciel est la propriété de François Billioud. Son utilisation doit se faire conformément à la licence qui vous a été accordée. Si vous n'avez pas encore obtenu de licence, merci de vous en procurer une avant de poursuivre votre utilisation. Attention, ce programme vient <b>SANS ABSOLUMENT AUCUNE GARANTIE</b>. Lisez attentivement la licence pour plus de détails.
about license button::Licence
about shortcut::F2
license::Licence
tutorials::Tutoriels vidéo
tutorials description::Des vidéos en ligne pour se familiariser avec MathEOS
tutorials shortcut::F3
update::Mise à jour
update description::Cherche d'éventuelles mises à jour de l'application
update shortcut::F4
unregister::Désactiver
unregister description::Désactiver MathEOS
premium update::Passer Premium
premium update shortcut::F5

-- main toolbar --
insert description::Insère le TP dans le cahier
insert shortcut::alt+I
edit tp description::Met à jour le TP dans le cahier
edit tp shortcut::alt+E
new tp::Nouveau
new tp description::Vider le contenu de l'onglet actif
new tp shortcut::ctrl+alt+N
special character description::Ouvrir/fermer le clavier de caractères spéciaux
special character shortcut::alt+S

-- bottom toolbar --
numeric keyboard description::Ouvrir/fermer le clavier numérique
numeric keyboard shortcut::alt+N
calculator description::Ouvre une calculatrice
calculator shortcut::alt+C
virtual keyboard description::Ouvrir/fermer le clavier virtuel
virtual keyboard shortcut::alt+V
left arrow description::Agrandit/Diminue la zone active
left arrow shortcut::ctrl+SPACE
right arrow description::Agrandit/Diminue la zone active
right arrow shortcut::ctrl+SPACE
zoom+ description::Zoom sur la zone active
zoom- description::Dézoom sur la zone active
consultation description::Ouvre un ancien chapitre dans une fenêtre séparée
consultation shortcut::alt+O
fullscreen description::Agrandir la partie active à 100%
fullscreen shortcut::alt+F
contents::Sommaire
contents description::Permet d'ouvrir un chapitre ou une évaluation
contents shortcut::ctrl+ENTER

===== ONGLETS =====
- lesson -
notebook::Cahier de cours
workbook::Cahier d'exercices
evaluation::Evaluation

- exercise -
geometry::Géométrie
function::Fonctions
table::Tableau
operation::Opération

== ONGLET TEXTE ==
text bold description::Gras
text bold shortcut::ctrl+G
text italic description::Italique
text italic shortcut::ctrl+I
text underline description::Souligner
text underline shortcut::ctrl+U
text strike description::Barrer
text strike shortcut::ctrl+B
text left align description::Aligner à gauche
text left align shortcut::ctrl+shift+G
text center align description::Aligner au centre
text center align shortcut::ctrl+shift+M
text right align description::Aligner à droite
text right align shortcut::ctrl+shift+D
text size description::Change la taille du texte
text color description::Change la couleur du texte
text title description::Insérer un titre dans l'éditeur
text title shortcut::ctrl+T
text subtitle description::Insérer un titre secondaire dans l'éditeur
text subtitle shortcut::ctrl+shift+T
text correct description::Passer en mode correction
text correct shortcut::ctrl+shift+C
text insert mark description::Insère une note ou un barème
text insert mark shortcut::ctrl+shift+N
text insert formula description::Ouvre le rédacteur de formules
text insert formula shortcut::ctrl+shift+F
text insert image description::Insère une image dans l'éditeur
text insert image shortcut::ctrl+shift+I

content::Contenu
title::Titre
comments::Commentaires
student::élève

save text description::Enregistre l'onglet
save::Sauvegarder
save description::Sauvegarder
save shortcut::ctrl+S
rename::Renommer
rename description::Renommer le chapitre
rename shortcut::ctrl+R

-- Cours --
new chapter description::Crée un nouveau chapitre
new chapter shortcut::ctrl+shift+O
lesson export description::Exporter un cours pour l'envoyer
lesson import description::Importer un cours depuis un fichier .mef

-- Exercices --
new exercise description::Crée un nouvel exercice
new exercise shortcut::ctrl+shift+E
exercises of chapter::Exercices du chapitre
exercise export description::Exporter vos exercices pour les envoyer
exercise import description::Importer une feuille d'exercices depuis un fichier .mef

-- Tests --
new test description::Préparer une nouvelle évaluation
new test shortcut::ctrl+shift+E
test title::Evaluation de Mathématiques
test mark::Note
test remark::Observations
test signature::Signature
test over::Terminé
test solution::Corrigé
test add solution description::Importer un corrigé depuis un fichier .mef
test check solution description::Consulter le corrigé
test change solution::Changer le corrigé
test edit solution description::Créer/éditer le corrigé
test edit test description::Revenir au devoir
test export test description::Exporter le devoir pour pouvoir l'envoyer
test export solution description::Exporter le corrigé pour pouvoir l'envoyer
test remove solution description::Supprimer le corrigé
test comments description::Commentaires sur l'évaluation
test select test folder description::Choisir le dossier contenant les fichiers élèves
test open student list description::Ouvrir la liste des devoirs des élèves
test save and next description::Sauvegarder la correction et passer à l'élève suivant
test import statement description::Importer l'énoncé d'un devoir (.mef)
test import corrections description::Réimporter votre devoir corrigé par l'enseignant (.mef)
test add auto solution::Ajout corrigé auto
test add auto solution description::Ajouter automatiquement le corrigé aux copies lors de la correction

test header name::Nom, Prénom
test header classroom::Classe
test header start::Début
test header end::Fin
test header last modified::Dernière modification

-- Printing --
preview paper::Type de papier
preview previous description::Page précédente
preview previous shortcut::LEFT
preview next description::Page suivante
preview next shortcut::RIGHT
preview print one description::Imprimer la page actuelle
preview print one shortcut::ctrl+P
preview print all description::Imprimer toutes les pages
preview print all shortcut::ctrl+shift+P

== FORMULES ==
(must be lower-case, with accent)
formula ignore::de|à|des|ou|au|par|est|sont|népérien
function operators<
    ²::carré
    ³::cube
    ³::cubique
    half::demi
    third::tiers
    quarter::quart
    sin::sinus
    cos::cosinus
    tan::tangeante
    ln::logarithme
    exp::exponentielle
    exp::exponentiel
    frac::sur
    root::racine
    +::plus
    -::moins
    ×::fois
    ÷::divisé
    ^::puissance
>
formula operators<
    <::inférieur
    >::supérieur
    =::égal
    =::égale
    =::égaux
    ≠::différent
    ≈::environ
    sub::indice
    angle::angle
>

== PARTIE GRAPHIQUE ==
graphic base::Normal
graphic dot description::Construire un point
graphic dot shortcut::ctrl+shift+P
graphic segment description::Construire un segment
graphic segment shortcut::ctrl+shift+S
graphic line description::Construire une droite
graphic line shortcut::ctrl+shift+L
graphic remove description::Supprimer un élément du graphique
graphic remove shortcut::ctrl+shift+Z
graphic rename description::Renommer un élément du graphique
graphic rename rightclic::Renommer
graphic rename shortcut::ctrl+shift+R
graphic text::Texte
graphic text description::Ajouter un texte
graphic text shortcut::ctrl+shift+T
graphic color description::Changer la couleur du tracé
graphic paint description::Colorer un élément du graphique
graphic paint shortcut::ctrl+shift+C
graphic dashed line::Tracer en pointillés
graphic drag description::Déplacer des éléments du graphique
graphic drag shortcut::ctrl+shift+D
graphic layer description::Ajouter une image sous le dessin
graphic layer shortcut::ctrl+shift+I
graphic resize layer::Redimensionner l'image
graphic resize layer shortcut::I
graphic image selection::Choisir...
graphic image selection shortcut::ctrl+shift+C

- options -
graphic magnetism::Magnétisme
graphic magnetism shortcut::ctrl+M
graphic orthonormal mark::Repère orthonormal
graphic orthonormal shortcut::ctrl+O
graphic display graduation::Afficher les graduations
graphic display graduation shortcut::ctrl+shift+G
graphic display readlines::Aide à la lecture
graphic display readlines shortcut::ctrl+L
graphic display cursor::Position du curseur
graphic display cursor shortcut::ctrl+P
graphic display grid::Quadrillage
graphic display grid shortcut::ctrl+shift+Q
graphic display axe x::Axe abscisses x
graphic display axe x shortcut::ctrl+shift+X
graphic display axe y::Axe ordonnées y
graphic display axe y shortcut::ctrl+shift+Y
graphic settle scale::Editer les axes
graphic settle scale shortcut::ctrl+shift+E
graphic settle magnetism::Régler le magnétisme
graphic settle magnetism shortcut::ctrl+shift+M
graphic create mark::Coder la figure
graphic create mark shortcut::C
graphic your text here::Texte

== ONGLET GEOMETRIE ==
geometry half-line description::Construire une demi-droite
geometry half-line shortcut::ctrl+shift+L
geometry arc description::Construire un arc
geometry arc shortcut::ctrl+shift+A
geometry display construction::Dessiner les éléments de construction
geometry display construction shortcut::ctrl+D
geometry mesures precision::Precision des mesures
geometry mesures precision shortcut::ctrl+R
geometry construction::Construction
geometry construction description::<html>Voir la construction<br/>étape par étape</html>
geometry construction shortcut::ctrl+shift+C
geometry cancel construction description::Retourner à la création
geometry cancel construction shortcut::ctrl+shift+C
geometry create orthogonal::Tracer une perpendiculaire
geometry create orthogonal shortcut::O
geometry create parallele::Tracer une parallele
geometry create parallele shortcut::P
geometry middle::Milieu
geometry middle shortcut::M

choose a point::Choisir un point
choose a segment::Choisir un segment
choose radius::Choisir le rayon
choose center::Choisir le centre
choose a start::Choisir un début
choose an end::Choisir une fin

visionneuse restart description::Revenir au début
visionneuse restart shortcut::R
visionneuse previous description::Reculer d'une étape
visionneuse previous shortcut::P
visionneuse next description::Avancer d'une étape
visionneuse next shortcut::S

geometry size::longueur
geometry angle::angle
geometry length::distance
geometry rayon::rayon

== ONGLET FONCTION ==
function points description::Ajouter des points
function points shortcut::ctrl+shift+P
function segments description::Tracer des segments
function segments shortcut::ctrl+shift+S
function line description::Tracer des droites
function line shortcut::ctrl+shift+D
function linear regression description::Tracer une droite moyenne de plusieurs points
function linear regression shortcut::ctrl+shift+L
function interpolation description::Tracer une courbe passant par des points
function interpolation shortcut::ctrl+shift+C
function trace::f(x)=?
function trace description::Tracer une fonction à partir de son équation
function trace shortcut::ctrl+shift+T
function x+::Limiter le domaine à x≥0
function x+ shortcut::ctrl+shift+X
function set domain::Domaine
function set domain description::Éditer le domaine de définition de la fonction
function set domain shortcut::D

== ONGLET TABLE ==
table mode suppression description::Supprimer des colones ou des lignes
table mode suppression shortcut::alt+Z
table mode normal description::Revenir à l'affichage normal
table mode insertion description::Ajouter des colones ou des lignes
table mode insertion shortcut::alt+A
table mode paint description::Colorier des colones, des lignes ou des cases
table mode paint shortcut::alt+C
table mode arrow creation description::Ajouter une flèche de proportionnalité
table mode arrow creation shortcut::alt+F
table mode arrow suppression description::Supprimer une flèche de proportionnalité
table mode arrow suppression shortcut::alt+R

table first cell splitted::Case à double entrée
table first cell splitted shortcut::C

== ONGLET OPERATIONS ==
operation addition description::Effectuer une addition
operation addition shortcut::shift+ADD
operation substraction description::Effectuer une soustraction
operation substraction shortcut::shift+SUBTRACT
operation multiplication description::Effectuer une multiplication
operation multiplication shortcut::shift+MULTIPLY
operation division description::Effectuer une division
operation division shortcut::shift+DIVIDE
operation paint description::Colorer un élément
operation paint shortcut::ctrl+shift+C
operation substract method::Autre méthode de soustraction
operation substract method description::Autre méthode de soustraction
operation substract method shortcut::ctrl+shift+S
operation template colors::Retenir les couleurs
operation template colors description::Applique le même code couleur à chaque nouvelle opération
operation template colors shortcut::ctrl+shift+T

-- SOMMAIRE --
chapter::Chapitre
exercise::Exercice
test::Contrôle

-- BOITES DE DIALOGUE --
-- Constants --
accept::J'accepte
ok::OK
cancel::Annuler
yes::Oui
no::Non
warning::Attention !
error::Erreur !
choose directory::Choisir le dossier
none::Aucun

-- Activation --
dialog manual activation<
    title::Activation Manuelle
    description::<html><font size='3'>Vous ne semblez pas connecté à internet.<br/>Pour activer votre produit hors ligne,<br/>contactez votre revendeur, ou rendez-vous sur<br/>votre <a href="http://lecoleopensouce.fr/matheos/?part=perso">espace perso MathEOS</a> depuis un appareil<br/>connecté, et entrez les informations suivantes.<br/>Ensuite, remplissez le cadre ci-dessous.</font></html>
    serial::Numéro de série
    key::Clé d'activation
    code::Code d'activation
>
error during activation title::Echec de l'activation
error during activation message::<html>L'activation du produit a échouée.<br/>Si vous êtes bien détenteur d'une licence d'utilisation du logiciel,<br/>merci de contacter votre revendeur ou le support.</html>
error serial title::Numéro de série éronné !
error serial message::<html>Le numéro de série de la licence semble éronné.<br/>Vérifiez votre saisie</html>
error license conflict title::Licence déjà activée
error license conflict message::<html>La licence est déjà activée sur une autre machine.<br/>Procédez d'abord à sa désactivation depuis MathEOS<br/>Ou depuis votre espace perso sur le site<br/><a href="http://lecoleopensource.fr/matheos?part=perso">http://lecoleopensource.fr/matheos?part=perso</a></html>
error license not found title::Licence introuvable !
error license not found message::<html>La licence indiquée est introuvable.<br/>Vérifiez votre saisie. Si le problème persiste,<br/>contactez le support ou votre revendeur.</html>
error license gone title::Licence épuisée !
error license gone message::<html>Toutes vos activations autorisées pour cette licence ont été utilisées.<br/>Veuillez acheter une nouvelle licence.<br/>Si vous souhaitez contester le décompte de vos activations,<br/>contactez le support ou votre revendeur.</html>
error license expired title::Licence expirée !
error license expired message::<html>Votre licence a expiré.<br/>Veuillez renouveler votre abonnement.</html>
error license deactivated title::Licence déjà désactivée !
error license deactivated message::<html>Cette licence a déjà été désactivée.<br/>Vous pouvez gérer vos licences depuis votre espace personnel<br/>si vous en avez un, sinon contactez votre revendeur.</html>

-- Validations --
not decimal::Vous devez saisir un nombre décimal !
not integer::Vous devez saisir un nombre entier !
not empty::Le champ ne peut pas être vide !
value not allowed::La valeur doit être

-- Interface --
dialog serial<
    title::Activation
    description::<html>Vous allez procéder à l'activation de votre produit.<br/>Si vous avez acheté le mauvais type de licence,<br/>contactez le service client.</html>
    premium::<html>Licence MathEOS-PREMIUM<br/><font size='3'>(Utilisation personnelle et privée)</font></html>
    education::<html>Licence MathEOS-EDUCATION<br/><font size='3'>(Utilisation professionnelle ou partagée)</font></html>
    promotional::<html>Licence promotionnelle<br/><font size='3'>(reçue lors d'une opération spéciale)</font></html>
    serial::<html>Numéro de série de la licence<br/><font size='3'>(de 4 à 8 caractères, reçu par email, ex: YHVN ou YHVN-VA)</font></html>
    confirm::<html>En activant le produit, vous renoncez à votre<br/>droit de rétractation.</html>
>

dialog new profile<
    title::Nouvel utilisateur
    description::Remplis les champs ci-dessous pour créer ton profil
    lastname::Nom
    firstname::Prénom
    level::Niveau
    classroom::Classe
    classroom description::exemple : entrez A si vous êtes en 6ème A (optionnel)
>

dialog display license<
    title::Licence
    description::Lisez les termes de la license utilisateur
>
dialog accept license<
    title::Licence
    description::Veuillez lire et accepter les termes de la license utilisateur
    accept::J'accepte
>
accept terms before proceeding::Lisez et acceptez les termes pour pouvoir poursuivre

margin-top::Haut
margin-left::Gauche
margin-bottom::Bas
margin-right::Droite

no browser::Impossible de trouver votre navigateur
no chapter::Veuillez créer d'abord votre premier chapitre
not allowed title::Opération restreinte
not allowed message::Vous découvrirez cet outil dans les classes supérieures
new profile advice title::Créez votre premier chapitre pour commencer !
new profile advice message::<html>Créez votre premier chapitre avec<br/>le bouton clignotant en haut à droite.</html>

dialog file not saved title::Fichier non enregistré !
dialog file not saved message::<html>Une erreur est survenue durant la sauvegarde et votre fichier n'a pas pu être enregistré.<br />Si le problème persiste, tentez de copier votre travail dans un nouveau fichier ou contactez l'équipe MathEOS</html>

dialog file input error title::Fichier non chargé !
dialog file input error message::<html>Une erreur est survenue durant le chargement<br />Si le problème persiste, envoyez votre fichier à l'équipe MathEOS</html>

dialog solution load error title::Corrigé non chargé !
dialog solution load error message::Aucun devoir ne correspond à ce corrigé.

dialog student files list title::Liste des devoirs élèves
dialog student files list message::<html>Choisissez l'un des devoirs dans la liste<br/>pour commencer la correction.</html>

dialog empty directory title::Dossier vide !
dialog empty directory message::<html>Le Dossier est vide.<br/>Le dossier doit contenir les documents .mef des élèves</html>

dialog suspicious import title::Action suspecte !
dialog suspicious import message::<html>Attention, ceci remplacera votre contenu. Si vous essayez d'importer un fichier élève,<br/>vous devez utiliser les outils de la barre d'outil<br/>Dans ce cas, appuyez sur Annuler</html>

dialog save old tp title::Sauvegarde du TP en cours
dialog save old tp message::<html><font size='5'>Un TP est déjà en cours dans l'onglet. <br />Souhaitez-vous l'enregistrer ?</font></html>

dialog file already exists title::Fichier existant
dialog file already exists message::<html>Un fichier de ce nom existe déjà.<br/>Voulez-vous supprimer l'ancien fichier pour le remplacer par le votre ?</html>

dialog confirm save title::Modifications non enregistrées
dialog confirm save message::<html>Souhaitez-vous enregistrer les modifications <br/>avant d'effectuer cette opération ?</html>

dialog new chapter title::Nouveau Chapitre
dialog new chapter message::Donner le nom du nouveau chapitre

dialog new test title::Nouvelle Evaluation
dialog new test message::Donner le sujet de l'évaluation

dialog language title::Choix de la langue
dialog language message::Choisissez la langue souhaitée

dialog theme title::Choix du thème
dialog theme message::Choisissez le thème souhaitée

dialog confirm deactivate title::Désactiver MathEOS
dialog confirm deactivate message::<html>Souhaitez-vous réellement désactiver MathEOS ?<br/>Selon votre licence, le nombre d'activations de MathEOS peut être limité.</html>

dialog confirm recover title::Fermeture inopinée
dialog confirm recover message::<html>Le logiciel semble avoir quitté brusquement.<br/>Votre session précédente a été enregistrée.<br/>Souhaitez-vous la récupérer ?</html>

dialog customize<
    title::Thème personnalisé
    description::Sélectionnez les Polices à utiliser.
    interface::Interface
    editor::Editeur de texte
    maths::Formules mathématiques
    sizes::Tailles des polices
>

dialog custom font size<
    title::Taille des polices
    description::Choisissez la taille pour chaque police
    menu::Menus
    item::Élements
    editor::Éditeur
    maths::Objets Mathématiques
    tooltip::Infobulles
    graphic::Légendes de graphique
>

-- Text --
dialog selection too long title::Sélection trop longue
dialog selection too long message::La sélection est trop longue !

dialog create title title::Créer un titre
dialog create title message::Enter le contenu du titre

dialog rename title title::Renommer un titre
dialog rename title message::Indiquez le nom du titre

dialog contents title::Sommaire
dialog contents message::Sélectionnez un chapitre à charger
dialog contents options<
Ouvrir
Supprimer
Annuler
>

dialog remove title::Suppression
dialog remove message::<html><b>Attention !</b> supprimer un élément est une opération irréversible.<br/>Êtes-vous certain de vouloir continuer ?</html>

dialog help comments title::Page de commentaires
dialog help comments message::<html>Ecrivez ici tous les commentaires sur une évaluation.<br/>Vous pouvez utiliser cet espace pour faire des copier/coller<br/>entre les copies des élèves ou pour résumer les erreurs commises.<br/>Le contenu est enregistré automatiquement si vous le fermez.</html>

dialog help solution title::Edition d'un corrigé
dialog help solution message::<html>L'éditeur contient à présent le corrigé du devoir.<br/>Pour revenir au sujet du devoir, cliquez sur <i>Revenir au devoir</i></html>

dialog mark scale<
    title::Barème
    description::Indiquez le barème à introduire
    numerator::Numérateur
    denominator::Dénominateur
>

-- Math --
dialog munder::Cuvette
dialog mover::Chapeau
dialog munderover::Cuvette et chapeau
dialog equation::Equation
dialog msup::Exposant
dialog mfrac::Fraction
dialog msub::Indice
dialog msubsup::Indice et exposant
dialog mmultiscript::Element chimique
dialog mfenced::Parenthèse large
dialog msqrt::Racine carré
dialog mroot::Racine
dialog system::Système d'équation
dialog mtable::Système d'équation
dialog alignment::Alignement
dialog insert formula title::Insérer une formule mathématique
dialog insert formula message::<html>Écrivez votre formule en français.<br/><font size="4">Par exemple a sur racine de 2.</font><br/>Vous pouvez utiliser les opérateurs classiques, mais aussi :<br/><font size="4">%s.</font></html>

selection cut title::Sélection réduite
selection cut message::La sélection a été réduite car elle était trop longue.

dialog angle mark title::Angle
dialog angle mark message::Indiquez le nom de l'angle

dialog angle arguments title::Nombre de lettres incorrect
dialog angle arguments message::Le nom doit contenir 1 ou 3 lettres

-- Evaluations --
dialog test authorizations<
    title::Outils autorisées
    description::Choississez les conditions de l'épreuve
    calculator::Calculatrice autorisée
    positionCursor::Affichage des coordonnées en géométrie
    classtest::Devoir en classe
    hometest::Devoir à la maison
>

-- Graphics --
dialog graphic change mark<
    title::Changer le repère
    description::Changez les valeurs limites des axes
    xmin::Xmin
    xmax::Xmax
    ymin::Ymin
    ymax::Ymax
    xscale::xEchelle
    yscale::yEchelle
    x::nom de l'axe horizontal
    y::nom de l'axe vertical
>

dialog graphic change magnetism<
    title::Changer le magnétisme
    description::Changez la précision du magnétisme
    x::Précision axe x
    y::Précision axe y
>

dialog geometry precision<
    title::Changer la précision des mesures
    description::Changez la précision des mesures (entrez 0,1 pour des mesures au dixième près)
    distance::Précision des mesures de distance
    angle::Précision des mesures d'angle en degré
>

dialog function trace<
    title::Tracer une fonction
    description::Écrivez l'expression de la fonction de <i>x</i>.<br/>Vous pouvez utiliser les symbols suivants : +, -, *, /, ^,<br/>les nombres <i>x</i>, <i>Pi</i> et <i>e</i>,<br/>ainsi que les fonctions exp, ln, sin, cos, tan, sqrt.
    name::Nom de la fonction
    input::f(x) = 
>

dialog function domain<
    title::Éditer le domaine
    description::Modifiez le domaine de définition de la fonction.<br/>Laissez vide pour des bornes infinies.<br/>Pour une fonction définie par intervalles, créez plusieurs fonctions. 
    xMin::Borne inférieure
    xMax::Borne supérieure
>

dialog graphic layer<
    title::Insérer une image
    description::Choisissez l'image à insérer et ses coordonnées.
    file::Fichier
    x::X
    y::Y
    width::largeur
    height::hauteur
>

dialog graphic layer resize<
    title::Redimensionner l'image
    description::Définissez la taille de l'image dans le repère.
    width::Largeur
    height::Hauteur
>

-- Table --
dialog new table<
    title::Création d'un nouveau tableau
    description::Entrez les informations suivantes
    rows::Nombre de lignes
    columns::Nombre de colonnes
>


-- Boites prédéfinies --
image dimensions title::Dimensions de l'image
image dimensions old::Ancienne largeur
image dimensions new::Nouvelle largeur


-- authorization --
authorization force tool::Modifier les outils (avancé)
authorization<
    title::Permissions
    description::Choississez les options autorisées
>
authorization calculator::La calculatrice est autorisée
authorization consultation::L'accès au cours est autorisé
authorization litteral characters::Ajoute les lettres x, y et a au clavier numérique
authorization standard characters::Autorise l'accès aux boutons de base du collège
authorization special comparators::Ajoute les comparateurs avancés
authorization square root::Ajoute le bouton racine carrée
authorization middle::Autorise le tracé automatique du milieu d'un segment
authorization parallel::Autorise le tracé automatique des parallèles
authorization notation::Permet de faire des marques pour identifier les objets graphiques
authorization display angle::Affiche les angles sur la figure
authorization half-line::Permet de tracer des demi-droites
authorization functions::Permet d'accéder à l'onglet Fonctions
authorization function trace::Permet de tracer des fonctions par leur équation
authorization graph display coords::Active l'affichage des coordonnées
authorization graph reading helper::Active l'aide à la lecture des coordonnées
authorization proportionality::Permet de tracer un tableau de proportionnalité
authorization advanced characters::Ajoute les équations et systèmes
authorization teacher tools::Ajoute les outils pour l'enseignant

-- Boites selon des paramètres --
== LOCAL PARAMETERS ==
classes<
école
6ème
5ème
4ème
3ème
professeur
>
teacher::professeur

-- Restrictions --
école<
authorization litteral characters
authorization standard characters
authorization special comparators
authorization square root
authorization middle
authorization parallel
authorization notation
authorization display angle
authorization half-line
authorization functions
authorization graph display coords
authorization proportionality
authorization advanced characters
authorization teacher tools
>
6ème<
authorization litteral characters
authorization special comparators
authorization square root
authorization functions
authorization function trace
authorization proportionality
authorization graph display coords
authorization teacher tools
>
5ème<
authorization advanced characters
authorization functions
authorization function trace
authorization square root
authorization teacher tools
>
4ème<
authorization advanced characters
authorization functions
authorization function trace
authorization teacher tools
>
3ème<
authorization teacher tools
>
professeur<
>


-- CHARGEMENT --
loading::Chargement...
profile loaded::Profil chargé

dialog choose profile<
    title::Que voulez-vous faire ?
    description::Souhaitez-vous ouvrir un profil existant, ou créer un nouveau profil ?
>

dialog update available title::Une nouvelle version est disponible !
dialog update available message::La version %s de MathEOS est disponible.
dialog update available options<
Télécharger la mise à jour
Non merci
Me le rappeler plus tard
>

dialog update ready title::Mise à jour disponible !
dialog update ready message::<html>Une mise à jour est prête à être installée.<br/>Souhaitez-vous l'installer maintenant ?<br/>(Le programme devra redémarrer)</html>

-- imports/export --
error invalid file title::Erreur de lecture
error invalid file message::Le fichier : %s n'est pas un fichier MathEOS valide

dialog import title::Correspondance non trouvée !
dialog import message::<html>Le fichier n'est pas encore dans votre cahier.<br/>Vous pouvez le mettre à la suite, ou l'ouvrir dans une fenêtre<br/>séparée d'où vous pourrez faire des copier/coller et ouvrir les TP par double-clic.</html>
dialog import options<
Mettre à la suite
Ouvrir dans une nouvelle fenêtre
Annuler
>

dialog import match title::Correspondance trouvée !
dialog import match message::<html>Un chapitre de même titre a été trouvé.<br/>Voulez-vous le remplacer par ce fichier,<br/>ouvrir le fichier dans une nouvelle fenêtre,<br/>ou créer un nouveau chapitre à partir de ce fichier ?</html>
dialog import match options<
Remplacer
Mettre à la suite
Ouvrir à côté
Annuler
>

file imported title::Fichier importé !
file imported message::<html>Le fichier a été importé dans le cahier %s au chapitre %s.</html>
warning file version higher title::Fichier issu d'une version supérieure !
warning file version higher message::<html>La version du fichier chargé est supérieure à celle du logiciel.<br/>Il est fortement recommandé de mettre à jour le logiciel avant toute opération sur le fichier.</html>
chapter import warning title::Import d'un nouveau chapitre.
chapter import warning message::<html>Ceci va créer le nouveau chapitre %s dans votre cahier.<br/>Continuer ?
test import failed title::Aucun devoir correspondant !
test import failed message::Le fichier importé ne correspond à aucun devoir de voitre cahier...
test correction over title::Correction terminée !
test correction over message::<html>Vous avez corrigé toutes les copies !<br/>Vous allez maintenant retourner en mode rédaction d'énoncé.</html>
test confirm solution removal title::Supprimer le corrigé ?
test confirm solution removal message::<html>Souhaitez-vous réellement supprimer le corrigé ?<br/>Attention, cette opération est irréversible !</html>
test author not matching title::Vous n'êtes pas l'auteur !
test author not matching message::<html>Vous n'êtes pas l'auteur de ce devoir !<br/>Vous ne pouvez charger que la correction de votre propre devoir.<br/>Si vous cherchez à insérez un corrigé, utilisez plutôt l'outil "importer un corrigé".</html>
test import not match title::Noms différents !
test import not match message::<html>Le titre du document ne correspond pas<br/>au titre du document actuellement dans votre cahier.<br/>Si vous continuez, le contenu actuel de l'onglet évaluation sera<br/>remplacé définitivement.<br/>Importer quand même ?</html>
error loading TP title::TP corrompu !
error loading TP message::Le TP n'a pas pu être chargé...
error loading file title::Fichier non chargé !
error loading file message::<html>Une erreur est survenue durant le chargement du fichier.<br/>Vérifiez que le fichier %s<br/>est compatible avec l'application.</html>
error writing file title::Fichier non sauvegardé !
error writing file message::<html>Une erreur est survenue durant la création du fichier.<br/>Vérifiez que le fichier %s n'est<br/>pas ouvert dans une autre application et que vous avez<br/>les droits suffisants pour écrire à cet emplacement.</html>
bug report title::Envoyer un rapport d'erreur
bug report message::<html><p>Un dysfonctionnement a été repéré.</p><p>Acceptez-vous d'envoyer un rapport d'erreur<br/>afin de nous aider à améliorer MathEOS ?</p><p style="font-size:0.9em;font-style:italic">Les données sont anonymes. Cependant, n'acceptez pas<br/>si votre fichier contient des informations confidentielles.</p></html>
license deactivated title::Licence désactivée !
license deactivated message::<html>Votre licence a été désactivée.<br/>Vous pouvez à présent désinstaller MathEOS.</html>
warning cannot write title::Mise à jour automatique impossible !
warning cannot write message::<html>MathEOS ne peut pas se mettre à jour automatiquement.<br/>Le répertoire %s est protégé.<br/>Veuillez faire la mise à jour manuellement,<br/>ou accorder les droits nécessaires à l'utilisateur.</html>
warning emulated version title::Restrictions de la démo
warning emulated version message::<html>Ceci est une émulation de MathEOS-PREMIUM<br/>Le temps de latence est dû à la connexion avec le server.<br/>Le pavé numérique n'est pas toujours reconnu par le serveur.<br/>Dans ce cas, utilisez les touches du pavé supérieur.<br/>L'application peut cesser de fonctionner si trop d'utilisateurs sont connectés.</html>
error deactivated title::Fonctionnalité désactivée
error deactivated message::Cette fonction a été désactivée sur ce server.
warning recovery title::Fichier de secours
warning recovery message::<html>Votre fichier ne s'est pas ouvert correctement.<br/>Souhaitez-vous utiliser le fichier de secours ?<br/>Dernière modification : %s</html>
error recovery failed title::Fichier de secours corrompu
error recovery failed message::<html>La récupération a échoué.<br/>Envoyez votre fichier au support technique.</html>
<<<<<<< HEAD

dialog premium title::Acheter une licence PREMIUM
dialog premium message::<html><p>Pour obtenir le logiciel complet et<br/>révéler tout le potentiel de MathEOS,<br/>achetez une licence sur le site officiel.</p><p>Si vous souhaitez acheter la licence,<br/>cliquez sur "Oui". Le logiciel se mettra à jour<br/>pendant que vous achetez votre licence.</p></html>
error connection required title::Connexion requise !
error connection required message::<html>Une connexion internet est nécessaire<br/>pour effectuer cette opération.</html>
warning update required title::Mise à jour requise !
warning update required message::<html>MathEOS doit effectuer une mise à jour<br/>avant d'installer la version PREMIUM.</html>

-- Only Premium --
only premium::Fonctionnalité PREMIUM
only premium layer::<html>Ce bouton permet d'importer une image dans le<br/>graphique afin de dessiner dessus.<br/>Cette fonctionnalité n'est pas disponible dans la démo.</html>
only premium functions::<html>Dans MathEOS-PREMIUM, vous pouvez dessiner une<br/>fonction écrivant sa formule en langage mathématique<br/>ou en français. Par exemple : <i>f(x)= 1 sur x</i></html>
only premium domain::<html>Dans MathEOS-PREMIUM, vous pouvez changer le domaine<br/>de définition d'une fonction.</html>
only premium operation::<html>Les opérations décimales ou à plus de 5 chiffres<br/>ne sont pas permises dans cette démo.</html>
only premium table::<html>Dans la version de démonstration, vous ne pouvez créer<br/>plus de 5 lignes ou 5 colonnes.</html>
only premium chapter::<html>Dans la version de démonstration, vous ne pouvez créer<br/>plus de 6 chapitres ou évaluations.</html>
only premium remove chapter::<html>Dans la version de démonstration, vous ne pouvez<br/>pas supprimer un chapitre ou une évaluation.</html>
only premium formula::<html>Entrez une formule mathématique en français<br/>et elle sera mise en forme. Par exemple :<br/><i>a sur racine de 2 + b moins 2 au carré</i><br/>Cette fonctionnalité n'est pas disponible dans la démo.</html>
only premium font::<html>Dans la version de démonstration, vous ne pouvez<br/>pas changer la police du logiciel.</html>
=======
error downloading title::Erreur lors du téléchargement !
error downloading message::<html>Une erreur est survenue durant le téléchargement.<br/>Vérifiez votre connexion et réessayez.</html>
>>>>>>> master
end
