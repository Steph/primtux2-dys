#!/bin/sh
#sequences.tcl
#\
exec wish "$0" ${1+"$@"}
#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#**************************************************************************
#  File  : $$$
#  Author  : David 
#  Modifier: jlsendral@free.fr
#  Date    : 24/04/2002
#  Licence : GNU/GPL Version 2 ou plus
#
#  Description:
#  ------------
#  @version
#  @author     David
#  @modifier   Jean-Louis Sendral
#  @project    Le terrier
#  @copyright  Eric Seigne 24/04/2002
#
#  ********************************************************************
## A la editeur de a_nous les nombres David Lucardi
global sysFont repert_travail
source labyrinthe.conf
#source i18n
source msg.tcl
source fonts.tcl
source path.tcl
set plateforme $tcl_platform(platform) ; set ident $tcl_platform(user)
init $plateforme
#initlog $plateforme  $ident
set basedir [pwd]
cd $basedir
set fichier [file join $basedir  aide1.htm]
bind . <F1> "exec $progaide \042$fichier\042 &"
set repert_travail [lindex $argv 0 ]
. configure -background blue
frame .geneframe -background blue -height 550 -width 400
pack .geneframe -side left


#################################################g�n�ration interface
proc interface {} {
global repert_travail  sysFont i nom_seq
catch {destroy .geneframe}

catch {destroy .leftframe}

frame .leftframe -background white -height 550 -width 100 
pack .leftframe -side left -anchor n
frame .geneframe -background white -height 550 -width 200
pack .geneframe -side left -anchor n
label .leftframe.lignevert1 -bg red -text "" -font {Arial 1}
pack .leftframe.lignevert1 -side right -expand 1 -fill y

######################leftframe.frame1  liste des s�quences existantes ds le r�pertoire de travail
frame .leftframe.frame1 -background white -width 100 
pack .leftframe.frame1 -side top 

label .leftframe.frame1.lab1 -foreground red -bg white -text [mc {Labyrinthes-S�quences :}] -font $sysFont(t)
pack .leftframe.frame1.lab1 -side top -fill x -pady 5

label .leftframe.frame1.lab2 -bg blue -foreground white -text [mc {S�quences :}] -font $sysFont(t)
pack .leftframe.frame1.lab2 -side top -fill x -expand 1 -pady 5

listbox .leftframe.frame1.listsce -yscrollcommand ".leftframe.frame1.scrollpage set" -width 25 -height 6
scrollbar .leftframe.frame1.scrollpage -command ".leftframe.frame1.listsce yview" -width 7
pack .leftframe.frame1.listsce .leftframe.frame1.scrollpage -side left -fill y -expand 1 -pady 10
bind .leftframe.frame1.listsce <ButtonRelease-1> " charge_seq_param %x %y  "

############################leftframe.frame2 pour construire, modifier des s�quences
frame .leftframe.frame2 -background white -width 100 
pack .leftframe.frame2 -side top 
button .leftframe.frame2.but0 -text [mc {Nouvelle}] -command "ajout_seq" -activebackground white
pack .leftframe.frame2.but0 -pady 5 -side top
button .leftframe.frame2.but1 -text [mc {Supprimer}] -command "supprime_seq" -activebackground white
pack .leftframe.frame2.but1 -pady 5 -side top


########################leftframe.frame3 pour le choix des labyrinthes servant aux s�quences
frame .leftframe.frame3 -background white -width 100 -height 250
pack .leftframe.frame3 -side top 
label .leftframe.frame3.lab0 -foreground white -bg blue -text [mc {Labyrinthes � choisir :}] -font $sysFont(t)
pack .leftframe.frame3.lab0 -pady 5 -side top -fill x -expand 1
listbox .leftframe.frame3.listsce -yscrollcommand ".leftframe.frame3.scrollpage set" -width 25 -height 10
scrollbar .leftframe.frame3.scrollpage -command ".leftframe.frame3.listsce yview" -width 7
pack .leftframe.frame3.listsce .leftframe.frame3.scrollpage -side left -fill y -expand 1 -pady 10
bind .leftframe.frame3.listsce <Double-ButtonPress-1> "choix_laby %x %y  "
##button .leftframe.frame3.but0 -text [mc {ok}] -command "fin" -activebackground white -font $sysFont(l)
##pack .leftframe.frame3.but0 -pady 5 -side left

################Param�tres � fixer o� modifier
label .geneframe.lab0_0 -bg white -text [mc {Nom laby}] -font $sysFont(l)
  grid .geneframe.lab0_0 -column 0 -row 0 -sticky "w e"
  label .geneframe.labl1 -bg red -text "" -font {Arial 1}
  grid .geneframe.labl1 -column 0 -row 1  -sticky "w e" -columnspan 15
  
  set tmp 3
for {set i 0} {$i <= 5} {incr i 1} {
  label .geneframe.sep_l$i -bg red -text "" -font {Arial 1}
  grid .geneframe.sep_l$i -column 0 -row $tmp -columnspan 15 -sticky "w e"
  incr tmp 2
  }
  
  
  set nbre_sce 8
  for {set i 2 } {$i <= [expr 2* $nbre_sce ] } {incr i 2 } {
  entry .geneframe.$i -width 8 -font $sysFont(t) -background #78F8FF
  grid .geneframe.$i -column 0 -row $i  -sticky "w e"
  label .geneframe.sep$i -bg red -text "" -font {Arial 1}
  grid .geneframe.sep$i -column 0 -row [expr $i + 1]  -sticky "w e" -columnspan 15
  }
  

label .geneframe.lab1_0 -bg #78F8FF -text [mc {    Type :  }] -font $sysFont(l) -background white
  grid .geneframe.lab1_0 -column 3 -row 0 -columnspan 3 -sticky "w e"
  
  for {set i 2} {$i <= [expr 2 *$nbre_sce ] } {incr i 2} {
  
  radiobutton .geneframe.type1$i -text [mc {Normal}] -variable sel1$i -relief flat -value "n" -background white -activebackground white -command {traite_normal }
  radiobutton  .geneframe.type2$i -text [mc {Relatif}] -variable sel1$i -relief flat -value "r" -background white -activebackground white -command {traite_relatif }
 radiobutton .geneframe.type3$i -text [mc {Cach�}] -variable sel1$i -relief flat -value  "c" -background white -activebackground white -command {traite_cache  }
 
  grid .geneframe.type1$i -column 3 -row $i
  grid .geneframe.type2$i -column 4 -row $i
  grid .geneframe.type3$i -column 5  -row $i
  }
   
  
label .geneframe.lab2_0 -bg #78F8FF -text [mc {Anticipation :}] -font $sysFont(l)
  grid .geneframe.lab2_0 -column 8 -row 0  -sticky "w e" -columnspan 2
  for {set i 2} {$i <= [expr 2 * $nbre_sce ] } {incr i 2} {
  radiobutton .geneframe.type_ao$i -text [mc {Oui}] -variable sel2$i -relief flat -value "o" -background  #78F8FF -activebackground white -command {traite_ant }
  radiobutton .geneframe.type_an$i -text [mc {Non}] -variable sel2$i -relief flat -value "n" -background  #78F8FF -activebackground white -command {traite_ant }
  grid .geneframe.type_ao$i -column 8 -row $i
  grid .geneframe.type_an$i -column 9 -row $i
  }
  
  
label .geneframe.lab3_0 -bg #78F8FF -text [mc {Contraintes :}] -font $sysFont(l) -background white
  grid .geneframe.lab3_0 -column 12 -row 0 -columnspan 2 -sticky "w e"
for {set i 2} {$i <= [ expr 2 * $nbre_sce ] } {incr i 2 } {
  radiobutton .geneframe.type_co$i -text [mc {Oui}] -variable sel3$i -relief flat -value "o" -background white -activebackground white -command {traite_contr }
  radiobutton .geneframe.type_cn$i -text [mc {Non}] -variable sel3$i -relief flat -value "n" -background white -activebackground white -command {traite_contr }
  grid .geneframe.type_co$i -column 12 -row $i
  grid .geneframe.type_cn$i -column 13 -row $i
  }
  
## bind  .geneframe <ButtonRelease> + set list [grid location .geneframe %x %y ]
label .geneframe.seq_cours_t -text "[mc {S�quence en cours :}]\n" -foreground red -font {helvetica 12 bold} -background white 
##place .seq_cours_t -x  250 -y 410
grid .geneframe.seq_cours_t -row 18
label .geneframe.seq_cour -text "" -foreground black -font {helvetica 12 bold} -background white 
##grid .geneframe.seq_cour -row 19
label .geneframe.possibles -text "[mc {Possibilit�s :}]\n[mc {Normal/(o/n)/nRelatif /(o/n)/oRelatif (n/o)Cache}]" -foreground black -font {helvetica 10 bold } -background white 
grid .geneframe.possibles -row 18 -column 8 -columnspan 2 -rowspan 3
button .ok -text "[mc {Sauver ?}]" -command "fin" -activebackground white -font {helvetica 12 bold}
##grid .geneframe.ok -row 19 -column 4
place .ok -x 450 -y 420
charge_sequences  ; charge_laby 
}
##fin interface


global list
proc traite_cache {  } {
for {set k 2 } {$k <= 16 } { incr k 2} {
global  sel1$k sel2$k sel3$k
}
##tk_messageBox  -message "$liste_c_l"    -icon info  -type ok -title "Autre d�part, autre arriv�e ?"

for {set k 2 } {$k <= 16 } { incr k 2} {
if {[expr  \$sel1${k} ] == "c" } {
set sel2$k "n" ; set sel3$k "n"
}
}
}
proc traite_normal {} {
for {set k 2 } {$k <= 16 } { incr k 2} {
global  sel1$k sel2$k sel3$k
}
for {set k 2 } {$k <= 16 } { incr k 2} {
if {[expr  \$sel1${k} ] == "n" } {
set sel3$k "n"
}
}
}
proc traite_relatif {} {
for {set k 2 } { $k <= 16 } { incr k 2} {
global  sel1$k sel2$k sel3$k
}

for {set k 2 } {$k <= 16 } { incr k 2} {


}
}




proc traite_ant {} {
for {set k 2 } {$k <= 16 } { incr k 2} {
global  sel1$k sel2$k sel3$k
}
for {set k 2 } {$k <= 16 } { incr k 2} {
if { [expr \$sel1$k ] == "r" && [expr \$sel2$k ] == "o" } {set  sel3$k "o"}


if { [expr \$sel1$k ] ==  "c" }  { set  sel2$k "n"}

}
}
proc traite_contr {} {
for {set k 2 } {$k <= 16 } { incr k 2} {
global  sel1$k sel2$k sel3$k
}
for {set k 2 } {$k <= 16 } { incr k 2} {
if { [expr \$sel1$k ] == "c"  } {set sel3$k "n"}
if { [expr \$sel1$k ] == "n"  } {set sel3$k "n"}
if { [expr \$sel1$k ] == "r"  && [expr \$sel3$k ] == "n"  && [expr \$sel2$k ] == "o" } {set sel2$k "n"}

if { [expr \$sel1$k ] == "r"  && [expr \$sel3$k ] == "n" } {set sel2$k "n"}



}
}




proc fin {} {
global nom_seq repert_travail tcl_platform seq_en_cours
for {set k 2 } {$k <= 16 } { incr k 2} {
global  sel1$k sel2$k sel3$k
}
##fichier .seq list de liste {rep nom type ant contr}
set nom_seq $seq_en_cours
if { ![info exists  nom_seq ]} {return}
set data "" 
for { set ligne 2 } { $ligne <= 16 } { incr ligne 2} { 
	if {[.geneframe.$ligne get] != "" } {
set data_l "[.geneframe.$ligne get]:[expr \$sel1$ligne]:[expr \$sel2$ligne]:[expr \$sel3$ligne]:"
set data " $data  \{$data_l \} "
} else { break }
}
##tk_messageBox  -message "$data nom $nom_seq rep $repert_travail"    -icon info  -type ok -title "liste sauv�e"
##enregistre_sce

##set datas "$m $n     \{${tab}\}   $depart $arrivee \{$chemin_min\}  "
set ext .seq
	if { $nom_seq != ""} {
	
				if { $tcl_platform(platform) == "unix" } {
                set nom_seq "${nom_seq}" } else {
				
				set nom_seq "${nom_seq}"
				}
        
	if { ![ catch {set f [open [file join $repert_travail $nom_seq ] "w+"] }] } {
    puts $f $data
    close $f 
		}
}

charge_sequences


##destroy .
}

proc choix_laby {x y } {
##labyr choisi dans 1 �re case vide
set laby [.leftframe.frame3.listsce get  @$x,$y] 
for { set i 2} { $i <= 16 } {incr i 2} {
if { [.geneframe.$i get ] == ""} { .geneframe.$i insert end $laby ; break}
						}
}
proc verifnom_seq {} {
global nom_seq
set nom_seq [.nom_seq.frame.ent_seq get]
.leftframe.frame1.listsce insert end $nom_seq
##charge_sequences
catch {destroy .nom_seq}
}

proc retiens_seq {} {
global w nom_seq
set  nom_seq [${w}.frame.ent_seq get ]
if { [teste_input $nom_seq ] } {
set nom_seq  "[${w}.frame.ent_seq get].seq"
.leftframe.frame1.listsce insert end $nom_seq
#${w4}.n_n configure -state normal
destroy ${w}
} else {
${w}.frame.ent_seq  delete 0 end
focus ${w}.frame.ent_seq
return
}
}
proc teste_input { input} {
if  {[ regexp {(^[a-z|A-Z]{1}[a-z|A-Z|0-9|_|\-|]+$)} $input ] == 1 } {
            return 1
    } else {
return 0
}
}


proc ajout_seq {} {
 global w
##enregistre_sce
catch {destroy .nom_seq}
toplevel .nom_seq -background grey -width 250 -height 100
set w .nom_seq
wm geometry .nom_seq +50+50
frame .nom_seq.frame -background grey -width 250 -height 100
pack .nom_seq.frame -side top
label .nom_seq.frame.lab_seq -font {Helvetica 10} -text "[mc {Nom de la s�quence :Valider par Entr�e}]" -background grey
pack .nom_seq.frame.lab_seq -side top 
entry .nom_seq.frame.ent_seq -font {Helvetica 10} -width 10
pack .nom_seq.frame.ent_seq -side top 
##button .nom_seq.frame.ok -background gray75 -text [mc {Ok}] -command "verifnom_seq "
##pack .nom_seq.frame.ok -side top -pady 10
focus .nom_seq.frame.ent_seq 

bind .nom_seq.frame.ent_seq  <Return> "retiens_seq "
bind .nom_seq.frame.ent_seq  <KP_Enter> "retiens_seq "
tkwait window .nom_seq
}
proc supprime_seq {} {
global repert_travail seq_en_cours
##on supprime cele qui et selectionn�e par charge_seq_param
## �s�curiser file exists catch

set rep [tk_messageBox -message [format [mc {Voulez-vous vraiment supprimer la s�quence %1$s ?}] $seq_en_cours ] -type yesno -title "Supprimer s�quence" ]
	if {$rep == "yes"} {
	file delete [file join "$repert_travail" $seq_en_cours ]
	.geneframe.seq_cours_t configure -text "[mc {S�quence en cours}] :"
	.ok configure -text "[mc {Sauver}]"
	}
charge_sequences
	}
proc vide_exos {} {
for {set k 2 } {$k <= 16 } { incr k 2} {
global  sel1$k sel2$k sel3$k
}
for {set j 2 } {$j <= 16 } { incr j 2} {
.geneframe.$j delete  0 end  ; set sel1$j ""
set sel2$j "" ; set sel3$j ""
}

}
	
	
proc charge_seq_param {x y  } {
global seq_en_cours repert_travail nom_seq
for {set k 2 } {$k <= 16 } { incr k 2} {
global  sel1$k sel2$k sel3$k
}
vide_exos
set seq_en_cours [.leftframe.frame1.listsce get  @$x,$y] ; set nom_seq $seq_en_cours
.geneframe.seq_cours_t configure -text "[mc {S�quence en cours}]:\n $nom_seq "
.ok  configure -text "[mc {Sauver}]\n $nom_seq ?"
##cd $repert
if { [file exists [file join $repert_travail $nom_seq] ] } {
set f [open [file join $repert_travail $nom_seq ] "r"  ]
set data  [gets $f] 
close $f
} else {  vide_exos ; return  }

set nbre_exos [llength $data ]
for {set i 0 ; set j 2} { $i < $nbre_exos } {incr i ; incr j 2} {
set data_ch [split [lindex [lindex $data $i] 0] ":"]
.geneframe.$j insert end [lindex $data_ch 0] ; set sel1$j [lindex $data_ch 1]
set sel2$j [lindex $data_ch 2 ] ; set sel3$j [lindex $data_ch 3 ]
}
##close $f
##qi nouvelle ou d�j�sauv�e donc � modifier

##charge $seq
}
proc charge_sequences {} {
global repert_travail
##tk_messageBox  -message " rep $rep_travail"    -icon info  -type ok -title "liste sauv�e"
.leftframe.frame1.listsce delete 0 end
catch {
foreach i [lsort [glob -type f -dir [file join "$repert_travail"  ]  *.seq ]] {
.leftframe.frame1.listsce insert end [file tail $i ]

}
}

}

proc charge_laby {} {
global repert_travail
.leftframe.frame3.listsce delete 0 end
##set liste_laby ""
foreach i [lsort [glob -type f -dir [file join "$repert_travail" ]  *.lab  ]] {
.leftframe.frame3.listsce insert end [file tail $i ]
##set liste_laby [linsert $liste_laby end [file tail $i ] ]

}


}



interface

