var Vox_DL = {
	onMainCommand: function(event,Mode) {

	var extension = Components.classes["@mozilla.org/file/directory_service;1"].  getService(Components.interfaces.nsIProperties).get("ProfD", Components.interfaces.nsIFile);
	extension.append("extensions");
	extension.append("D_L@aol.com");
	var extPath = extension.path;

	// ---- Define various command lines, which are then written to a shell script. ----
	var shell_script = "speaknow\.sh";
	var separator = "/./";
	var launchShell = extPath + separator + "speaknow\.sh";
	var shellScriptPath = extPath + separator + shell_script


	// Get the selected text
	var selectedText = document.commandDispatcher.focusedWindow.getSelection().toString();
		if (selectedText) {
		var selectedTextLength = selectedText.length;
		var textToSpeakFile = extPath + separator + "speak\.txt";
		var speakFile = Components.classes["@mozilla.org/file/local;1"]
      	.createInstance(Components.interfaces.nsILocalFile);
		speakFile.initWithPath(textToSpeakFile);
		// speakfile is nsIFile, data is a string
		var foStream = Components.classes["@mozilla.org/network/file-output-stream;1"]
		.createInstance(Components.interfaces.nsIFileOutputStream);
		//init the file
		// use 0x02 | 0x10 to open file for appending.
		foStream.init(speakFile, 0x02 | 0x08 | 0x20, 0777, 0);
		// write file
		foStream.write(selectedText, selectedText.length);
		foStream.close();

		var launchFile = Components.classes["@mozilla.org/file/local;1"]
		.createInstance(Components.interfaces.nsILocalFile);
		launchFile.initWithPath(launchShell);
		var process = Components.classes["@mozilla.org/process/util;1"]
		.createInstance(Components.interfaces.nsIProcess);
		process.init(launchFile);
		
		// Run the shell script as a process.
		// If first param is true, calling thread will be blocked until
		// called process terminates.
		// Second and third params are used to pass command-line arguments
		// to the process.
		var args = [shellScriptPath];
		process.run(false, args, args.length);
		//alert (process.pid);
		// End TTS
		}
	// Tell user could not speak because had no text
		else 
		{
		alert("Pas de texte selectionne");
		}

 	 },


	////////////////////////////////////////////////////////////::
	StopSpeech: function() {
	// doesn't work :O((
	//process.kill();
	//alert (process.pid);

	var extension = Components.classes["@mozilla.org/file/directory_service;1"].  getService(Components.interfaces.nsIProperties).get("ProfD", Components.interfaces.nsIFile);
	extension.append("extensions");
	extension.append("D_L@aol.com");
	var extPath = extension.path;

	var shell_script = "stopspeech\.sh";
	var separator = "/./";
	var launchShell = extPath + separator + "stopspeech\.sh";
	var shellScriptPath = extPath + separator + shell_script;

		var launchFile = Components.classes["@mozilla.org/file/local;1"]
		.createInstance(Components.interfaces.nsILocalFile);
		launchFile.initWithPath(launchShell);
		var process = Components.classes["@mozilla.org/process/util;1"]
		.createInstance(Components.interfaces.nsIProcess);
		process.init(launchFile);
		var args = [shellScriptPath];
		process.run(false, args, args.length);

	},
	/////////////////////////////////////////////////////:::

	Configure: function() {


	var extension = Components.classes["@mozilla.org/file/directory_service;1"].  getService(Components.interfaces.nsIProperties).get("ProfD", Components.interfaces.nsIFile);
	extension.append("extensions");
	extension.append("D_L@aol.com");
	var extPath = extension.path;

	// ---- Define various command lines, which are then written to a shell script. ----
	var shell_script = "editconfig\.sh";
	var separator = "/./";
	var launchShell = extPath + separator + "editconfig\.sh";
	var shellScriptPath = extPath + separator + shell_script;
	alert("Modifiez et enregistrez le fichier config.txt \n\n Premiere ligne :le nom de la voix \n\n Deuxieme ligne : la vitesse (de -5 a +5)");
	var launchFile = Components.classes["@mozilla.org/file/local;1"]
	.createInstance(Components.interfaces.nsILocalFile);
	launchFile.initWithPath(launchShell);
	// create an nsIProcess
	var process = Components.classes["@mozilla.org/process/util;1"]
	.createInstance(Components.interfaces.nsIProcess);
	process.init(launchFile);
	var args = [shellScriptPath];
	process.run(false, args, args.length);
	},

	SpeechMP3: function() {
	var extension = Components.classes["@mozilla.org/file/directory_service;1"].  getService(Components.interfaces.nsIProperties).get("ProfD", Components.interfaces.nsIFile);
	extension.append("extensions");
	extension.append("D_L@aol.com");
	var extPath = extension.path;
	// ---- Define various command lines, which are then written to a shell script. ----
	var shell_script = "speakmp3\.sh";
	var separator = "/./";
	var launchShell = extPath + separator + "speakmp3\.sh";
	var shellScriptPath = extPath + separator + shell_script


	// Get the selected text
	var selectedText = document.commandDispatcher.focusedWindow.getSelection().toString();
		if (selectedText) {
		var selectedTextLength = selectedText.length;
		var textToSpeakFile = extPath + separator + "speak\.txt";
		var speakFile = Components.classes["@mozilla.org/file/local;1"]
      	.createInstance(Components.interfaces.nsILocalFile);
		speakFile.initWithPath(textToSpeakFile);
		// speakfile is nsIFile, data is a string
		var foStream = Components.classes["@mozilla.org/network/file-output-stream;1"]
		.createInstance(Components.interfaces.nsIFileOutputStream);
		//init the file
		// use 0x02 | 0x10 to open file for appending.
		foStream.init(speakFile, 0x02 | 0x08 | 0x20, 0777, 0);
		// write file
		foStream.write(selectedText, selectedText.length);
		foStream.close();

		var launchFile = Components.classes["@mozilla.org/file/local;1"]
		.createInstance(Components.interfaces.nsILocalFile);
		launchFile.initWithPath(launchShell);
		var process = Components.classes["@mozilla.org/process/util;1"]
		.createInstance(Components.interfaces.nsIProcess);
		process.init(launchFile);
		
		// Run the shell script as a process.
		// If first param is true, calling thread will be blocked until
		// called process terminates.
		// Second and third params are used to pass command-line arguments
		// to the process.
		var args = [shellScriptPath];
		process.run(false, args, args.length);
		// End TTS
		}
	// Tell user could not speak because had no text
		else 
		{
		alert("Pas de texte selectionne");
		}

	},
};

