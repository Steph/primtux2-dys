#!/bin/sh
#Calcul_en_Pluie.tcl
#\
exec wish "$0" ${1+"$@"}
#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#**************************************************************************
#  File  : $$$
#  Author  :jl sendral
#  Modifier,adaptation: jlsendral@.fr

#  Date    : 24/04/2002
#  Licence : GNU/GPL Version 2 ou plus
#
#  Description:
#  ------------
#
#  @version
#  @author     Jean-Louis Sendral
#  @project    Le terrier
#  @copyright  Eric Seigne 24/04/2002
#
#  *************************************************************************
##source i18n
set basedir [file dir $argv0]
cd $basedir
. configure -background black -width 640 -height 480
wm geometry . +0+0
wm title . "Aide Labyrinthes Projet leterrierLogiciel GPL"

text .text -yscrollcommand ".scroll set" -setgrid true -width 75 -height 20 -wrap word -background white -font {Arial 14}
scrollbar .scroll -command ".text yview"
pack .scroll -side right -fill y
pack .text -expand yes -fill both


set f [open [file join aide.txt] "r"]
while {![eof $f]} {
.text insert end [read $f 10000]

}


