
proc majparcours {men} {
  global Home
  set ind 1
  set what parcours
  foreach i [glob [file join  $Home problemes  *.par]] {
    set f [open [file join  $Home problemes [file tail $i]] "r"]
    set parcours [gets $f]
    close $f

    set tmp [lindex [lindex $parcours 0] 0]
    regsub -all {.txt} $tmp "" tmp
    set tit [file tail $i]
    regsub -all {.par} $tit "" tit

    set indp [string range $tmp end end]
    #set indtit [string first "." $enonce]
    #set titre [string range $enonce 0 $indtit]
    menu .menu.action.m$men.niv$ind$men -tearoff 0 
    .menu.action.m$men add radio -label "$ind - $tit" -command "execp problemes$indp.tcl [lindex [lindex $parcours 0] 0] [lindex [lindex $parcours 0] 1] [file tail $i]"

    incr ind
  }
}

proc majmenu {what men} {
global Home
set ind 1
set ext .txt
foreach i [glob [file join  $Home problemes  $what*$ext]] {
set f [open [file join  $Home problemes [file tail $i]] "r"]
set tmp [gets $f]
eval $tmp
set tmp [gets $f]
eval $tmp
	while {$tmp != "::1"} {
	set tmp [gets $f]
	}

	while {$tmp != "::"} {
	set tmp [gets $f]
	if {$tmp != "::"} {catch {eval $tmp}}
	if {[string first enonce $tmp]!= -1} {break}
	}
close $f

set tmp [file tail $i]
regsub -all {.txt} $tmp "" tmp
set indp [string range $tmp end end]
set indtit [string first "." $enonce]
set titre [string range $enonce 0 $indtit]

menu .menu.action.m$men.niv$ind$men -tearoff 0 
.menu.action.m$men add cascade -label "$ind - $titre" -menu .menu.action.m$men.niv$ind$men

foreach item $niveaux {
.menu.action.m$men.niv$ind$men add radio -label "$item" -command "execp problemes$indp.tcl [file tail $i] $item none"
}
incr ind
}

}

menu .menu


# Creation du menu Fichier
menu .menu.fichier -tearoff 0
.menu add cascade -label [mc {Fichier}] -menu .menu.fichier
.menu.fichier add cascade -label [mc {Editeur}] -menu .menu.fichier.editeur
menu .menu.fichier.editeur -tearoff 0

.menu.fichier.editeur add command -label [mc {Probl�mes}] -command "exec $iwish editprob.tcl &"
.menu.fichier.editeur add command -label [mc {Séquence}] -command "exec $iwish editparcours.tcl &"


.menu.fichier add command -label [mc {Bilans}] -command "exec $iwish gestion.tcl &"
.menu.fichier add sep
.menu.fichier add command -label [mc {Quitter}] -command exit

menu .menu.action -tearoff 0
.menu add cascade -label [mc {Activites}] -menu .menu.action
menu .menu.action.m1 -tearoff 0
.menu.action add cascade -label [mc {D�nombrement}] -menu .menu.action.m1

menu .menu.action.m2 -tearoff 0 
.menu.action add cascade -label [mc {Additions-Soustractions}] -menu .menu.action.m2 
menu .menu.action.m3 -tearoff 0 
.menu.action add cascade -label [mc {Ligne gradu�e}] -menu .menu.action.m3 
menu .menu.action.m4 -tearoff 0 
.menu.action add cascade -label [mc {Multiplications}] -menu .menu.action.m4 
menu .menu.action.m5 -tearoff 0 
.menu.action add cascade -label [mc {Séquence(s)}] -menu .menu.action.m5 
menu .menu.action.m6 -tearoff 0 

#menu .menu.options -tearoff 0
#.menu add cascade -label [mc {Reglages}] -menu .menu.options

#set ext .msg
#menu .menu.options.lang -tearoff 0 
#.menu.options add cascade -label "[mc {Langue}]" -menu .menu.options.lang

#foreach i [glob [file join  $basedir msgs *$ext]] {
#set langue [string map {.msg ""} [file tail $i]]
#.menu.options.lang add radio -label $langue -variable langue -command "setlang $langue"
#}


#menu .menu.options.dossier -tearoff 0 
#.menu.options add cascade -label [mc {Dossier de travail}] -menu .menu.options.dossier
#.menu.options.dossier add radio -label [mc {Individuel}] -variable repert -value 0 -command "changehomeupdate"
#.menu.options.dossier add radio -label [mc {Commun}] -variable repert -value 1 -command "changehomeupdate"



.menu add command -label [mc {Utilisateur}] -command "setwindowsusername"

menu .menu.aide -tearoff 0
.menu add cascade -label [mc {?}] -menu .menu.aide


	#if {[file exists [file join $baseHome reglages lang.conf]] == 1} {
	#set f [open [file join $baseHome reglages lang.conf] "r"]
  	#gets $f lang
  	#close $f
	set fich "_index.htm"
	set fich $lang$fich
	set fichier [file join [pwd] aide $fich]
	#} else {
	#set fichier [file join [pwd] aide fr_index.htm]
	#}
  


if {[file exists $fichier] != 1} {
set fichier [file join [pwd] aide fr_index.htm]
}
majmenu adsous 2
majmenu den 1
majmenu ligneg 3
majmenu multip 4
majparcours 5




.menu.aide add command -label [mc {Aide}] -command "exec $progaide file:$fichier &"
.menu.aide add command -label [mc {A propos ...}] -command "source apropos.tcl"

. configure -menu .menu
