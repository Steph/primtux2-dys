<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE script:module PUBLIC "-//OpenOffice.org//DTD OfficeDocument 1.0//EN" "module.dtd">
<script:module xmlns:script="http://openoffice.org/2000/script" script:name="Main" script:language="StarBasic">REM  *****  BASIC  *****

&apos;**************************************************
&apos;AUTHOR		: T. VATAIRE
&apos;DATE		: 05/02/10
&apos;VERSION	: 1.0.0
&apos;**************************************************
&apos;This module contains functions relative to the
&apos;WriterRotationTool extension
&apos;**************************************************
&apos;Dependencies : Message, MessageOutput, Logger, Util, 
&apos;				XUserInputInterception
&apos;**************************************************
&apos;LICENCE	: GPL v3.0
&apos;**************************************************

option explicit

private const STR_LOG_FILENAME = &quot;WriterRotationTool.log&quot;

private const STR_TEXTGRAPHICOBJECT_SERVICE = &quot;com.sun.star.text.TextGraphicObject&quot;
private const STR_GRAPHICOBJECTSHAPE_SERVICE = &quot;com.sun.star.drawing.GraphicObjectShape&quot;
private const STR_SHAPECOLLECTION_SERVICE = &quot;com.sun.star.drawing.ShapeCollection&quot;
private const STR_TEXTFRAME_SERVICE = &quot;com.sun.star.text.TextFrame&quot;

private const STR_XDRAWPAGESUPPLIER_IFACE = &quot;com.sun.star.drawing.XDrawPageSupplier&quot;

private const STR_DISPATCH_COMMAND_ROTATE = &quot;.uno:ToggleObjectRotateMode&quot;

&apos;the interval of waiting before to check again if the controller has completed the selection operation
private const INT_SELECT_INTERVAL = 50
&apos;the waiting time before to deem that the selection operation has failed
private const INT_SELECT_TIMEOUT = 2000

&apos;the entry point of the WriterRotationTool extension
sub main

	dim wDoc as object
	dim wDocController as object
	dim graphicObj as object
	
	initLogSession(STR_LOG_FILENAME, INT_LEVEL_DEBUG)
	wDoc = thisComponent
	if (isTextDocument(wDoc)) then
		if (not wDoc.isReadonly()) then
			wDocController = wDoc.currentController
			if (XUserInputInterception_lockInputDevices(wDocController)) then
				on local error goto unexpectedError
				_processSelection(wDoc)
				goto restoreInputDevices
				unexpectedError:
					messageOutput_log(message_new(&quot;main : An unexpected error occured.&quot;, INT_MESSAGE_TYPE_ERROR, true))
				restoreInputDevices:
					XUserInputInterception_unlockInputDevices(wDocController)
			end if
		end if
	end if
	closeLogSession()

end sub

&apos;converts the selected object, inserts the result, and executes the command .uno:ToggleObjectRotateMode
sub _processSelection(wDoc as object)

	dim success as boolean
	dim wSel as object
	
	&apos;here &quot;currentController&quot; has already been checked
	&apos;the selection could be null if a fontwork object is selected =&gt; ignore
	wSel = wDoc.currentSelection
	success = true
	if (supportsService(wSel, STR_TEXTGRAPHICOBJECT_SERVICE)) then
		success = _processTextGraphicObject(wDoc, wSel)
	end if
	if (success) then
		executeDispatchCommand(wDoc.currentController.frame, STR_DISPATCH_COMMAND_ROTATE)
	end if

end sub

&apos;converts the graphic object, inserts the result, removes the old object, afterwards defines the active selection on the result
function _processTextGraphicObject(wDoc as object, textGraphicObject as object) as boolean

	dim success as boolean
	dim convResult as object
	
	&apos;cancel if the document doesn&apos;t implement the XDrawPageSupplier interface used to remove the original object
	&apos;this test is made because of the marker &quot;deprecated&quot; for the XDrawPageSupplier interface
	&apos;this suggests that the access to graphical objects in text documents will no longer be done that way thereafter
	success = (hasUnoInterfaces(wDoc, STR_XDRAWPAGESUPPLIER_IFACE))
	if (success) then
		convResult = _convertGraphicObj(wDoc, textGraphicObject)
		success = (not isNull(convResult))
		if (success) then
			success = (_insertConvResult(textGraphicObject, convResult, wDoc))
			if (success) then
					wDoc.drawPage.remove(textGraphicObject)
					&apos;here &quot;currentController&quot; has already been checked
					success = _selectConvResult(wDoc.currentController, convResult)
			end if
		end if
	else
		messageOutput_log(message_new(&quot;_processTextGraphicObject : The document doesn&apos;t implements the &lt; &quot; &amp; STR_XDRAWPAGESUPPLIER_IFACE &amp; _
									  &quot;&gt; interface.&quot;, INT_MESSAGE_TYPE_ERROR))
	end if
	
	_processTextGraphicObject = success

end function

&apos;selects the result of the conversion after it have been inserted
function _selectConvResult(wDocController as object, convResult as object) as boolean

	dim success as boolean
	
	on local error goto errSelect
	wDocController.select(convResult)
	&apos;avoids some problems of synchronization with the select() method
	success = _waitsUntilObjectIsSelected(wDocController, convResult)
	goto finally
	errSelect:
		success = false
	finally:
	if (not success) then
		messageOutput_log(message_new(&quot;_selectConvResult : Unable to select the inserted object.&quot;, INT_MESSAGE_TYPE_ERROR))
	end if
			
	_selectConvResult = success

end function

&apos;waits until the inserted object is selected or the timeout is reached
&apos;returns true if the selection of the newly inserted object has succeeded, false otherwise
function _waitsUntilObjectIsSelected(wDocController as object, insertedObject as object) as boolean

	dim success as boolean
	dim currentSelection as object
	dim elapsedTime as integer

	currentSelection = wDocController.selection
	while ((not supportsService(currentSelection, STR_SHAPECOLLECTION_SERVICE)) and (elapsedTime &lt; INT_SELECT_TIMEOUT))
		wait INT_SELECT_INTERVAL
		elapsedTime = elapsedTime + INT_SELECT_INTERVAL
		currentSelection = wDocController.selection
	wend
	success = (supportsService(currentSelection, STR_SHAPECOLLECTION_SERVICE))
	if (success) then
		success = (currentSelection.count &gt; 0)
		if (success) then
			success = (equalUnoObjects(currentSelection.getByIndex(0), insertedObject))
		end if
	end if
	
	_waitsUntilObjectIsSelected = success	

end function

&apos;converts a TextGraphicObject into a GraphicObjectShape
function _convertGraphicObj(textDocument as object, textGraphicObject as object) as object

	dim graphicObjectShape as object
	
	on local error goto errInst
	graphicObjectShape = textDocument.createInstance(STR_GRAPHICOBJECTSHAPE_SERVICE)
	goto noErrInst
	errInst:
		messageOutput_log(message_new(&quot;_convertGraphicObj : Failed to instanciate the service &lt;&quot; &amp; STR_GRAPHICOBJECTSHAPE_SERVICE &amp; &quot;&gt; : abort.&quot;, _
									  INT_MESSAGE_TYPE_ERROR, true))
	noErrInst:
	on local error resume next
	if (not isNull(graphicObjectShape)) then
		graphicObjectShape.graphic = textGraphicObject.graphic
		graphicobjectshape.AdjustBlue = textGraphicObject.AdjustBlue
		graphicobjectshape.AdjustContrast = textGraphicObject.AdjustContrast
		graphicobjectshape.AdjustGreen = textGraphicObject.AdjustGreen
		graphicobjectshape.AdjustLuminance = textGraphicObject.AdjustLuminance
		graphicobjectshape.AdjustRed = textGraphicObject.AdjustRed
		graphicobjectshape.BottomMargin = textGraphicObject.BottomMargin
		graphicobjectshape.ContourOutside = textGraphicObject.ContourOutside
		graphicobjectshape.Description = textGraphicObject.Description
		graphicobjectshape.Gamma = textGraphicObject.Gamma
		graphicobjectshape.GraphicColorMode = textGraphicObject.GraphicColorMode
		graphicobjectshape.GraphicCrop = textGraphicObject.GraphicCrop
		graphicobjectshape.HoriOrient = textGraphicObject.HoriOrient
		graphicobjectshape.HoriOrientPosition = textGraphicObject.HoriOrientPosition
		graphicobjectshape.HoriOrientRelation = textGraphicObject.HoriOrientRelation
		graphicobjectshape.IsFollowingTextFlow = textGraphicObject.IsFollowingTextFlow
		graphicobjectshape.LeftMargin = textGraphicObject.LeftMargin
		graphicobjectshape.Name = textGraphicObject.Name
		graphicobjectshape.Opaque = textGraphicObject.Opaque
		graphicobjectshape.PageToggle = textGraphicObject.PageToggle
		graphicobjectshape.RightMargin = textGraphicObject.RightMargin
		graphicobjectshape.Surround = textGraphicObject.Surround
		graphicobjectshape.SurroundAnchorOnly = textGraphicObject.SurroundAnchorOnly
		graphicobjectshape.SurroundContour = textGraphicObject.SurroundContour
		graphicobjectshape.TextWrap = textGraphicObject.TextWrap
		graphicobjectshape.Title = textGraphicObject.Title
		graphicobjectshape.TopMargin = textGraphicObject.TopMargin
		graphicobjectshape.Transparency = textGraphicObject.Transparency
		graphicobjectshape.UserDefinedAttributes = textGraphicObject.UserDefinedAttributes
		graphicobjectshape.VertOrient = textGraphicObject.VertOrient
		graphicobjectshape.VertOrientPosition = textGraphicObject.VertOrientPosition
		graphicobjectshape.VertOrientRelation = textGraphicObject.VertOrientRelation
		graphicobjectshape.WrapInfluenceOnPosition = textGraphicObject.WrapInfluenceOnPosition
		graphicobjectshape.ZOrder = textGraphicObject.ZOrder
		graphicObjectShape.setSize(textGraphicObject.size)
		graphicObjectShape.surroundContour = true
		graphicObjectShape.moveProtect = textGraphicObject.positionProtected
		graphicObjectShape.sizeProtect = textGraphicObject.sizeProtected
	end if
	
	_convertGraphicObj = graphicObjectShape

end function

&apos;true if the anchor is in a protected section or if a parent section is protected, false otherwise
function _isAnchorInProtectedSection(anchor as object) as boolean

	dim isSectionProtected as boolean
	dim textSection as variant
	dim cText as object
	
	textSection = anchor.textSection
	&apos;the first section is empty if the anchor isn&apos;t in a section
	if (not isEmpty(textSection)) then
		do
			isSectionProtected = textSection.isProtected
			textSection = textSection.parentSection
		&apos;but a parent section is null if it doesn&apos;t exist
		loop while ((not isNull(textSection)) and (not isSectionProtected))
	end if

	_isAnchorInProtectedSection = isSectionProtected

end function

&apos;true if the content of the textFrame is protected, or if thisTextFrame is into a protected context (parent sections or parent textFrames are protected)
&apos;false otherwise or if an error occured
function _isTextFrameInProtectedContext(textFrame as object) as boolean

	dim isContextProtected as boolean
	
	isContextProtected = textFrame.contentProtected
	if (not isContextProtected) then
		isContextProtected = _isAnchorableObjectInProtectedContext(textFrame)
	end if
	
	_isTextFrameInProtectedContext = isContextProtected

end function

&apos;true if the anchorable object is in a protected context (protected sections or textFrames)
&apos;false otherwise or if an error occured
function _isAnchorableObjectInProtectedContext(anchorableObject as object) as boolean

	dim isInProtectedContext as boolean
	dim anchor as object
	dim cText as object

	&apos;raises an error if the &quot;anchorType&quot; property doesn&apos;t exist
	select case anchorableObject.anchorType
		case com.sun.star.text.TextContentAnchorType.AT_PAGE
			isInProtectedContext = false
		case com.sun.star.text.TextContentAnchorType.AT_FRAME
			isInProtectedContext = _isTextFrameInProtectedContext(anchorableObject.anchorFrame)
		case com.sun.star.text.TextContentAnchorType.AS_CHARACTER, _
			 com.sun.star.text.TextContentAnchorType.AT_CHARACTER, _
			 com.sun.star.text.TextContentAnchorType.AT_PARAGRAPH
			anchor = anchorableObject.anchor
			isInProtectedContext = _isAnchorInProtectedSection(anchor)
			if (not isInProtectedContext) then
				cText = anchor.text
				if (supportsService(cText, STR_TEXTFRAME_SERVICE)) then
					isInProtectedContext = _isTextFrameInProtectedContext(cText)
				end if
			end if
		case else
			&apos;this should never happen. if this were the case, no further action will be done.
			isInProtectedContext = true
	end select

	_isAnchorableObjectInProtectedContext = isInProtectedContext

end function

&apos;inserts the result of the conversion
function _insertConvResult(initialObject as object, convResult as object, wDoc as object) as boolean

	dim success as boolean
	dim wText as object
	dim anchor as object

	on local error goto errInsert
	&apos;raises an error if the &quot;anchorType&quot; property doesn&apos;t exists
	success = (not _isAnchorableObjectInProtectedContext(initialObject))
	if (success) then
		convResult.anchorType = initialObject.anchorType
		select case convResult.anchorType
			case com.sun.star.text.TextContentAnchorType.AT_PAGE
				convResult.anchorPageNo = initialObject.anchorPageNo
				wText = wDoc.text
				wText.insertTextContent(wText, convResult, false)
			case com.sun.star.text.TextContentAnchorType.AT_FRAME
				anchor = initialObject.anchorFrame
				anchor.insertTextContent(anchor, convResult, false)
			case com.sun.star.text.TextContentAnchorType.AS_CHARACTER, _
				 com.sun.star.text.TextContentAnchorType.AT_CHARACTER, _
				 com.sun.star.text.TextContentAnchorType.AT_PARAGRAPH
				anchor = initialObject.anchor
				anchor.text.insertTextContent(anchor, convResult, false)
			case else
				&apos;this should never happen. if this were the case, no further action will be done.
				success = false
		end select
	end if
	goto noErrInsert
	errInsert:
		success = false
		messageOutput_log(message_new(&quot;_insertConvResult : An error occured while inserting the new content : abort.&quot;, INT_MESSAGE_TYPE_ERROR, true))
	noErrInsert:
	
	_insertConvResult = success

end function

</script:module>