#  $Id: fonts.tcl,v 1.2 2004/11/14 15:38:27 david Exp $
#  This is an extract of Whiteboard.tcl, a very great project,
#  Copyright (c) 1999-2002  Mats Bengtsson
#  http://hem.fyristorg.com/matben

switch -- $tcl_platform(platform) {
    unix {
	set thisPlatform $tcl_platform(platform)
	if {[package vcompare [info tclversion] 8.3] == 1} {	
	    if {[string equal [tk windowingsystem] "aqua"]} {
		set thisPlatform "macosx"
	    }
	}
    }
    windows - macintosh {
	set thisPlatform $tcl_platform(platform)
    }
}
set this(platform) $thisPlatform

package require Img


# IMPORTANT : Change la font des boites de dialogue TCL (msgBox)
option add *Dialog.msg.font {Arial 10}
option add *Dialog.background #ffe70e widgetDefault



# Taille du libelle de l'exercice
set sysFont(exercice) {Helvetica 16 bold}
# Taille du libelle de la retenue
set sysFont(ret) {Helvetica 14 bold}
# Taille des caractères dans le tableau
set sysFont(tableau) {Helvetica 14 normal}
set sysFont(tableau_hist_titre) {Helvetica 11 normal}
set sysFont(tableau_hist) {Helvetica 8 normal}
# Taille des cellules du tableau
set sysFont(taillerect) 35
# Taille du message dans le bandeau du haut
set sysFont(messhaut) {Helvetica 11 normal}
# Taille du message dans le bandeau du bas
set sysFont(messbas) {Helvetica 12 normal}


# Couleur du Fond
set sysColor(color_mult_fond) #98b8ff
set sysColor(color_add_fond) #98b8ff
set sysColor(color_sous_fond) #98b8ff
set sysColor(color_sous2_fond) #98b8ff

# Couleur de l'aide
set sysColor(color_mult_aide) #2266ff
set sysColor(color_add_aide) #2266ff
set sysColor(color_sous_aide) #2266ff
set sysColor(color_sous2_aide) #2266ff

# Couleur du Fond de l'Historique
set sysColor(color_hist_fond) #2266ff
set sysColor(color_pingoin_fond) #2266ff

# Taille du libelle de l'historique
set sysFont(historique) {Helvetica 10 bold}
set sysFont(historique_scen) {Helvetica 10 normal}	

# Taille de la légende
set sysFont(legende) {Helvetica 8 normal}

# Options tableau
set sysFont(text_op_avec_couleur) "Non"
set sysFont(erreur_op_avec_etoile) "Non"
set sysFont(erreur_op_avec_chiffre) "Non"
set sysFont(couleur_col_text_op) "grey"
set sysFont(couleur_col_resultat_op) "white"


switch -- $this(platform) {
    unix {
		set sysFont(s) {Helvetica 10 normal}
		set sysFont(ms) {Helvetica 11 normal}
		set sysFont(msb) {Helvetica 11 bold}
		set sysFont(sb) {Helvetica 10 bold}
		set sysFont(m) $sysFont(s)
		set sysFont(l) {Helvetica 18 normal}
		set sysFont(lb) {Helvetica 18 bold}
		set sysFont(t) {Helvetica 14 bold}
		set sysFont(esp) {adobe-courier 30 normal}
		set sysFont(nt) {Helvetica 14 normal}
    }

    macintosh {
		set sysFont(s) {Geneva 9 normal}
		set sysFont(ms) {Geneva 11 normal}
		set sysFont(msb) {Geneva 11 bold}
		set sysFont(sb) {Geneva 9 bold}
		set sysFont(m) application
		set sysFont(l) {Helvetica 18 normal}
		set sysFont(lb) {Helvetica 18 bold}
		set sysFont(t) application
		set sysFont(esp) {Helvetica 20 bold}
		set sysFont(nt) {Helvetica 14 normal}
    }

    macosx {
		set sysFont(s) {Geneva 9 normal}
		set sysFont(ms) {Geneva 11 normal}
		set sysFont(msb) {Geneva 11 bold}
		set sysFont(sb) {Geneva 9 bold}
		set sysFont(s) {{Lucida Grande} 11 normal}
		set sysFont(sb) {{Lucida Grande} 11 bold}
		set sysFont(m) application
		set sysFont(t) application
		set sysFont(l) {Helvetica 18 normal}
		set sysFont(lb) {Helvetica 18 bold}
		set sysFont(esp) {Helvetica 20 bold}
		set sysFont(nt) {Helvetica 14 normal}
    }

    windows {
		set sysFont(s) {Arial 8 normal}
		set sysFont(ms) {Helvetica 11 normal}
		set sysFont(msb) {Helvetica 11 bold}
		set sysFont(sb) {Arial 8 bold}
		set sysFont(t) {Helvetica 18 bold}

		set sysFont(m) $sysFont(s)
		set sysFont(l) {Helvetica 16 normal}
		set sysFont(lb) {Helvetica 16 bold}
		set sysFont(esp) {Courier 18 normal}
		set sysFont(nt) {Helvetica 14 normal}
    }
}
