#!/bin/sh
# laby.tcl
# \
exec wish "$0" ${1+"$@"}
#*************************************************************************
#  Copyright (C) 2002 Jean louis Sendral <jlsendral@free.fr>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#**************************************************************************
#  File  : laby.tcl
#  Author  : Jean Louis Sendral <jlsendral@free.fr>
#  Modifier:
#  Date    : 30/05/2004
#  Licence : GNU/GPL Version 2 ou plus
#
#  Description:
## Conf�re Linux magazine n�62 et plan informatique IPT : EPI
#  ------------
#
#  @version    $Id: laby.tcl,v 0.1 2004/05/30 
#  @author     Jean Louis Sendral
#  @modifier
#  @project    Le terrier
#  @copyright  Jean Louis Sendral
#
#***********************************************************************
#**************************************************************************
#  File  : laby.tcl
#  Author  : Jean Louis Sendral <jlsendral@free.fr>
#  Modifier:
#  Date    : 30/05/2004
#  Licence : GNU/GPL Version 2 ou plus
#
#  Description:
#  ------------
#
#  @version    $Id: laby.tcl,v 0.1 2004/05/30
#  @author     Jean Louis Sendral
#  @modifier
#  @project    Le terrier
#  @copyright  Jean Louis Sendral
#
#***********************************************************************
global sysFont
source labyrinthe.conf
source msg.tcl
source fonts.tcl
source path.tcl
set plateforme $tcl_platform(platform) ; set ident $tcl_platform(user)
init $plateforme
set type_laby [lindex $argv 1 ]
set nom_laby  [lindex $argv 0] 
set rep_travail [lindex $argv 2]
set bouton_ant [lindex $argv 3]
##set bouton_ant 0
set basedir [pwd]
proc retiens_m { } {
global  w2 m
set  m [${w2}.n_m get ]
if { [ teste_input $m ] } {
set m [${w2}.n_m get ]
${w2}.n_n configure -state normal
focus ${w2}.n_n
} else {
${w2}.n_m delete 0 end
focus ${w2}.n_m
return
}
}

proc retiens_n { } {
global w2 n
set  n [${w2}.n_n get ]
if { [teste_input $n ] } {
set n  [${w2}.n_n get ]
#${w4}.n_n configure -state normal
destroy ${w2}
} else {
${w2}.n_n delete 0 end
focus ${w2}.n_n
return
}
}
 global m n
 proc teste_input { input} {
if  {[ regexp {(^[0-9]+$)} $input ] == 1 && $input <= 12} {
            return $input
    } else {
return 0
}
}
if { $nom_laby == "aucun" || $nom_laby == {}} {
##forcer le type � normal
set type_laby "normal"
set deja_sauve 0
catch {destroy .top2 } 
set w2 [toplevel .top2]
wm geometry $w2 250x150+100+0
wm resizable $w2 no no
wm title $w2 [mc {choix_m_n}]
catch {grab ${w2}}
lower  . ${w2}
raise ${w2} .
focus -force  ${w2}
label ${w2}.text_m -text [ mc {m}] -font {arial 10}
label ${w2}.contr -text [mc {Saisissez  2 nombres <= 12 :}] -font {arial 10}
place ${w2}.contr  -x 30 -y 20
place ${w2}.text_m -x 35 -y 50
entry ${w2}.n_m -width 6 -font {arial 10} -justify center
place ${w2}.n_m -x 110 -y 50

focus ${w2}.n_m
bind ${w2}.n_m <Return> "retiens_m "
bind ${w2}.n_m <KP_Enter> "retiens_m "
label ${w2}.text_n -text [mc {n}] -font {arial 10}
place ${w2}.text_n -x 35 -y 90
entry ${w2}.n_n -width 6 -font {arial 10} -justify center
place ${w2}.n_n -x 110 -y 90
${w2}.n_n configure -state disabled
#focus ${w2}.n_divis
bind ${w2}.n_n <Return> "retiens_n "
bind ${w2}.n_n <KP_Enter> "retiens_n "
tkwait window ${w2}
} else {
set deja_sauve 1
cd $rep_travail 
set f [open [file join $nom_laby ] "r"  ]
set data  [gets $f] ;#init du labyrinthe.
close $f
set m [lindex $data 0] ; set n [lindex $data 1]
if { ![catch { set chemin_min [lindex $data 5] } ] } { set chemin [lindex $data 5] }
set l_tab {} 
 ##puts "$data   [lindex $data 2] "
array set tab_laby  [lindex $data 2]
##init de d�part,arrive� et type labyrinthe
set depart [lindex $data 3] ; set arrivee [lindex $data 4] ; set type_laby [lindex $argv 1 ]
}
##################################Initialisations###############################
#################################################################################
proc init_general {} {
global N O S E ST m n cell_N trans   cell_O  cell_S  cell_E  cell_ST tab_laby trans fc deja_sauve coul_fond flag_bilan
global nom_elev nom_classe tcl_platform indice_recup deb_arr_er
if { ![info exists n ] || ![info exists m ] } { exit}
if { $tcl_platform(platform) == "unix" } {
set nom_elev $tcl_platform(user)
set nom_classe [lindex [exec  id -G -n ] 1]
if { $nom_classe == {} } {
set nom_classe classe
}
} else {
 set nom_elev eleve
 set nom_classe classe
}
##set nom_classe "classe2"
set trans 20
##m n pris dans les menus prevoir sauver labyrinthe  et donc son choix
## tableau lin�aire repr�sentant leles cases.
## les directions : ouvrir porte O : cell | 1 << $O faire des tableaux ??
set N 0 ; set O 1 ; set S 2 ; set E 3 ; set ST 4
##global N O S E ST
## les bits sont St E S O N : � 0 case non visit�e portes ferm�es
## masques pour connaitre l'�tat des portes et l'�tat de la cellule : cell & cell_N pour la nord
set cell_N 1 ; set cell_O 2 ; set cell_S 4 ; set cell_E 8 ; set cell_ST 16
##set m 6 ; set n 6
## coulrur du fond  et des murs cach�s  
set coul_fond bisque
set flag_bilan 0 ; set indice_recup 1 ; set deb_arr_er 0
if { $deja_sauve == 0 } {
 for {set i 0 } { $i <= [expr $n*$m -1 ] } { incr i } {
	set tab_laby($i) 0
	if { $i > 0 && $i < [ expr $m -1 ]} {
	set tab_laby($i) 1 }

   if { $i > [expr ${m}*(${n}- 1)] && \
			$i < [expr ${m}*${n}- 1] } {set tab_laby($i) 4 }
		if { [ expr $i % $m ] == 0 } { set tab_laby($i) 2 }
	if { [ expr $i % $m ] ==  [expr $m - 1] } {
	set tab_laby($i) 8 }
##les coins
	if { $i == 0} {set tab_laby($i) 3 }
	if { $i == [expr $m -1 ]} {set tab_laby($i) 9 }
	if { $i ==  [expr $m * [expr $n - 1] ] } {set tab_laby($i) 6 }
	if { $i == [expr ${m}*${n}- 1] } {set tab_laby($i) 12 }
	}
}
}
##set tab [array get tab_laby]
##################################Init canvas########################################################
########################################################################################################
proc contraintes {} {
		global contr_plus tcl_platform depart ici nbre_rec indice_recup flag_rel_pas_contr
		catch { .bottom.check_contr configure -state disabled}
##		.right.anticiper configure -state active
##			 recommencer {}
			 if {$contr_plus == 1} {
			 if { $nbre_rec == 0 && $flag_rel_pas_contr !=1 } { set nbre_rec -1}
			 if { $indice_recup == 1 } { set indice_recup 0}
			 .right.anticiper configure -state active
			 #			 set ici $depart
        button .right.modif_av_td -text [mc {AV TD}] -font {Helvetica 10 bold } -command "que_av_td"
 		pack .right.modif_av_td -side left -padx 1
		button .right.modif_av_tg -text [mc {AV TG}] -font {Helvetica 10 bold } -command "que_av_tg"
 		pack .right.modif_av_tg -side left -padx 1
		button .right.modif_re_td -text [mc {RE TD}] -font {Helvetica 10 bold } -command "que_re_td"
 		pack .right.modif_re_td -side left -padx 1
		button .right.modif_re_tg -text [mc {RE TG}] -font {Helvetica 10 bold } -command "que_re_tg"
 		pack .right.modif_re_tg -side left -padx 1
		} else {
		catch {destroy .right.modif_av_td .right.modif_av_tg .right.modif_re_td .right.modif_re_tg}
#		set ici $depart
		bind . <Left> {set vers gauche ; va  $vers }
		bind . <Down> {set vers bas ; va  $vers }
		bind . <Right> { set vers droite ;   va  $vers }
		bind . <Up> {set vers haut ;  va  $vers }
			if { $tcl_platform(platform) == "unix" }  {
		bind . <KP_Right> { set vers droite ;   va  $vers }
		bind . <KP_Left> {set vers gauche ; va  $vers }
		bind . <KP_Down> {set vers bas ; va  $vers}
		bind .  <KP_Up> {set vers haut ;  va  $vers }
				}
		
				}
recommencer {}
				}




proc init_traces {} {
global m n trans largeur hauteur x_no y_no x_so y_so x_ne y_ne x_se y_se trans tab_laby data deja_sauve type_laby  coul_fond
global tcl_platform flag_bilan nom_elev nom_classe  flag_sauv progaide contr_plus basedir flag_rel_pas_contr ant ep_fleche
##taille des cases
set taille_lar 35 ; set taille_hau 35 ; set flag_sauv 0 ; set ep_fleche 6
set largeur [expr $taille_lar - 0] ; set hauteur [expr $taille_hau - 0]
. configure -background blue
. configure -width 600 -height 640
wm geometry . +0+0
wm title  . "[mc {laby}] $type_laby $n sur $m"
wm resizable . no yes
##pb windows pr files
if { $tcl_platform(platform) == "unix" } {
bind .  <F1>  "exec ${basedir}/$progaide ${basedir}/aide1.html $"
}
##cd $reper
set haut_canvas  [expr ($n*$hauteur) +30 ] ; set haut_frame  [expr $haut_canvas  +140]
 frame .frame -bg blue -height $haut_frame  -width 600
  pack  .frame -side top -fill both -expand yes
 frame .right  -bg blue -width 20
  pack .right -side top -fill both -expand yes -pady 4 -ipady 1
  frame .bottom -bg blue -width 180
  pack  .bottom -side top  -expand yes  -fill both 
##  -fill both  -expand yes
if { $type_laby == "relatif" } {
label .bottom.message -text  "[mc {Utiliser les fl�ches du clavier pour:Faire AVancer la fl�che  jaune (AV) La faire  Reculer (RE) La faire Tourner vers sa Droite (TD) La faire Tourner vers sa gauche (TG)}]" -font {helvetica 12}

} else {
 label .bottom.message -text "[mc {consignes}]" -font {helvetica 12}
 }




 button .right.refaire -text [mc {Recommencer}] -font {Helvetica 12 bold } -command "recommencer {} "
 pack .right.refaire -side left
  button .right.anticiper -text [mc {Anticiper}] -font {Helvetica 12 bold } -command "anticiper"
 pack .right.anticiper -side left -padx 10
## .right.anticiper invoke
		## button .right.modif -text [mc {Modifs para}] -font {Helvetica 12 bold } -command "recommencer {} "
## pack .right.modif -side left
  button .right.quit -text [mc {Quitter/Sauver Bilan}] -font {Helvetica  12 bold }\
  -command { set flag_sauv 1 ; if { [info exists data1 ]  } {sauver_bilan ;  exit } else {
  tk_messageBox -message "[mc {Labyrinthe sauv� ou abandon du travail(dommage ...!!)}]" -title "[mc {Sauver/Abandon}]" -icon info -type ok   
set mess_ab " Abandon de l'exo ..."  
  exit} }
 ##command  { if { $flag_bilan == 1} {
 ## set rep_sb [  tk_messageBox -message "Conserver  les bilans ?" -title "Conserver les bilans" -icon info -type yesno ] 
###  if { $rep_sb == "yes" } { sauver_bilan ; exit } else { exit} } else { exit } }
 pack .right.quit -side right -padx 2
  button .right.impr -text [mc {Imprimer}] -font {Helvetica 12 bold } -command "imprime .frame.c "
  if { $type_laby == "normal"  } {
  button .right.grille_ant -text [mc {Grille?}] -font {Helvetica 12 bold } -command "voir_grille"
 		pack .right.grille_ant -side left -padx 1
##.right.anticiper invoke
		}
 if  { $type_laby == "cache" } {
		   .right.impr configure -state disabled
		   }
  pack .right.impr -side right -padx 2
if { $tcl_platform(platform) != "unix" } {
 .right.impr configure -state disabled
}
 button .right.sauver -text [mc {Sauver}] -font {Helvetica 12 bold } -command "sauver  "
 pack .right.sauver -side right  -padx 2
 
 pack  .bottom.message -side right
 
## place .bottom.message  -x  0 -y 0
##.bottom.message configure -text " n :[lindex $argv 0 ]  "
######## #Le canvas pour le labyrinthe########
 ##################################
 set fc .frame.c 
  canvas $fc -height  $haut_canvas -width [expr $trans + $m*$taille_lar +10] -bg  $coul_fond -highlightbackground blue
label .bottom.t_nom -text "[mc {Nom :}]" -font {helvetica 11 bold}
label .bottom.t_classe -text "[mc {Classe :}]"  -font {helvetica 11 bold}
entry .bottom.nom -width 15 -font {helvetica 11 } -justify center 
entry .bottom.classe  -width 10 -font {helvetica 11 } -justify center 
place .bottom.t_nom -x 0 -y 5
place .bottom.t_classe  -x 0 -y 35
place .bottom.nom -x 60 -y 5
place .bottom.classe -x 60  -y 35
.bottom.nom  insert end $nom_elev
.bottom.classe insert end $nom_classe
###si relatifif 
if { $type_laby == "relatif" } {
label .bottom.contr -text "[mc {Contraintes suppl�mentaires sur les ordres ?}]" -font {helvetica 8 bold}
##place .bottom.contr -x 250  -y 30
checkbutton .bottom.check_contr -variable contr_plus  -text "[mc {Contraintes suppl�mentaires sur les ordres ?}]"\
-font {helvetica 9 bold} -onvalue 1 -indicatoron true -selectcolor green -command  "contraintes"
place .bottom.check_contr -x 0  -y 60
set contr_plus 0 
set flag_rel_pas_contr 0
##catch { .bottom.check_contr configure -state disabled}
##invoke .bottom.check_contr
}
if { $type_laby == "cache" } {
text .bottom.bloc_notes -width 25 -height 4 -setgrid 1 -yscrollcommand ".bottom.scroll set" -font {helvetica 9 }
scrollbar .bottom.scroll -command ".bottom.bloc_notes yview"
place .bottom.bloc_notes -x 170 -y 0
place .bottom.scroll -x 340 -y 0 -height 60
.bottom.bloc_notes insert @0,0 "[mc {Blocs notes :}]" 
}
if { $tcl_platform(platform) == "unix" } { .bottom.nom configure -state disabled ;  .bottom.classe configure -state disabled }
  ## set nom [entry $fc.entree_nom -width 15 -font {Helvetica 10 bold} -justify center ]
##set nom1 [.frame.c  create window 10  30  -window .frame.c.entree_nom ]
##set classe  [entry .frame.c.entree_classe  -width 10 -font {Helvetica 10 bold} -justify center ]
##set classe1 [.frame.c  create window 10  70  -window  .frame.c.entree_classe ] 
 pack $fc -expand true ; ##520 � adapter � la largeur
## afficher tous les murs
 .frame.c create rect $trans $trans [expr $trans + $m*$taille_lar ] [expr $trans + $n*$taille_hau ]   -width 2 -outline black
focus -force .  
}

#################Sauver le labyrinthe tableau d�part arriv�e nbre lignes colonnes
proc sauver { } {
global rep_travail tab_laby m n deja_sauve depart arrivee chemin_min chemin tcl_platform Home
set tab  {}
for {set i 0 } { $i <= [expr [ llength [array get tab_laby ]  ] - 1]}  {incr i }  {
set tab [linsert $tab end [lindex [array get tab_laby ] $i ] ]
}
set datas "$m $n     \{${tab}\}   $depart $arrivee \{$chemin_min\}  "
set types {    {{Fichiers labyrinthes}            {.lab}        }}
set ext .lab
catch {set nom_laby [tk_getSaveFile -filetypes $types -initialdir $rep_travail ]}
    if {$nom_laby != ""} {
	
				if { $tcl_platform(platform) == "unix" } {
                set name_laby "${nom_laby}" } else {
				set name_laby "${nom_laby}${ext}"
				}
       set f [open [file join $Home reglages reglage.conf] "w+"  ]
	   puts $f  [file tail $name_laby]
	puts $f [file dirname $nom_laby ]
	close $f 
	   
		}
	if { ![ catch {set f [open $name_laby "w"] }] } {
    puts $f $datas
    close $f 
  set deja_sauve 1
 .right.sauver configure -state disabled }
    }


###################################Procedures, fonctions utilitaires##################
####################################################################################
############# pour obtenir la liste des coor de la case d'indice indice
proc indice_xy { indice } {
global m
return [ list [expr $indice / $m ] [expr $indice % $m] ]
}
#### et pour avoir l'indice de la case correspondant aux coordonne�s de la case
proc xy_indice { x y} {
global m
return [expr  $x*$m +$y]
}

#####################################
######## Pour les coordonnes des coins de la  case de num�ro n_case
proc cas_nseo { n_case} {
##ne suffit
global largeur hauteur x_no y_no x_so y_so x_ne y_ne x_se y_se trans
set x_case [lindex [indice_xy $n_case ] 1] ;##au sens canvas
set y_case [lindex [indice_xy $n_case ] 0]
set x_no [expr ${x_case}*$largeur + $trans]
set y_no [expr ${y_case}*$hauteur + $trans ]
##donc
set y_so [expr $y_no + $hauteur  ] ; set x_so  $x_no
set y_ne $y_no ; set x_ne [expr $x_no + $largeur]
set x_se $x_ne ; set y_se $y_so
}

##.bottom.message configure -text [array get tab]
##update
##############Test des bords#################
proc pas_bord_e { c } {
global m
if { [expr $c % $m ] == [expr $m - 1] } { return 0} else { return 1}
}

proc pas_bord_o { c } {
global m
if { [expr $c % $m ] == 0 } {return 0} else { return 1 }
}
proc pas_bord_n { c } {
global m
if { $c >= $m } {return 1} else { return 0 }
}
proc pas_bord_s { c } {
global m n
if { $c <  [expr $m  * ($n - 1)  ] }  {return 1} else { return 0 }
}

##################Choix case suivante pour avancer dans le labyrinthe"""""""""""""""""
proc case_suivante {case  }  {
global N O S E cell_N m n  cell_O  cell_S  cell_E  cell_ST tab_laby
##�tat des portes PB DES BORDS ??? tester les bords pour e  et o
set cell $tab_laby($case) ; set l_cases {}
##set cell_s_n $tab_laby([expr $case - $m])
## �tat des portes des celules si 1 ouverte et etat occup�e de la suivante
 if {[expr [expr $cell & $cell_S ] == $cell_S ] }  { set etat_p_s 1} else { set etat_p_s 0}
if { [expr [expr $cell & $cell_N ] == $cell_N ] } { set etat_p_n 1} else { set etat_p_n 0 }
if {[expr [expr $cell & $cell_O ] == $cell_O ]} {set etat_p_o 1} else { set etat_p_o  0 }
if [expr [expr $cell & $cell_E ] == $cell_E ] {set etat_p_e 1} else { set etat_p_e 0 }
set etat_p_st [expr [expr $cell & $cell_ST ] == $cell_ST ]

if { [ pas_bord_n $case ] == 1   && $etat_p_n == 0  && \
    [expr $tab_laby([expr $case - $m]) & $cell_ST ] == 0 } {set l_cases [linsert  $l_cases 0 N ]  }
if { [ pas_bord_o $cell ] == 1 && $etat_p_o == 0  && \
	[expr $tab_laby([expr $case - 1 ]) & $cell_ST ] == 0 } {set l_cases [  linsert $l_cases 0 O]  }
if { [ pas_bord_s $case ] == 1  && $etat_p_s ==0  && \
[expr $tab_laby([expr $case + $m]) & $cell_ST ] == 0  } {set l_cases [  linsert $l_cases 0 S]  }
if { [ pas_bord_e $case ] == 1 &&   $etat_p_e == 0 && \
[expr $tab_laby([expr $case + 1]) & $cell_ST ] == 0 } { set l_cases [linsert $l_cases 0 E ] }
if { $l_cases != {} } {
 set l [llength $l_cases] ; set has [expr int(${l}*rand()) ]
	return 	[lindex $l_cases $has]

	} else { return $l_cases }
}
set trace {}
###############################Elaboration du chemin##############################
###########################################################################
proc  cheminer { dep retour mur chemin  } {
##appel par chemin n-depart "" tab_laby
global m n N O E S ST tab_laby x_no y_no x_so y_so x_ne y_ne x_se y_se trace coul_fond
if { $mur == [expr ${m}*{$n} ] || [lindex $chemin end ] == {} } {
		return  $retour
##list $chemin
}
set vers [case_suivante [lindex $chemin end ] ]
set case_ac [lindex $chemin end ] 
### set trace [linsert $trace end "$retour v $vers $mur $chemin\n" ]
 set tab_laby($case_ac) [expr $tab_laby($case_ac) | [expr 1 << $ST ] ]
if { $vers != {} } { incr mur ;
##detruire mur	maj tab_laby  dep et cas_suiv
	switch $vers {
	"N" { set cas_suiv  [expr  $case_ac - $m]
	 set tab_laby($case_ac) [expr $tab_laby($case_ac) | [expr 1 << $N ] ]

	set tab_laby($cas_suiv) [expr $tab_laby($cas_suiv) | [expr 1 << $S ] ]

	   }
	"O" { set cas_suiv  [expr $case_ac - 1 ]
	 set tab_laby($case_ac) [expr $tab_laby($case_ac) | [expr 1 << $O ] ]

	 set tab_laby($cas_suiv) [expr $tab_laby($cas_suiv) | [expr 1 << $E ] ]

		}
	"S" { set cas_suiv  [expr $case_ac + $m ]
	 set tab_laby($case_ac) [expr $tab_laby($case_ac) | [expr 1 << $S ] ]

	 set tab_laby($cas_suiv) [expr $tab_laby($cas_suiv) | [expr 1 << $N ] ]

	 }
	"E" { set cas_suiv  [expr $case_ac + 1 ]
	 set tab_laby($case_ac) [expr $tab_laby($case_ac) | [expr 1 << $E ] ]

	 set tab_laby($cas_suiv) [expr $tab_laby($cas_suiv) | [expr 1 << $O ] ]

	}
	}

     cas_nseo [lindex $chemin end ] ; ##x_no y_no x_so y_so x_ne y_ne x_se y_se
   		switch $vers {
			"N" {.frame.c create line $x_no $y_no $x_ne $y_ne -fill   $coul_fond  -width 2 }
			"O" {.frame.c create line $x_no $y_no $x_so $y_so -fill $coul_fond   -width 2 }
			"S" {.frame.c create line $x_so $y_so $x_se $y_se -fill $coul_fond  -width 2}
			"E" {.frame.c create line $x_ne $y_ne $x_se $y_se -fill $coul_fond  -width 2 }
		}
		set chemin [linsert $chemin end $cas_suiv ]
		return [cheminer $dep $cas_suiv  $mur  $chemin  ]
	} else {


		set chemin [lrange $chemin 0 end-1 ]
		return [linsert  [cheminer $dep [lindex $chemin end ]   $mur   $chemin  ] end [lindex $chemin end ]]

}
}
###################Tracer les murs en coul_fond pour cach�########################################
###########################################################################
proc le_plus_pres { el} {
global chemin m
set chem [lrange $chemin 1 end]
##tk_messageBox  -message " $chem et $el en dehors"  -icon info  -type ok -title "le_plus_pres"
if { [lsearch $chem $el  ] != -1 } { return $el }
for {set i 0} {$i <  [llength $chem ] } { incr i } {
##tk_messageBox  -message " [expr abs([lindex $chem $i] - $el ) ] 1 ou 8 ?"  -icon info  -type ok -title "le_plus_pres"
if { [expr abs([lindex $chem $i ] - $el ) ] == 1 || [expr abs([lindex $chem $i] - $el )] == $m} {
return [lindex $chem $i ] 
	}
}

}



proc choisir_d_a {x y} {
global m n liste_d_a largeur hauteur chemin
##.bottom.nom insert end "$x $y"
##choisir sur les bordures ??
bind .frame.c ""
##set liste_d_a "bon"
##.bottom.nom insert end "bon"
if { $x <= 20 || $y <= 20 || $x >= [expr 20 + ${m}* ${largeur} ] || $y >= [expr 20 + ${n}* ${hauteur} ]} { return [le_plus_pres [expr int(rand()*$m*${n}) ]] }
return [le_plus_pres [ expr int([expr $y - 20] / 35)*$m + int([expr $x - 20] / 35) ]]

}

proc case_init {lieu x y} {
#pb avec choix au clic et possibilit�sdans chemin!!
global m n depart arrivee tab_laby x_no y_no x_so y_so x_ne y_ne x_se y_se liste_d_a bon deja_sauve chemin
 switch $lieu {
		dep {
		if { $deja_sauve != 0 } {set dep $depart  } else {
		set dep [choisir_d_a $x $y] } ; set depart $dep
		set cell $tab_laby($dep) ; cas_nseo $dep
if { [expr $dep % $m ] == 0 } {
.frame.c create line $x_no $y_no $x_so $y_so -fill red -width 4 -tags dep_arr } elseif \
 { [expr $dep% $m ] == [expr $m - 1] } { .frame.c create line $x_ne $y_ne $x_se $y_se -fill red -width 3 -tags dep_arr} elseif \
{  $dep> 0 && $dep< [expr $m - 1] } { .frame.c create line $x_no $y_no $x_ne $y_ne -fill red -width 3 -tags dep_arr} elseif \
 {  $dep> [expr  $m*($n-1)] && $dep< [expr ($m*$n)-1] } { .frame.c create line $x_so $y_so $x_se $y_se -fill red -width 2 -tags dep_arr} else {
 .frame.c create rectangle [expr $x_no + 4] [expr $y_no + 4 ] [expr $x_se - 4 ] [expr $y_se - 4 ]  -fill red -width 3 -tags dep_arr
 }
			}
		
		arr {
	if { $deja_sauve != 0 } {set arr $arrivee  } else {	
	set arr [choisir_d_a $x $y]  ; set arrivee $arr }
		set cell $tab_laby($arr) ; cas_nseo $arr
if { [expr $arr % $m ] == 0 } {
.frame.c create line $x_no $y_no $x_so $y_so -fill green -width 4 -tags dep_arr} elseif \
 { [expr $arrivee % $m ] == [expr $m - 1] } { .frame.c create line $x_ne $y_ne $x_se $y_se -fill green -width 3 -tags dep_arr } elseif \
{  $arrivee > 0 && $arrivee < [expr $m - 1] } { .frame.c create line $x_no $y_no $x_ne $y_ne -fill green -width 3 -tags dep_arr} elseif \
 {  $arrivee > [expr  $m*($n-1)] && $arrivee < [expr ($m*$n)-1] } { .frame.c create line $x_so $y_so $x_se $y_se -fill green -width 3 -tags dep_arr} else {
 .frame.c create rectangle [expr $x_no + 4] [expr $y_no + 4 ] [expr $x_se - 4 ] [expr $y_se - 4 ]  -fill green -width 3 -tags dep_arr
 }
		}
 }

 set liste_d_a "bon"
}


proc tracer {coul} {
global tab_laby x_no y_no x_so y_so x_ne y_ne x_se y_se m n cell_N trans depart data type_laby basedir iwish
global  cell_O  cell_S  cell_E  cell_ST arrivee liste_d_a deja_sauve nbre_rec chemin chemin_min flag_essai
array set tab_laby [array get tab_laby]
##.bottom.message configure -text "$m $n $depart $arrivee \n [lrange [lindex $data 2] 0 13]\n [lrange [array get tab_laby] 0 13]"

for { set  case 0} { $case < [expr $m*$n] } {incr case} {
 cas_nseo $case ; ##x_no y_no x_so y_so x_ne y_ne x_se y_se
 set cell $tab_laby($case)
if {[expr [expr $cell & $cell_S ] == 0 ] } {
	.frame.c create line $x_so $y_so $x_se $y_se -fill $coul -width 2 -tags trait
	}
if {[expr [expr $cell & $cell_N ] == 0 ] } {
	.frame.c create line $x_no $y_no $x_ne $y_ne -fill $coul -width 2 -tags trait

	}
if {[expr [expr $cell & $cell_O ] == 0 ] } {
	.frame.c create line $x_no $y_no $x_so $y_so -fill $coul -width 2 -tags trait
	}
if {[expr [expr $cell & $cell_E ] == 0 ] } {
	.frame.c create line $x_ne $y_ne $x_se $y_se -fill $coul -width 2 -tags trait
	}
}

###tracer d�part arriv�e ou le red�finir . ils sont sur le bord
if { $deja_sauve == 0 && $nbre_rec == 0} {
## bind . <Left> { }  ;  bind . <Right> { }
##bind . <Up> {} ;  bind . <Down> { }
##tk_messageBox  -message "[mc {Choisir, en cliquant sur 2 cases, d�part et arriv�e du labyrinthe.Puis Sauver.Puis Quitter}]"  -icon info  -type ok -title [mc {Sauver un labyrinthe.}] 
##d�sactiver recomm grille? ..
## essai set depart [expr 3*$m +4] ; set arrivee [expr $m +2]
catch {.right.refaire configure -state disabled ; .right.sauver configure -state disabled ;\
 .right.grille_ant configure -state disabled ; .right.sauver configure -state disabled ;\
 .right.anticiper configure -state disabled ; .right.bilan configure -state disabled }
set flag_essai 1
set autre  "yes"
 while { $autre == "yes" } {
 set accepte "no"
 while { $accepte == "no" } {
catch {.frame.c delete tags dep_arr curseur} 
tk_messageBox  -message "[mc {Choisir, en cliquant sur 2 cases, d�part et arriv�e du labyrinthe.Puis Sauver.Puis Quitter}]"  -icon info  -type ok -title [mc {Sauver un labyrinthe.}]
set deja_sauve 0
 set  liste_d_a ""
bind .frame.c <Button-1> " case_init dep %x %y  "
##set depart [choisir_d_a %x %y] 
##set cell $tab_laby($arrivee) ; cas_nseo $depart
tkwait variable liste_d_a
set  liste_d_a ""
bind .frame.c <Button-1> {case_init arr %x %y }  
tkwait variable liste_d_a
set  liste_d_a ""
bind .frame.c <Button-1> ""

set chemin_min [trouve_min $chemin]
##tk_messageBox  -message " d : $depart a : $arrivee et chem_min $chemin_min"  -icon info  -type ok -title "verif depart arrivee"
deplacer $type_laby ""
auto $chemin_min
deplacer $type_laby ""
##auto appelle tracer!!!
set accepte [tk_messageBox  -message "[mc {D�part et arriv�e  conviennent il ?}]" -type yesno -icon question  -default yes -title "[mc {Labyrinthe accept�?}]" ]
 }
##bind . <Insert> { auto $chemin_min  }
set flag_essai 0
.right.refaire configure -state disabled ; .right.sauver configure -state normal ;\
 .right.grille_ant configure -state disabled ; .right.sauver configure -state normal ; .right.anticiper configure -state disabled
 ##anihiler les fleches du clavier
 tkwait variable deja_sauve
set autre [tk_messageBox  -message "[mc {Autre  d�part, autre arriv�e ?}]" -type yesno -icon question  -default no -title "[mc {Autre d�part, autre arriv�e ?}]" ]
 }
 
 bind . <Left> { }  ;  bind . <Right> { }
bind . <Up> {} ;  bind . <Down> { }
cd $basedir 
destroy . ; exec $iwish labyrinthes.tcl



##auto $chemin_min 

} else {
##else rien on garde les valeurs
##en mode cr�ation stopper les fl�ches
case_init dep 0 0 ; case_init arr 0 0 

}
}
proc tout_droit { che } {
if { [llength $che ] == 1 } {
 return $che } else {
if { [lsearch -exact [lrange  $che 1 end ] [lindex $che 0] ] == -1 } {
return [linsert [ tout_droit [lrange $che 1 end]] 0 [lindex $che 0] ]
} else {
return [ tout_droit  [lrange $che [expr [lsearch -exact [lrange  $che 1 end ] [lindex $che 0] ] + 1] end] ]
}
}
}
#########################Activite de d�placement dans le labyrinthe####################
proc peut_on { lieu vers } {
global tab_laby cell_N   cell_O  cell_S  cell_E  cell_ST m n
	switch $vers {
		"droite" {
		if { [expr [expr $tab_laby($lieu) & $cell_E ] == 0] || [expr $lieu % $m] == [expr $m -1] } {
		return 0 } else { return 1}
		}
		"gauche" {
		if { [expr [expr $tab_laby($lieu) & $cell_O ] == 0] || [expr $lieu % $m ] == 0  } {
		return 0 } else { return 1}
		}
		"haut" {
		if { [expr [expr $tab_laby($lieu) & $cell_N ] == 0] || [expr $lieu <= [expr $m -1] && $lieu >= 0 ]} {
		return 0 } else { return 1}
		}
		"bas" {
		if { [expr [expr $tab_laby($lieu) & $cell_S ] == 0] || [expr $lieu <= [expr $m*$n -1] && [expr $lieu >= [expr $m*($n-1)]]]   } {
		return 0 } else { return 1}
		}

	}

}
proc clignote {} {
global ici curseur
bell ; bell
for  { set i 1} {$i <= 1 } { incr i } {
after 50
.frame.c itemconfigure $curseur  -fill red
update
.frame.c itemconfigure $curseur  -fill yellow
after 50
update
}
}
proc clignote_bon {} {
global ici curseur
bell ; bell
for  { set i 1} {$i <= 3 } { incr i } {
after 200
.frame.c itemconfigure $curseur  -fill yellow -width 4
update
after 200
.frame.c itemconfigure $curseur  -fill green -width 4
update
}
}


proc vide_fleche {}  {
global fleche_d fleche_g fleche_h fleche_b curseur curs_n
set fleche_g "" ; set fleche_d "" ; set fleche_h "" ; set fleche_b "" ; set curseur "" ; set curs_n ""
}
#################pour le laby cache###############################
##############################Rendra visible  les murs... invisibles###############
proc traite_cache {lieu vers } {
global global largeur hauteur x_no y_no x_so y_so x_ne y_ne x_se y_se echec reussite
cas_nseo $lieu

  switch $vers {
	"droite" { if { [pas_bord_e $lieu] == 1 } {
				.frame.c create line $x_ne $y_ne $x_se $y_se -fill red -width 1  }
				}
	"gauche" { if { [pas_bord_o $lieu] == 1 } {
		.frame.c create line $x_no $y_no $x_so $y_so -fill red -width 1  }
		}
	"bas" { if { [pas_bord_s $lieu] == 1 } {
		.frame.c create line $x_so $y_so  $x_se $y_se -fill red -width 1 }
		}
	"haut" { if { [pas_bord_n $lieu] == 1 } {
	.frame.c create line $x_no $y_no $x_ne $y_ne -fill red -width 1  }
	}
	}
}

#####################Pour enchainer selon le type de laby et les ordres av re tg td ...#################
#########################################################################################
proc tout_unset { } {
global x_no y_no x_so y_so x_ne y_ne x_se y_se
unset  x_no y_no x_so y_so x_ne y_ne x_se y_se
}
########################## pur se d�placer pas � aps
proc va { ou  } {
global tab_laby largeur hauteur curseur ici m n curs_n fleche_d fleche_g fleche_h fleche_b coul_fond ep_fleche
global echec reussite nbre_essais type_laby  x_no y_no x_so y_so x_ne y_ne x_se y_se arrivee depart time_fin time_debut
global nom_laby type_laby tab_bilan ant nbre_rec flag_que w2  flag_bilan contr_plus flag_rel_pas_contr basedir chemin
global flag_essai
##cas_nseo $ici
catch { .frame.c delete tags img }
bind . <Insert> {}

 set tab_bilan($nom_laby,$type_laby,$nbre_rec,dep) {} 
 set tab_bilan($nom_laby,$type_laby,$nbre_rec,ordres_bad) {} 
if { $type_laby == "relatif"  &&  $tab_bilan($nom_laby,$type_laby,$nbre_rec,c_rel) ==  ""} {
set tab_bilan($nom_laby,$type_laby,$nbre_rec,c_rel)  ""
if { $contr_plus == 1} {
.right.modif_av_tg configure -state disabled
.right.modif_re_td configure -state disabled
.right.modif_av_td configure -state disabled
.right.modif_re_tg configure -state disabled
##.bottom.message configure -text  "Utiliser les fl�ches du clavier pour :\nFaire AVancer la fl�che  jaune (AV)\nLa faire  Reculer (RE)\nLa faire Tourner vers sa Droite (TD)\nLa faire Tourner vers sa gauche (TG)" -font {helvetica 12}
}
}


.right.anticiper configure -state disabled

if {  $nbre_essais == 0} {set time_debut [clock seconds] }
incr nbre_essais
##.frame.c  move $curseur $largeur 0 pb des bords
##focus .frame.c
	if { $curseur == $curs_n } {
	switch $ou {
 	"droite" {
 	if { [peut_on $ici droite ] == 1 } {
	.frame.c  move $curseur $largeur 0 ; set ici  [expr $ici + 1 ] ; incr reussite
	} else { incr echec ; clignote ; if { $type_laby == "cache" } {traite_cache $ici $ou }}
	}
	"gauche" {
	if { [peut_on $ici gauche ] == 1 } {
	.frame.c  move $curseur -$largeur 0 ; set ici [expr $ici - 1 ] ; incr reussite
	} else {incr echec ; clignote ; if { $type_laby == "cache" } {traite_cache $ici $ou}}
	}
	"haut" {
	if { [peut_on $ici haut ] == 1 } {
	.frame.c  move $curseur  0 -$hauteur ; set ici [expr $ici - $m ] ; incr reussite
	} else { incr echec ;  clignote ; if { $type_laby == "cache" } { traite_cache $ici $ou }}
	}
	"bas" {
	if { [peut_on $ici bas ] == 1 } {
	.frame.c  move $curseur  0 $hauteur ; set  ici [expr $ici + $m ] ; incr reussite
	} else { incr echec ; clignote ; if { $type_laby == "cache" } { traite_cache $ici $ou }}
	}
		} 
		} elseif { $curseur == $fleche_d } { 

		switch $ou {
 			"haut" { if { [peut_on $ici droite ] == 1 } {
		tout_unset ;	cas_nseo $ici ; incr reussite
	.frame.c  move $curseur $largeur 0 ; set ici  [expr $ici + 1 ]
	} else { clignote ; incr echec }
	}
			"bas" { if { [peut_on $ici gauche ] == 1 } {
		tout_unset ;	cas_nseo $ici ; incr reussite
			.frame.c  move $curseur -$largeur 0 ; set ici  [expr $ici - 1 ]
		} else { clignote }

				}
			"droite" {.frame.c delete $curseur ; vide_fleche
		tout_unset ;	cas_nseo $ici
set fleche_b [.frame.c create line [expr [expr $x_ne + $x_no] / 2 ]  $y_ne [expr [expr $x_so + $x_se] / 2 ] $y_so  -fill yellow  -width $ep_fleche -arrow last -tags curseur_b ]
			 set curseur $fleche_b

				}
			"gauche" {.frame.c delete $curseur ; vide_fleche
		tout_unset ;	cas_nseo $ici
set fleche_h [ .frame.c create line  [ expr [expr $x_so + $x_se] / 2 ] $y_so [expr [expr $x_ne + $x_no] / 2] $y_ne -fill yellow  -width $ep_fleche -arrow last -tags curseur_h ]
			 set curseur $fleche_h

				}

##.bottom.message configure -text "$x_no $y_no $x_so $y_so $x_ne $y_ne $x_se $y_se"

		}

		} elseif { $curseur == $fleche_g } {

		switch $ou {
 			"haut" { if { [peut_on $ici gauche ] == 1 } {
		tout_unset ;	cas_nseo $ici ; incr reussite
	.frame.c  move $curseur -$largeur 0 ; set ici  [expr $ici - 1 ]
	} else { clignote ; incr echec}
	}
			"bas" { if { [peut_on $ici droite ] == 1 } {
		tout_unset ;	cas_nseo $ici ; incr reussite
			.frame.c  move $curseur $largeur 0 ; set ici  [expr $ici + 1 ]
		} else { clignote ; incr echec }

				}
			"gauche" { .frame.c delete $curseur ; vide_fleche
		tout_unset ;	cas_nseo $ici
set fleche_b [ .frame.c create line [expr [expr $x_no + $x_ne] / 2]  $y_no [expr [expr $x_so + $x_se] / 2 ] $y_so  -fill yellow  -width $ep_fleche -arrow last -tags curseur_b ]
			 set curseur $fleche_b

				}
			"droite" {.frame.c delete $curseur ;  vide_fleche
		tout_unset ;	cas_nseo $ici
set fleche_h [ .frame.c create line  [ expr [expr $x_so + $x_se ] / 2 ] $y_so [expr [expr $x_ne + $x_no ] / 2] $y_no -fill yellow  -width $ep_fleche -arrow last -tags curseur_h ]
			 set curseur $fleche_h

				}

		}

		} elseif { $curseur == $fleche_h } {

		switch $ou {

 			"haut" { if { [peut_on $ici haut ] == 1 } {
		tout_unset ;	cas_nseo $ici ; incr reussite
	.frame.c  move $curseur 0 -$hauteur ; set ici  [expr $ici - $m ]
	} else { clignote ; incr echec }
	}
			"bas" { if { [peut_on $ici bas ] == 1 } {
		tout_unset ;	cas_nseo $ici ; incr reussite
			.frame.c  move $curseur 0 $hauteur ; set ici  [expr $ici + $m ]
		} else { clignote ; incr echec }

				}
			"gauche" { .frame.c delete $curseur ; vide_fleche
		tout_unset ;	cas_nseo $ici
set fleche_g [ .frame.c create line $x_ne [expr [expr $y_ne + $y_se ] / 2] $x_no [expr [expr $y_so + $y_no ] / 2 ]  -fill yellow  -width $ep_fleche -arrow last -tags curseur_g ]
			 set curseur $fleche_g

				}
			"droite" {.frame.c delete $curseur ; vide_fleche
		tout_unset ;	cas_nseo $ici
set fleche_d [ .frame.c create line  $x_no [expr [expr $y_so + $y_no ] / 2 ] $x_se [expr [expr $y_se + $y_ne] / 2]  -fill yellow  -width $ep_fleche -arrow last -tags curseur_d ]
			 set curseur $fleche_d

				}

		}

		} elseif { $curseur == $fleche_b } {

		switch $ou {
 			"haut" { if { [peut_on $ici bas ] == 1 } {
		tout_unset ;	cas_nseo $ici ; incr reussite
	.frame.c  move $curseur 0 $hauteur ; set ici  [expr $ici + $m ]
	} else { clignote ; incr echec}
	}
			"bas" { if { [peut_on $ici haut ] == 1 } {
		tout_unset ;	cas_nseo $ici ; incr reussite
			.frame.c  move $curseur 0 -$hauteur ; set ici  [expr $ici - $m ]
		} else { clignote ; incr echec }

				}
			"droite" {.frame.c delete $curseur ; vide_fleche
		tout_unset ;	cas_nseo $ici
set fleche_g [ .frame.c create line  $x_ne [expr [expr $y_ne + $y_se] / 2 ] $x_no [expr [expr $y_so + $y_no ] / 2 ]  -fill yellow  -width $ep_fleche -arrow last -tags curseur_g ]
			 set curseur $fleche_g

				}
			"gauche" {.frame.c delete $curseur ; vide_fleche
		tout_unset ;	cas_nseo $ici
set fleche_d [ .frame.c create line  $x_no [expr [expr $y_so + $y_no] / 2 ] $x_se [expr [expr $y_se + $y_ne] / 2]  -fill yellow  -width $ep_fleche -arrow last -tags curseur_d ]
			 set curseur $fleche_d

				}

		}

		} 
##metre ici si trop d'erreurs !!!
if {  $echec >= 100 } {
  tk_messageBox -message "[mc {Trop d'erreurs. longueur minimun}] : [llength $chemin ]" -title "[mc {Trop d'erreurs}]" -icon info -type ok   
.frame.c create image  [expr [lindex  [indice_xy $arrivee] 1 ] * 35 + 30] [expr [expr int([lindex [indice_xy $arrivee]  0 ])] * 35 +30]     -image [image create photo -file [file join $basedir images mal.gif]]  -tags img
##set echec_tf_a $echec_tf
update
after 2000 ; .frame.c delete tags img ; exit
}




if { $ici == $arrivee } {
 bind . <Left> { }  ;  bind . <Right> { }
bind . <Up> {} ;  bind . <Down> { }
.frame.c create image  [expr [lindex  [indice_xy $arrivee] 1 ] * 35 + 30] [expr [expr int([lindex [indice_xy $arrivee]  0 ])] * 35 + 30]     -image [image create photo -file [file join $basedir images bien.gif]]  -tags img
set time_fin [clock seconds] ; set duree_exo  [expr $time_fin - $time_debut] 
set duree_exo_mn [expr $duree_exo / 60]  ; set duree_exo_s [expr $duree_exo % 60]
set tab_bilan($nom_laby,$type_laby,$nbre_rec,duree)   [list $duree_exo_mn $duree_exo_s ]
##set tab_bilan($nom_laby,$type_laby,$nbre_rec,type) $type_laby 
set tab_bilan($nom_laby,$type_laby,$nbre_rec,ech_err)  [list $echec $reussite]
##settab_bilan($nom_laby,$type_laby,$nbre_rec,ordres_donnes_tous) ""
if { $flag_essai == 0} { bilan_sans_aff ; catch {destroy $w2 } ; sauver_bilan }
##catch {destroy $w2 } sauver_bilan
##catch {destroy $w2 }  sauver_bilan 
##pr�pare le fichier bilan pour l'instant ils sont tous sauv�s!!
button .bottom.bilan -text "[mc {BILAN}]" -command "set flag_bilan 1 ; bilan {} " -font {helvetica 11 bold }
##place .bottom.classe -x 50 -y 30
place  .bottom.bilan -x 215 -y 5
clignote_bon ; .right.refaire  configure -state normal
catch {.bottom.check_contr configure -state normal }
if { $type_laby == "relatif" && $nbre_rec == 0 && $contr_plus == 0} {
set flag_rel_pas_contr 1

}
if { $type_laby != "cache" } {
.bottom.message configure -text "[format  [mc {C'est fini. Echecs : %1$s R�ussites : %2$s }] $echec $reussite]\
\nen $duree_exo_mn mn  $duree_exo_s sec\n\n\n" -font {helvetica 12}
}
if { $type_laby == "cache" } {
##set recom "pour recommencer Cliquer sur Recommencer"
.bottom.message configure -text   "[format [mc {C'est fini. Murs atteints :  %1$s R�ussites :  %2$s}] $echec $reussite] \n\
en $duree_exo_mn mn  $duree_exo_s sec \n [ mc {recomm} ]\n " -font {helvetica 12}
##set tab_bilan($nom_laby,$type_laby,$nbre_rec,ech_err)  [list $echec $reussite]
bilan {} ; catch {destroy $w2 } ; sauver_bilan 
##set tab_bilan($nom_laby,type) $type_laby 
##bind . <Insert> " set reussite 0 ; set echec 0 ; .frame.c delete $curseur ; tracer $coul_fond ; deplacer cache {} "
}
set reussite 0 ; set echec 0
return
}
 
}

###########################Se d�placer dans le labyrinthe###########################
####################################################################################
proc deplacer { type  dep} {
global depart arrivee chemin  bon_chemin depart largeur hauteur curseur ep_fleche
global tab_laby x_no y_no x_so y_so x_ne y_ne x_se y_se m n cell_N   cell_O  cell_S  cell_E  cell_ST ici curs_n
global fleche_d fleche_g fleche_h fleche_b tcl_platform nbre_essais ant ici1 contr_plus curseur_dep curseur
cas_nseo $depart ; set ici $depart ; set nbre_essais 0
##les curseurs rect fleches � partir du d�part (pb pour fleche ??
##set curs_n [ .frame.c create rectangle [expr $x_no + 4] [expr $y_no + 4 ] [expr $x_se - 4 ] [expr $y_se - 4 ] -width 1 -fill yellow  -tags curseur ]
if { $type != "normal" } {
.right.anticiper configure -state disabled
}
if { $type == "relatif" } {
##tab_bilan($nom_laby,$type_laby,$i,dep)
vide_fleche
if { [pas_bord_o $depart] == 0 } { 
set fleche_d [ .frame.c create line  $x_no  [expr ($y_no + $y_so)/2] \
             $x_ne [expr [expr $y_ne + $y_se ] / 2] -fill yellow -width $ep_fleche -arrow last -tags curseur_d ]
		set  curseur $fleche_d	
		} elseif { [pas_bord_e $depart] == 0 } {
set fleche_g [ .frame.c create line  $x_ne [expr ($y_ne + $y_se)/2] $x_no  [expr ($y_no + $y_so)/2] \
            -fill yellow -width $ep_fleche -arrow last -tags curseur_g ]
		set  curseur $fleche_g	
		} elseif { [pas_bord_s $depart] == 0 } {
set fleche_h [ .frame.c create line  [ expr ($x_so + $x_se)/2 ] $y_so [expr  ($x_ne + $x_no) / 2] $y_no \
		-fill yellow  -width $ep_fleche -arrow last -tags curseur_h ]
		set  curseur $fleche_h	
		} elseif { [pas_bord_n $depart] == 0 } {						
set fleche_b [ .frame.c create line [expr ($x_ne + $x_no)/2]  $y_ne [expr ($x_so + $x_se)/2 ] $y_so  -fill yellow  -width $ep_fleche -arrow last -tags curseur_b ]
	set  curseur $fleche_b	
	} else {
###sinon d�part au milieu tester peut-on
set fleche_d [ .frame.c create line  $x_no  [expr ($y_no + $y_so)/2] \
             $x_ne [expr [expr $y_ne + $y_se ] / 2] -fill yellow -width $ep_fleche -arrow last -tags curseur_d ]
		set  curseur $fleche_d	
	}
	}
if { $type == "normal" || $type == "cache" } {
set curs_n [ .frame.c create rectangle [expr $x_no + 4] [expr $y_no + 4 ] [expr $x_se - 4 ] [expr $y_se - 4 ] -width 1 -fill yellow  -tags curseur ]
set curseur $curs_n

}
set curseur_dep $curseur
############Les �v�nements
## consignes �ecrire dr ga h b ou av re tg td
if {  $ant == 0 }  {
bind . <Right> { set vers droite ;   va  $vers }
##bind . <KP_Right> { set vers droite ;   va  $vers }
bind . <Left> { set vers gauche ; va  $vers  }
##bind . <KP_Left> { set vers gauche ; va  $vers  }
bind .  <Up> {set vers haut ;  va  $vers }
##bind .  <KP_Up> {set vers haut ;  va  $vers }
bind . <Down> {set vers bas ;  va  $vers }
##bind . <KP_Down> {set vers bas ;  va  $vers }
bind    .  <Shift-Insert>     "sauver_ps"
if { $tcl_platform(platform) == "unix" }  {
bind . <KP_Right> { set vers droite ;   va  $vers }
bind . <KP_Left> { set vers gauche ; va  $vers  }
bind . <KP_Down> {set vers bas ;  va  $vers }
bind .  <KP_Up> {set vers haut ;  va  $vers }
}

} elseif { $ant == 1 } {
		set ici1 $depart }
##quand fini memoriser scores et bind pour recommencer
##quand termin�.bottom.message configure -text "$echec et $reussite"
}
########################Programme principal#########################
######################################################################
proc l_choix_arrivee {che} {
set liste {}
foreach el $che {
if { [ pas_bord_e $el ] == 0 || [ pas_bord_o $el ] == 0 \
    || [ pas_bord_n $el ] == 0 || [ pas_bord_s $el ] == 0  } {
set liste [linsert $liste end $el]
}
}
return $liste
}

###Pour recommencer avec le m�me labyrinthe. Surtout pour le cach�########
#####################################################
proc recommencer {dep} {
global type_laby nom_laby curseur deja_sauve tcl_platform coul_fond nbre_rec ant tab_bilan flag_que depart ici1
global  echec reussite reussite_cumul echec_cumul   indice_recup contr_plus time_debut_ecrit rep_partiel_cumul 
global echec_tf reussite_tf liste_erreur_cumul liste_ordres_donnes_tous chemin
 catch {.frame.c  delete tags img}
bind . <Insert> { auto $chemin }
if { $ant == 1} {
set rep_partiel_cumul {} ; set  liste_erreur_cumul { } ; set liste_ordres_donnes_tous {}
set time_debut_ecrit [clock seconds]
set echec_tf 0 ;  set reussite_tf 0
set deb_arr_er 0
.right.anticiper configure -state disabled ; catch {destroy .bottom.bilan}
if {$dep == "deb" } { .anticiper.tester configure -state normal
				.anticiper.tester_deb configure -state normal
			} else { .anticiper.tester_deb configure -state normal
			.anticiper.tester  configure -state normal
			set ici1 $depart
				}
##.right.anticiper configure -state normal 
.anticiper.code delete 0 end
 bind . <Left>  { }  ;  bind . <Right> { }
bind . <Up> {} ;  bind . <Down> { }
##.right.anticiper invoke 
			 }  else {
##set tab_bilan($nom_laby,$type_laby,$nbre_rec,ordres_donnes_tous) ""			 
.right.anticiper configure -state disabled 
catch {destroy .anticiper} ;  catch {destroy .ordres  .bottom.bilan} 
##mettre bind � rien pour ant=1
bind . <Right> { set vers droite ;   va  $vers }
##bind . <KP_Right> { set vers droite ;   va  $vers }
bind . <Left> { set vers gauche ; va  $vers  }
##bind . <KP_Left> { set vers gauche ; va  $vers  }
bind .  <Up> {set vers haut ;  va  $vers }
##bind .  <KP_Up> {set vers haut ;  va  $vers }
bind . <Down> {set vers bas ;  va  $vers }
##bind . <KP_Down> {set vers bas ;  va  $vers }
if { $tcl_platform(platform) == "unix" }  {
bind . <KP_Right> { set vers droite ;   va  $vers }
bind . <KP_Left> { set vers gauche ; va  $vers  }
bind . <KP_Down> {set vers bas ;  va  $vers }
bind .  <KP_Up> {set vers haut ;  va  $vers }
}
}
##effacer l'arriv�e
if { $deja_sauve == 0 } {
.right.sauver configure -state normal
} else {
.right.sauver configure -state disabled
}
.frame.c delete $curseur
.right.refaire  configure -state disabled
incr nbre_rec 
if { $type_laby == "cache" } {
tracer $coul_fond } else { tracer black
}
if { $type_laby == "relatif" && $contr_plus == 1 && $ant != 1} {
catch { .right.modif_av_tg configure -state normal
.right.modif_re_tg configure -state normal
.right.modif_re_td configure -state normal
.right.modif_av_td configure -state normal }
#si de plus que ll'invoker !! ie si antt 1??cf plus loin
}

set reussite 0 ; set echec 0 ;  set flag_que "" ;  incr  indice_recup 
if { $type_laby != "relatif" || $ant != 1 } {
set  tab_bilan($nom_laby,$type_laby,$nbre_rec,c_rel)  ""
}
set  tab_bilan($nom_laby,$type_laby,$nbre_rec,ech_err)  ""
set tab_bilan($nom_laby,$type_laby,$nbre_rec,ordres_donnes_tous) ""
###on recommence avec le m�me statut ant ou pas ant
if { $type_laby == "relatif" && $ant != 1 } {
catch { .bottom.check_contr configure -state disabled}
.bottom.message configure -text  "[mc {Utiliser les fl�ches du clavier pour :Faire AVancer la fl�che  jaune (AV)La faire  Reculer (RE)La faire Tourner vers sa Droite (TD)La faire Tourner vers sa gauche (TG)}]" -font {helvetica 12}
} else {
.bottom.message configure -text [mc {consignes}] -font {helvetica 12} -foreground black
}
##choisir le bon_re_tg s'i ly alieu
###.bottom.check_contr invoke ; ".right.modif$has_que" invoke ; .right.anticiper invoke

if { $type_laby == "relatif" && $contr_plus  == 1 && $ant == 1 } {
set nbre_r [expr $nbre_rec - 1]
set cas_contr  [string range $tab_bilan($nom_laby,$type_laby,$nbre_r,c_rel) 0 4]
##deplacer $type_laby "" 
##.right.modif_re_td invoke
##simuler que
##tk_messageBox  -message $cas_contr  -icon info  -type ok -title "debug"
	switch $cas_contr {
			"re_td" { set tab_bilan($nom_laby,$type_laby,$nbre_rec,c_rel)  "[mc {re_td}]"; deplacer relatif "" 
		.bottom.message configure -text "[mc {Utilise :R ou  r pour  reculer d'une case. D ou d pour tourner d'un quart de tour vers la  droite.}]"  -foreground black
		}
		"re_tg" {set tab_bilan($nom_laby,$type_laby,$nbre_rec,c_rel)  "[mc {re_tg}]" ; deplacer relatif ""
		.bottom.message configure -text "Utilise :R ou  r pour  reculer d'une case.G ou g pour tourner d'un quart de tour vers la  gauche."  -foreground black
		}
		"av_td" { set tab_bilan($nom_laby,$type_laby,$nbre_rec,c_rel)  "[mc {av_td}]" ; deplacer relatif ""
		.bottom.message configure -text "Utilise :A ou  a pour  reculer d'une case.D ou d pour tourner d'un quart de tour vers la  droite."  -foreground black
		}
	"av_tg" { set tab_bilan($nom_laby,$type_laby,$nbre_rec,c_rel)  "[mc {av_tg}]" ; deplacer relatif ""
		.bottom.message configure -text "Utilise :A ou  a pour  reculer d'une case.G ou g pour tourner d'un quart de tour vers la  gauche."  -foreground black
		}
	
				}
 .right.anticiper configure -state disabled 
				##fin switch
##catch .right.modif_re_tg invoke
} else {
deplacer $type_laby "" } 
##incr nbre_rec
} 

proc voir_mur {} {
global  coul_fond
tracer black
update
after 2500
tracer  $coul_fond
update
after 1500
##update
}
proc voir_grille { } {
global type_laby  coul_fond
voir_grille1 red ; tracer black
update 
after 3000
voir_grille1   $coul_fond ; tracer  $coul_fond
update
##.frame.c  itemconfigure rect -width 2 -outline blue
if { $type_laby == "normal"  } {
tracer black ; return
}

}
proc voir_grille1 {coul} {
global m n largeur hauteur trans
for {set col 1} { $col < $m } {incr col } {
.frame.c  create line [expr $trans + (${col}*$largeur) ] $trans  [expr $trans + (${col}*$largeur) ] [expr $trans + (${n}*$hauteur) ] -fill $coul 
}
for {set lig 1} { $lig < $n } {incr lig} {
.frame.c  create line $trans [expr $trans + (${lig}*$hauteur) ]   [expr $trans + (${m}*$largeur) ] [expr $trans + (${lig}*$hauteur) ] -fill $coul 
}
##.frame.c  itemconfigure rect -width 2 -fill red
}

##################Les proc�dures Que !! que av tg ....#####################
###########################################################################
proc que_av_td {} {
global vers tcl_platform flag_que nom_laby type_laby tab_bilan  nbre_rec contr_plus ant
set flag_que "av_td"
if {$contr_plus == 1} { .right.anticiper configure -state normal }
if {$nbre_rec >= 1 && $ant == 0 } {.right.anticiper configure -state disabled}
set tab_bilan($nom_laby,$type_laby,$nbre_rec,c_rel)  "av_td"
.right.modif_av_tg configure -state disabled
.right.modif_re_tg configure -state disabled
.right.modif_re_td configure -state disabled
.right.modif_av_td configure -state disabled
.bottom.message configure -text "[mc {Seuls AV et TD sont possibles}]" 
bind . <Left> { bell ; bell }
##si ant_rel set echec bell  ??
bind . <Down> {}
bind . <Right> { set vers droite ;   va  $vers }
bind . <Up> {set vers haut ;  va  $vers }
if { $tcl_platform(platform) == "unix" }  {
bind . <KP_Right> { set vers droite ;   va  $vers }
bind . <KP_Left> { }
bind . <KP_Down> {}
bind .  <KP_Up> {set vers haut ;  va  $vers }
}
}
#########################
proc que_re_td {} {
global vers tcl_platform flag_que nom_laby type_laby tab_bilan  nbre_rec contr_plus ant
set flag_que "re_td"
set tab_bilan($nom_laby,$type_laby,$nbre_rec,c_rel)  "re_td"
if {$contr_plus == 1} { 
##metrre car_laby � re_td_Fais ou Tout au bon moment.(ds tester!)
##set tab_bilan($nom_laby,$type_laby,$nbre_rec,c_rel)  "re_td"
.right.anticiper configure -state normal 
}
if {$nbre_rec >= 1 && $ant == 0 } {.right.anticiper configure -state disabled}
.right.modif_av_tg configure -state disabled
.right.modif_re_tg configure -state disabled
.right.modif_av_td configure -state disabled
.right.modif_re_td configure -state disabled
.bottom.message configure -text "[mc {Seuls RE et TD sont possibles}]" 
bind . <Left> { }
bind . <Up> {}
bind . <Right> { set vers droite ;   va  $vers }
bind . <Down> {set vers bas ;  va  $vers }
if { $tcl_platform(platform) == "unix" }  {
bind . <KP_Right> { set vers droite ;   va  $vers }
bind . <KP_Left> { }
bind . <KP_Up> {}
bind .  <KP_Down> {set vers bas ;  va  $vers }
}
}
###########################
proc que_av_tg {} {
global vers tcl_platform flag_que nom_laby type_laby tab_bilan  nbre_rec contr_plus ant
set flag_que "av_tg"
if {$contr_plus == 1} { .right.anticiper configure -state normal }
if {$nbre_rec >= 1 && $ant == 0 } {.right.anticiper configure -state disabled}
set tab_bilan($nom_laby,$type_laby,$nbre_rec,c_rel)  "av_tg"
.right.modif_re_td configure -state disabled
.right.modif_re_tg configure -state disabled
.right.modif_av_td configure -state disabled
.right.modif_av_tg configure -state disabled
.bottom.message configure -text "[mc {Seuls AV et TG sont possibles}]" 
bind . <Right> { }
bind . <Down> {}
bind . <Left> { set vers gauche ;   va  $vers }
bind . <Up> {set vers haut ;  va  $vers }
if { $tcl_platform(platform) == "unix" }  {
bind . <KP_Left> { set vers gauche ;   va  $vers }
bind . <KP_Right> { }
bind . <KP_Down> {}
bind .  <KP_Up> {set vers haut ;  va  $vers }
}
}
############################
proc que_re_tg {} {
global vers tcl_platform flag_que nom_laby type_laby tab_bilan  nbre_rec contr_plus ant
set flag_que "re_tg"
set tab_bilan($nom_laby,$type_laby,$nbre_rec,c_rel)  "re_tg"
if {$contr_plus == 1} { .right.anticiper configure -state normal }
if {$nbre_rec >= 1 && $ant == 0 } {.right.anticiper configure -state disabled}
.right.modif_av_tg configure -state disabled
.right.modif_re_td configure -state disabled
.right.modif_av_td configure -state disabled
.right.modif_re_tg configure -state disabled
.bottom.message configure -text "[mc {Seuls RE et TG sont possibles}]" 
bind . <Right> { }
bind . <Up> {}
bind . <Left> { set vers gauche ;   va  $vers }
bind . <Down> {set vers bas ;  va  $vers }
if { $tcl_platform(platform) == "unix" }  {
bind . <KP_Left> { set vers gauche ;   va  $vers }
bind . <KP_Right> { }
bind . <KP_Up> {}
bind .  <KP_Down> {set vers haut ;  va  $vers }
}
}
#########################################################################################

###########################Procedure principale#####################
####################################################################

proc compte {liste el} {
if { $liste == "" } { return 0} 
if { $el == [lindex $liste 0] } { return [expr 1 + [compte [lrange $liste 1 end] $el ] ] } else {
		return  [compte [lrange $liste 1 end] $el ] }
}
proc elem_jumeaux {liste } {
if {  [llength $liste ] <= 1 } {return "" }
if { [lindex $liste 0] == [lindex $liste 1] } { 
	return [ elem_jumeaux [lrange $liste 2 end] ] } else {
	return [concat [lindex $liste 0]  [ elem_jumeaux [lrange $liste 1 end] ] ]
	}
}
proc jumeaux {l1 l2} {
set l11 "" ;  set l21 ""
if { [llength $l1 ] <= 1 } { return [list $l1 $l2] }
for {set i 0} { $i <  [llength $l1 ] } { incr i} {
if { $i == [ expr [llength $l1] -1 ] } { set l11 [linsert $l11 end [lindex $l1 $i]] ;\
							set l21 [linsert $l21 end [lindex $l2 $i]] ; return [list $l11 $l21 ] }
if { [lindex $l1 $i] != [lindex $l1 [expr $i + 1]] } { 

set l11 [linsert $l11 end [lindex $l1 $i] ]
set l21 [linsert  $l21 end [lindex $l2 $i ] ]
} else { incr i}
}
return [list $l11 $l21 ]
}


proc cherche_bloc {che} {
global d�part arrivee m
if { $che == "" || [llength $che ] == 1  } { return ""}
set l_bloc "" ; set l_bloc_i "" ;##pb pour le 1er et dernier si double � c�t�
##tk_messageBox -message "$che inv : [inv {4 5 6 7 8 9} ]" -title "Mauvais codage d'un ordre." -icon info -type ok 
##tk_messageBox -message " $m " -title "m" -icon info -type  ok
for {set  el  0 } {$el <= [expr [ llength $che ] - 1] } { incr el} {
set nbre_el [compte $che [lindex $che $el] ]
set el_e  [lindex $che $el ]
##tk_messageBox -message " $el [lindex $che $el ]" -title "el" -icon info -type  ok
if { $nbre_el > 1} { set double 1 } else  { set double 0}

if { $el == 0 } { set el_pr [ expr [lindex $che 0] + 1 ] ; set el_po [lindex $che 1 ]}\
 elseif { $el ==  [expr [llength $che ] - 1] } {
				 set el_po [expr [lindex $che end ] + 1 ] ; set el_pr [lindex $che [ expr $el - 1] ]} else {
set el_pr [lindex $che [ expr $el - 1] ] 
set el_po [lindex $che [ expr $el + 1] ]
 }
 ##tk_messageBox -message " $el_po $el_pr" -title "pb el_po" -icon info -type  ok
if { [ expr [expr [ expr abs([expr $el_e - $el_pr ])] != 1  && [ expr abs([expr $el_e - $el_pr ])] != $m  ]\
  || [expr [ expr abs([expr $el_e - $el_po ])] != 1  && [ expr abs([expr $el_e - $el_po]) ] != $m ] ] && $double == 1} {
##&& $double

set l_bloc  [linsert $l_bloc end [lindex $che $el ] ] 
set  l_bloc_i  [linsert $l_bloc_i end $el] 
##tk_messageBox -message "blocs : $l_bloc" -icon info -type  ok
##tk_messageBox -message "[lindex $che $el ] est pris ds $che et double : $double " -title "l_blocs au fur et � mesure" -icon info -type  ok
}
}
##�liminer les 12 12



##tk_messageBox -message "$che et  $l_bloc et $l_bloc_i et [jumeaux $l_bloc  $l_bloc_i ]" -title "l_blocs " -icon info -type  ok
return [jumeaux $l_bloc  $l_bloc_i ] 
}



proc trouve_min {che } {
global arrivee depart m
set chem_u [lrange $che 1 end]
##tk_messageBox -message "jumeaux  4 4 [jumeaux "4 4" "2 2"]  ,4 5 [jumeaux "4 5" "2 3"] ,  4 5 5 [jumeaux "4 5 5" "2 1 1" ]" -title "choix ind_d" -icon info -type  ok
##depart et arrivee blocs ? si oui prendre le double
##set ind_a [lsearch $chem_u $arrivee ] ; set ind_d [lsearch $chem_u $depart]
##
set liste_bloc [cherche_bloc $chem_u ]

##tk_messageBox -message "blocs : $liste_bloc" -icon info -type  ok

##parcourir chem_u pour avoir d�part et arrivee non bloc !!
for { set i 0} { $i < [llength $chem_u ] } {incr i } {
##le 1er depart non bloc estle bon
set flag 0
  if { [lindex $chem_u $i ] == $depart } {
 ## tk_messageBox -message " [lindex $chem_u $i ] =? $depart" -title "choix ind_d" -icon info -type  ok
			for { set j 0} { $j < [llength [lindex $liste_bloc 0 ] ] } {incr j } {
			if {  [lindex [lindex  $liste_bloc 0 ] $j ] == $depart && $j != $i } {
##tk_messageBox -message " $i et $j " -title "pb ind_d suite" -icon info -type ok
			##c'est pas un  un bloc on prend
			set ind_d $i ; set flag 1 ; break
	
			}
	 if { $flag == 1} { break }		
			}
  if { $flag== 0} {set ind_d $i ; break } ;##c'est le double non bloc
  }
	
  if { $flag == 1} { break }
}

for { set i 0} { $i < [llength $chem_u ] }  {incr i } {
##le 1er arrivee non bloc estle bon
set flag 0
  if { [lindex $chem_u $i ] == $arrivee } {
 			for { set j 0} { $j < [llength [lindex $liste_bloc 0 ] ] } {incr j } {
			if {  [lindex [lindex  $liste_bloc 0] $j ] == $arrivee && $j != $i } {
	##c'est pas un  un bloc on prend
			set ind_a $i ; set flag 1 ; break
	
			}
		 if { $flag == 1} { break}	
			}
  if { $flag== 0} {set ind_a $i ; break }
  }
	if { $flag == 1} { break }
 
}
##tk_messageBox -message " $ind_d $ind_a " -title "_d_a" -icon info -type  ok
##	set ind_a [lsearch $chem_u $arrivee ] ; set ind_d [lsearch $chem_u $depart]		
##	  tk_messageBox -message " [lrange $chem_u $ind_d $ind_a ]" -title "pb _d_a" -icon info -type  ok		
	if { $ind_d <= $ind_a } { set chem_d_a [lrange $chem_u $ind_d $ind_a ] } elseif\
	{$ind_d > $ind_a } { set chem_d_a [lrange $chem_u $ind_a $ind_d ] } 
##tk_messageBox -message " $chem_d_a" -title "chemin a a" -icon info -type  ok

set liste_b {}
## cf plus hautdepart estil bloc ? si oui prendre l'autre de m�me pour arivee � ne faire que pour eux
##changer s'il y alieu d'indice de depart
##tk_messageBox -message "$chem_u  et $chem_d_a  et $ind_d $ind_a et $depart  et $arrivee et $liste_bloc  " -title "Mauvais codage d'un ordre.!!!!" -icon info -type  ok
set liste_bloc_d_a [cherche_bloc $chem_d_a ]
##tk_messageBox -message " listes blocs_d_a : $liste_bloc_d_a  " -title " pb liste bloc de d a" -icon info -type  ok
##bonne liste d'ordres ? tenir compte des blocs sauter s'il  y a lieu
for { set i  0 } { $i <= [expr [llength $chem_d_a ] -1]} { incr i } {
###set  moi [lsearch $chem_d_a [lindex $chem_d_a $i]] ; ##= i ??
##moi en double ?? attention si liste vide  !!!!
 if { [lsearch [ lindex $liste_bloc 0] [lindex $chem_d_a $i ] ] == -1 } {
## pas bloc ds chem_u donc pa ds  d_a : est unique on le prend
##tk_messageBox -message "[lindex $chem_d_a $i] pris pas bloc ds [ lindex $liste_bloc_d_a 0]" -title "les pris suite1 " -icon info -type  ok
set liste_b [linsert $liste_b end [lindex $chem_d_a $i ] ]
} else {
## est il double ds d_a
## est il le bloc ou est ce son double? le double peut ne pas exister dans chem_d_a d'o� pb de recul
##pour sauter bloc si existe qui est juste avant le double
##indices differents pas bloc on  le prend si unique, itou et on saute si son double existe ds chem_d_a
		if  { [compte   $chem_d_a [lindex $chem_d_a $i] ]  == 2 }  {
## double donc on aute du double_libre au bloc_mur		
		set liste_b [linsert $liste_b end [lindex $chem_d_a $i ] ]
		set  chem_d_a_s $chem_d_a
		set liste1 [lrange  $chem_d_a [expr $i + 1] [expr [llength $chem_d_a] -1]]
		set x [lindex $chem_d_a $i ]
##		tk_messageBox -message "$liste1 contient ??? [lindex $chem_d_a $i ]" -title "liste suite2" -icon info -type  ok
##		set liste_p [ lreplace $liste1 $x $x  "" ]
##			tk_messageBox -message "$liste1" -title "liste suite2_pr�s" -icon info -type  ok
		set ind_p [lsearch $liste1 [lindex $chem_d_a $i ] ]
		set autre [ expr $ind_p  ] ; ##because lreplace
		set i  [ expr $autre + 1  + $i ] ; ## � v�rifier autre +1 fait par incri
##		tk_messageBox -message "le double [lindex $chem_d_a $i] y est on �limine  $liste_b et $autre" -title "suite3" -icon info -type  ok

		}	else {
		set el_e [lindex $chem_d_a $i ]
		if { $i ==  0 } { set el_pr [ expr [lindex $chem_d_a 0] + 1 ] ; set el_po [lindex $chem_d_a [ expr $i + 1] ]}\
		elseif { $i ==  [expr [llength $chem_d_a ] - 1] } {
				 set el_po [expr [lindex $chem_d_a end ] + 1 ] ; set el_pr [lindex $chem_d_a [ expr $i - 1] ]} else {
		
		 set el_pr [lindex $chem_d_a [expr $i -1 ] ]
		set el_po [lindex $chem_d_a [expr $i + 1 ] ]
		}
		if {[ expr [expr [ expr abs([expr $el_e - $el_pr ])] != 1  && [ expr abs([expr $el_e - $el_pr ])] != $m  ]\
  || [expr [ expr abs([expr $el_e - $el_po ])] != 1  && [ expr abs([expr $el_e - $el_po]) ] != $m ] ]  } {
		##bloc_mur sans double on recule
		##c'est un bloc_mur faut reculer hors chem_d_a	en vidant liste_b et reculer dans chem_u
		set kkk [lsearch $chem_u [lindex $chem_d_a $i ]]  ; ##le 1 er donc le double non bloc mais pb des blocs entre!!!
		if { $ind_d <= $ind_a } {set dep_p $ind_d } else {set dep_p $ind_a}
		set liste_b [inv [lrange $chem_u $kkk $dep_p ]] 
##		tk_messageBox -message " bloc : [lindex $chem_d_a $i ] avec double � l'ext�rieur : $kkk (i) reculer : $liste_b"
		
		
##pb liste_bloc vide ce quie est le cas qd un seul des deux est ds bloc chem mais pas ds bloc chemi_d_a
		

		} else {
##		tk_messageBox -message "i : $i =?  [lindex  [lindex $liste_bloc_d_a 1]  [lsearch $chem_d_a [lindex $chem_d_a $i ] ] ]" -title "liste suite2_pr�s_pr�s" -icon info -type  ok
		## il y a en a un seul bloc_mur ou bloc_non_mur ?	$i != bloc_non_mur on prend	
##tk_messageBox -message "[lindex $chem_d_a $i] double non bloc et $i " -title "liste suite2_pr�s" -icon info -type  ok
		set liste_b [linsert $liste_b end [lindex $chem_d_a $i ] ]
##	tk_messageBox -message "pb [lrange  $chem_d_a [expr $i + 1] [expr [llength $chem_d_a] -1]] et [lindex $chem_d_a $i ]" -title "liste suite2" -icon info -type  ok

		
		}
		
		
		}
##pb +1 ou non

}

##tk_messageBox -message "$liste_b  pour $ind_d  $ind_a" -title "bonne liste avant inversion" -icon info -type  ok

}
if { $ind_d <= $ind_a } {
##tk_messageBox -message "[sans_double_n_bloc {63 62 61 60 50 58 57 50 41 40 48 40 32 24 16} ]" -title "bonne liste sans double" -icon info -type  ok
return [sans_double_n_bloc $liste_b ] } else {
##tk_messageBox -message "[sans_double_n_bloc {63 62 61 60 59 56 57 49 41 40 48 40 32 56 16} ]" -title "bonne liste sans double" -icon info -type  ok
				return [sans_double_n_bloc [inv $liste_b]]
}

} 
proc sans_double_n_bloc {list } {
 ##4  5 56 12 5 ...
 set list_bis $list
 set double [ double $list ]
 ##tk_messageBox -message "il ya $double comme double dans $list" -title "les  doubles" -icon info -type  ok
  if { $double == "" } { return $list}
for { set i 0 } { $i < [llength $double] } { incr i } {
		for { set j 0 } { $j < [llength $list_bis ] } { incr j } {
		if { [lindex $list_bis $j ] == [lindex $double $i ] && [lsearch [double $list_bis ] [lindex $double $i ]] != -1 } {
		set list_bis1 [lrange $list_bis [expr $j + 1 ] [expr [llength $list_bis] - 1] ]
		set ind_d [lsearch $list_bis1 [lindex $double $i ] ]
		set ind_f [expr $ind_d +$j + 1]
		if {[expr $ind_d +$j + 1] >= [expr [llength $list_bis ] - 1] } { set ind_f [expr [llength $list_bis] -1] }
		set list_bis [lreplace  $list_bis [expr $j + 1 ] $ind_f ]
		##pb indice trop grand
		break
		}
		
		}

}


return $list_bis
}
 
 
proc double {l} {
set l1 "" ; set l2 ""
foreach el $l {
if { [compte $l $el ] == 2 && [lsearch $l1 $el ] == - 1} {set l1 [linsert $l1 end $el] } 
}
return $l1
}


proc inv { l } {
if { $l == ""} {return ""} else {
	return [concat [ inv [lrange  $l 1 end] ] [lindex $l 0]]
}
}

proc auto {chemin } {
global type_laby coul_fond deja_sauve nbre_rec ant
##pour chemin faudrait traiter les retours
if { $deja_sauve != 0 } {
if {$type_laby == "cache" } { tracer $coul_fond } else {tracer black}
}
##.anticiper.code insert end $chemin

fais_auto $chemin
if { $ant == 1 && $type_laby == "normal"} {.anticiper.code delete 0 end ; .anticiper.code configure -background white -font {helvetica 14 bold } }
##deplacer ""
}
proc fais_auto {chem } {
global type_laby depart curseur
if { [llength $chem ] == 1  } { catch {after 1000 ; .frame.c delete tags curseur curseur_h curseur_b  curseur_d curseur_g } ; deplacer $type_laby "" ;  return}
##.frame.c create rectangle [expr $x_no + 4] [expr $y_no + 4 ] [expr $x_se - 4 ] [expr $y_se - 4 ] -width 1 -fill yellow  -tags curseur
##si le premier est plusloinle marquer fais_autodu prem ason �gal remonter[fais-auto linverse) et repartir du deuxi�me
traite_auto [lindex $chem 0] [lindex $chem 1]
after 500
update
fais_auto [lrange $chem 1 end]

}
proc traite_auto {prem deux } {
global curseur m n largeur hauteur ant type_laby
if { $ant == 1 && $type_laby == "normal"} { .anticiper.code configure  -background black -font {helvetica 16 bold } }

 if { $prem == [expr $deux - 1 ] } { .frame.c move $curseur  $largeur 0 
									if { $ant == 1 && $type_laby == "normal"} {
										.anticiper.code insert end " D" 
									}
 
 
 }
 if { $prem == [expr $deux + 1 ] } { .frame.c move $curseur  -$largeur 0 
						if { $ant == 1 && $type_laby == "normal"} {
										.anticiper.code insert end " G" 
										}
 
 }
if { $prem == [expr $deux - $m ] } { .frame.c move $curseur  0 $hauteur  
						if { $ant == 1 && $type_laby == "normal"} {
										.anticiper.code insert end " B" 
										}


 }
 if { $prem == [expr $deux + $m ] } { .frame.c move $curseur  0 -$hauteur 
						if { $ant == 1 && $type_laby == "normal" } {
										.anticiper.code insert end " H" 
										}
 
 
 }
}







proc labyrinthe { } { 
global depart chemin  bon_chemin depart reussite echec type_laby nom_laby arrivee m n deja_sauve coul_fond 
global nbre_rec ant tab_bilan flag_que echec_cumul  reussite_cumul echec_cumul1  reussite_cumul1 contr_plus flag_essai
init_general
init_traces
 .frame.c itemconfigure rect -outiline 2 -outline green
if { $deja_sauve ==  0 } { ;###r�cup�rer  le chemin car non sauv�
set depart 0

set chemin [cheminer $depart {} 0 [list $depart]]
set bon_chemin [ tout_droit [lrange $chemin 1 end] ]
##set deja_sauve 0
set l_choix_arrivee [l_choix_arrivee  $bon_chemin ]
set depart [lindex $l_choix_arrivee [ expr int([llength $l_choix_arrivee]*rand())]]
##pour tester auto
##set depart 0
##set ind_depart [lsearch  $l_choix_arrivee $depart]
##set l_choix [lreplace $l_choix_arrivee $ind_depart $ind_depart ]
##set arrivee [lindex $l_choix [ expr int([llength $l_choix]*rand())]]
##set chemin_min [trouve_min $chemin]
##auto $chemin_min 
##bind . <Insert> { auto $chemin_min  }
}
##bind . <Insert> {auto $bon_chemin} pour le moment bon_chemin ??
##bind . <Insert> { set depart 0 ; auto $bon_chemin  } 
if { $deja_sauve == 1 } {
##set chemin [cheminer $depart {} 0 [list $depart]]
##set bon_chemin [ tout_droit [lrange $chemin 1 end] ]
bind . <Insert> { auto $chemin_min  }
.right.sauver configure -state disabled
}
##prendre la fin ds bon_chemin
##.bottom.message configure -text "$l $depart $ind_depart\n $l_choix  $arrivee"
##sauver
##tracer en bleu pour le cach�
##set type_laby "relatif"
set nbre_rec 0 ; ##pour tracer en mode cr�er.	
set reussite 0 ; set echec 0 ;  set ant 0 ; set flag_que "" ; set flag_essai 0
set echec_cumul  0 ; set reussite_cumul  0 ; set echec_cumul1  0 ; set reussite_cumul1  0

	switch $type_laby {
		"cache" {
		 button .right.modif -text [mc {Murs?}] -font {Helvetica 12 bold } -command "voir_mur"
 		pack .right.modif -side left -padx 1
		 button .right.grille -text [mc {Grille?}] -font {Helvetica 12 bold } -command "voir_grille"
 		pack .right.grille -side left -padx 1
		tracer $coul_fond
		}
		"relatif" { tracer black
		if { $contr_plus == 1 } {
		button .right.modif_av_td -text [mc {AV TD}] -font {Helvetica 10 bold } -command "que_av_td"
 		pack .right.modif_av_td -side left -padx 1
		button .right.modif_av_tg -text [mc {AV TG}] -font {Helvetica 10 bold } -command "que_av_tg"
 		pack .right.modif_av_tg -side left -padx 1
		button .right.modif_re_td -text [mc {RE TD}] -font {Helvetica 10 bold } -command "que_re_td"
 		pack .right.modif_re_td -side left -padx 1
		button .right.modif_re_tg -text [mc {RE TG}] -font {Helvetica 10 bold } -command "que_re_tg"
 		pack .right.modif_re_tg -side left -padx 1
		}
		}
		"normal" {
		tracer black }
	}

 set  tab_bilan($nom_laby,$type_laby,$nbre_rec,c_rel)  ""
 set  tab_bilan($nom_laby,$type_laby,$nbre_rec,ech_err)  ""
 set  tab_bilan($nom_laby,$type_laby,$nbre_rec,ordres_bad)  ""
 set tab_bilan($nom_laby,$type_laby,$nbre_rec,ordres_donnes_tous) ""
 set tab_bilan($nom_laby,$type_laby,$nbre_rec,dep)  ""
.right.refaire  configure -state disabled
##.bottom.message configure -text " $type_laby"
if { $deja_sauve == 1  } {deplacer $type_laby ""}
##deplacer $type_laby ""
##bilan
}
labyrinthe

#############Pour anticiper les d�placements###################
####################################################
proc anticiper { }  {
global m n tab_laby depart arrivee ici1 fl_h fl_d fl_b fl_g fl_a fl_r fl_tg fl_td basedir ant rec tab_bilan nom_laby
global  rep_partiel_cumul reussite_cumul echec_cumul time_debut_ecrit nbre_rec duree_Fais duree_Tout
global echec_tf reussite_tf liste_erreur liste_erreur_cumul liste_ordres_donnes_tous chemin  bon_chemin
global type_laby contr_plus code_h code_b
.bottom.message configure -text "[mc {Utilise les fl�ches, les lettres ou le clavier puis Fais  pour pas � pas ou Tout pour depuis le d�part}]"
cd $basedir
set fl_g  [image create photo fleche_g	-file  [file join  images "fl_g.gif"] ]
set fl_d [ image create photo fleche_d	-file [file join  images "fl_d.gif"] ]
set fl_h [ image create photo fleche_h	-file [file join  images "fl_h.gif"] ]
set fl_b [ image create photo fleche_b	-file [file join  images "fl_b.gif"]]

set fl_a  [image create photo fleche_a	-file  [file join  images "fl_a.gif"] ]
set fl_r [ image create photo fleche_r	-file [file join  images "fl_r.gif"] ]
set fl_tg [ image create photo fleche_tg	-file [file join  images "fl_tg.gif"] ]
set fl_td [ image create photo fleche_td	-file [file join  images "fl_td.gif"]]
set poub [ image create photo poub	-file [file join  images "poubelle.gif"]]
set ici1 $depart ; set ant 1 ; set rep_partiel_cumul {} ; set liste_erreur { }
##si on choisit anticiper on y reste
catch {destroy .ordres ; destroy .anticiper ;  .bottom.bilan}
.right.anticiper configure -state disabled
frame .anticiper -height 40
frame .ordres -bg blue
pack .anticiper -before  .right     -fill both -expand yes -side top 
entry .anticiper.code -width 40   -font {Helvetica 13 bold } -foreground red -xscrollcommand  ".anticiper.scroll1 set"
scrollbar .anticiper.scroll1 -command ".anticiper.code xview" -orient horizontal

place  .anticiper.code -x 0 -y 0
place .anticiper.scroll1  -x 0 -y 25 -width 380
pack .ordres -before  .anticiper     -fill both -side top
## plus valable avec clics.anticiper.code insert end "$bon_chemin et  $chemin"
##frame .fleches -bg blue
##pack .fleches -before .ordres
if { $type_laby == "relatif" &&  $contr_plus == 1 } {
##message � modifier Que av td ou ....ie conserver le message de que
##ici anticipe implcite
label .bottom.message_contr -text "[mc {Containtes :}]" -font {helvetica 10} -foreground red
pack  .bottom.message_contr -side right -padx 10 -pady 20
if {$tab_bilan($nom_laby,$type_laby,$nbre_rec,c_rel) == "re_tg" } {
 .bottom.message configure -text "[mc {Utilise :R ou  r pour  reculer d'une case.G ou g pour tourner d'un quart de tour vers la  gauche.}]"  -foreground black
 .bottom.message_contr configure -text "[mc {Contraintes :R ou r ou G ou g}]" -font {helvetica 12 bold} -foreground black -justify center
 }
 if {$tab_bilan($nom_laby,$type_laby,$nbre_rec,c_rel) == "re_td" } {
.bottom.message configure -text "[mc {Utilise :R ou  r pour  reculer d'une case. D ou d pour tourner d'un quart de tour vers la  droite.}]"  -foreground black
 .bottom.message_contr configure -text "[mc {Contraintes :R ou r ou D ou d}] " -font {helvetica 12 bold} -foreground black -justify center
 }
 if {$tab_bilan($nom_laby,$type_laby,$nbre_rec,c_rel) == "av_td" } {
 .bottom.message configure -text "[mc {Utilise :A ou  a pour  reculer d'une case.D ou d pour tourner d'un quart de tour vers la  droite.}]"  -foreground black
  .bottom.message_contr configure -text "[mc {Contraintes :A ou a ou D ou d}] " -font {helvetica 12 bold} -foreground black -justify center
 }
 
 if {$tab_bilan($nom_laby,$type_laby,$nbre_rec,c_rel) == "av_tg" } {
.bottom.message configure -text "[mc {Utilise :A ou  a pour  reculer d'une case.G ou g pour tourner d'un quart de tour vers la  gauche.}]"  -foreground black
  .bottom.message_contr configure -text "[mc {Contraintes :A ou a ou G ou g}] " -font {helvetica 12 bold} -foreground black -justify cente
 }
			set code_h A ; set code_b R }  else {
			set code_h H ; set  code_b B }
button .ordres.ah  -text [mc $code_h] -font {Helvetica 16 bold } -foreground red -command "coder $code_h "			
button .ordres.ab  -text [mc $code_b] -font {Helvetica 16 bold } -foreground red  -command "coder $code_b "
button  .ordres.ad  -text [mc {D}] -font {Helvetica 16 bold } -foreground red  -command "coder D "
 button .ordres.ag  -text [mc {G}] -font {Helvetica 16  bold } -foreground red  -command "coder G"
 if { $type_laby == "relatif" } {
button .ordres.fl_ahr   -image $fl_a -command "coder $code_h "
button .ordres.fl_abr   -image $fl_r  -command "coder $code_b "
button .ordres.fl_ad   -image $fl_td  -command "coder D "
button .ordres.fl_ag   -image $fl_tg  -command "coder G "
} else {		
button .ordres.fl_ah   -image $fl_h  -command "coder $code_h "
button .ordres.fl_ab   -image $fl_b  -command "coder $code_b "
button .ordres.fl_ad   -image $fl_d  -command "coder D "
button .ordres.fl_ag   -image $fl_g  -command "coder G "
} 
 checkbutton .ordres.fleche -variable ordre_fl  -text "[mc {Fl�ches}]" -command  " ordres fleche " -onvalue 1 -offvalue 0 
   checkbutton .ordres.lettre -variable ordre_le  -text  "[mc {Lettres}]"  -command  " ordres lettre  "  -onvalue 1 -offvalue 0
 .ordres.fleche invoke ; .ordres.lettre invoke
 button .anticiper.vider  -image $poub  -command "vider "
 ##-text [mc{Vide}]
 button .anticiper.tester -text [mc {Fais}] -font {Helvetica 14 bold } -command " tester der " -foreground red
  button .anticiper.tester_deb -text [mc {Tout}] -font {Helvetica 14 bold } -command " tester deb " -foreground red
 pack    .anticiper.vider .anticiper.tester .anticiper.tester_deb  -side right
 ##pack  .ordres.ah      .ordres.ab   -side left
##pack   .ordres.ad   .ordres.ag     -side  left
##pack .ordres.fl_ah     .ordres.fl_ab  .ordres.fl_ad     .ordres.fl_ag     -side  left -padx 3
pack .ordres.fleche .ordres.lettre -side right
focus -force .anticiper.code
##ou bind tester deb
 bind .anticiper.code <Return>  ""
if { $nbre_rec == 0} { 
 set time_debut_ecrit [clock seconds] 
 }
set  duree_Fais 0 ; set duree_Tout 0
set echec_tf 0 ; set reussite_tf 0 ; set liste_erreur_cumul {}
 set liste_ordres_donnes_tous {}
 ##anihiler les fleches du clavier
 bind . <Left> { }  ;  bind . <Right> { }
bind . <Up> {} ;  bind . <Down> { }
  }
 ## .right.anticiper invoke
 
proc ordres { forme } {
##pb si relatif et ant=1
global code_h code_b type_laby ant
global ordre_fl ordre_le fl_h fl_b   fl_g   fl_d ordre_fl fl_a fl_r fl_td fl_tg
switch $forme {
	"fleche"  {
	if { $ordre_fl == 1 } {
		if { $type_laby == "relatif" && $ant == 1 } {
		pack  .ordres.fl_ahr     .ordres.fl_abr  .ordres.fl_ad     .ordres.fl_ag     -side  left -padx 3
##		button .ordres.fl_ahr   -image $fl_a  -command "coder $code_h" ;   button .ordres.fl_abr   -image $fl_r  -command  "coder $code_b "
##		button .ordres.fl_ad   -image $fl_td  -command "coder D " ;  button .ordres.fl_ag   -image $fl_tg  -command "coder G "	
		} else {
		pack  .ordres.fl_ah     .ordres.fl_ab  .ordres.fl_ad     .ordres.fl_ag     -side  left -padx 3
##		 button .ordres.fl_ah   -image $fl_h  -command "coder H" ;   button .ordres.fl_ab   -image $fl_b  -command "coder B "
##		button .ordres.fl_ad   -image $fl_d  -command "coder D " ;  button .ordres.fl_ag   -image $fl_g  -command "coder G "
		}	 	
	  	  } else {
	  catch { destroy  .ordres.fl_ah  .ordres.fl_ahr  .ordres.fl_ab .ordres.fl_abr  .ordres.fl_ad     .ordres.fl_ag }
	  if { $type_laby == "relatif" && $ant == 1 } { 
	 button .ordres.fl_ahr   -image $fl_a  -command "coder $code_h" ;   button .ordres.fl_abr   -image $fl_r  -command  "coder $code_b "
	button .ordres.fl_ad   -image $fl_td  -command "coder D " ;  button .ordres.fl_ag   -image $fl_tg  -command "coder G "
	  } else {
	  button .ordres.fl_ah   -image $fl_h  -command "coder H" ;   button .ordres.fl_ab   -image $fl_b  -command "coder B "
	button .ordres.fl_ad   -image $fl_d  -command "coder D " ;  button .ordres.fl_ag   -image $fl_g  -command "coder G "
	  }
	  
	  }
	  
	  
		}
		"lettre"  {
	if { $ordre_le == 1 } {
	pack   .ordres.ah      .ordres.ab  .ordres.ad   .ordres.ag     -side  left
	} else { destroy    .ordres.ah      .ordres.ab  .ordres.ad   .ordres.ag   

	if { $type_laby == "relatif" && $ant == 1} {
	button .ordres.ah   -text $code_h  -font {Helvetica 16 bold } -foreground red -command "coder $code_h " ;   button .ordres.ab   -text $code_b -foreground red  -font {Helvetica 16 bold } -command "coder $code_b "
	} else {
	button .ordres.ah  -text [mc {H}] -font {Helvetica 16 bold } -foreground red -command "coder H " ; button .ordres.ab  -text [mc {B}] -font {Helvetica 16 bold } -foreground red -command "coder B "  
	}
	button  .ordres.ad  -text [mc {D}] -font {Helvetica 16 bold } -foreground red -command "coder D "  ;  button .ordres.ag  -text [mc {G}] -font {Helvetica 16  bold } -foreground red -command "coder G"
	}
	}
	}
}
 ##pour le bouton laby anticiper
 if { $bouton_ant ==1 } {.right.anticiper invoke}
 if { $bouton_ant == 1 && $type_laby == "relatif"  } {
 ##choisir au hasard _av_tg _av_td re_.....
 set liste_que { _av_tg  _av_td  _re_tg  _re_td }
 set has_que [lindex $liste_que [expr int(rand()*[llength $liste_que]) ] ]
 .bottom.check_contr invoke ; ".right.modif$has_que" invoke ; .right.anticiper invoke}
## .anticiper.code insert end $has_que 

 proc coder  {vers} {
.anticiper.code insert end "$vers "
}
proc vider {}   {
.anticiper.code delete 0 end
}
proc explode_mot {mot} {
if { $mot == "" } {
			return $mot } else {
			return [concat [string index $mot 0] [explode_mot [string range $mot 1 end]] ]
			}
}

proc explode {liste} {
if { $liste == "" } { return {} } else {
return [concat [explode_mot [lindex $liste 0]] [explode [lrange $liste 1 end ] ] ]

}
}

proc test_que { ordre } {
global tab_bilan type_laby nom_laby nbre_rec contr_plus

 if {[string first "re_tg" $tab_bilan($nom_laby,$type_laby,$nbre_rec,c_rel) ] != -1  && $contr_plus == 1 }  {
if { $ordre == "R" || $ordre == "G" || $ordre == "r" || $ordre == "g" } {
return 1 } else {return 0}
}
if {[string first "re_td" $tab_bilan($nom_laby,$type_laby,$nbre_rec,c_rel) ] != -1 && $contr_plus == 1 } {
if { $ordre == "R" || $ordre == "D" || $ordre == "r" || $ordre == "d" } {
return 1 } else {return 0}
}
if {[string first "av_td" $tab_bilan($nom_laby,$type_laby,$nbre_rec,c_rel)] != -1  && $contr_plus == 1 } {
if { $ordre == "A" || $ordre == "D" || $ordre == "a" || $ordre == "d" } {
return 1 } else {return 0}
}
if {[string first "av_tg" $tab_bilan($nom_laby,$type_laby,$nbre_rec,c_rel)] != -1  && $contr_plus == 1 } {
if { $ordre == "A" || $ordre == "a" || $ordre == "G" || $ordre == "g" } {
return 1 } else {return 0}
}
return 0
}
 
 
 
 


################################################################################################
#############proc tester qui efectue les ordres et termine######################################
##anticiper : Fais et Tout ind�pendants. Stop � la premi�re erreur
proc tester { depuis }  {
global m n tab_laby depart arrivee  curseur largeur hauteur type_laby  x_no y_no x_so y_so x_ne y_ne x_se y_se ici1 w2
global rep_partiel_cumul reussite_cumul echec_cumul tab_bilan time_debut type_laby nom_laby  
global nbre_rec rep_tout reussite_cumul1 echec_cumul1  flag_bilan time_debut_ecrit break_o fais_tout
global duree_Fais duree_Tout echec_tf reussite_tf liste_erreur liste_erreur_cumul curseur_dep curseur chemin
global liste_ordres_donnes_tous contr_plus fleche_d  fleche_g  fleche_h fleche_b ep_fleche
##m�mo du curseur d�part dans curseur_dep
catch {.frame.c delete tags img }
bind . <Insert> { }
if {[.anticiper.code get ] == ""} { return}
set rep_p  [.anticiper.code get]
.anticiper.code delete 0 end 
if { $depuis == "deb" } {set nom_depuis "Tout"} else {set nom_depuis "Fais"}
set time_fin [clock seconds] 
###duree  de l'ecrit entre les frappes sur anticiper et depuistime_debut_ecrit
set duree_ecrit [expr [clock seconds] - $time_debut_ecrit ]
##set "duree_${depuis}" [clock seconds]
##set aux
if {$depuis == "deb" } {
set duree_Tout [expr $duree_Tout + $duree_ecrit]
}
if {$depuis == "der" } {
set duree_Fais [expr $duree_Fais + $duree_ecrit]
}
catch {destroy .bottom.bilan}
##message pour no relatif contraintes

if { $type_laby == "relatif" &&  $contr_plus == 1 } { 
##message � modifier Que av td ou ....ie conserver le message de que
##ici anticipe implcite
if {$tab_bilan($nom_laby,$type_laby,$nbre_rec,c_rel) == "re_tg" } {
 .bottom.message configure -text "[mc {Utilise :R ou  r pour  reculer d'une case.G ou g pour tourner d'un quart de tour vers la  gauche.}]"  -foreground black
 ## 
 }
 if {$tab_bilan($nom_laby,$type_laby,$nbre_rec,c_rel) == "re_td" } {
.bottom.message configure -text "[mc {Utilise :R ou  r pour  reculer d'une case. D ou d pour tourner d'un quart de tour vers la  droite.}]"  -foreground black
 }
 if {$tab_bilan($nom_laby,$type_laby,$nbre_rec,c_rel) == "av_td" } {
 .bottom.message configure -text "[mc {Utilise :A ou  a pour  reculer d'une case.D ou d pour tourner d'un quart de tour vers la  droite.}]"  -foreground black
 }
 
 if {$tab_bilan($nom_laby,$type_laby,$nbre_rec,c_rel) == "av_tg" } {
.bottom.message configure -text "[mc {Utilise :A ou  a pour  reculer d'une case.G ou g pour tourner d'un quart de tour vers la  gauche.}]"  -foreground black
 } 
 } else {
		.bottom.message configure -text "[mc {Utilise les fl�ches, les lettres ou le clavier puis choisis Fais  pour pas � pas ou Tout pour depuis le d�part}]" \
		-foreground black}
 ##
 if { $type_laby == "normal"  } {
  .frame.c delete $curseur 
}  
##set reussite 0 ; set echec 0 
 set break_o 0 ; set break_l 0
 set liste_erreur { }
if {$nbre_rec == 0 } { set time_debut [clock seconds] }
if {$nbre_rec != 0 && $fais_tout == $depuis } {
set time_debut [clock seconds]
##set time_debut_ecrit
}
if {$nbre_rec != 0 && $fais_tout != $depuis } {
### on change de depuis  par recommencer
set reussite_cumul 0 ; set echec_cumul 0 ; set  rep_partiel_cumul {}
set echec_tf 0  ; set reussite_tf 0 ; set liste_erreur { }
set time_debut [clock seconds] 
if { $depuis == "deb" } { set duree_Fais 0 ; set action Tout} else { set duree_Tout 0 ; set action Fais }
 } 
if { $depuis == "deb" } {set action Tout} else { set action Fais }
 ##pour que + anticiper
 
 set fais_tout $depuis
set tab_bilan($nom_laby,$type_laby,$nbre_rec,dep) $depuis
##correspond au rel contr  suivant
##set tab_bilan($nom_laby,$type_laby,$nbre_rec,c_rel)  "re_td_${action} ou av_td ..."
 if {$tab_bilan($nom_laby,$type_laby,$nbre_rec,c_rel) == "re_tg" && $contr_plus == 1} {
 set tab_bilan($nom_laby,$type_laby,$nbre_rec,c_rel)  "re_tg_${action}"
 ## 
 }
 if {$tab_bilan($nom_laby,$type_laby,$nbre_rec,c_rel) == "re_td" && $contr_plus == 1} {
 set tab_bilan($nom_laby,$type_laby,$nbre_rec,c_rel)  "re_td_${action}"
 }
 if {$tab_bilan($nom_laby,$type_laby,$nbre_rec,c_rel) == "av_td" && $contr_plus == 1} {
 set tab_bilan($nom_laby,$type_laby,$nbre_rec,c_rel)  "av_td_${action}"
 }
 
 if {$tab_bilan($nom_laby,$type_laby,$nbre_rec,c_rel) == "av_tg" && $contr_plus == 1} {
 set tab_bilan($nom_laby,$type_laby,$nbre_rec,c_rel)  "av_tg_${action}"
 }
 
 .right.refaire configure -command "recommencer $depuis"
###exclure l'un l'autre Tout et Fais annuler!!!
if { $depuis == "deb" } {
 set liste_ind_echec {}  ; set liste_ordre_ef {} 
 set ici1 $depart 
 ## set curseur $curseur_dep ; cas_nseo $depart

##refaire d�part � chaque fin non arriv�e 
##.anticiper.code insert end "$arrivee $depart"

if { $type_laby == "relatif" && $contr_plus  == 1 } {
##tab_bilan($nom_laby,$type_laby,$i,dep)
##set ici1 $depart 
} else {
 set ici1 $depart
 cas_nseo $depart
##vide_fleche
set curs_n [ .frame.c create rectangle [expr $x_no + 4] [expr $y_no + 4 ] [expr $x_se - 4 ] [expr $y_se - 4 ] -width 1 -fill yellow  -tags curseur ]
set curseur $curs_n
	}		
bind .anticiper.code <Return> "tester deb"
bind .anticiper.code <KP_Enter> "tester deb"
##curseur au d�part. L'y mettre aussi d�s break sur deb ou pas arriv� sur deb
##set ici1 $depart 
## set reussite 0  ; set echec 0  ;##ou cumuler les err reussites ?
.anticiper.tester configure -state disabled
 ##clignote
 ##after 700 .frame.c delete $curs_n
 }
 if { $depuis == "der" } {
 ##le curseur reste l� o� il est mais le refaire la premi�re fois selon fl�che ou pas
 
 set liste_ordre_ef {} ; set liste_ind_echec {}
 bind .anticiper.code <Return> "tester der"
 bind .anticiper.code <KP_Enter> "tester der"
 .anticiper.tester_deb configure -state disabled
 ##set reussite 0  ; set echec 0
 cas_nseo $ici1  ; focus -force .anticiper.code
if { $type_laby == "relatif" && $contr_plus  == 1 } {
 
 } else {
 
 set curs_n1 [ .frame.c create rectangle [expr $x_no + 4] [expr $y_no + 4 ] [expr $x_se - 4 ] [expr $y_se - 4 ] -width 1 -fill yellow  -tags curseur ]
set curseur $curs_n1
 }
 }
 ##si relatif fl�che curs_n1 ???
 ##vide_fleche

##set rep_p  [.anticiper.code get]
##D�composer en unit�s en mettant des espaces
set rep_p [explode $rep_p]

###brek d�s que erreur de syntaxe est supprim�e et le restant ignor�. 
##si relatif acevc contr A R � la place de H B G D pour TG TD
if { $type_laby == "relatif" && $contr_plus  == 1 } {
foreach ordre $rep_p { 
if  { ! [regexp {^[A|R|G|D|a|r|g|d]{1}$}  $ordre ] } {
tk_messageBox -message "$ordre [mc {ne va pas !Il est supprim�}]" -title "[mc {Mauvais codage d'un ordre.}]" -icon info -type ok 
 set rep_pr  [lrange $rep_p 0 [expr [lsearch $rep_p  $ordre] -1]] ; set rep_p $rep_pr
.anticiper.code delete 0 end  ; .anticiper.code insert end "$rep_p " ; return
}
}
} else {
foreach ordre $rep_p { 
if  { ! [regexp {^[H|B|G|D|h|b|g|d]{1}$}  $ordre ] } {
tk_messageBox -message "$ordre [mc {ne va pas !Il est supprim�}]" -title "[mc {Mauvais codage d'un ordre.}]" -icon info -type ok 
 set rep_pr  [lrange $rep_p 0 [expr [lsearch $rep_p  $ordre] -1]] ; set rep_p $rep_pr
.anticiper.code delete 0 end  ; .anticiper.code insert end "$rep_p " ; return
}
}
}

set rep_pro $rep_p

##if { $depuis == "deb" } {.anticiper.code delete 0 end}

##ordres effectu�s jusqu'� la premi�re erreur : stop, on m�morise les bon ordres qui ont 
##�t� ex�cut�s. Anticiper sur cach� non trait�. m�moriser liste r�elles
set liste_ordres_donnes $rep_p
set liste_ordres_donnes_tous [linsert $liste_ordres_donnes_tous end $liste_ordres_donnes]
foreach ordre $rep_p { 
set lieu_ordre [lsearch $rep_pro $ordre] ;  set rep_pro [lreplace $rep_pro $lieu_ordre $lieu_ordre "*"]
##debug.anticiper.code insert end " $lieu_ordre $rep_pro"
##faire afficher l' ordre en couleur et fin �c�ne bien ??
if { $type_laby == "relatif" && $contr_plus  == 1 } {
##fais_rel_cont $ordre 
##.frame.c  move $curseur $largeur 0
.anticiper.code configure  -background black -font {helvetica 16 bold}
if { $curseur == $fleche_d } { 
##test_que { ordre }
		switch $ordre {
 			A - a { if { [peut_on $ici1 droite ] == 1 && [test_que $ordre] } { after 800 
		tout_unset ;	cas_nseo $ici1 ; incr reussite_tf ;\
		.anticiper.code insert end " $ordre "
		 set ici1  [expr $ici1 + 1 ] ; cas_nseo $ici1  
		set fleche_d [ .frame.c create line  $x_no  [expr ($y_no + $y_so)/2] \
             $x_ne [expr [expr $y_ne + $y_se ] / 2] -fill yellow -width $ep_fleche -arrow last -tags curseur_d ]
		.frame.c delete $curseur ; update 
		set curseur $fleche_d
		

	} else { clignote ; bell ;incr echec_tf ; set liste_ind_echec [linsert $liste_ind_echec  end $lieu_ordre ] ;\
	set break_o 1 ; break }
	}
			R - r { if { [peut_on $ici1 gauche ] == 1 && [test_que $ordre] } { after 800 ;\
		tout_unset ;	cas_nseo $ici1 ; incr reussite_tf ; .anticiper.code insert end " $ordre"
	
	
			set ici1  [expr $ici1 - 1 ] ; cas_nseo $ici1 
			set fleche_d [ .frame.c create line  $x_no  [expr ($y_no + $y_so)/2] \
             $x_ne [expr [expr $y_ne + $y_se ] / 2] -fill yellow -width $ep_fleche -arrow last -tags curseur_d ]
			.frame.c delete $curseur ; update
			set curseur $fleche_d
		} else {clignote ; bell  ; bell ; incr echec_tf ; set liste_ind_echec [linsert $liste_ind_echec  end $lieu_ordre ];\
	set break_o 1 ; break }

				}
			D - d { 
					if  {[test_que $ordre] } {
		tout_unset ;	cas_nseo $ici1 ; .anticiper.code insert end " $ordre" ; after 800 ;\
set fleche_b [.frame.c create line [expr [expr $x_ne + $x_no] / 2 ]  $y_ne [expr [expr $x_so + $x_se] / 2 ] $y_so  -fill yellow  -width $ep_fleche -arrow last -tags curseur_b ]
			.frame.c delete $curseur ; set curseur $fleche_b ; update
						} else { 
						clignote ; bell  ; bell ; incr echec_tf ; set liste_ind_echec [linsert $liste_ind_echec  end $lieu_ordre ];\
					set break_o 1 ; break
						
						}
						
						
				}
			G - g { if  {[test_que $ordre] } {
		tout_unset ;	cas_nseo $ici1 ; .anticiper.code insert end " $ordre"  ; after 800 ;\
set fleche_h [ .frame.c create line  [ expr [expr $x_so + $x_se] / 2 ] $y_so [expr [expr $x_ne + $x_no] / 2] $y_ne -fill yellow  -width $ep_fleche -arrow last -tags curseur_h ]
			.frame.c delete $curseur ; set curseur $fleche_h ; update

				} else { 
						clignote ; bell  ; bell ; incr echec_tf ; set liste_ind_echec [linsert $liste_ind_echec  end $lieu_ordre ];\
					set break_o 1 ; break
						
						}
				}
##.bottom.message configure -text "$x_no $y_no $x_so $y_so $x_ne $y_ne $x_se $y_se"
				}
		

		} elseif { $curseur == $fleche_g } {

		switch $ordre {
 			A - a { if { [peut_on $ici1 gauche ] == 1 && [test_que $ordre] } { after 800 ;\
		tout_unset  ; incr reussite_tf ; .anticiper.code insert end " $ordre"
##	.frame.c  move $curseur -$largeur 0 
	set ici1  [expr $ici1 - 1 ] ;	cas_nseo $ici1 
	set fleche_g [ .frame.c create line  $x_ne [expr ($y_ne + $y_se)/2] $x_no  [expr ($y_no + $y_so)/2] \
            -fill yellow -width $ep_fleche -arrow last -tags curseur_g ]
			.frame.c delete $curseur ; update
	set curseur $fleche_g 
	} else { incr echec_tf ; clignote ; bell ; bell ; set liste_ind_echec [linsert $liste_ind_echec  end $lieu_ordre ];\
	set break_o 1 ; break }
			}
			R - r { if { [peut_on $ici1 droite ] == 1 && [test_que $ordre] } { ; after 800 ;\
		tout_unset  ; incr reussite_tf ; .anticiper.code insert end " $ordre"
##			.frame.c  move $curseur $largeur 0 
			set ici1  [expr $ici1 + 1 ] ;	cas_nseo $ici1
		set fleche_g [ .frame.c create line  $x_ne [expr ($y_ne + $y_se)/2] $x_no  [expr ($y_no + $y_so)/2] \
            -fill yellow -width $ep_fleche -arrow last -tags curseur_g ]
			.frame.c delete $curseur
		set curseur $fleche_g 	; update
		} else {incr echec_tf ; clignote ; bell ; bell ; set liste_ind_echec [linsert $liste_ind_echec  end $lieu_ordre ];\
	set break_o 1 ; break } 

				}
			G - g { if  {[test_que $ordre] } {
			.frame.c delete $curseur 
		tout_unset ;	cas_nseo $ici1 ; .anticiper.code insert end " $ordre" ; after 800 ;\
set fleche_b [ .frame.c create line [expr [expr $x_no + $x_ne] / 2]  $y_no [expr [expr $x_so + $x_se] / 2 ] $y_so  -fill yellow  -width $ep_fleche -arrow last -tags curseur_b ]
			 set curseur $fleche_b ; update } else {
			 incr echec_tf ; clignote ;bell ; bell ; set liste_ind_echec [linsert $liste_ind_echec  end $lieu_ordre ];\
	set break_o 1 ; break }
			 }

				
			D - d { if  {[test_que $ordre] } {
			.frame.c delete $curseur 
		tout_unset ;	cas_nseo $ici1 ; .anticiper.code insert end " $ordre" ; after 800 ;\
set fleche_h [ .frame.c create line  [ expr [expr $x_so + $x_se ] / 2 ] $y_so [expr [expr $x_ne + $x_no ] / 2] $y_no -fill yellow  -width $ep_fleche -arrow last -tags curseur_h ]
			 set curseur $fleche_h ; update } else {
			 clignote ; bell  ; bell ; incr echec_tf ; set liste_ind_echec [linsert $liste_ind_echec  end $lieu_ordre ];\
	set break_o 1 ; break }
			 }

		}		

		

		} elseif { $curseur == $fleche_h } {

		switch $ordre {
 			A - a { if { [peut_on $ici1 haut ] == 1 && [test_que $ordre] } { after 800 ;\
		tout_unset  ; incr  reussite_tf ; .anticiper.code insert end " $ordre"
##	.frame.c  move $curseur 0 -$hauteur 
	set ici1  [expr $ici1 - $m ];	cas_nseo $ici1
	set fleche_h [ .frame.c create line  [ expr ($x_so + $x_se)/2 ] $y_so [expr  ($x_ne + $x_no) / 2] $y_no \
		-fill yellow  -width $ep_fleche -arrow last -tags curseur_h ]
		.frame.c delete $curseur
		set  curseur $fleche_h	; update
	##	
				} else {clignote ;  bell ; bell ; incr echec_tf ; set liste_ind_echec [linsert $liste_ind_echec  end $lieu_ordre ] ; set break_o 1 ; break }
			}
			R - r { if { [peut_on $ici1 bas ] == 1 && [test_que $ordre] } { after 800 ;\
		tout_unset  ; incr reussite_tf ; .anticiper.code insert end " $ordre"
##			.frame.c  move $curseur 0 $hauteur 
			set ici1  [expr $ici1 + $m ] ;	cas_nseo $ici1
		set fleche_h [ .frame.c create line  [ expr ($x_so + $x_se)/2 ] $y_so [expr  ($x_ne + $x_no) / 2] $y_no \
		-fill yellow  -width $ep_fleche -arrow last -tags curseur_h ]
		.frame.c delete $curseur
		set  curseur $fleche_h	; update
			
		} else {clignote ; bell ; bell ; incr echec_tf ;\
		set liste_ind_echec [linsert $liste_ind_echec  end $lieu_ordre ] ; set break_o 1 ; break ; }

				}
			G - g { if { [test_que $ordre] } {
			.frame.c delete $curseur 
		tout_unset ;	cas_nseo $ici1  ; .anticiper.code insert end " $ordre" ; after 800 ;\
	set fleche_g [ .frame.c create line $x_ne [expr [expr $y_ne + $y_se ] / 2] $x_no [expr [expr $y_so + $y_no ] / 2 ]  -fill yellow  -width $ep_fleche -arrow last -tags curseur_g ]
			.frame.c delete $curseur
			set curseur $fleche_g ; update

				} else {clignote ; bell ; bell ; incr echec_tf ;\
				set liste_ind_echec [linsert $liste_ind_echec  end $lieu_ordre ] ; set break_o 1 ; break }
				
				}
			D - d {if { [test_que $ordre] } {
			.frame.c delete $curseur 
		tout_unset ;	cas_nseo $ici1 ; .anticiper.code insert end " $ordre" ; after 800 ;\
set fleche_d [ .frame.c create line  $x_no [expr [expr $y_so + $y_no ] / 2 ] $x_se [expr [expr $y_se + $y_ne] / 2]  -fill yellow  -width $ep_fleche -arrow last -tags curseur_d ]
			.frame.c delete $curseur
			set curseur $fleche_d ; update

				} else  {clignote ; bell ; bell ; incr echec_tf ;\
				set liste_ind_echec [linsert $liste_ind_echec  end $lieu_ordre ] ; set break_o 1 ; break}

		}
		}
		} elseif { $curseur == $fleche_b } {

		switch $ordre {
 			A - a { if { [peut_on $ici1 bas ] == 1 && [test_que $ordre] } { after 800 
		tout_unset ; incr reussite_tf ; .anticiper.code insert end  " $ordre"
	set ici1  [expr $ici1 + $m ] ; cas_nseo $ici1
	set fleche_b [ .frame.c create line [expr ($x_ne + $x_no)/2]  $y_ne [expr ($x_so + $x_se)/2 ] $y_so  -fill yellow  -width $ep_fleche -arrow last -tags curseur_b ]
	.frame.c delete $curseur
	set  curseur $fleche_b	 ; update
	} else { clignote ; bell ; bell ; incr echec_tf ; set liste_ind_echec [linsert $liste_ind_echec  end $lieu_ordre ] ; set break_o 1 ; break }
	}
			R - r { if { [peut_on $ici1 haut ] == 1 && [test_que $ordre] } { after 800 ;\
		tout_unset ; incr reussite_tf ; .anticiper.code insert end " $ordre"
			set ici1  [expr $ici1 - $m ] ; cas_nseo $ici1
			set fleche_b [ .frame.c create line [expr ($x_ne + $x_no)/2]  $y_ne [expr ($x_so + $x_se)/2 ] $y_so  -fill yellow  -width $ep_fleche -arrow last -tags curseur_b ]
		.frame.c delete $curseur
		set  curseur $fleche_b	; update
		} else {  clignote ; bell ; bell ; incr echec_tf ;\
		set liste_ind_echec [linsert $liste_ind_echec  end $lieu_ordre] ;  set break_o 1 ; break}

				}
			D - d { if { [test_que $ordre] } {
			
			.frame.c delete $curseur ; after 800 
		tout_unset ;	cas_nseo $ici1 ; .anticiper.code insert end " $ordre"  
set fleche_g [ .frame.c create line  $x_ne [expr [expr $y_ne + $y_se] / 2 ] $x_no [expr [expr $y_so + $y_no ] / 2 ]  -fill yellow  -width $ep_fleche -arrow last -tags curseur_g ]
			 .frame.c delete $curseur
			 set curseur $fleche_g ; update

				} else {
				{ clignote ; bell ; bell ; incr echec_tf ;\
				set liste_ind_echec [linsert $liste_ind_echec  end $lieu_ordre ] ; set break_o 1 ; break }
				}
			}	
			G - g { if { [test_que $ordre] } {
			
			.frame.c delete $curseur ; after 800 ;\
		tout_unset ;	cas_nseo $ici1 ; .anticiper.code insert end " $ordre"
set fleche_d [ .frame.c create line  $x_no [expr [expr $y_so + $y_no] / 2 ] $x_se [expr [expr $y_se + $y_ne] / 2]  -fill yellow  -width $ep_fleche -arrow last -tags curseur_d ]
			.frame.c delete $curseur
			set curseur $fleche_d ; update

				} else {
				 clignote ; bell ; bell ; incr echec_tf ;\
				set liste_ind_echec [linsert $liste_ind_echec  end $lieu_ordre ] ; set break_o 1 ; break }
				}
			}
		}


} else {
##set curseur $curseur1
switch $ordre {
 	"D" - "d" {
 	if { [peut_on $ici1 droite ] == 1 } { after 800  ;  \
	.anticiper.code insert end " $ordre" ; .anticiper.code configure  -background black -font {helvetica 16 bold}
	.frame.c  move $curseur $largeur 0 ; set ici1  [expr $ici1 + 1 ] ; update ; if { $depuis == "deb" || $depuis == "der" } { incr reussite_tf }
	} else { incr echec_tf ; set liste_ind_echec [linsert $liste_ind_echec end $lieu_ordre ]  ; \
	.anticiper.code insert end " $ordre mauvais"
	clignote ; if { $type_laby == "cache" } {traite_cache $ici1 $ou } ; set break_o 1 ; break
			}
	}
	"G" - "g" {
		if { [peut_on $ici1 gauche ] == 1 } { after 800  ;  \
	.anticiper.code insert end " $ordre" ; .anticiper.code configure  -background black -font {helvetica 16 bold}
	.frame.c  move $curseur -$largeur 0 ; set ici1 [expr $ici1 - 1 ] ; update ;  if { $depuis == "deb" || $depuis == "der"}  { incr reussite_tf }
	} else { incr echec_tf ; set liste_ind_echec  [linsert $liste_ind_echec end $lieu_ordre ] ;\
	clignote ; if { $type_laby == "cache" } {traite_cache $ici1 $ou} ; set break_o 1 ; break }
		}
	"H" - "h" { 
	if { [peut_on $ici1 haut ] == 1 } { after 800  ;  \
	.anticiper.code insert end " $ordre" ; .anticiper.code configure  -background black -font {helvetica 16 bold}
	.frame.c  move $curseur  0 -$hauteur ; set ici1 [expr $ici1 - $m ] ; update ;    if { $depuis == "deb" || $depuis == "der"}  { incr reussite_tf }
	} else { incr echec_tf  ; set liste_ind_echec  [linsert $liste_ind_echec end $lieu_ordre ] ; \
	clignote ; if { $type_laby == "cache" } { traite_cache $ici1 $ou } ; set break_o 1 ; break}
	}
	"B" - "b" {
	if { [peut_on $ici1 bas ] == 1 } { after 800  ; \
	.anticiper.code insert end " $ordre"; .anticiper.code configure  -background black -font {helvetica 16 bold}
	.frame.c  move $curseur  0 $hauteur ; set  ici1 [expr $ici1 + $m ] ; update ;   if { $depuis == "deb" || $depuis == "der"}  {  incr reussite_tf }
	} else {incr echec_tf ;  set liste_ind_echec [linsert $liste_ind_echec  end $lieu_ordre ] ;\
	clignote ; if { $type_laby == "cache" } { traite_cache $ici1 $ou } ; set break_o 1 ; break}
	}
	}	
##fin swichth
}
	set liste_ordre_ef  [linsert  $liste_ordre_ef  end $ordre]
	## arriv�e sans erreur mais liste trop longue
	if { $ici1 ==  $arrivee && [lrange $rep_p  [expr $lieu_ordre +1 ] end] != {} } {
	set break_l 1 ; set liste_ordre_ef  [lrange $rep_p  0 $lieu_ordre ] 
##	.bottom.message configure -text  "Bonne arriv�e avec $liste_ordre_ef\nMais trop longue" -foreground red
	 tk_messageBox -message "[format [mc {Bonne arriv�e avec %1$s Mais trop longue}] $liste_ordre_ef]" -title "[mc {liste trop longue :}]" -icon info -type ok
	 .anticiper.code insert end [lrange $rep_p  0 $lieu_ordre ] ; break
	}

} 
###r�cup�re les erreurs [lrange $rep_p 0  $lieu_ordre ] qd break_o � 1
#fin foreach liste ordres
###si break par mauvais codage m�moriser  puis return
if { $break_o == 1 } {
	set liste_erreur [lrange $rep_p 0  $lieu_ordre ] 
	after 1000
	.anticiper.code delete 0 end 
	 set liste_ordre_ef  [lrange $rep_p 0 [expr $lieu_ordre -1] ]
	 set fin_liste [lrange $rep_p  $lieu_ordre end]
##d�tailler pour rel contr que
	 tk_messageBox -message "[format [mc {Mauvais ordre : %1$s, d�but de la fin de liste : %2$s Bonne liste : %3$s}] $ordre $fin_liste $liste_ordre_ef ]" -title "[mc {Ordre erronn� :}]" -icon info -type ok   	 	
	 }
#liste apr�s arriv�e non vide :impossible maintenant
##if { $ici1 == $arrivee  && $liste_ind_echec  
##set  deb_arr_er 1
##clignote_bon ; .right.refaire  configure -state normal
##.bottom.message configure -text  "Bonne arriv�e\navec $echec_cumul �chec(s)\n"  -foreground red 
##} else 
## set deb_arr_er 0
 ##et si break_l =1 ?
if { $ici1 == $arrivee &&  $depuis == "deb" || $break_l == 1} {
.frame.c create image  [expr [lindex  [indice_xy $arrivee] 1 ] * 35 + 30] [expr [expr int([lindex [indice_xy $arrivee]  0 ])] * 35 + 30]     -image [image create photo -file [file join images bien.gif]]  -tags img
##liste_ind pour les listes finales trop longues avec aucun echec (corrig� pour Tout!!)
set time_fin [clock seconds] ; set duree_exo [expr [expr $time_fin - $time_debut] + $duree_Tout ] 
##[expr [expr $time_fin - $time_debut] + $duree_ecrit ] 
set duree_exo_mn [expr $duree_exo / 60]  ; set duree_exo_s [expr $duree_exo % 60]
set tab_bilan($nom_laby,$type_laby,$nbre_rec,duree)   [list $duree_exo_mn $duree_exo_s ]
##set tab_bilan($nom_laby,$type_laby,$nbre_rec,type) $type_laby 
##le cumul ds liste ordres se fait dans echec et ordre
#m�moriser r�sultats interm�diaires et �limier ce qu'il ya apr�s arrivee??(pas icci)
##cumul pour recommencer
##set echec_cumul  0 ; set reussite_cumul  0 ; set echec_cumul1  0 ; set reussite_cumul1  0
	set echec_cumul  [expr $echec_cumul1 + $echec_tf]  ;  set reussite_cumul [ expr $reussite_cumul1 + $reussite_tf]
	set tab_bilan($nom_laby,$type_laby,$nbre_rec,ech_err)  [list $echec_tf  $reussite_tf ]	
set rep_partiel $liste_ordre_ef
set rep_partiel_fais $rep_partiel
set rep_tout [linsert $rep_partiel_cumul end  $rep_partiel ]
set tab_bilan($nom_laby,$type_laby,$nbre_rec,ordres) $rep_tout
set tab_bilan($nom_laby,$type_laby,$nbre_rec,ordres_bad) $liste_erreur_cumul
set tab_bilan($nom_laby,$type_laby,$nbre_rec,ordres_donnes_tous) $liste_ordres_donnes_tous
clignote_bon ; .right.refaire  configure -state normal 
###debug .anticiper.code insert end [lindex [array get tab_bilan] 1]
.anticiper.code configure  -background white -font {helvetica 14}
after 1000
.bottom.message configure -text " [format  [mc {Bravo, C'est fini Echecs : %1$s R�ussites  %2$s }] $echec_tf $reussite_tf]\n [mc {avec liste des listes erreurs}] $liste_erreur_cumul\n\
[mc {pour les ordres :}]$rep_partiel\n[mc {Au total}] $echec_tf �checs $reussite_tf r�ussites.\n [mc {pour en tout :}]$duree_exo_mn min $duree_exo_s sec\n et $liste_ordres_donnes_tous " -foreground red
##Bilan � adapter  au cas anticiper
.anticiper.tester_deb configure -state disabled
bilan_sans_aff   ; catch {destroy $w2 } ; sauver_bilan 
button .bottom.bilan -text "[mc {BILAN}]" -command " set flag_bilan 1 ; bilan $depuis" -font {helvetica 11 bold }
place  .bottom.bilan -x 165 -y 5

if { $type_laby == "relatif" &&  $contr_plus == 1 } { 
catch { .bottom.check_contr configure -state disabled ; .anticiper.tester_deb  configure  -state disabled \
	bottom.anticiper configure  -state disabled} } else {
catch {.bottom.check_contr configure -state active}
##pack .bottom.bilan -side right
.anticiper.tester_deb  configure  -state disabled }
##break
						 } 
						 
						 ##&&  $depuis == "der"						 
if { $ici1 == $arrivee &&  $depuis == "der"  } {
.anticiper.tester configure -state disabled
.frame.c create image  [expr [lindex  [indice_xy $arrivee] 1 ] * 35 + 30] [expr [expr int([lindex [indice_xy $arrivee]  0 ])] * 35 +30]     -image [image create photo -file [file join images bien.gif]]  -tags img

	set time_fin [clock seconds] ; set duree_exo  [expr [expr $time_fin - $time_debut] + $duree_Fais]
	
	set echec_cumul  [expr $echec_cumul1 + $echec_tf]  ;  set reussite_cumul [ expr $reussite_cumul1 + $reussite_tf]
	set tab_bilan($nom_laby,$type_laby,$nbre_rec,ech_err)  [list $echec_tf $reussite_tf]	
##	set tab_bilan($nom_laby,$type_laby,$nbre_rec,ordres) {}
	set duree_exo_mn [expr $duree_exo / 60]  ; set duree_exo_s [expr $duree_exo % 60]
	set tab_bilan($nom_laby,$type_laby,$nbre_rec,duree)   [list $duree_exo_mn $duree_exo_s ]
	
	set rep_partiel $liste_ordre_ef
	set rep_partiel_fais $rep_partiel
	set rep_tout [linsert $rep_partiel_cumul end  $rep_partiel ]
	set tab_bilan($nom_laby,$type_laby,$nbre_rec,ordres) $rep_tout
	set tab_bilan($nom_laby,$type_laby,$nbre_rec,ordres_bad) $liste_erreur_cumul
	set tab_bilan($nom_laby,$type_laby,$nbre_rec,ordres_donnes_tous) $liste_ordres_donnes_tous
	catch {destroy .bottom.bilan}
	button .bottom.bilan -text "[mc {BILAN}]" -command "set flag_bilan 1 ;  bilan $depuis" -font {helvetica 11 bold }
	place  .bottom.bilan -x 165 -y 5
	
	if { $type_laby == "relatif" &&  $contr_plus == 1 } { 
catch {.bottom.check_contr configure -state disabled ; .anticiper.tester  configure  -state disabled \
	bottom.anticiper configure  -state disabled }  } else {
	
	catch {.bottom.check_contr configure -state active}
	.anticiper.tester  configure -state disabled }
	.anticiper.code configure  -background white -font {helvetica 14}
	
	bilan_sans_aff ; catch {destroy $w2 } ; sauver_bilan 
##bilan $depuis
##pack .bottom.bilan -side right	
	clignote_bon ; .right.refaire  configure -state normal
	after 1000
	.bottom.message configure -text  "[format [mc {Bonne arriv�e avec en tout pour %1$s : %2$s �chec(s) %3$s r�ussites avec les ordres %4$s et les listes echecs %5$s}] $nom_depuis $echec_tf $reussite_tf $rep_tout $liste_erreur_cumul ]" -foreground red 
	
	}
 
if { $ici1 != $arrivee  || $break_o == 1 }  {
##pr�voir sorties selon nbre �checs
after 1000 ;.anticiper.code delete 0 end
.anticiper.code configure  -background white -font {helvetica 14}
if {$depuis  == "deb" } { 
.frame.c delete $curseur
 set ici1 $depart
cas_nseo $depart

if { $type_laby == "relatif" } {
##tab_bilan($nom_laby,$type_laby,$i,dep)
##vide_fleche
cas_nseo $depart
if { [pas_bord_o $depart] == 0 } { 
set fleche_d [ .frame.c create line  $x_no  [expr ($y_no + $y_so)/2] \
             $x_ne [expr [expr $y_ne + $y_se ] / 2] -fill yellow -width $ep_fleche -arrow last -tags curseur_d ]
		set  curseur $fleche_d	
		} elseif { [pas_bord_e $depart] == 0 } {
set fleche_g [ .frame.c create line  $x_ne [expr ($y_ne + $y_se)/2] $x_no  [expr ($y_no + $y_so)/2] \
            -fill yellow -width $ep_fleche -arrow last -tags curseur_g ]
		set  curseur $fleche_g	
		} elseif { [pas_bord_s $depart] == 0 } {
set fleche_h [ .frame.c create line  [ expr ($x_so + $x_se)/2 ] $y_so [expr  ($x_ne + $x_no) / 2] $y_no \
		-fill yellow  -width $ep_fleche -arrow last -tags curseur_h ]
		set  curseur $fleche_h	
		} elseif { [pas_bord_n $depart] == 0 } {						
set fleche_b [ .frame.c create line [expr ($x_ne + $x_no)/2]  $y_ne [expr ($x_so + $x_se)/2 ] $y_so  -fill yellow  -width $ep_fleche -arrow last -tags curseur_b ]
	set  curseur $fleche_b	
	} else {
###sinon d�part au milieu tester peut-on
set fleche_d [ .frame.c create line  $x_no  [expr ($y_no + $y_so)/2] \
             $x_ne [expr [expr $y_ne + $y_se ] / 2] -fill yellow -width $ep_fleche -arrow last -tags curseur_d ]
		set  curseur $fleche_d	
	}
	} else {
	
set curs_n [ .frame.c create rectangle [expr $x_no + 4] [expr $y_no + 4 ] [expr $x_se - 4 ] [expr $y_se - 4 ] -width 1 -fill yellow  -tags curseur ]
set curseur $curs_n
#set curseur $curseur1
	}

##debug.anticiper.code insert end "$curseur_dep et $curseur"

}
##m�moriser r�sultats interm�diaires traiter les erreurs
set rep_partiel $liste_ordre_ef
set liste_erreur_cumul [linsert $liste_erreur_cumul end $liste_erreur]
set rep_partiel_cumul  [linsert $rep_partiel_cumul end  $rep_partiel ]
set echec_cumul2 $echec_cumul1 ; ##bon sauf pour 1ere fois
##cumul ne servent plus
set echec_cumul1 [expr $echec_cumul1 + $echec_tf] ; set reussite_cumul1 [expr $reussite_cumul1 + $reussite_tf]
.bottom.message configure -text "[format  [mc {Bilan provisoire. Echecs : %1$s R�ussites : %2$s}] $echec_tf $reussite_tf]\n\
[mc {avec les ordres}] $rep_partiel\n[mc {liste erreurs}] $liste_erreur \n [mc {avec}] $echec_tf �checs $reussite_tf r�ussites.\n\
liste tap�e : $liste_ordres_donnes" -foreground red -font {helvetica 10}
#incr $compte_etape set erreur_etape(i+1) erreur_etape(i) +3  ie incr 3
## si pas exit $echec_tffff sera toujours > =3 !!!
##faire un set cumul2 cumul1 puis cumul1 -cumul2 >=3 ie echecc_tf !!qui change bien chaque fois
##echec_tf nbre �checs � l'int�rieur d'un Fais (ouTout) echec_cumul ds recomm Fais ou Tout

## longueur du chemin 
if {  $echec_tf >= 10} {
  tk_messageBox -message "[mc {Trop d'erreurs. longueur minimun}]: [llength $chemin ]" -title "[mc {Trop d'erreurs}]" -icon info -type ok   
.frame.c create image  [expr [lindex  [indice_xy $arrivee] 1 ] * 35 + 30] [expr [expr int([lindex [indice_xy $arrivee]  0 ])] * 35 +30]     -image [image create photo -file [file join images mal.gif]]  -tags img
##set echec_tf_a $echec_tf
.right.quit invoke
update 
after 2000 ; .frame.c delete tags img ;  exit
}


##vider les �checs
set liste_b {} ; set rep1  $rep_partiel
set liste_ordres_donnes {}
 
##deb		
##.bottom.message configure -text "[format  [mc {Bilan provisoire. Echecs : %1$s R�ussites : %2$s }] $echec $reussite]\n\
##avec les ordres $rep_partiel\n et au total $echec_cumul1 �checs $reussite_cumul1 r�ussites.\nBons ordres :$liste_b" -foreground red
##after 2000
	
## delete interessant que si l'ordi ne corrige pas les erreurs
	##der"
## .bottom.message configure -text  "Il y a  $echec �chec(s)\nOrdres :$rep_partiel\nBons : $liste_b\nMauvais $liste_ind_echec" 
##set echec_cumul [expr $echec_cumul + $echec] 

   
##clignote_bon ; .right.refaire  configure -state normal
###catch {destroy .bottom.bilan} ; button .bottom.bilan -text "BILAN" -command "set flag_bilan 1 ;  bilan $depuis" -font {helvetica 11 bold }
##place  .bottom.bilan -x 165 -y 5
##.bottom.message configure -text  "Bonne arriv�e\navec $echec_cumul �chec(s)\n"  -foreground red 
}


set rep_tout {} ; set liste_ind_echec  {}
 
 }

proc sauver_bilan {} {
global basedir nom_classe w2 data1 indice_recup tab_travail  flag_sauv data11 
set savingFile [file join $basedir data  ${nom_classe}.bil ]
catch {set f [open [file join $savingFile] "a+"]}

 if { $indice_recup == 1 } {
	 set data11 [join [ lrange [split $data1 ":"] 1 end ] ":"]
	set tab_travail(1) $data1
	} else { 
	set tab_travail(2)  $data1 ; set data12 [ join [ lrange [split $data1 ":"] 1 end ] ":"]

	if { [string first  $data11  $data12 ] == -1} { ; ##on peut engranger 0
	puts $f $tab_travail(1) 
	close $f ; return
	} else {
	 set tab_travail(1)  $tab_travail(2)
	set data11  $data12
	}
	
	}
		if {  $flag_sauv == 1} { puts $f  $tab_travail(1) }
close $f
##catch {destroy ${w2}.text.sauv_bilan}	
return
 }
proc bilan_sans_aff { } {
global type_laby nom_laby tcl_platform w2 tab_bilan ant nbre_rec rep_tout flag_que
global basedir nom_classe nom_elev data1 flag_bilan 
if { $tcl_platform(platform) == "windows" } {
set nom_elev [.bottom.nom get]  ;  .bottom.nom configure -state disabled
set nom_classe [.bottom.classe get ] ;  .bottom.classe configure -state disabled
}
set date [clock format [clock seconds] -format "%H%M%D"]
set data1 "$date:$nom_elev:$nom_laby:$type_laby"
for {set i 0 } { $i <= $nbre_rec } { incr i} {
##si normal et anticiper bilan particulier
if { $type_laby == "normal" && $ant == 1} {
	if {	$tab_bilan($nom_laby,$type_laby,$i,dep) == "deb" } {set rep_ant "Tout" } else { set rep_ant "Fais" }
##	marge ; ${w2}.text insert end  "$type_laby avec anticipation $rep_ant \n" black
	set data1 "$data1:$rep_ant"
				}  elseif { $type_laby == "relatif"   }  {
				##comme ds normal Fais. ici relatif re_tg_Tout. si
			if { $tab_bilan($nom_laby,$type_laby,$i,c_rel) != ""}  {     
##si ant rep_ant � Tout ou Fais av_td_Tout ?
			set contr_rel  $tab_bilan($nom_laby,$type_laby,$i,c_rel) ; set rep_ant $contr_rel
		set data1 "$data1:${contr_rel}"
##		marge ; ${w2}.text insert end  "$type_laby avec la restriction $contr_rel\n"   black 
			}	else {
		set data1 "$data1:rr"}
##		marge ; ${w2}.text insert end  "$type_laby \n" 
		}  else {
		if {$type_laby == "cache" } {set  data1 "$data1:c" } else  { set data1 "$data1:nr"}
		
##		marge ; ${w2}.text insert end  "$type_laby \n"   black }
##		marge ; ${w2}.text insert end "essai $i :\n"
## � fiarer rel contr
if { $type_laby == "relatif" && $ant  == 1 } {
set data1 "$data1:$tab_bilan($nom_laby,$type_laby,$i,ordres)"
}
if { $type_laby == "normal" && $ant == 1 } {
if {$tab_bilan($nom_laby,$type_laby,$i,dep) == "deb" } {
##et si relati ant ??
		set data1 "$data1:$tab_bilan($nom_laby,$type_laby,$i,ordres)" } else {
		set data1 "$data1:$tab_bilan($nom_laby,$type_laby,$i,ordres)"
##		set data1 "$data1:vide"
		}

		
		
		
		
		}
if { $tab_bilan($nom_laby,$type_laby,$i,dep)  !=  "der" } { 
set echec [lindex $tab_bilan($nom_laby,$type_laby,$i,ech_err) 0] ; set reuss  [lindex $tab_bilan($nom_laby,$type_laby,$i,ech_err) 1]
set min [lindex $tab_bilan($nom_laby,$type_laby,$i,duree)  0] ; set sec [lindex $tab_bilan($nom_laby,$type_laby,$i,duree)  1]  
##marge ; marge ;  ${w2}.text insert end "$echec �chec(s), $reuss r�ussite(s),  en $min mn $sec sec \n" blue
set duree [list $min $sec]
set data1 "$data1:$tab_bilan($nom_laby,$type_laby,$i,ech_err):$duree:$tab_bilan($nom_laby,$type_laby,$i,ordres_bad)"
set data1 "$data1:$tab_bilan($nom_laby,$type_laby,$i,ordres_donnes_tous)"
} else {
set echec [lindex $tab_bilan($nom_laby,$type_laby,$i,ech_err) 0] ; set reuss  [lindex $tab_bilan($nom_laby,$type_laby,$i,ech_err) 1]
 set min [lindex $tab_bilan($nom_laby,$type_laby,$i,duree)  0] ; set sec [lindex $tab_bilan($nom_laby,$type_laby,$i,duree)  1] 
## marge ; marge ; ${w2}.text insert end " $echec �chec(s), $reuss r�ussite(s),  en $min mn $sec sec \n "
 set data1 "$data1:$tab_bilan($nom_laby,$type_laby,$i,ech_err):[list $min $sec]:$tab_bilan($nom_laby,$type_laby,$i,ordres_bad)"
set data1 "$data1:$tab_bilan($nom_laby,$type_laby,$i,ordres_donnes_tous)"
		}
 ##${w2}.text insert end  "============================================================\n"
##${w2}.text see end
}
  }
 
 proc bilan { dep} {
global type_laby nom_laby tcl_platform w2 tab_bilan ant nbre_rec rep_tout flag_que
global basedir nom_classe nom_elev data1 flag_bilan 
if { $tcl_platform(platform) == "windows" } {
set nom_elev [.bottom.nom get]  ;  .bottom.nom configure -state disabled
set nom_classe [.bottom.classe get ] ;  .bottom.classe configure -state disabled
}
##catch {destroy .top2 }
 set w2 [toplevel .top2]
set date [clock format [clock seconds] -format "%H%M%D"]
##set data1 "$date:$nom_elev:$nom_laby:$type_laby"
wm geometry $w2 +250+0
wm title $w2 [mc {bil_laby}]
###grab ${w2}
text ${w2}.text -yscrollcommand "${w2}.scroll set" -setgrid true -width 60 -height 15 \
 -wrap word -background bisque -font   {Helvetica  12}
scrollbar ${w2}.scroll -command "${w2}.text yview"
pack ${w2}.scroll -side right -fill y
pack ${w2}.text -expand yes -fill both
##lower .frame  ${w2} 
${w2}.text tag configure green -foreground green
${w2}.text tag configure red -foreground red
${w2}.text tag configure yellow -foreground yellow
${w2}.text tag configure normal -foreground black
${w2}.text tag configure black1 -foreground black -underline yes
${w2}.text tag configure white -foreground white -underline yes
 ${w2}.text tag configure white1 -foreground white     -font  {helvetica 16}
 ${w2}.text tag configure blue -foreground blue
proc marge {} {
global w2
${w2}.text insert end  "     "
}
##[lindex [split [lindex [array names  tab_bilan] 0] ","] 0]
${w2}.text insert end  "  Le $date, exercice : $nom_laby\n"     red
${w2}.text insert end   "[mc {resum_bilan}]  $nom_elev : \n"   black

marge ; ${w2}.text insert end  "[mc {type_laby}]"   black1
for {set i 0 } { $i <= $nbre_rec } { incr i} {
##si normal et anticiper bilan particulier
if { $type_laby == "normal" && $ant == 1} {
	if {	$tab_bilan($nom_laby,$type_laby,$i,dep) == "deb" } {set rep_ant "[mc {Tout}]" } else { set rep_ant "[mc {Fais}]" }
	marge ; ${w2}.text insert end  "$type_laby [mc {avec anticipation}] $rep_ant \n" black
##	set data1 "$data1:$rep_ant"
				}  elseif { $type_laby == "relatif"   }  {
			if { $tab_bilan($nom_laby,$type_laby,$i,c_rel) != ""}  {     
		set contr_rel  $tab_bilan($nom_laby,$type_laby,$i,c_rel) ; set rep_ant $contr_rel
##		set data1 "$data1:${contr_rel}"
##contre_rel av_tg en Que AV TG....
		marge ; ${w2}.text insert end  "$type_laby [mc {avec la restriction}] $contr_rel\n"   black}  else {
##		set data1 "$data1:rr"}
		marge ; ${w2}.text insert end  "$type_laby \n" 
		}  else {
###if {$type_laby == "cache" } {set  data1 "$data1:c" } else  { set data1 "$data1:nr"}
		
		marge ; ${w2}.text insert end  "$type_laby \n"   black }
		marge ; ${w2}.text insert end "[mc {essai}] $i :\n"
		
		
		if {$type_laby == "normal" && $ant == 1 } {set bool_n 1 } else {set bool_n 0}
		if {$type_laby == "relatif" && $ant == 1 } {set bool_r 1 } else {set bool_r 0}
		if { $bool_n || $bool_r } {
marge ; marge
if {$tab_bilan($nom_laby,$type_laby,$i,dep) == "deb" } {
		${w2}.text insert end  "[mc {Listes effectives �tant, dans l'ordre}] :\n$tab_bilan($nom_laby,$type_laby,$nbre_rec,ordres_donnes_tous)\n  "
		marge ; marge ; ${w2}.text insert end  "[mc {et comme d�but des listes erreurs}] :\n $tab_bilan($nom_laby,$type_laby,$i,ordres_bad)\n" black
		marge ; marge ; ${w2}.text insert end  "[mc {avec les listes d'ordres corrig�es (une par tentative)}]:\n"
	    ${w2}.text insert end  "$tab_bilan($nom_laby,$type_laby,$i,ordres)\n" black
##		 et comme d�but des liste erreurs : $tab_bilan($nom_laby,$type_laby,$i,ordres_bad)\n" black
##		 marge ; marge ; ${w2}.text insert end  "Listes effectives �tant :\n$tab_bilan($nom_laby,$type_laby,$nbre_rec,ordres_donnes_tous)\n  "
##		set data1 "$data1:$tab_bilan($nom_laby,$type_laby,$i,ordres)" 
		} else {
##		set data1 "$data1:vide"
		}

if {$tab_bilan($nom_laby,$type_laby,$i,dep) == "der" } {
		${w2}.text insert end  "[mc {Listes effectives �tant, dans l'ordre}] :\n$tab_bilan($nom_laby,$type_laby,$nbre_rec,ordres_donnes_tous)\n  "
		marge ; marge ; marge ; marge ; ${w2}.text insert end  "[mc {et comme d�but des listes erreurs}] :\n $tab_bilan($nom_laby,$type_laby,$i,ordres_bad)\n" black
		marge ; marge ; ${w2}.text insert end  "[mc {avec les listes d'ordres corrig�es (une par tentative)}] :\n"
	   ${w2}.text insert end  "$tab_bilan($nom_laby,$type_laby,$i,ordres)\n" black
##		et comme listes erreurs: $tab_bilan($nom_laby,$type_laby,$i,ordres_bad)\n" black
##		marge ; marge ; ${w2}.text insert end  "Listes effectives �tant :\n$tab_bilan($nom_laby,$type_laby,$nbre_rec,ordres_donnes_tous)\n"
##		set data1 "$data1:$tab_bilan($nom_laby,$type_laby,$i,ordres)" nom_depuis
		} else {
##		set data1 "$data1:vide"
		}
		
	}
if { $tab_bilan($nom_laby,$type_laby,$i,dep)  ==  "deb" } { 
set echec [lindex $tab_bilan($nom_laby,$type_laby,$i,ech_err) 0] ; set reuss  [lindex $tab_bilan($nom_laby,$type_laby,$i,ech_err) 1]
set min [lindex $tab_bilan($nom_laby,$type_laby,$i,duree)  0] ; set sec [lindex $tab_bilan($nom_laby,$type_laby,$i,duree)  1]  
marge ; marge ;  ${w2}.text insert end "[format [mc {Et %1$s �chec(s), %2$s r�ussite(s) pour cet essai}] $echec $reuss ].\n          [mc {Dur�e de tous les essais pr�c�dents}] (de $rep_ant) : $min mn $sec sec \n" blue
set duree [list $min $sec]
##set data1 "$data1:$tab_bilan($nom_laby,$type_laby,$i,ech_err):$duree"
} elseif { $tab_bilan($nom_laby,$type_laby,$i,dep)  == "der" } {
set min [lindex $tab_bilan($nom_laby,$type_laby,$i,duree)  0] ; set sec [lindex $tab_bilan($nom_laby,$type_laby,$i,duree)  1] 
set echec [lindex $tab_bilan($nom_laby,$type_laby,$i,ech_err) 0] ; set reuss  [lindex $tab_bilan($nom_laby,$type_laby,$i,ech_err) 1]
set min [lindex $tab_bilan($nom_laby,$type_laby,$i,duree)  0] ; set sec [lindex $tab_bilan($nom_laby,$type_laby,$i,duree)  1]  
marge ; marge ;  ${w2}.text insert end "Et $echec �chec(s), $reuss r�ussite(s) pour cet essai.\n          Dur�e de tous les essais pr�c�dents (de $rep_ant) :  $min mn $sec sec \n" blue
set duree [list $min $sec]
# marge ; marge ;  ${w2}.text insert end " [lindex $tab_bilan($nom_laby,$type_laby,$i,ech_err) 0] �checs  en $min mn $sec sec \n "
## set data1 "$data1:$tab_bilan($nom_laby,$type_laby,$i,ech_err):[list $min $sec]"
		} else {
set echec [lindex $tab_bilan($nom_laby,$type_laby,$i,ech_err) 0] ; set reuss  [lindex $tab_bilan($nom_laby,$type_laby,$i,ech_err) 1]
set min [lindex $tab_bilan($nom_laby,$type_laby,$i,duree)  0] ; set sec [lindex $tab_bilan($nom_laby,$type_laby,$i,duree)  1]  
marge ; marge ;  ${w2}.text insert end "Et $echec �chec(s), $reuss r�ussite(s) pour cet essai et ce en $min mn $sec sec \n" blue
set duree [list $min $sec]		
		
		}
		
		
 ${w2}.text insert end  "============================================================\n"
${w2}.text see end
## ${w2}.text insert end  "Les bilans peuvent �tre sauv�s par Quitter\n"
 ##${w2}.text see end
 ##catch {destroy ${w2}.text.sauv_bilan}
## button ${w2}.text.sauv_bilan -text "Sauver Bilan" -command "sauver_bilan " 
 ###${w2}.text window create end  -window ${w2}.text.sauv_bilan  -align top -padx 30
 ##${w2}.text.sauv_bilan configure  -state disabled
 } ; ##fin des recomm
 if  {  $tcl_platform(platform)  == "unix"  && $flag_bilan == 1 }  {
###after 1000
set rep [tk_messageBox  -message "[mc {imprimer}]" -type yesno -icon question  -default no -title [mc {Imprimer?}] ]
 if {  $rep  == "yes"  }  {
### set  t    [ ${w2}.text  get  0.0  end  ]
if { ![catch { exec  lpr   <<  [ ${w2}.text  get  0.1  end  ]}] } {
}
 ###  $b_lis  $e_lis
 ### ${w2}.text  get  0 end  ]
 }
##raise ${w2} .
 }
 set flag_bilan 0
 catch {raise ${w2}} .
}

##.bottom.message configure -text ""
##[array get tab_laby] \n $trace"
##$chem $trace "
##[array get tab_laby
##imprimer le canvas pb avec les rep�res
proc imprime {c} {
set rep [tk_messageBox  -message "[mc {Voulez vous vraiment imprimer ?}]" -type yesno -icon question  -default no -title [mc {Imprimer?}] ]
if { $rep == "yes" } {
if {[info exists env(PRINTER)]} {
    set psCmd "lpr -P$env(PRINTER)"
} else {
    set psCmd "lpr"
}
} else { return }
set postscriptOpts { -pagewidth 160m -pageanchor c  -pagex 110m -pagey 100m
##140m 90m
}
 
if {[catch {eval exec $psCmd <<    \
		  {[eval $c postscript $postscriptOpts]}} msg]} {
		    tk_messageBox -message  \
		      "Error when printing: $msg" -icon error -type ok
		}
}
proc sauver_ps {} {
global basedir TRACEDIR_ARBRE Home  tcl_platform
  set date [clock format [clock seconds] -format "%H%M%d%b"]
set TRACEDIR_ARBRE $Home
cd  $Home
global nom_elev nom_class
set nom_elev  $tcl_platform(user)
.frame.c   postscript -file "${nom_elev}$date.ps"
# -height 20c -width 20c  -file "$nom_elev.ps"
cd $basedir
}