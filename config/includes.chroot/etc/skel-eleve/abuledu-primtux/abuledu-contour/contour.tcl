#!/bin/sh
#contour.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 André Connes <andre.connes@wanadoo.fr>
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
#**************************************************************************
#  File  : contour.tcl
#  Author  : André Connes <andre.connes@wanadoo.fr>
#  Modifier:
#  Date    : 15/04/2003 modifié le 06/12/2004
#  Licence : GNU/GPL Version 2 ou plus
# 
#  Description:
#  ------------
# 
#  @version    $Id: contour.tcl,v 1.16 2006/03/25 00:55:15 abuledu_francois Exp $
#  @author     André Connes
#  @modifier   
#  @project    Le terrier
#  @copyright  André Connes
# 
#***********************************************************************

source contour.conf
source msg.tcl
source aider.tcl
source fonts.tcl
source lanceapplication.tcl

set glob(couleurs) [list noir jaune rouge vert bleu]

#########################################
#
# reglages de l'espace de l'utilisateur
#
#########################################

  #
  # respecter les reglages de l'eleve
  #
  if {![file exists [file join  $glob(home_contour)]]} {
    catch { file mkdir [file join $glob(home_contour)]}
    catch { file mkdir [file join $glob(home_reglages)] }
    foreach f [list lang charsize couleur depart progression dir_images mode_souris] {
      file copy [file join [pwd] reglages $f.conf] [file join $glob(home_reglages) $f.conf]
    }
  }
  # pour ne pas bugger si ancienne version (voir ci-dessous)
  if {![file exists [file join  $glob(home_reglages) a_regler.conf]]} {
    catch {
      file copy [file join [pwd] reglages a_regler.conf] [file join $glob(home_reglages) a_regler.conf]
    }
  }
  
  #
  # respecter les reglages de l'eleve : traitement equivalent a touch
  #
  catch {
    set f [open [file join [pwd] reglages a_regler.conf] "r"]
    gets $f aReglerPROF
    close $f
    set f [open [file join $glob(home_reglages) a_regler.conf] "r"]
    gets $f aReglerUSER
    close $f
  
    if { $aReglerUSER < $aReglerPROF } {
      foreach f [list lang charsize couleur depart progression dir_images mode_souris a_regler] {
        file copy -force [file join [pwd] reglages $f.conf] [file join $glob(home_reglages) $f.conf]
      }
    }
  }
  
  # langue  
  set f [open [file join $glob(home_reglages) lang.conf] "r"]
  gets $f lang
  close $f

  ::msgcat::mclocale $lang
  ::msgcat::mcload [file join [file dirname [info script]] msgs]

source fin_sequence.tcl

  # taille des numeros par defaut
  set f [open [file join $glob(home_reglages) charsize.conf] "r"]
  gets $f glob(charSize)
  close $f
  # couleur des numeros et des traits
  set f [open [file join $glob(home_reglages) couleur.conf] "r"]
  gets $f glob(couleur)
  close $f
  # numero de depart
  set f [open [file join $glob(home_reglages) depart.conf] "r"]
  gets $f glob(depart)
  close $f
  # pas de progression
  set f [open [file join $glob(home_reglages) progression.conf] "r"]
  gets $f glob(progression)
  close $f
  # dossier images
  set f [open [file join $glob(home_reglages) dir_images.conf] "r"]
  gets $f glob(theme)
  close $f
  # mode de selection des points a la souris
  set f [open [file join $glob(home_reglages) mode_souris.conf] "r"]
  gets $f glob(modeSouris)
  close $f

  # astuce suivante : ajoute automatiquement toute nouvelle langue chez l'utilisateur
#  catch { file mkdir [file join $glob(home_msgs)] }
#  foreach f [glob [file join msgs *.msg]] {
#	catch { file copy [file join $f] [file join $glob(home_msgs)]}
#  }
  #
  # Etat des boutons des menus
  #
  set glob(etat_boutons) normal

  #set nom_utilisateur defaut
  #set classe defaut
  
  set glob(etat_boutons) normal

##################################################################################

proc setlang {lang} {
  global env glob
  set env(LANG) $lang
  catch {set f [open [file join $glob(home_reglages) lang.conf] "w"]}
  puts $f [file rootname [file tail $lang]]
  close $f
}

##########################################################

proc tailleNumeros { t } {
  global glob
  set f [open [file join $glob(home_reglages) charsize.conf] "w"]
  puts $f $t
  close $f
  set glob(charSize) $t
}

##########################################################

proc epaisseurTraits { e } {
  global glob
  set f [open [file join $glob(home_reglages) epaisseur.conf] "w"]
  puts $f $e
  close $f
  set glob(epaisseur) $e
} ;# epaisseurTraits 

##########################################################

proc color { c } {
  # traduction fr <-> en
  global glob
  set colors [list black yellow red green blue]
  set i [lsearch $glob(couleurs) $c]
  set color [lindex $colors $i]
  return $color
}

proc couleurNumerosTraits { c } {
  global glob
  set f [open [file join $glob(home_reglages) couleur.conf] "w"]
  puts $f $c
  close $f
  set glob(couleur) $c
} ;# couleurNumerosTraits 

##########################################################

proc ok_nouveauDepart { } {
  global glob var_depart
  # valeur numerique attendue
  if {[catch {expr $var_depart} r]} {
    set depart 1
  } else {
    set depart $r
  }
  set f [open [file join $glob(home_reglages) depart.conf] "w"]
  puts $f $depart
  close $f
  set glob(depart) $depart
}

proc nouveauDepart { } {
  destroy .top_taille
  set tt [toplevel .top_depart]
  raise .top_depart
  wm title $tt [mc "NouveauDepart"]
  wm geometry $tt 420x140+380+560
  entry $tt.ent_depart -width 5 -textvariable var_depart
  label $tt.lab_valide -text "[mc Valider_svp]"
  focus $tt.ent_depart
  $tt.ent_depart delete 0 end
  bind $tt.ent_depart <Return> "ok_nouveauDepart; destroy .top_depart"
  bind $tt.ent_depart <KP_Enter> "ok_nouveauDepart; destroy .top_depart"
  pack $tt.ent_depart -padx 20 -pady 30
  pack $tt.lab_valide -padx 20 -pady 5
} ;# nouveauDepart 

##########################################################

proc ok_pasProgression { } {
  global glob var_progression
  # valeur numerique attendue
  if {[catch {expr $var_progression} r]} {
    set progression 1
  } else {
    set progression $r
  }
  set f [open [file join $glob(home_reglages) progression.conf] "w"]
  puts $f $progression
  close $f
  set glob(progression) $progression
}

proc pasProgression { } {
  destroy .top_taille
  set tt [toplevel .top_progression]
  raise .top_progression
  wm title $tt [mc "PasProgression"]
  wm geometry $tt 420x140+380+560
  entry $tt.ent_progression -width 5 -textvariable var_progression
  label $tt.lab_valide -text "[mc Valider_svp]"
  focus $tt.ent_progression
  $tt.ent_progression delete 0 end
  bind $tt.ent_progression <Return> "ok_pasProgression; destroy .top_progression"
  bind $tt.ent_progression <KP_Enter> "ok_pasProgression; destroy .top_progression"
  pack $tt.ent_progression -padx 20 -pady 30
  pack $tt.lab_valide -padx 20 -pady 5
} ;# pasProgression 

##########################################################

proc setsouris { ms } {
  global glob
  set f [open [file join $glob(home_reglages) mode_souris.conf] "w"]
  puts $f $ms
  close $f
}

##########################################################

proc setdossier { d } {
  global glob
  set f [open [file join $glob(home_reglages) dir_images.conf] "w"]
  puts $f $d
  close $f
}

#########################################################"

proc choisir_theme { } {
  tk_messageBox -type ok -message [mc "Choisir une thème dans Réglages - Dossiers-Images"]
}

#########################################################"

proc main_loop {} {
  global . glob maxcolonnes env

  #
  # Creation du menu utilise comme barre de menu:
  #
  catch {destroy .menu}
  catch { destroy .frame.c}
  catch {destroy .frame}
  menu .menu -tearoff 0

  #
  # Creation du menu Fichier
  #
  menu .menu.fichier -tearoff 0
  .menu add cascade -state $glob(etat_boutons) \
	-label [mc Fichier] -menu .menu.fichier

  set etat_fichier "normal"

  .menu.fichier add command -label [mc Quitter] -command exit

  #
  # Creation du menu Images
  #
  menu .menu.images -tearoff 0
  .menu add cascade -label [mc Images] -menu .menu.images

  if { $glob(theme) == "Pas de thème" } {
    .menu.images add radio -label "Pas de thème" -command "choisir_theme"
  } else {  
    catch {
      foreach i [lsort [glob [file join images $glob(theme) *.jpg]]] {
        set nom_image [file rootname [file tail $i]]
        if { [file exists [file join [pwd] points $glob(theme) $nom_image]] } { 
          .menu.images add radio -label [file tail $i] -command "lanceappli contour_tracer.tcl $glob(theme) [file tail $i]"
        }
      }
    }
    catch {
      foreach i [lsort [glob [file join images $glob(theme) *.png]]] {
        set nom_image [file rootname [file tail $i]]
        if { [file exists [file join [pwd] points $glob(theme) $nom_image]] } { 
          .menu.images add radio -label [file tail $i] -command "lanceappli contour_tracer.tcl  $glob(theme) [file tail $i]"
        }
      }
    }
    catch {
      foreach i [lsort [glob [file join images $glob(theme) *.gif]]] {
        set nom_image [file rootname [file tail $i]]
        if { [file exists [file join [pwd] points $glob(theme) $nom_image]] } { 
          .menu.images add radio -label [file tail $i] -command "lanceappli contour_tracer.tcl  $glob(theme) [file tail $i]"
        }
      }
    }
  }  
  
  #
  # Creation du menu Reglages
  #
  menu .menu.reglages -tearoff 0
  .menu add cascade -state $glob(etat_boutons) \
	-label [mc Reglages] -menu .menu.reglages

  set etat_reglages "normal"

  # dossiers images
  menu .menu.reglages.dir_images -tearoff 0 
  .menu.reglages add cascade -label "[mc Dossiers_images]" -menu .menu.reglages.dir_images

  foreach d [lsort [glob [file join [pwd] images *]]] {
    if { [file isdirectory $d] } {
      set theme [file rootname [file tail $d]]
      if { [file isdir [file join [pwd] points $theme]] } {
        .menu.reglages.dir_images add radio -label $theme -command "setdossier $theme; lanceappli contour.tcl 0"
      }
    }
  }
  
  # taille des numeros
  menu .menu.reglages.numeros -tearoff 0
  .menu.reglages add cascade -label [mc "TailleNumeros"] -menu .menu.reglages.numeros

     foreach i [list 10 12 14 16] {
      .menu.reglages.numeros add radio -label $i -variable i -command "tailleNumeros $i"
    }  
  # epaisseur des traits
  menu .menu.reglages.epaisseur -tearoff 0
  .menu.reglages add cascade -label [mc "EpaisseurTraits"] -menu .menu.reglages.epaisseur

     foreach i [list 1 2 3 4 5] {
      .menu.reglages.epaisseur add radio -label $i -variable i -command "epaisseurTraits $i"
    }  
  
  # couleur des numeros et des traits
  menu .menu.reglages.couleur -tearoff 0
  .menu.reglages add cascade -label [mc "CouleurNumerosTraits"] -menu .menu.reglages.couleur

     foreach i $glob(couleurs) {
      .menu.reglages.couleur add radio -label $i -variable i -command "couleurNumerosTraits $i"
    }  
  
  # translation du depart
  .menu.reglages add command -label [mc "NouveauDepart"] -command "nouveauDepart"
  
  # pas de progression
  .menu.reglages add command -label [mc "PasProgression"] -command "pasProgression"
  
  # souris
  menu .menu.reglages.souris -tearoff 0 
  .menu.reglages add cascade -label "[mc Souris]" -menu .menu.reglages.souris

    set ms "Survoler"
    .menu.reglages.souris add radio -label $ms -variable ms -command "setsouris $ms; lanceappli contour.tcl 0"
    set ms "Cliquer"
    .menu.reglages.souris add radio -label $ms -variable ms -command "setsouris $ms; lanceappli contour.tcl 0"
    set ms "Double-cliquer"
    .menu.reglages.souris add radio -label $ms -variable ms -command "setsouris $ms; lanceappli contour.tcl 0"
    set ms "Pas-de-souris"
    .menu.reglages.souris add radio -label $ms -variable ms -command "setsouris $ms; lanceappli contour.tcl 0"

  # langues
  menu .menu.reglages.lang -tearoff 0 
  .menu.reglages add cascade -label "[mc Langue]" -menu .menu.reglages.lang

  foreach i [glob [file join  [pwd] msgs *.msg]] {
    set langue [string map {.msg "" } [file tail $i]]
    .menu.reglages.lang add radio -label $langue -variable langue -command "setlang $langue; lanceappli contour.tcl 0"
  }

  #
  # Creation du menu aide
  #
  menu .menu.aide -tearoff 0
  .menu add cascade -state $glob(etat_boutons) \
	-label [mc Aide] -menu .menu.aide
  set l_langues [glob  [file join [pwd] aides aide.*.html]]
  foreach langue $l_langues {
    set lang [lindex [split $langue "."] end-1]
    .menu.aide add command -label "[mc Aide] $lang" -command "aider $lang"
  }
  .menu.aide add command -label [mc {A_propos ...}] -command "source apropos.tcl"

  . configure -menu .menu

  #######################################################################"
  . configure -background blue
  frame .frame -background blue
  pack .frame -side top -fill both -expand yes
  ###################On crée un canvas####################################

  set c .frame.c
  canvas $c -bg blue -highlightbackground blue
  pack $c -expand true

  set nligne 0
  set ncolonne 0

  set myimage [image create photo -file sysdata/background.png]
  label $c.imagedisplayer -image $myimage -background blue
  grid $c.imagedisplayer -column 3 -row 0 -sticky e

} ;# fin main_loop

########################################################################"

  # Nom de l'utilisateur par défaut sous windows
  if {$glob(platform) == "windows"} {
    set nom eleve
    # sauver le réglage du nom
    catch {set f [open [file join $glob(home_contour) reglages trace_user] "w"]}
    puts $f "$glob(trace_dir)/$nom"
    close $f
  }

bind . <Control-q> {exit}

wm geometry . 400x400+0+0
. configure -background blue
wm title . "[mc title] : $glob(theme) ($glob(modeSouris))"

main_loop
