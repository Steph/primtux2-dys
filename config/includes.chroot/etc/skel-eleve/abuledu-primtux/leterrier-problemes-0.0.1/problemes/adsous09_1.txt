set etapes 1
set niveaux {0 1 2 3 5}
::1
set niveau 2
set ope {{2 8} {2 8} {2 8}}
set interope {{1 15 1} {1 15 1} {1 15 1}}
set ope1 [expr int(rand()*[lindex [lindex $ope 0] 1]) + [lindex [lindex $ope 0] 0]]
set ope2 [expr int(rand()*[lindex [lindex $ope 1] 1]) + [lindex [lindex $ope 1] 0]]
set ope3 [expr int(rand()*[lindex [lindex $ope 2] 1]) + [lindex [lindex $ope 2] 0]]
set volatil 1
set operations [list [list [expr $ope1 ]+[expr $ope2]+[expr $ope3]] [list [expr $ope1 ]+[expr $ope3]+[expr $ope2]] [list [expr $ope2]+[expr $ope1]+[expr $ope3]] [list [expr $ope2]+[expr $ope3]+[expr $ope1]] [list [expr $ope3]+[expr $ope1]+[expr $ope2]] [list [expr $ope3]+[expr $ope2]+[expr $ope1]] [list [expr $ope1]+[expr $ope2 + $ope3]] [list [expr $ope2 + $ope3]+[expr $ope1]] [list [expr $ope1 + $ope2]+[expr $ope3]] [list [expr $ope3]+[expr $ope1 + $ope2]] [list [expr $ope2]+[expr $ope3 + $ope2]] [list [expr $ope3 + $ope2]+[expr $ope2]]]
set enonce "Les fruits.\nTim a $ope1 fruits.\nTom en a $ope2 et Vincent $ope3 .\nCombien ont-ils de fruits ensemble?"
set cible [list [list 5 4 {} source0] [list 5 4 {} source0] [list 5 4 {} source0]]
set intervalcible 20
set taillerect 45
set orgy 40
set orgxorig 10
set orgsourcey 80
set orgsourcexorig 670
set source {poire.gif}
set orient 1
set labelcible {Tim Tom Vincent}
set quadri 0
set reponse [list [list {1} [list {Ils ont} [expr $ope1 + $ope2 + $ope3] {fruits ensemble.}]]]
set ensembles [list $ope1 $ope2  $ope3]
set canvash 350
set c1height 160
set opnonautorise {}
::




