#!/bin/sh
#incomplet.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
#**************************************************************************
#  File  : $$$
#  Author  : davidlucardi@aol.com
#  Modifier: 
#  Date    : 
#  Licence : GNU/GPL Version 2 ou plus
# 
#  Description:
#  ------------
# 
#  @version    
#  @author     David Lucardi
#  @modifier   
#  @project    Le terrier
#  @copyright  Eric Seigne 
# 
#  *************************************************************************
global sysFont nbreu essais auto couleur aide longchamp xcol ycol listevariable categorie startdirect user strbilan Homeconf repertoire tabaide tablistevariable tablongchamp tabstartdirect Home initrep tableauindices2 baseHome essaiseval lecture_mot lecture_mot_cache tablecture_mot tablecture_mot_cache

source menus.tcl
source parser.tcl
source eval.tcl
source fonts.tcl
source path.tcl
source msg.tcl
source compagnon.tcl

proc cancelkey {A} {
set A ""
#focus .
}

#variables
#nbreu : nombre d'items effectues
#essais : total des essais effectues
#auto : flag de detection du mode de fonctionnement 
#couleur : couleur associ�e � l'exercice
#aide : pr�cise � quel moment doit intervenir l'aide
#longchamp : pr�cise si les champs sont de longueur variable

set nbreu 0
set essais 0
set essaiseval 0
set auto 0
set couleur purple
set aide 2
set longchamp 1
set xcol 0
set ycol 0
set listevariable 1
set categorie ""
set startdirect 1
set strbilan ""

set filuser [lindex $argv 1]
set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)
if {$plateforme == "unix"} {set ident $env(USER)}
initlog $plateforme $ident
inithome

#interface
. configure -background black -width 640 -height 480
wm geometry . +52+0


frame .menu -height 40
pack .menu -side bottom -fill both

button .menu.b1 -image [image create photo final -file [file join sysdata debut.gif]] -command "main .text"
pack .menu.b1 -side right
button .menu.bb1 -image [image create photo speak -file [file join sysdata speak.gif]] -command "litout"
pack .menu.bb1 -side right

tux_commence


text .text -yscrollcommand ".scroll set" -setgrid true -width 55 -height 17 -wrap word -background white -font $sysFont(l)
scrollbar .scroll -command ".text yview"
pack .scroll -side right -fill y
pack .text -expand yes -fill both
#recup�ration des options de reglages
catch {
set f [open [file join $baseHome reglages $filuser] "r"]
set categorie [gets $f]
set repertoire [gets $f]
set aller [gets $f]
set aller_incomplet [lindex $aller 6]
close $f
set aide [lindex $aller_incomplet 0]
set longchamp [lindex $aller_incomplet 1]
set listevariable [lindex $aller_incomplet 2]
#set categorie [lindex $aller_incomplet 3]
set startdirect [lindex $aller_incomplet 4]
set lecture_mot [lindex $aller_incomplet 5]
set lecture_mot_cache [lindex $aller_incomplet 6]
}

set initrep [file join $Home textes $repertoire]

#chargement du texte avec d�tection du mode
set auto [charge .text [file join $Home textes $repertoire $categorie]]

bind .text <ButtonRelease-1> "lire"
bind .text <Any-Enter> ".text configure -cursor target"
bind .text <Any-Leave> ".text configure -cursor left_ptr"

bind . <KeyPress> "cancelkey %A"

if {$auto == 1} {
set aide $tabaide(incomplet)
set longchamp $tablongchamp(incomplet)
set listevariable $tablistevariable(incomplet)
set startdirect $tabstartdirect(incomplet)
set lecture_mot $tablecture_mot(incomplet)
set lecture_mot_cache $tablecture_mot_cache(incomplet)
}

#focus .text
#.text configure -state disabled

wm title . "[mc {Exercice}] $categorie - [lindex [lindex $listexo 6] 1]"
label .menu.titre -text "[lindex [lindex $listexo 6] 1] - [mc {Observe}]" -justify center
pack .menu.titre -side left -fill both -expand 1


proc main {t} {
#liste principale de phrases contenant les mots sans ponctuation, liste de mots � cacher
#listessai : tableau pour tenir � jour les essais sur chaque mot
#texte : le texte initial
#longmot : longueur maximale des champs de texte

global sysFont plist listemotscaches listessai texte nbmotscaches auto longchamp longmot iwish aide user startdirect lecture_mot_cache
set nbmotscaches 0
#catch {destroy .menu.b1}
#button .menu.b1 -text [mc {Fin}] -command "fin"
#button .menu.b1 -image [image create photo final -file [file join sysdata fin.gif]] -command "fin"
#pack .menu.b1 -side right
.menu.b1 configure -image [image create photo final -file [file join sysdata fin.gif]] -command "fin"

bind .text <ButtonRelease-1> ""
bind .text <Any-Enter> ""
bind .text <Any-Leave> ""

    if {$lecture_mot_cache == "0" } {
catch {destroy .menu.bb1} 
}

$t configure -state normal
set what [mc {Complete la phrase avec le mot convenable.}]

if {$startdirect == 0} {set what "[format [mc {Bonjour %1$s .}] [string map {.log \040} [file tail $user]]] $what"}
tux_exo $what

# S�lection du mode auto ou manuel pour la g�n�ration de l'exercice
    if {$auto==0} {
    pauto $t
    } else {
    pmanuel $t
    #pauto $t
    }
    $t configure -state disabled -selectbackground white -selectforeground black
}

proc pauto {t} {
global sysFont plist listemotscaches listessai texte nbmotscaches aide longchamp longmot listeaide totalp indexp curseur iwish filuser
    set curseur 0.0
    set texte ""
    set plist [parse $t]
    set indexp 0
set cur 1.0
    while 1 {
      set cur [$t search -regexp -count len {[A-Z]} $cur end]

      if {$cur == ""} {
	    break
	}
      set tmp $cur
      set cur [$t search -regexp -count length {[.!?\012]} $cur end]
	if {$cur == ""} {
	    break
	}
      lappend texte [regexp -inline -all -- {\S+} [$t get $tmp "$cur + 1c"]]
      }
for {set i [expr [llength $texte]-1]} {$i >= 0} {incr i -1} {
if {[llength [lindex $texte $i]] <=1} { set texte [lreplace $texte $i $i] }
}

if {[llength $texte] == 0} {
   set answer [tk_messageBox -message [mc {Erreur de traitement ou texte trop court.}] -type ok -icon info] 
   exec $iwish aller.tcl $filuser &
   exit
   }
    set totalp [expr [llength $texte] -1]

$t delete 1.0 end

goon $t
}

proc goon {t} {
global sysFont plist listemotscaches listessai texte nbmotscaches aide longchamp longmot listeaide total totalp indexp essais nbreu linepos curseur listexo auto listemots tableauindices2 lecture_mot_cache
set listemotscaches ""
set phrase [string map {\\" \" \{ "" \} ""} [lindex $texte $indexp]]
set total 0
set nbreu 0
set essais 0

$t configure -state normal
$t insert end \n
$t insert end "$phrase \n"
#on choisit le mot

if {$auto == 0} {
set pp [string map {. \040 ! \040 ? \040 \{ \040 \} \040 \[ \040 \] \040 \" \040 \\" \040 ' \040 , \040 ; \040 : \040 ( \040 ) \040 - \040}  $phrase]
regsub -all \040+ $pp \040 pp

    set indexmot [expr int(rand()*[llength $pp])]
    lappend listemotscaches [lindex $pp $indexmot]
} else {
    lappend listemotscaches [lindex $listemots $indexp]
     if {$tableauindices2([lindex $listemots $indexp])!= " "} {
     catch {destroy .menu.lab2}
     label .menu.lab2 -text "[mc {Indice :}] $tableauindices2([lindex $listemots $indexp])"

     pack .menu.lab2
          }

}
#################################################################"
#on recherche les mots cach�s dans le texte, on les supprime et on les remplace par une zone de saisie
    set re1 {\m}
    set re2 {\M} 
    #set curseur [$t search [lindex $listemotscaches 0] $curseur end]
    set curseur [$t search -regexp $re1[lindex $listemotscaches 0]$re2 $curseur end]

    $t delete $curseur "$curseur + [string length [lindex $listemotscaches 0]] char"
    entry $t.$indexp\ent0 -font $sysFont(l) -width [expr [string length [lindex $listemotscaches 0]] + 1] -bg yellow
    $t window create $curseur -window $t.$indexp\ent0
    bind $t.$indexp\ent0 <Return> "verif 0 $t"
    set curseur [$t index end]

    set listessai(0) 0
#on ajoute des mots � la liste d'aide
set tmp ""
for {set i 0} {$i < $totalp} {incr i 1} {
set pp [string map {. \040 ! \040 ? \040 \{ \040 \} \040 \[ \040 \] \040 \" \040 \\" \040 ' \040 , \040 ; \040 : \040 ( \040 ) \040 - \040}  [lindex $texte $i]]
set tmp [concat $tmp $pp]
 }
regsub -all \040+ $tmp \040 tmp

set longlist 12
for {set i 0} {$i < $longlist} {incr i 1} {
set index1 [expr int(rand()*[string length $tmp])]
if {[lindex $tmp $index1] != " " && [lindex $tmp $index1] != ""} {
lappend listemotscaches [lindex $tmp $index1]
}
catch {destroy .menu.lab}
catch {destroy .menu.titre}
#label .menu.lab -text "[lindex [lindex $listexo 6] 2] [mc {Phrase}] [expr $indexp + 1] [mc {sur}] $totalp"
label .menu.lab -text "[lindex [lindex $listexo 6] 2] [format [mc {Phrase %1$s sur %2$s}] [expr $indexp +1] $totalp]"

pack .menu.lab
}

focus $t.$indexp\ent0
set listeaide $listemotscaches
    if {$aide== 1} {
    afficheaide 0 $t
     }
incr total
$t configure -state disabled
    if {$lecture_mot_cache == "1" } {
.menu.bb1 configure -command "speaktexte [list [lindex $texte $indexp]]" 
}

}


####################################################################################################""
proc pmanuel {t} {
global sysFont plist listemotscaches listessai texte nbmotscaches aide longchamp longmot listeaide totalp indexp couleur texte curseur listemots tableauindices2 auto
    set totalp 0
    set indexp 0
    set curseur 0.0
    set texte ""
    set nbmotscaches 0
######################################################
set listemots ""
set listemotscaches ""

# Construction de la liste des mots � cacher, � partir des tags
    set plist [parse $t]
    set indexp 0

set cur 1.0

set liste [$t tag ranges $couleur]
    for {set i 0} {$i < [llength $liste]} {incr i 2} {
    set str [$t get [lindex $liste $i] [lindex $liste [expr $i + 1]]]
      set cur [$t search -regexp -count len {[A-Z]} $cur end]

      set cur [$t search -backwards -regexp -count length {[\n.!?\012]} [lindex $liste [expr $i + 1]]]
      if {$cur == ""} {
	    set cur 1.0
	}
      set tmp $cur
      set cur [$t search -forwards -regexp -count length {[.!?\012]} [lindex $liste $i]]
	if {$cur == ""} {
	    set cur [$t index end]
	}
      set sentence [regexp -inline -all -- {\S+} [$t get "$tmp+ 1c" "$cur + 1c"]]
      if {[lindex $texte end] != $sentence} {
      lappend texte $sentence
      lappend listemots $str
      if {[catch {set tmp $tableauindices2($str)}] ==1} {
      set tableauindices2($str) " "
      }
        
      incr nbmotscaches
      }
      }
if {[llength $texte] == 0} {
   #set answer [tk_messageBox -message [mc {Erreur de traitement ou texte trop court.}] -type ok -icon info] 
   #exit
    set auto 0
    pauto $t
    return

   }

     set longmot 0
        foreach mot $listemots {
            if {$longmot < [string length $mot]} {
            set longmot  [string length $mot]
            }
        }
#Si aucun mot n'a �t� masqu�, on repasse en mode auto.
    if {$nbmotscaches == 0} {
    pauto $t
    return
    }
    set totalp [expr [llength $texte]]

$t delete 1.0 end
goon $t

}


proc verif {i t} {
global sysFont listemotscaches listessai nbreu essais aide listeaide listevariable totalp indexp user categorie strbilan disabledfore disabledback essaiseval
incr essais

    if {[lindex $listemotscaches $i] == [$t.$indexp\ent$i get]} {
    incr nbreu
    if {$essais == 1} {tux_content_phrase2} else {tux_continue_bien}
    bind $t.$indexp\ent$i <Return> {}
    catch {destroy .w1}
    $t.$indexp\ent$i configure -state disabled -$disabledfore blue
#######################################################################
    catch {destroy .menu.lab}
    set indc [expr $indexp + 1]
    set str2 [format [mc {N�%1$s : %2$s essai(s) .}] $indc $essais]
    set strbilan $strbilan$str2
    set essaiseval [expr $essaiseval + $essais]
    label .menu.lab -text [format [mc {%1$s essai(s).}] $essais]
    pack .menu.lab
    bell
    update
    after 2000
    if {[incr indexp]>= $totalp} {
    catch {destroy .menu.lab2}
    .menu.lab configure -text [mc {Exercice termine. }]
    set score [expr int((($indexp*100)/($essaiseval))*(($indexp*100)/$totalp)/100)]
    if {$score <50} {tux_triste_final $score}
    if {$score >=50 && $score <75 } {tux_moyen_final $score}
    if {$score >=75} {tux_content_final $score}
    return
    } else {
    .menu.lab configure -text [format [mc {Complete.}] [mc {Phrase %1$s sur %2$s}] [expr $indexp +1] $totalp]
    goon $t
    tux_phrasesuivante
    }

#####################################################################""
    } else {
    $t.$indexp\ent$i delete 0 end
# Affichage de l'aide
	if { $listessai($i)  >= 1} {tux_triste_phrase2} else {tux_moyen_phrase2}
        if {[incr listessai($i)] >= [expr $aide -1] } {
        afficheaide $i $t
        return
        }
    }
affichecouleur $i $t
}



proc afficheaide {ind t} {
global sysFont listeaide listemotscaches listeaide alear xcol ycol indexp
catch {
set xcol [winfo x .w1]
set ycol [winfo y .w1]
destroy .w1
}
toplevel .w1
.w1 configure -width 300 -height 200
#wm geometry .w1 +$xcol+$ycol
wm geometry .w1 -0-0
wm transient .w1 .
text .w1.text1 -yscrollcommand ".w1.scrolly set" -width 20 -height 10 -setgrid true -wrap word -background white -font $sysFont(l) -selectbackground white -selectforeground black
scrollbar .w1.scrolly -command ".w1.text1  yview"
pack .w1.scrolly -side right -fill y
pack .w1.text1 -expand yes -fill both
set tmpliste {}
wm title .w1 [mc {Clique sur une reponse}]
    foreach tmp $listeaide {
        if {[lsearch $tmpliste $tmp]==-1} {
        lappend tmpliste $tmp
        }
    }
set lntmpliste [llength $tmpliste]
    for {set i 0} {$i < $lntmpliste} {incr i 1} {
    set tab($i) [lindex $tmpliste $i]
    set alear($i) $i
    }

  for {set i 0} {$i < [expr $lntmpliste*2]} {incr i 1} {
  set t1 [expr int(rand()*$lntmpliste)]
  set t2 [expr int(rand()*$lntmpliste)]
  set temp $alear($t1)
  set alear($t1) $alear($t2)
  set alear($t2) $temp
  }

    for {set i 0} {$i < $lntmpliste} {incr i 1} {
    label .w1.text1.lab$i -text [lindex $tmpliste $alear($i)] -background yellow -font $sysFont(l)

    .w1.text1 window create current -window .w1.text1.lab$i
    .w1.text1 insert current \040\040\040\040
    bind .w1.text1.lab$i <1> "verifaide \173$tab($alear($i))\175 $i $ind $t \173[lindex $listemotscaches $ind]\175"
    }
    bind .w1 <Destroy> "affichecouleur $ind $t"
    if {[$t.$indexp\ent$ind cget -state] != "disabled"} {
    $t.$indexp\ent$ind configure -background white
    }
}

proc affichecouleur {ind t} {
global sysFont listessai xcol ycol indexp disabledfore disabledback
    catch {
    set xcol [winfo x .w1]
    set ycol [winfo y .w1]
    }
switch $listessai($ind) {
    0 { $t.$indexp\ent$ind configure -$disabledback yellow -bg yellow}
    1 { $t.$indexp\ent$ind configure -$disabledback green -bg green}
    default { $t.$indexp\ent$ind configure -$disabledback red -bg red}
    }
}

proc verifaide {rep i ind t mot} {
    global sysFont essais nbreu listemotscaches aide listessai listeaide alear xcol ycol listevariable indexp totalp user categorie total indexp strbilan disabledfore essaiseval
    incr essais

    if {$rep == $mot} {
    incr nbreu
    if {$essais == 1} {tux_content_phrase2} else {tux_continue_bien}
    bind $t.$indexp\ent$ind <Return> {}
    $t.$indexp\ent$ind delete 0 end
    $t.$indexp\ent$ind insert end $mot
    $t.$indexp\ent$ind configure -state disabled -$disabledfore blue
    catch {destroy .w1}
######################################################################"
#######################################################################
    catch {destroy .menu.lab}
    set indc [expr $indexp + 1]

    set str2 [format [mc {%1$s essai(s).}] $essais]
    set str2 [format [mc {N�%1$s : %2$s essai(s) .}] $indc $essais]
   set essaiseval [expr $essaiseval + $essais]

    set strbilan $strbilan$str2

    #set str1 [mc {Exercice termine. }]
    #enregistreval $str1 $categorie $str2 $user

    label .menu.lab -text [format [mc {%1$s essai(s).}] $essais]
    pack .menu.lab
    bell
    update
    after 2000
    if {[incr indexp]>= $totalp} {
    catch {destroy .menu.lab2}
    .menu.lab configure -text [mc {Exercice termine. }]
set score [expr int((($indexp*100)/($essaiseval))*(($indexp*100)/$totalp)/100)]
if {$score <50} {tux_triste_final $score}
if {$score >=50 && $score <75 } {tux_moyen_final $score}
if {$score >=75} {tux_content_final $score}

    } else {
    .menu.lab configure -text [format [mc {Complete.}] [mc {Phrase %1$s sur %2$s}] [expr $indexp +1] $totalp]
    tux_phrasesuivante
    goon $t
    }

#####################################################################""


###################################################################"
    } else {
    .w1.text1.lab$i configure -bg red
    if { $listessai($ind)  >= 1} {tux_triste_phrase2} else {tux_moyen_phrase2}
    incr listessai($ind)
    affichecouleur $ind $t
    }
}


if {$startdirect == 0 } {
main .text
}

proc fin {} {
global sysFont categorie user strbilan listexo essaiseval iwish nbreu totalp totaleval indexp filuser aide startdirect repertoire lecture_mot_cache

variable repertconf
if {$essaiseval != 0} {
set score [expr int((($indexp*100)/($essaiseval))*(($indexp*100)/$totalp)/100)]

} else {
set score 0
}
set str1 [mc {Exercice Phrases incompletes}]

switch $startdirect {
1 {set startconf [mc {Le texte est visible au debut}]}
0 {set startconf [mc {Le texte n'est pas visible au debut}]}
}

switch $aide {
1 { set aideconf [mc {Au debut}]}
2 { set aideconf [mc {Apres le premier essai}]}
3 { set aideconf [mc {Apres le deuxieme essai}]}
}

switch $lecture_mot_cache {
1 { set lectmotcacheconf [mc {Les phrases peuvent �tre entendues.}]}
0 { set lectmotcacheconf [mc {Les phrases ne peuvent pas �tre entendues.}]}
}

set exoconf [mc {Parametres :}]
set exoconf "$exoconf $startconf - "
set exoconf "$exoconf Aide : $aideconf"
set exoconf "$exoconf  Son : $lectmotcacheconf"

enregistreval $str1\040[lindex [lindex $listexo 6] 1] \173$categorie\175 $strbilan $score $repertconf 6 $user $exoconf $repertoire
exec $iwish aller.tcl $filuser &
exit
}

proc boucle {} {
global categorie startdirect listexo auto listemotscaches user strbilan
set str1 [mc {Exercice Phrases incompletes}]
enregistreval $str1 \173$categorie\175 $strbilan $user
set strbilan ""
set listexo ""
set listemotscaches ""
set categorie "Au choix"
.text configure -state normal
set auto [charge .text $categorie]
.text configure -state disabled
focus .text
.menu.b1 configure -text [mc {Commencer}] -command "main .text"
catch {destroy .menu.suiv}
catch {destroy .menu.lab}
if {$startdirect == 0 } {
main .text
}
}



