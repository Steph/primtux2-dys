#!/bin/sh
#oeuf.tcl
#Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

############################################################################
# Copyright (C) 2006 David Lucardi
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : parachute.tcl
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 26/08/2006
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version $Id: oeuf.tcl,v 1.3 2006/12/02 07:02:19 david Exp $
# @author     David Lucardi
# @project
# @copyright  David Lucardi 26/08/2006
#
#
#########################################################################
source fonts.tcl
source path.tcl
source msg.tcl
source eval.tcl

global Home basedir arg1 niveau listeval user exo
#detail des params par niveau
#{+ {0 5} {1 1} 10 200 1 1}
# operande {nb1} {nb2} nbitems timing complement?(0 ou 1) intervalle


set ident $tcl_platform(user)
set plateforme $tcl_platform(platform)
initlog $plateforme $ident
initapp $plateforme
loadlib $plateforme
inithome
#wm deiconify .

set arg1 [lindex $argv 0]
set niveau [lindex $argv 1]
set exo [lindex $argv 2]


set c .c
canvas $c -width 400 -height 600 
pack $c

set bgn #ffff80
set bgl #ff80c0
#wm overrideredirect . 1
. configure -background #ffff80

proc changeniveau {c what} {
global listeval niveau listdata Home

if {$niveau > 0 && $what == -1} {
incr niveau -1}

if {$niveau < [expr [llength $listdata]] -1 && $what == 1} {
incr niveau 1}
$c itemconfigure niveau -text "[mc {Niveau}]\n[expr $niveau +1] / [llength $listdata]" 
if {$niveau <=3} {
set scene [expr $niveau +1]
} else {
set scene [expr int(rand()*3) +1]
}
set ext .tcl
after cancel jeu $c
destroy $c.text1 $c.but1 $c.but2 $c.but3 $c.but4
source oeuf$scene$ext

}

#wm protocol . WM_DELETE_WINDOW "quitte $c"
####################################################""
#procÚdure principale
#######################################################"

if {$niveau <=3} {
set scene [expr $niveau +1]
} else {
set scene [expr int(rand()*3) +1]
}
set ext .tcl
source oeuf$scene$ext