#!/bin/sh
#contour.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
#**************************************************************************
#  File  : $Id: play_contour.tcl,v 1.9 2006/03/25 00:55:15 abuledu_francois Exp $
#  Author  : andre.connes@wanadoo.fr
#  Date    : 05/01/2003 Modification : 16/02/2004
#  Licence : GNU/GPL Version 2 ou plus
# 
#  Description:
#  ------------
# 
#  @version    
#  @author     Andre Connes
#  @modifier   
#  @project    Le terrier
#  @copyright  Eric Seigne 24/04/2002
# 
#  *************************************************************************

source contour.conf
source msg.tcl
source fin_sequence.tcl
source lanceapplication.tcl

set dossier_images [lindex $argv 0]
set fichier_image [lindex $argv 1]

  #
  # langue par defaut
  #
  set f [open [file join [pwd] reglages lang.conf] "r"]
  gets $f lang
  close $f
  ::msgcat::mclocale $lang
  ::msgcat::mcload [file join [file dirname [info script]] msgs]

  #
  # couleur des numeros et des traits
  #
  set f [open [file join $glob(home_reglages) couleur.conf] "r"]
  gets $f glob(couleur)
  close $f

  #
  # taille des numeros par defaut
  #
  set f [open [file join [pwd] reglages charsize.conf] "r"]
  gets $f glob(charSize)
  close $f

  set sysFont "Courier $glob(charSize) normal"

# Decommenter la ligne suivante si Img est installee
package require Img

####################################################
#
# sauvegarde
#
####################################################

proc sauvegarde { } {
  global glob can_img dossier_images bgimg
  .bframe.but_pouce configure -state disabled
  # sauvegarder les donnees de l'image actuelle
  append glob(list_points) " 0 0" ;# sentinelle
  catch { [file mkdir [file join [pwd] points]] }
  catch { [file mkdir [file join [pwd] points $dossier_images]] }
  set nom_image [file rootname [file tail $bgimg]]
  set f [open [file join [pwd] points $dossier_images $nom_image] "w"] 
  puts $f $glob(list_points)
  close $f
  # afficher les contours
  $can_img delete all
  set lp $glob(list_points)
  while { [string first " 0 0" $lp] > 0 } {
    set fin [string first " 0 0" $lp]
    set list_intermediaire [string range $lp 0 $fin]
    set debut [expr $fin +4]
    set lp [string range $lp $debut end]  
    $can_img create line $list_intermediaire -fill [color $glob(couleur)]
  }
} ;# sauvegarde

####################################################
#
# gommer
#
####################################################

proc gommer { } {
  global glob can_img
  if { [string length $glob(list_points)] > 0 } {
    set llist [split $glob(list_points)]
    set l [llength $llist]
    set x [lindex $llist [expr $l -2]]
    set y [lindex $llist [expr $l -1]]
    set t [lreplace $llist [expr $l -2] [expr $l -1]]
    if { [expr $x + $y] == 0 } {
      set l [llength $t]
      set u [lreplace $t [expr $l -2] [expr $l -1]]
      set t $u
    }
    $can_img delete oval($glob(npoints))
    $can_img delete numero($glob(npoints))
    set glob(list_points) [join $t]
    incr glob(npoints) -1
  }
  # si maintenant plus de point desactiver les boutons
  if { [string length $glob(list_points)] <= 0 } {
    .bframe.but_gomme configure -state disabled
    .bframe.but_discontinu configure -state disabled
    .bframe.but_pouce configure -state disabled
  }
} ;# gommer

####################################################
#
# color
#
####################################################

proc color { c } {
  # traduction fr <-> en
  global glob
  set i [lsearch $glob(couleurs) $c]
  return [lindex $glob(colors) $i]
}

####################################################
#
# saute_trait
#
####################################################

proc saute_trait { } {
  global glob bool_saute
  append glob(list_points) " 0 0"
  .bframe.but_discontinu configure -state disabled
} ;# saute_trait

####################################################
#
# ajouter_points
#
####################################################

proc clicwhat { x y } {
  global glob can_img sysFont
  .bframe.but_gomme configure -state normal
  .bframe.but_discontinu configure -state normal
  .bframe.but_pouce configure -state normal
  append glob(list_points) " $x $y"
  # afficher le numero du point
  incr glob(npoints)
  $can_img create oval [expr $x -0]  [expr $y-0] [expr $x +1] [expr $y +1] \
    -fill [color $glob(couleur)] \
    -tag oval($glob(npoints))
  $can_img create text [expr $x -3] [expr $y -2] -text $glob(npoints) -font $sysFont \
    -fill [color $glob(couleur)] \
    -tag numero($glob(npoints))
}

proc ajouter_points { } {
  global can_img glob cadre bgimage dossier_images fichier_image
  $can_img delete all

  # initialiser la sauvegarde des points
  set glob(npoints) 0
  set glob(list_points) ""
  
  set bgimg $fichier_image
  
  set bgimage [image create photo -file [file join images $dossier_images $bgimg]]

  set hbgimage [image height $bgimage]
  set wbgimage [image width $bgimage]

  $can_img create image \
	[expr $glob(org) + int($wbgimage/2)+1] [expr $glob(org) + int($hbgimage/2)+1] -image $bgimage

  bind $can_img <Button-1> "clicwhat %x %y"

} ;# ajouter_points

# #################################################################################
#
#                                   main
#
# #################################################################################

proc main { can_img } {
  global glob bgimg bgimage dossier_images fichier_image
 
  $can_img delete all

  # afficher l'image sélectionnée
  set bgimg $fichier_image
  if { ! [file isfile [file join [pwd] images $dossier_images $bgimg]] } {
    lanceappli editeur.tcl
  }
  set bgimage [image create photo -file [file join [pwd] images $dossier_images $bgimg]]

  # On utilise des rectangles pour dessiner les cases
  ajouter_points    

} ;# main

#####################################################################################
#
# fenetre principale
#
#      placement de la fenetre en haut et a gauche de l'ecran
#
#####################################################################################
#wm resizable . 0 0
#wm geometry . [expr [winfo screenwidth .]-10]x[expr [winfo screenheight .]-80]+0+0
. configure -bg $glob(bgcolor)
  wm title . "[mc title_ed] : [file rootname $fichier_image]"

frame .frame -bg $glob(bgcolor)
pack .frame -side top -fill both -expand yes

# ################ On cree un canvas image ###########################################
# a gauche

set can_img .frame.can_image
canvas $can_img -width $glob(width) -height $glob(height) -bg $glob(bgcolor) -highlightbackground $glob(bgcolor)
pack $can_img -side left -expand true 

# ############# on cree une frame pour pouce, porte ##################################
# en bas

frame .bframe -bg $glob(bgcolor)
pack .bframe -side bottom -expand true

  button .bframe.but_gomme -image \
    [image create photo fgomme \
    -file [file join sysdata gomme.png]] \
    -state disabled \
    -command "gommer"
  grid .bframe.but_gomme -padx 10 -column 0 -row 1

  button .bframe.but_discontinu -image \
    [image create photo fdiscontinu \
    -file [file join sysdata discontinu.png]] \
    -state disabled \
    -command "saute_trait"

  grid .bframe.but_discontinu -padx 10 -column 2 -row 1

  button .bframe.but_pouce -image \
    [image create photo fpouce \
    -file [file join sysdata pouce.png]] \
    -state disabled \
    -command "sauvegarde"
  grid .bframe.but_pouce -padx 10 -column 4 -row 1

  button .bframe.but_quitter -image \
    [image create photo fquitter \
    -file [file join sysdata quitter_minus.png]] \
    -command "lanceappli editeur.tcl"
  grid .bframe.but_quitter -padx 10 -column 6 -row 1

main $can_img
