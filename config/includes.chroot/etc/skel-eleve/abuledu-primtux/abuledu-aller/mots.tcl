############################################################################
# Copyright (C) 2002 David Lucardi
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : fichier.php
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 26/04/2002
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version
# @author     David Lucardi
# @project
# @copyright  David Lucardi 26/04/2002
#
#
#########################################################################
#!/bin/sh
#mots.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}
global sysFont font2 listdata longlist1 bgl categorie essais auto couleur aide c longchamp startdirect user compt Homeconf repertoire tabaide tablistevariable tablongchamp tabstartdirect Home initrep baseHome lecture_mot lecture_mot_cache tablecture_mot tablecture_mot_cache

source menus.tcl
source parser.tcl
source eval.tcl
source fonts.tcl
source path.tcl
source msg.tcl
source compagnon.tcl

proc cancelkey {A} {
set A ""
#focus .
}
###########################################################
#font2 : police de caract�re
#listdata : donn�es du jeu, dans une liste
#bgl, bgn : couleurs utilis�es
# longlist1 : nombre de mots
#essais nombre de tentatives sur l'item en cours
#categorie : variable pour la categorie

set bgn #ffff80
set bgl #ff80c0
set font2 $sysFont(lb)
set essais 0
set auto 0
set couleur orange
set aide 2
set longchamp 1
set xcol 0
set ycol 0
set listevariable 1
set categorie ""
set startdirect 1
set compt 0

set filuser [lindex $argv 1]

set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)
if {$plateforme == "unix"} {set ident $env(USER)}
initlog $plateforme $ident
inithome

#interface

set c .frame.c

. configure -background $bgn


frame .menu -height 40
pack .menu -side bottom -fill both
#bind .menu  <Destroy> "fin"

button .menu.b1 -image [image create photo final -file [file join sysdata debut.gif]] -command "main"
#button .menu.b1 -text [mc {Commencer}] -command "main"
pack .menu.b1 -side right
button .menu.bb1 -image [image create photo speak -file [file join sysdata speak.gif]] -command "litout"
pack .menu.bb1 -side right
tux_commence

frame .barre -width 40
pack .barre -side left -fill both

text .text -yscrollcommand ".scroll set" -setgrid true -width 55 -height 17 -wrap word -background white -font $sysFont(l)
scrollbar .scroll -command ".text yview"
pack .scroll -side right -fill y
pack .text -expand yes -fill both



#ouverture du fichier de configuration pour r�cup�rer des variables
catch {
#set f [open [file join $baseHome reglages mot.conf] "r"]
#set aller [gets $f]
#close $f
#set aide [lindex $aller 0]
#set longchamp [lindex $aller 1]
#set listevariable [lindex $aller 2]
#set categorie [lindex $aller 3]
#set startdirect [lindex $aller 4]
set f [open [file join $baseHome reglages $filuser] "r"]
set categorie [gets $f]
set repertoire [gets $f]
set aller [gets $f]
set aller_mots [lindex $aller 3]
close $f
set aide [lindex $aller_mots 0]
set longchamp [lindex $aller_mots 1]
set listevariable [lindex $aller_mots 2]
#set categorie [lindex $aller_mots 3]
set startdirect [lindex $aller_mots 4]
set lecture_mot [lindex $aller_mots 5]
set lecture_mot_cache [lindex $aller_mots 6]
}

set initrep [file join $Home textes $repertoire]

#chargement du texte avec d�tection du mode
set auto [charge .text [file join $Home textes $repertoire $categorie]]
bind .text <ButtonRelease-1> "lire"
bind .text <Any-Enter> ".text configure -cursor target"
bind .text <Any-Leave> ".text configure -cursor left_ptr"

bind . <KeyPress> "cancelkey %A"
if {$auto == 1} {
set aide $tabaide(mot)
set longchamp $tablongchamp(mot)
set listevariable $tablistevariable(mot)
set startdirect $tabstartdirect(mot)
set lecture_mot $tablecture_mot(mot)
set lecture_mot_cache $tablecture_mot_cache(mot)
}

focus .text
.text configure -state disabled -selectbackground white -selectforeground black

wm title . "[mc {Exercice}] $categorie - [lindex [lindex $listexo 3] 1]"
label .menu.titre -text "[lindex [lindex $listexo 3] 1] - [mc {Observe}]" -justify center
pack .menu.titre -side left -fill both -expand 1
wm geometry . +52+0
#############################""""""
proc main {} {
global sysFont listdata categorie listerep c bgn essais auto couleur aide listaide listexo iwish user startdirect

set listdata {}

    if {$auto==0} {
    pauto .text
    } else {
    pmanuel .text
    }


catch {destroy .text}
catch {destroy .scroll}
#.menu.b1 configure -text [mc {Fin}] -command "fin"
.menu.b1 configure -image [image create photo final -file [file join sysdata fin.gif]] -command "fin"

#button .menu.suiv -text [mc {Autre texte}] -command "boucle"
#pack .menu.suiv -side right




button .barre.ok -image [image create photo imagbut -file [file join sysdata ok.gif] ] -command "verif $c"
pack .barre.ok -side bottom

    catch {destroy .menu.lab}
    catch {destroy .menu.titre}
    label .menu.lab -text [lindex [lindex $listexo 3] 2] -justify center
#[mc {Remets les mots en ordre en les d�placant sur les traits roses.}]
    pack .menu.lab -side left -fill both -expand 1

set what [mc {Remets les mots en ordre en les d�pla�ant sur les traits roses, puis valide en appuyant sur le bouton pouce.}]
if {$startdirect == 0} {set what "[format [mc {Bonjour %1$s .}] [string map {.log \040} [file tail $user]]] $what"}
tux_exo $what
wm geometry .wtux +200+340


frame .frame -width 640 -height 380 -background $bgn
pack .frame -side top -fill both -expand yes
#wm geometry . +52+0

scrollbar .frame.vscroll -command "$c yview"
canvas $c -scrollregion {0c 0c 50c 25c} -width 640 -height 380 -background $bgn -highlightbackground $bgn -yscrollcommand ".frame.vscroll set"




pack .frame.vscroll -side right -fill y
pack $c -expand yes -fill both

#set str [string map {' \040 , \040 ; \040 : \040 \( \040 \) \040 - \040} $listdata]
set str [string map {\\ \040} $listdata]
regsub -all \040+ $listdata \040 listdata
regsub -all {[\{\}\(\)\\\"]} $listdata "" listdata


set listaide $str
set listerep $listdata
set leng [llength $listdata]

#on m�lange la liste
for {set i 1} {$i <= $leng} {incr i 1} {
  set t1 [expr int(rand()*$leng)]
  set t2 [expr int(rand()*$leng)]
  set tmp [lindex $listdata $t1]
  set listdata [lreplace $listdata $t1 $t1 [lindex $listdata $t2]]
  set listdata [lreplace $listdata $t2 $t2 $tmp]
  }
set nb $leng
	#regsub -all {[.!,;:?]} $listdata "" listdata

#On place l'image et ses �tiquettes de syllabes sur le canevas
place $c

}


############################################################################"
proc pauto {t} {
global sysFont listdata iwish filuser 
set cur 1.0
set i 0
set plist {}

    while 1 {
      set cur [$t search -regexp -count len {[A-Z]} $cur end]
      if {$cur == ""} {
	    break
	}
      set tmp $cur
      set cur [$t search -regexp -count length {[.!?\012]} $cur end]
	if {$cur == ""} {
	    break
	}
      set str [$t get $tmp "$cur + 1c"]
      regsub -all \" $str \\\" str

      lappend plist $str
   }

for {set i [expr [llength $plist]-1]} {$i >= 0} {incr i -1} {
if {[llength [lindex $plist $i]] <=1} { set plist [lreplace $plist $i $i] }
}


#on m�lange les phrases
set leng [llength $plist]

   if {[llength $plist] < 1} {
   set answer [tk_messageBox -message [mc {Erreur de traitement ou texte trop court.}] -type ok -icon info] 
   exec $iwish aller.tcl $filuser &
   exit
   }


for {set j 0} {$j <= [expr int(rand()*3)]} {incr j 1} {
for {set i 1} {$i <= $leng} {incr i 1} {
  set t1 [expr int(rand()*$leng)]
  set t2 [expr int(rand()*$leng)]
  set tmp [lindex $plist $t1]
  set plist [lreplace $plist $t1 $t1 [lindex $plist $t2]]
  set plist [lreplace $plist $t2 $t2 $tmp]
  }
}
set listdata [lindex $plist 0]
}

#######################################################################"
proc pmanuel {t} {
global sysFont couleur listdata
set plist {}

set liste [$t tag ranges $couleur]
    for {set i 0} {$i < [llength $liste]} {incr i 2} {
    set str [$t get [lindex $liste $i] [lindex $liste [expr $i + 1]]]
    regsub -all \" $str \\\" str
    lappend plist $str        
    }

for {set i [expr [llength $plist]-1]} {$i >= 0} {incr i -1} {
if {[llength [lindex $plist $i]] <=1} { set plist [lreplace $plist $i $i] }
}

set leng [llength $plist]
    if {$leng < 1} {
    pauto $t
    return
    }

for {set j 0} {$j <= [expr int(rand()*3)]} {incr j 1} {
for {set i 1} {$i <= $leng} {incr i 1} {
  set t1 [expr int(rand()*$leng)]
  set t2 [expr int(rand()*$leng)]
  set tmp [lindex $plist $t1]
  set plist [lreplace $plist $t1 $t1 [lindex $plist $t2]]
  set plist [lreplace $plist $t2 $t2 $tmp]
  }
}
set listdata [lindex $plist 0]

}

###########################################################
proc afficheaide {} {
global sysFont listaide
catch {destroy .w1}
toplevel .w1
.w1 configure -width 300 -height 100
wm title .w1 [mc {Aide}]
wm geometry .w1 -0-0
wm transient .w1 .
text .w1.text1 -yscrollcommand ".w1.scrolly set" -width 20 -height 10 -setgrid true -wrap word -background white -font $sysFont(l) -selectbackground white -selectforeground black
scrollbar .w1.scrolly -command ".w1.text1  yview"
pack .w1.scrolly -side right -fill y
pack .w1.text1 -expand yes -fill both

 .w1.text1 insert current $listaide
}


##################################################
#placement des images, textes sur le canevas
#################################################
proc place {c} {
# erreur : variable pour d�terminer � quel moment on peut afficher l'aide
global sysFont font2 listdata longlist1 bgl ale erreur listerep listepos essais auto couleur categorie aide longchamp lecture_mot lecture_mot_cache

set erreur 0
#on efface tout et on place les �l�ments sur le canevas
$c delete all

#longlist1 sert � r�cup�rer les syllabes du mot � afficher
set list1 $listdata
set longlist1 [llength $list1]
.menu.bb1 configure -command "speaktexte [list $listerep]"

#on m�lange les syllabes
for {set i 0} {$i < $longlist1} {incr i 1} {
  set ale($i) $i
  }
for {set i 1} {$i <= $longlist1} {incr i 1} {
  set t1 [expr int(rand()*$longlist1)]
  set t2 [expr int(rand()*$longlist1)]
  set temp $ale($t1)
  set ale($t1) $ale($t2)
  set ale($t2) $temp
  }

#on d�termine la longueur de la +longue syllabe, pour la dimension des traits
set max 0
for {set i 0} {$i < $longlist1} {incr i 1} {
#    if {[font measure $sysFont(lb) [lindex $list1 $i]] > $max} {
#	set max [font measure $sysFont(lb) [lindex $list1 $i]]
#    }
if {[string length [lindex $list1 $i]] > $max} {
	set max [string length [lindex $list1 $i]]
    }

}
set max [expr int($max) + 3]

#on place les syllabes et les traits
set listecoord {0 0 0 0}
set listecible {0 0 0 0}
set ypos0 20
for {set i 0} {$i < $longlist1} {incr i 1} {
  $c create text 0 0 -text [lindex $list1 $i] -tags source$i -font $font2
  set listc [$c bbox [$c find withtag source$i]]
if {[lindex $listecoord 2] > [expr 620 - int(([lindex $listc 2] - [lindex $listc 0])/2)]} {
set listecoord {0 0 0 0}
incr ypos0 50
}
  $c coords source$i [expr [lindex $listecoord 2]+ int(([lindex $listc 2] - [lindex $listc 0])/2) +10] $ypos0
lappend listepos [expr [lindex $listecoord 2]+ int(([lindex $listc 2] - [lindex $listc 0])/2)]\040$ypos0
   set listecoord [$c bbox [$c find withtag source$i]]

}

set ypos1 [expr $ypos0 + 100]

    for {set i 0} {$i < $longlist1} {incr i 1} {
    if {$longchamp == 0} {
    set max [expr int([string length [lindex $listerep $i]]) +2]
   }
    $c create text 0 0 -text [string repeat "_" $max] -tags cible$i -font $sysFont(lb) -fill $bgl
    set listci [$c bbox [$c find withtag cible$i]]
        if {[lindex $listecible 2] > [expr 600 - int(([lindex $listci 2] - [lindex $listci 0])/2)]} {
        set listecible {0 0 0 0}
        incr ypos1 50
        }
    $c coords cible$i [expr [lindex $listecible 2]+ int(([lindex $listci 2] - [lindex $listci 0])/2) + 10] $ypos1
    set listecible [$c bbox [$c find withtag cible$i]]
    $c addtag drag withtag source$i
    	if {$lecture_mot == "1" } {
	$c bind cible$i <1> "speaktexte [list [lindex $listerep $i]]"
	$c bind cible$i <Any-Enter> "$c configure -cursor target"
	$c bind cible$i <Any-Leave> "$c configure -cursor left_ptr"

	}
	$c bind source$i <1> "itemStartDrag $c %x %y"

    }
#$c configure -scrollregion 0c 0c 30c $ypos1

$c bind drag <ButtonRelease-1> "itemStopDrag $c %x %y"
    if {$lecture_mot_cache == "0" } {
catch {destroy .menu.bb1}
}
$c bind drag <B1-Motion> "itemDrag $c %x %y"
}

##############################################################"
# v�rification, lorsque l'on appuie sur le bouton v�rifier
################################################################
proc verif {c} {
global sysFont longlist1 listdata ale erreur font2 categorie bonnereponse listerep listepos aide essais user compt
set compt 0
incr essais
#on concat�ne les syllabes plac�es dans le string reponse
set reponse ""
for {set j 0} {$j < $longlist1} {incr j 1} {
   set ciblecoord [$c bbox [$c find withtag cible$j]]
   foreach i [$c find overlapping [lindex $ciblecoord 0] [lindex $ciblecoord 1] [lindex $ciblecoord 2] [lindex $ciblecoord 3]] {
# on renvoie les syllabes mal plac�es � leur place

     if {[lsearch [$c gettags $i] cible$j] == -1} {

         if { [$c itemcget $i -text] != [lindex $listerep $j] } { 

set numsource ""
set str [lindex [$c gettags $i] 0]
          for {set l 1} {$l <= [string length $str]} {incr l 1} {
               if {[string match {[0-9]} [string index $str end]] == 1} {
                 set numsource [string index $str end]$numsource
                 set  str [string range $str 0 [expr [string length $str] -2] ]
                }
            }

              $c coords $i [lindex [lindex $listepos $numsource] 0] [lindex [lindex $listepos $numsource] 1]
              }
        if {[lsearch [$c gettags $i] source$j] != -1} {            
        }
        incr compt
        set reponse $reponse[$c itemcget $i -text]
        }
   }
}
set bonnereponse ""
for {set k 0} {$k < [llength $listerep]} {incr k 1} {
set bonnereponse $bonnereponse[lindex $listerep $k]
}

#on compare avec la chaine initiale
if {[string compare $reponse $bonnereponse] == 0} {
   $c itemconf drag -tag dead
   $c delete withtag verif
       catch {destroy .barre.tete}
       #label .barre.tete -image [image create photo imag3 -file [file join sysdata pbien.gif]]
       #pack .barre.tete -side top
    set str0 [mc {Exercice termine en }]
    set str2 [format [mc {%1$s essai(s).}] $essais]
    set str1 [mc {Exercice Mots melanges}]
    .menu.lab configure -text $str0$str2
    #enregistreval $str1 $categorie $str2 $user

        catch {destroy .barre.ok}
set score [expr ($compt*100)/([llength $listdata]+($essais -1))]
if {$score < 0} {set score 0}
if {$score <50} {tux_triste $score}
if {$score >=50 && $score <75 } {tux_moyen $score}
if {$score >=75} {tux_content $score}

     update
     } else {
       if {$erreur > 1} {tux_echoue2} else {tux_echoue1}
 if {[incr erreur] >=$aide} {
         #$c create text 440 60 -text $bonnereponse -font $font2
         afficheaide
         }
      catch {destroy .barre.tete}
      #label .barre.tete -image [image create photo imag3 -file [file join sysdata pmal.gif]]
      #pack .barre.tete -side top
    }
}


################################################################"
proc itemStartDrag {c x y} {
    global sysFont lastX lastY sourcecoord flag
    catch {destroy .barre.tete}
    set flag 1
    catch {$c delete withtag figure}
    set sourcecoord [$c coords current]
    set lastX [$c canvasx $x]
    set lastY [$c canvasy $y]
    $c raise current
    tux_continue
    }

#################################################################
proc itemStopDrag {c x y} {
global sysFont lastX lastY sourcecoord listdata flag
    set flag 0
    set strcible cible
    set strsource [lindex [$c gettags current] 0]
    set ciblecoord [$c bbox [$c find withtag $strcible]]
    set coord [$c bbox current]
    if {[llength [$c find overlapping [lindex $coord 0] [lindex $coord 1] [lindex $coord 2] [lindex $coord 3]]] > 2} {
    $c coords current [lindex $sourcecoord 0] [lindex $sourcecoord 1]
    return
    }
    
    foreach i [$c find overlapping [lindex $coord 0] [lindex $coord 1] [lindex $coord 2] [lindex $coord 3]] {
         if {[lsearch -regexp [$c gettags $i] cible*] != -1} {
            $c coords current [$c coords $i]
            }
    }
}

#########################################################"
proc itemDrag {c x y} {
    global sysFont lastX lastY
    set x [$c canvasx $x]
    set y [$c canvasy $y]
    $c move current [expr $x-$lastX] [expr $y-$lastY]
    set lastX $x
    set lastY $y
}
if {$startdirect == 0 } {
main
}

proc fin {} {
global sysFont categorie user essais listdata compt listexo iwish filuser aide startdirect repertoire lecture_mot lecture_mot_cache
variable repertconf
    set str2 [format [mc {%1$s essai(s) pour %2$s mot(s) sur %3$s.}] $essais $compt [llength $listdata]]
    set str1 [mc {Exercice Mots melanges}]
    set score [expr ($compt*100)/([llength $listdata]+($essais -1))]
switch $startdirect {
1 {set startconf [mc {Le texte est visible au debut}]}
0 {set startconf [mc {Le texte n'est pas visible au debut}]}
}

switch $aide {
1 { set aideconf [mc {Au debut}]}
2 { set aideconf [mc {Apres le premier essai}]}
3 { set aideconf [mc {Apres le deuxieme essai}]}
}

switch $lecture_mot_cache {
1 { set lectmotconf [mc {La phrase peut �tre entendue.}]}
0 { set lectmotconf [mc {La phrase ne peut pas �tre entendue.}]}
}

switch $lecture_mot {
1 { set lectmotcacheconf [mc {Les mots peuvent �tre entendus.}]}
0 { set lectmotcacheconf [mc {Les mots ne peuvent pas �tre entendus.}]}
}

set exoconf [mc {Parametres :}]
set exoconf "$exoconf $startconf - "
set exoconf "$exoconf Aide : $aideconf"
set exoconf "$exoconf  Son : $lectmotconf $lectmotcacheconf"

    enregistreval $str1\040[lindex [lindex $listexo 3] 1] \173$categorie\175 $str2 $score $repertconf 3 $user $exoconf $repertoire
exec $iwish aller.tcl $filuser &
exit
}


proc boucle {} {
global  categorie startdirect sysFont user auto compt essais listdata
    set str2 [format [mc {%1$s essai(s) pour %2$s mot(s) sur %3$s.}] $essais $compt [llength $listdata]]
    set str1 [mc {Exercice Mots melanges}]
    enregistreval $str1 \173$categorie\175 $str2 $user
set listdata ""
set essais 0
set compt 0
catch {destroy .frame}
catch {destroy .barre}
catch {destroy .w1}
frame .barre -width 40
pack .barre -side left -fill both

text .text -yscrollcommand ".scroll set" -setgrid true -width 55 -height 17 -wrap word -background white -font $sysFont(l)
scrollbar .scroll -command ".text yview"
pack .scroll -side right -fill y
pack .text -expand yes -fill both

set categorie "Au choix"
set auto [charge .text $categorie]
.text configure -state disabled
focus .text
.menu.b1 configure -text [mc {Commencer}] -command "main"
catch {destroy .menu.suiv}
catch {destroy .menu.lab}
if {$startdirect == 0 } {
main
}
}




























