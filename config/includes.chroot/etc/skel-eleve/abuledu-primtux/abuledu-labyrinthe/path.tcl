#!/bin/sh
#menus.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
#**************************************************************************
#  File  : $$$
#  Author  : davidlucardi@aol.com
#  Modifier: 
#  Date    : 
#  Licence : GNU/GPL Version 2 ou plus
# 
#  Description:
#  ------------
# 
#  @version    
#  @author     David Lucardi
#  @modifier   
#  @project    Le terrier
#  @copyright  Eric Seigne 
# 
#  *************************************************************************
proc setwindowsusername {} {
global user
catch [destroy .utilisateur]
toplevel .utilisateur -background grey -width 250 -height 100
wm geometry .utilisateur +50+50
frame .utilisateur.frame -background grey -width 250 -height 100
pack .utilisateur.frame -side top
label .utilisateur.frame.labobj -font {Helvetica 10} -text "Quel est ton nom?" -background grey
pack .utilisateur.frame.labobj -side top 
entry .utilisateur.frame.entobj -font {Helvetica 10} -width 10
pack .utilisateur.frame.entobj -side top 
button .utilisateur.frame.ok -background gray75 -text "Ok" -command "verifnom"
pack .utilisateur.frame.ok -side top -pady 10
}

proc verifnom {} {
global env
set nom [string tolower [string trim [string map {\040 ""} [.utilisateur.frame.entobj get]]]]
if {$nom !=""} {
set env(USERNAME) $nom
}
catch [destroy .utilisateur]
}

proc init {plateforme} {
global Home basedir baseHome 


if {![file exists [file join $Home ]]} {
	file mkdir [file join $Home ]
	file copy -force [file join reglages] [file join $Home ]
	file copy -force  [file join data ] [file join $Home ]
}

switch $plateforme {
    unix {
	if {![file exists [file join $Home  log]]} {
	file mkdir [file join  $Home  log]
	}

    }
    windows {
	if {![file exists [file join labyrinthe Log]]} {
	file mkdir [file join labyrinthe Log]
	}

    	}
}

}


proc initlog {plateforme ident} {
global LogHome user basedir Home
switch $plateforme {
    unix {
	set LogHome [file join $Home log] 

    }
    windows {
	set LogHome [file join $basedir  labyrinthe  Log]
    }
}

if {$ident != ""} {
     set user [file join $LogHome $ident.log]
     } else {
     set user [file join $LogHome labyrinthe.log]
     }
}

proc inithome {} {
global baseHome basedir Homeconf Home
variable reperttext
variable repertconf
set f [open [file join $baseHome reglages repert.conf] "r"]
set reperttext [gets $f]
set repertconf [gets $f]
close $f
#on synchronise les 2 variables en attendant

switch $repertconf {
0 {set Home $baseHome}
1 {set Home [file join $basedir textes]}
}
switch $repertconf {
0 {set Homeconf $baseHome}
1 {set Homeconf $basedir}
}

}


proc changehome {} {
global Home basedir baseHome Homeconf
variable reperttext
variable repertconf
set f [open [file join $baseHome reglages repert.conf] "w"]
#on synchronise les 2 variables en attendant
puts $f $repertconf
puts $f $repertconf
close $f
switch $repertconf {
0 {set Home $baseHome}
1 {set Home [file join $basedir textes]}
}
switch $repertconf {
0 {set Homeconf $baseHome}
1 {set Homeconf $basedir}
}
}

global basedir Home baseHome iwish Homeconf progaide
if {[info tclversion] == "8.4"} {
    	set disabledfore disabledforeground
      set disabledback disabledbackground
	} else {
	set disabledfore fg
      set disabledback bg
	}

set basedir [file dir $argv0]
cd $basedir
if {$env(HOME) == "c:\\"} {
set Home [file join $basedir]
set Homeconf [file join $basedir]

} else {
set Home [file join $env(HOME) leterrier labyrinthe]
set Homeconf [file join $env(HOME) leterrier labyrinthe]
}
set baseHome $Home



switch $tcl_platform(platform) {
    unix {

switch [string range [lindex [exec uname -r] 0] 2 2] {
      2 { set progaide runbrowser }
      4 { set progaide runbrowser }
      6 { set progaide runbrowser }
      default { set progaide konqueror }
	}

	set iwish wish
	}
    windows {
	set progaide shellexec.exe
	set iwish wish83
	if {[info tclversion] == "8.4"} {
	set iwish wish84
	}
	}
	}



