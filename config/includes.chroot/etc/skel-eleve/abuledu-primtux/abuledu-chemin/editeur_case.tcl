#!/bin/sh
# editeur_case.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 Andr� Connes <andre.connes@toulouse.iufm.fr>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#**************************************************************************
#  File  : editeur_case.tcl
#  Author  : Andr� Connes <andre.connes@toulouse.iufm.fr>
#  Modifier:
#  Date    : 19/05/2004
#  Licence : GNU/GPL Version 2 ou plus
#
#  Description:
#  ------------
#
#  @version    $Id: editeur_case.tcl,v 1.11 2006/03/27 13:13:16 abuledu_andre Exp $
#  @author     Andr� Connes
#  @modifier
#  @project    Le terrier
#  @copyright  Andr� Connes
#
#***********************************************************************
global sysFont glob

source chemin.conf
source msg.tcl
source fonts.tcl
source aider.tcl

  #
  # langue par d�faut
  #
  set f [open [file join $glob(home_reglages) lang.conf] "r"]
  gets $f lang
  close $f
  ::msgcat::mclocale $lang
  ::msgcat::mcload [file join [file dirname [info script]] msgs]

wm resizable . 0 0
wm geometry . [winfo vrootwidth .]x[winfo vrootheight .]+0+0
. configure -background $glob(bgcolor)
#wm title . "[mc {title_m}]"
wm title . "[mc {Editeur Case}]"

################################################
# les r�pertoires des exercices peuvent �tre : #
# - textes                                     #
# - images                                     #
# - sons                                       #
# - vid�os                                     #
# - etc.                                       #
# donc 2 proc�dures � adapter au cas par cas ! #
################################################
proc init_dir_exos {} {
  global glob
  catch {set f [open [file join $glob(home_reglages) dir_exos.conf] "r"]}
  set glob(dir_exos) [gets $f]
  close $f
}

proc change_dir_exos {} {
  global glob
  catch {set f [open [file join $glob(home_reglages) dir_exos.conf] "w"]}
  #
  # ici ajouter le code
  set new_dir_exos $glob(home_chemin_editeur) ;# bidon !
  #
  puts $f $new_dir_exos
  close $f
}

# changer les dossiers de travail et charger les listes d'exos
proc fait_liste_exos {} {
global glob
    
  change_dir_exos
    
  #
  # construire la proc suivante
  #
  #set liste_exos ???

  main_loop
}

proc lanceappli {appli niveau} {
	global glob
	set appli [file join $appli]
	exec $glob(wish) $appli $niveau &
}

proc verifier {yn xn} {
  global glob case
  if {[llength $glob(trac�)] > 0 } {
    set xo [lindex $glob(trac�) end]
    set yo [lindex $glob(trac�) end-1]
    if { $xo == $xn && $yo == $yn && $case($yo,$xo) == "in" } {
	return -1
    }
    if {[expr abs($xn-$xo)+abs($yn-$yo)] == 1 } {  
      set ys [expr $yn-1]
      if { [expr abs($xn-$xo)+abs($ys-$yo)] != 0 && \
		 $case($ys,$xn)=="in"} {
	return 0
      }
      set ys [expr $yn+1]
      if { [expr abs($xn-$xo)+abs($ys-$yo)] != 0 && \
		 $case($ys,$xn)=="in"} {
	return 0
      }
      set xs [expr $xn-1]
      if { [expr abs($xs-$xo)+abs($yn-$yo)] != 0 && \
		 $case($yn,$xs)=="in"} {
	return 0
      }
      set xs [expr $xn+1]
      if { [expr abs($xs-$xo)+abs($yn-$yo)] != 0 && \
		 $case($yn,$xs)=="in"} {
	return 0
      }
      return 1
    } else {
      return 0
    }
  } else {
    return 1
  }
}

proc set_case {fc x y} {
  global glob case
  set j [expr int([$fc canvasx $x] / $glob(wlen))]
  set i [expr int([$fc canvasy $y] / $glob(hlen))]
  set cas [verifier $i $j]
  if { $cas == 1 } {
    # ajouter la case dans le chemin
    $fc create rect [expr $glob(org) + $j*$glob(wlen) +1] [expr $glob(org) + $i*$glob(hlen) +1] \
	[expr $glob(org) + ($j+1)*$glob(wlen) +1] [expr $glob(org) + ($i+1)*$glob(hlen) +1] \
	-fill green -width 1 -tag rect($i,$j)
    lappend glob(trac�) $i $j
    set case($i,$j) "in"
  } elseif { $cas == -1 } {
    # retirer la case du chemin
    $fc create rect [expr $glob(org) + $j*$glob(wlen) +1] [expr $glob(org) + $i*$glob(hlen) +1] \
	[expr $glob(org) + ($j+1)*$glob(wlen) +1] [expr $glob(org) + ($i+1)*$glob(hlen) +1] \
	-fill #00ffB0 -width 1 -tag rect($i,$j)
    set case($i,$j) "out"
    set glob(trac�) [lreplace $glob(trac�) end-1 end] 
  }

} ;#set_case

proc afficher_grille { } {
  global fc fb glob case

  ####################Cr�ation du tableau###################################
  # On utilise des rectangles pour dessiner les cases
  # L'avantage des rectangles, c'est qu'ils peuvent constituer des objets
  # ind�pendants, utile si on veut leur associer des comportements

  # on cr�e la grille (ATTENTION : i=ligne et j=colonne)
  for {set i 0} {$i < $glob(nbrow)} {incr i 1} {
    for {set j 0} {$j < $glob(nbcol)} {incr j 1} {
      $fc create rect [expr $glob(org) + $j*$glob(wlen) +1] [expr $glob(org) + $i*$glob(hlen) +1] \
	[expr $glob(org) + ($j+1)*$glob(wlen) +1] [expr $glob(org) + ($i+1)*$glob(hlen) +1] \
	-fill #00ffB0 -width 1 -tag rect($i,$j) 
      $fc bind rect($i,$j) <1> {set_case $fc %x %y} 
    }
  }
  #
  # ajoute de sentinelles en -1 et glob(nb...)
  #
  for {set i -1} {$i <= $glob(nbrow)} {incr i 1} {
    for {set j -1} {$j <= $glob(nbcol)} {incr j 1} {
	set case($i,$j) "out"  
    }
  }
  set glob(trac�) ""
} ;# afficher_grille

proc my_savefile { } {
  global glob
  set tchemin {
	{{Tous les fichiers} *}
  }
  set fname [tk_getSaveFile -title [mc Selection] \
	-initialdir [file join $glob(home_chemin) grilles] \
	-filetypes $tchemin]
  catch {set f [open $fname "w"]}
  puts $f $glob(trac�)
  close $f
  exit
}

###########################################################################################""
proc main_loop {} {
  global fc fb sysFont glob

  #########################
  #      Barre de menus   #
  #########################

  # Creation du menu utilise comme barre de menu
  #---------------------------------------------
  catch {destroy .menu}
  catch {destroy .frame}

  menu .menu -tearoff 0

  # Creation du menu Fichier
  menu .menu.fichier -tearoff 0
  .menu add cascade -label [mc {Fichier}] -menu .menu.fichier
  .menu.fichier add command -label [mc {Enregistrer}] -command my_savefile
  .menu.fichier add command -label [mc {Quitter}] -command exit

  # Creation du menu Aide
  menu .menu.aide -tearoff 0
  .menu add cascade -label [mc {Aide}] -menu .menu.aide
  .menu.aide add command -label [mc {Aide}] -command aider
  .menu.aide add command -label [mc {A propos ...}] -command "source apropos.tcl"

  . configure -menu .menu

  # creation des frames
  #####################
  . configure -background $glob(bgcolor)
  frame .frame -bg $glob(bgcolor) -height $glob(height) -width $glob(width)
  pack  .frame -side top -fill both -expand yes

  frame .fbottom -bg $glob(bgcolor) -width $glob(width)
  pack  .fbottom -side top -fill both -expand yes
  label .fbottom.message -text [mc faittrace] -font 12x24
  pack  .fbottom.message 

  # creation du canvas
  ####################

  set fc .frame.c
  canvas $fc -height [expr $glob(nbrow)*$glob(hlen)+5] -width $glob(width) \
	-bg $glob(bgcolor) -highlightbackground $glob(bgcolor)
  pack $fc -expand true

  afficher_grille

} ;# fin de main_loop

############
# Bindings #
############

bind . <Control-q> {exit}

########################################################################
#                           programme principal                        #
########################################################################

main_loop

