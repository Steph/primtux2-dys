############################################################################
# Copyright (C) 2007 David Lucardi
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : editeur.tcl
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version    $Id: editparcours.tcl,v 1.1.1.1 2004/04/16 11:45:45 erics Exp $
# @author     David Lucardi
# @project
# @copyright  David Lucardi
#
#
#########################################################################
#!/bin/sh
#Editeur.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

##################################"sourcing
set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)

source fonts.tcl
source path.tcl
#source utils.tcl
#source eval.tcl
source msg.tcl

inithome
initlog $plateforme $ident
#changehome

. configure -background white

menu .menu
# Creation du menu Fichier
menu .menu.fichier -tearoff 0
.menu add cascade -label [mc {Fichier}] -menu .menu.fichier
.menu.fichier add command -label "[mc {Nouveau}] (Crtl-n)" -command "nouveau"
.menu.fichier add command -label "[mc {Ouvrir}] (Crtl-o)" -command "ouvre"
.menu.fichier add command -label "[mc {Enregistrer}] (Crtl-w)" -command "enregistre_sce"
.menu.fichier add separator
.menu.fichier add command -label "[mc {Quitter}] (Crtl-q)" -command "destroy ."
. configure -menu .menu

#################################################g�n�ration interface

proc nouveau {} {
global file
set answer [tk_messageBox -message [mc {Enregistrer les modifications?}] -type yesno -icon info]
if {$answer == "yes"} {
enregistre_sce
}
set file ""
.geneframe.frame1.listact delete 0 end
.geneframe.frame1.a1 configure -text [mc {Séquence(s)}]
wm title . "Editeur de Séquence : Nouveau"
}

proc sauve {} {
global file Home 
set types {    {{Fichiers par}            {.par}        }}
set ext .par
catch {set file [tk_getSaveFile -filetypes $types -initialdir [file join $Home problemes] -initialfile $file]}
    if {$file != ""} {
        if  {[string match -nocase *.par $file]==0} {
        set file $file$ext
        }
}
return $file
}

proc ouvre {} {
global file parcours Home
set types {    {{Fichiers par}            {.par}        }}

set file [tk_getOpenFile -initialdir [file join $Home problemes] -filetypes $types]
if {$file != ""} {
wm title . "Editeur de Séquence : [file tail $file]"

.geneframe.frame1.a1 configure -text [file tail $file]
set f [open [file join  $file] "r"]
set parcours [gets $f]
close $f
.geneframe.frame1.listact delete 0 end
foreach i $parcours {
.geneframe.frame1.listact insert end $i
}
}
}

proc interface {} {
global sysFont
catch {destroy .geneframe}

frame .geneframe -background white -height 300 -width 400
pack .geneframe -side left -anchor n

frame .geneframe.frame1 -background white -width 100 
grid .geneframe.frame1 -padx 10 -column 0 -row 0
label .geneframe.frame1.a1 -bg blue -foreground white -text [mc {Séquence(s)}] -font $sysFont(s)
grid .geneframe.frame1.a1 -pady 5 -column 0 -row 0 -columnspan 2

listbox .geneframe.frame1.listact -yscrollcommand ".geneframe.frame1.scrollpage set" -width 15 -height 6
scrollbar .geneframe.frame1.scrollpage -command ".geneframe.frame1.listact yview" -width 7
grid .geneframe.frame1.listact -column 0 -row 1 -sticky nwes
grid .geneframe.frame1.scrollpage -column 1 -row 1 -sticky nwes
bind .geneframe.frame1.listact <ButtonRelease-1> "changelistact %x %y"

frame .geneframe.frame2 -background white -width 100 
grid .geneframe.frame2 -padx 10 -column 1 -row 0 
button .geneframe.frame2.but0 -text " >> " -command "delitem" -activebackground white
grid .geneframe.frame2.but0 -padx 10 -pady 10 -column 0 -row 1
button .geneframe.frame2.but1 -text " << " -command "additem" -activebackground white
grid .geneframe.frame2.but1 -padx 10 -pady 10 -column 0 -row 2

frame .geneframe.frame3 -background white -width 100 
grid .geneframe.frame3 -padx 10 -column 2 -row 0
label .geneframe.frame3.a1 -bg blue -foreground white -font $sysFont(s) -text "Probl�mes"
grid .geneframe.frame3.a1 -pady 5 -column 0 -row 0 -columnspan 2
listbox .geneframe.frame3.listsce -yscrollcommand ".geneframe.frame3.scrollpage set" -width 18 -height 6
scrollbar .geneframe.frame3.scrollpage -command ".geneframe.frame3.listsce yview"
grid .geneframe.frame3.listsce  -column 0 -row 1 -sticky nwes
grid .geneframe.frame3.scrollpage -column 1 -row 1 -sticky nwes
bind .geneframe.frame3.listsce <ButtonRelease-1> "changelistsce %x %y"

peuplelistact

frame .geneframe.frame4 -background white -width 100 
grid .geneframe.frame4 -padx 10 -column 3 -row 0
label .geneframe.frame4.lab2 -bg blue -foreground white -text [mc {Niveau}] -font $sysFont(s)
grid .geneframe.frame4.lab2 -column 0 -row 0 -pady 5 -columnspan 2
listbox .geneframe.frame4.listpar -yscrollcommand ".geneframe.frame4.scrollpage set" -width 6 -height 6
scrollbar .geneframe.frame4.scrollpage -command ".geneframe.frame4.listpar yview" -width 7
grid .geneframe.frame4.listpar -column 0 -row 1 -sticky nwes
grid .geneframe.frame4.scrollpage -column 1 -row 1 -sticky nwes
bind .geneframe.frame4.listpar <ButtonRelease-1> "changelistpar %x %y"

peuplelistpar


########################leftframe.frame3
frame .geneframe.frame5 -background white -width 100
grid .geneframe.frame5 -row 1 -column 0 -columnspan 4 
button .geneframe.frame5.but0 -text [mc {ok}] -command "fin" -activebackground white
grid .geneframe.frame5.but0 -column 0 -row 0 -padx 10 -pady 10 -sticky nwes
frame .geneframe.frame6 -background white -width 100
grid .geneframe.frame6 -row 2 -column 0 -columnspan 4 
label .geneframe.frame6.enonce -bg white
grid .geneframe.frame6.enonce -row 0 -column 0 -sticky news 
}

###################################################################################"
proc enregistre_sce {} {
global file Home
set lstmp ""
for {set k 0} {$k < [.geneframe.frame1.listact size]} {incr k 1} {
lappend lstmp [.geneframe.frame1.listact get $k]
}
if {$file ==""} {set file [sauve]}
if {$file != ""} {
wm title . "Editeur de Séquence : [file tail $file]"

set f [open [file join $Home problemes [file tail $file]] "w"]
puts $f $lstmp
close $f
}
}


proc fin {} {
enregistre_sce
destroy .
}

#######################################################################
proc delitem {} {
global itempb file
	if {[.geneframe.frame1.listact size] == 1} {
	tk_messageBox -message [mc {Impossible de supprimer la fiche}] -type ok -title [file tail $file]
	return
	}
if {[.geneframe.frame1.listact get active] !=""} {
.geneframe.frame1.listact delete [.geneframe.frame1.listact index active]
set itempb [.geneframe.frame1.listact get active]
}
}


proc additem {} {
global parcours itempb itemniv
if {$itemniv != "" && $itempb !=""} {
set item "$itempb $itemniv"
.geneframe.frame1.listact insert end $item
}
}


proc changelistact {x y} {
global itempar
set itempar [.geneframe.frame1.listact get @$x,$y]
if {$itempar !=""} {
.geneframe.frame1.a1 configure -text $itempar
}
}

proc changelistpar {x y} {
global itemniv
set itemniv [.geneframe.frame4.listpar get @$x,$y]
if {$itemniv != ""} {
.geneframe.frame4.lab2 configure -text "Niveau : $itemniv"
}
}

proc changelistsce {x y} {
global itempb Home
set itempb [.geneframe.frame3.listsce get @$x,$y]
if {$itempb != ""} {
.geneframe.frame3.a1 configure -text "Probl�me : $itempb"
set f [open [file join  $Home problemes $itempb] "r"]
set tmp [gets $f]
eval $tmp
	while {$tmp != "::1"} {
	set tmp [gets $f]
	}

	while {$tmp != "::"} {
	set tmp [gets $f]
	if {$tmp != "::"} {eval $tmp}
	}
close $f
.geneframe.frame6.enonce configure -text $enonce -wraplength 400
peuplelistpar
}
}

proc peuplelistact {} {
global itempb Home
foreach i [glob [file join  $Home problemes  *.txt]] {
.geneframe.frame3.listsce insert end [file tail $i]
}
.geneframe.frame3.a1 configure -text "Probl�me : [.geneframe.frame3.listsce get 0]"
set itempb [.geneframe.frame3.listsce get 0]
}

proc peuplelistpar {} {
global itemniv itempb Home
.geneframe.frame4.listpar delete 0 end
if {$itempb !=""} {
set f [open [file join  $Home problemes $itempb] "r"]
set tmp [gets $f]
set tmp [gets $f]
eval $tmp
close $f
foreach item $niveaux {
.geneframe.frame4.listpar insert end $item
}
.geneframe.frame4.lab2 configure -text "Niveau : [.geneframe.frame4.listpar get 0]"
set itemniv [.geneframe.frame4.listpar get 0]
}
}

global itempar file itempb itemniv
wm title . "Editeur de Séquence : Nouveau"
set itempar ""
set file ""
set itempb ""
set itemniv ""

interface

