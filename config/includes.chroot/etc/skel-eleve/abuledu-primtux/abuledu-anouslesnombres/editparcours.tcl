############################################################################
# Copyright (C) 2003 Eric Seigne
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : editeur.tcl
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version    $Id: editparcours.tcl,v 1.1.1.1 2004/04/16 11:45:45 erics Exp $
# @author     David Lucardi
# @project
# @copyright  Eric Seigne
#
#
#########################################################################
#!/bin/sh
#Editeur.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

##################################"sourcing
set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)

source fonts.tcl
source path.tcl
source utils.tcl
source eval.tcl
source msg.tcl

inithome
initlog $plateforme $ident
changehome

set fichier [file join $basedir aide index.htm]
bind . <F1> "exec $progaide \042$fichier\042 &"

global arg tabitem
set arg [lindex $argv 0]
. configure -background white

foreach item {calapa barque train constellation nombre ordinal voir} {
set tabitem([mc $item]) $item
}
#variables
#################################################génération interface
proc interface {what} {
global sysFont arg Home
catch {destroy .geneframe}
catch {destroy .leftframe}
frame .geneframe -background white -height 300 -width 400
pack .geneframe -side left -anchor n

frame .geneframe.frame1 -background white -width 100 
pack .geneframe.frame1 -side left -padx 10  
frame .geneframe.frame1.frame1top -background white -width 100 
pack .geneframe.frame1.frame1top -side top 
label .geneframe.frame1.frame1top.a1 -bg blue -foreground white -text [mc {calapa}] -font $sysFont(s)
pack .geneframe.frame1.frame1top.a1 -side top -fill x -expand 1 -pady 5

frame .geneframe.frame1.frame1bottom -background white -width 100 
pack .geneframe.frame1.frame1bottom -side top

listbox .geneframe.frame1.frame1bottom.listact -yscrollcommand ".geneframe.frame1.frame1bottom.scrollpage set" -width 15 -height 6
scrollbar .geneframe.frame1.frame1bottom.scrollpage -command ".geneframe.frame1.frame1bottom.listact yview" -width 7
pack .geneframe.frame1.frame1bottom.listact .geneframe.frame1.frame1bottom.scrollpage -side left -fill y -expand 1
bind .geneframe.frame1.frame1bottom.listact <ButtonRelease-1> "changelistact %x %y"
foreach item {calapa barque train constellation nombre ordinal voir} {
.geneframe.frame1.frame1bottom.listact insert end [mc $item]
}

frame .geneframe.frame2 -background white -width 100 
pack .geneframe.frame2 -side left 

frame .geneframe.frame2.frame2top -background white -width 100 
pack .geneframe.frame2.frame2top -side top 
label .geneframe.frame2.frame2top.a1 -bg blue -foreground white -font $sysFont(s)
pack .geneframe.frame2.frame2top.a1 -side left -fill x -expand 1 -pady 5

frame .geneframe.frame2.frame2bottom -background white -width 100 
pack .geneframe.frame2.frame2bottom -side top


listbox .geneframe.frame2.frame2bottom.listsce -yscrollcommand ".geneframe.frame2.frame2bottom.scrollpage set" -width 15 -height 6
scrollbar .geneframe.frame2.frame2bottom.scrollpage -command ".geneframe.frame2.frame2bottom.listsce yview" -width 7
pack .geneframe.frame2.frame2bottom.listsce .geneframe.frame2.frame2bottom.scrollpage -side left -fill y -expand 1
bind .geneframe.frame2.frame2bottom.listsce <ButtonRelease-1> "changelistsce %x %y"
set f [open [file join $Home reglages calapa.conf] "r"]
set listtmp [gets $f]
close $f

for {set k 0} {$k < [llength $listtmp]} {incr k 1} {
.geneframe.frame2.frame2bottom.listsce insert end [lindex [lindex [lindex $listtmp $k] 0] 0]
}
.geneframe.frame2.frame2top.a1 configure -text [lindex [lindex [lindex $listtmp 0] 0] 0]

button .geneframe.frame2.frame2bottom.but0 -text " >> " -command "additem" -activebackground white
pack .geneframe.frame2.frame2bottom.but0 -padx 10 -side left

frame .leftframe -background white -height 480 -width 100 
pack .leftframe -side left -anchor n


######################leftframe.frame1
frame .leftframe.frame1 -background white -width 100 
pack .leftframe.frame1 -side top 


label .leftframe.frame1.lab2 -bg blue -foreground white -text [mc {Scenario}] -font $sysFont(s)
pack .leftframe.frame1.lab2 -side top -fill x -expand 1 -pady 5

listbox .leftframe.frame1.listpar -yscrollcommand ".leftframe.frame1.scrollpage set" -width 20 -height 6
scrollbar .leftframe.frame1.scrollpage -command ".leftframe.frame1.listpar yview" -width 7
pack .leftframe.frame1.listpar .leftframe.frame1.scrollpage -side left -fill y -expand 1 -anchor n
bind .leftframe.frame1.listpar <ButtonRelease-1> "changelistpar %x %y"
button .leftframe.frame1.but0 -text [mc {Suppr.}] -command "delitem" -activebackground white
pack .leftframe.frame1.but0 -padx 10 -side left

########################leftframe.frame3
frame .leftframe.frame3 -background white -width 100
pack .leftframe.frame3 -side top 
button .leftframe.frame3.but0 -text [mc {ok}] -command "fin" -activebackground white
pack .leftframe.frame3.but0 -side bottom -pady 10
##########################################################################################################

 charge 0
}


####################################################################"""
proc charge {index} {
global listdata activelist indexpage totalpage arg Home tabitem
set ext .conf
set f [open [file join $Home reglages $arg$ext] "r"]
set listdata [gets $f]
close $f
set totalpage [llength $listdata] 
set indexpage $index
	.leftframe.frame1.listpar delete 0 end
for {set k 0} {$k < [llength $listdata]} {incr k 1} {
	.leftframe.frame1.listpar insert end [mc [lindex [lindex $listdata $k] 0]]\040[lindex [lindex $listdata $k] 1]
	}
.leftframe.frame1.listpar selection set $indexpage
#.leftframe.frame1.lab2 configure -text [lindex $listdata 0]
.leftframe.frame1.lab2 configure -text [mc [lindex [lindex $listdata 0] 0]]\040[lindex [lindex $listdata 0] 1]

}



###################################################################################"
proc enregistre_sce {} {
global Home arg tabitem
set ext .conf
set lstmp ""
for {set k 0} {$k < [.leftframe.frame1.listpar size]} {incr k 1} {
lappend lstmp $tabitem([lindex [.leftframe.frame1.listpar get $k] 0])\040[lindex [.leftframe.frame1.listpar get $k] 1]
}
set f [open [file join $Home reglages $arg$ext] "w"]
puts $f $lstmp
close $f
}


proc fin {} {
enregistre_sce
destroy .
}

#######################################################################
proc delitem {} {
global listdata indexpage totalpage arg Home
	if {[.leftframe.frame1.listpar size] == 1} {
	tk_messageBox -message [mc {Impossible de supprimer la fiche}] -type ok -title [mc $arg]
	return
	}

.leftframe.frame1.listpar delete [.leftframe.frame1.listpar index active]
.leftframe.frame1.lab2 configure -text [.leftframe.frame1.listpar get active]


}


proc additem {} {
global tabitem
set item [.geneframe.frame1.frame1bottom.listact get active]\040[.geneframe.frame2.frame2bottom.listsce get active]
.leftframe.frame1.listpar insert end $item
}


proc changelistact {x y} {
global Home tabitem
set alias [.geneframe.frame1.frame1bottom.listact get @$x,$y]
set nom $tabitem([.geneframe.frame1.frame1bottom.listact get @$x,$y])
set ext .conf
set f [open [file join $Home reglages $nom$ext] "r"]
set listtmp [gets $f]
close $f
.geneframe.frame2.frame2bottom.listsce delete 0 end
for {set k 0} {$k < [llength $listtmp]} {incr k 1} {
.geneframe.frame2.frame2bottom.listsce insert end [lindex [lindex [lindex $listtmp $k] 0] 0]
}
.geneframe.frame1.frame1top.a1 configure -text $alias
.geneframe.frame2.frame2top.a1 configure -text [lindex [lindex [lindex $listtmp 0] 0] 0]
}

proc changelistpar {x y} {
.leftframe.frame1.lab2 configure -text [.leftframe.frame1.listpar get @$x,$y]
}

proc changelistsce {x y} {
.geneframe.frame2.frame2top.a1 configure -text [.geneframe.frame2.frame2bottom.listsce get @$x,$y]
#.leftframe.frame1.lab2 configure -text [.geneframe.frame2.frame2bottom.listsce get @$x,$y]
}

interface $arg

