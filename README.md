**PrimTux-Dys**
===========
[PrimTux] (http://primtux.fr) est une distribution éducative basée sur Debian Jessie adaptée à **l'école primaire**. **Cette version est particulièrement adaptée aux enfants Dysférents et aux collégiens**.

- 2 utilisateurs et 2 environnements.
- Un menu d'accueil permet de finaliser son installation et de trouver les tutos nécessaires à l'utilisation de la distribution.
- [Filtrage intégré] (http://wiki.primtux.fr/doku.php/filtrage)
- Lanceurs de logiciels éducatifs: BNE.
- Panneau d'administration des applications
- Panneau d'installation des logiciels non-libres.

----------

- Version live **(mot de passe: tuxprof pour l'ouverture de session administrateur en live, pas de mot de passe pour la session élève)** et installable sur systèmes PAE et non PAE
- Base: Debian jessie
- Bureau: openbox + rox + lxpanel.

----------

**- Logiciels de base inclus:**

- Bureautique : Libreoffice (Traitement de texte, tableur) avec une interface adpatée à chaque niveau (libreoffice des écoles), dictionnaire (qdictionnaire), Agenda (Osmo), annotateur de fichiers pdf (Xournal), [nixnote] (https://doc.ubuntu-fr.org/nevernote), freeplane, yagf
- Graphisme : Visionneuse d'images, Editeur d'images, Capture d'écran, Explorateur d'images.
- Internet : Navigateur internet, filtrage intégré, Explorateur ftp.
- Son et vidéo : Éditeur de fichiers son (Audacity), Convertisseur de fichiers vidéo, Convertisseur de fichiers audio, VLC (lecteur de fichiers audio-vidéo), gmplayer (lecteur vidéo), Logiciel de gravure, Logiciel de montage vidéo
- De nombreux outils de configuration du système

**- Logiciels éducatifs inclus:**

- Lecture : Aller, Associations, imageo.
- Calcul : à nous les nombres, calcul mental, calcul réfléchi, calculatrice, calculette capricieuse, contour, fukubi, la course aux nombres, le nombre cible, opérations, problèmes, suites, suites arithmétiques, tierce, TuxMath, Abuledu calcul réfléchi, matheos.
- Clavier-souris : Jnavigue , mulot, Pysycache, klettres, klavaro, ktouch, apprenti clavier.
- Compilations : Childsplay, GCompris, Omnitux, pysiogame.
- Dessin : Tux Paint.
- Géométrie-Logique : chemin, epi: labyrinthe, labyrinthe caché, comparaison, piles, symcolor, tangrams (gtans), Jclic Puzzles, [geogebra] (https://www.geogebra.org/?lang=fr), instrumenpoche.
- Sciences : Stellarium, microscope virtuel, Scratch
- Jeux: blobby volley, frozen-bubble, Hannah's horse, monsterz, Mr Patate, ri-li, seahorse adventures, supertux, blinken.
- Pour le maître: pylote (logiciel pour TBI), l'administration de tuxpaint, de GCompris, de Pysycache.
- Utilitaires: Après installation de PrimTux sur disque dur, mise à jour, installation de Flash et Java, personnalisation, installation de logiciels non-libres, Systemback permet de copier PrimTux sur un autre disque dur.

**- Logiciels d'accessiblité inclus:**

- [Gespeaker] (https://doc.ubuntu-fr.org/gespeaker), [Onboard] (https://doc.ubuntu-fr.org/onboard), une loupe d'écran, [Voxoofox] (http://bertrand.lambard.free.fr/?p=149), Chromium et l'extension speechnotes, Lirecouleur et Tranquility (pour épurer les pages web) pour firefox.

**- Primtux peut être agrémentée de nombreux logiciels libres et non-libres:**  
- Outils: Oracle Java et Free, libdvdcss, photofiltre, xnview, les polices écoles, ardora, webstrict, systemback.
- Éducatifs: activités Jclic, raconte-moi, clicmenu, le matou matheux, la course aux nombres, kiwix-vikidia, je lis avec biba, les exoos d'aleccor, calculatice, chewingword, le conjugueur, atlas Houot, exercices Beaunis et free, solitaire, primaths, lettergames, Je compte ça compte, dropbox, [appliplanet] (http://applipla.net/), xnconvert, [Donner la parole] (http://donnerlaparole.sourceforge.net/installer.html), posop, araword.

----------

**- Poids:** 3,9 go / 10 go installée - **Consommation RAM:** 250 mo au chargement. 768 mo de ram (et swap de 1.5 fois la ram, habituel sous linux) sont conseillés pour la faire tourner correctement.

----------

**Support :**

- <http://primtux.fr>
- <http://forum.primtux.fr>
- <http://wiki.primtux.fr>
- <http://wiki.primtux.fr/doku.php/primtux2-dys>
- <http://ressources.primtux.fr>
- Sources de la distribution: <https://git.framasoft.org/Steph/primtux2-dys/tree/master>
- Page Debian Derivative de PrimTux: <https://wiki.debian.org/Derivatives/Census/PrimTux>
- Page Distrowatch de PrimTux: <http://distrowatch.com/primtux>
- Guide de démarrage rapide: <http://primtux.developpez.com/tutoriels/demarrage-rapide/>
- Un très bel article chez Mammoutux: <http://mammoutux.free.fr/?PrimTux-la-nouvelle-distribution>

----------

**Changelog :**

### 2017-04-18 PrimTux2-Dys - Première version

