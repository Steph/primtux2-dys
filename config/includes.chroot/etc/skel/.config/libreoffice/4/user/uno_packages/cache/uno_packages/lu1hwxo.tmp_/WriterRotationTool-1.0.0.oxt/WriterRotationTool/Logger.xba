<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE script:module PUBLIC "-//OpenOffice.org//DTD OfficeDocument 1.0//EN" "module.dtd">
<script:module xmlns:script="http://openoffice.org/2000/script" script:name="Logger" script:language="StarBasic">
option explicit

&apos;=============================================================================
&apos;	author		: Cedric Bosdonnat &lt;cbosdonnat@starxpert.fr&gt;
&apos;	contributor	: Thibault Vataire &lt;tvataire@starxpert.fr&gt;
&apos;	version		: 0.3.0
&apos;	date		: 06/06/09
&apos;=============================================================================
&apos;Dependencies	: Util
&apos;=============================================================================
&apos;Licence		: LGPL V3.0
&apos;=============================================================================

public const INT_LEVEL_NONE = 0
public const INT_LEVEL_ERROR = 1
public const INT_LEVEL_WARNING = 2
public const INT_LEVEL_DEBUG = 3

&apos;max log file size in bytes
private const INT_DEFAULT_MAX_LOG_SIZE = 1048576
private const STR_ROTATION_STAMP = &quot;============== ROTATION ==============&quot;
private const STR_LOG_DIR = &quot;log&quot;

private iFile as integer
private sLogFile as string
private iLogLevel as integer
private bIsLogSessionInit as boolean
&apos;max size of the log file in bytes. if 0, log rotation is deactivated.
private dMaxLogSize as double


&apos;=============================================================================================================================
&apos;PUBLIC FUNCTIONS
&apos;=============================================================================================================================

&apos;==============================================================================
&apos;	function: initLogSession
&apos;		Initialise la session :
&apos;		- ouvre le fichier
&apos;		- défini la taille maximale du fichier de log à INT_DEFAULT_MAX_LOG_SIZE octets
&apos;==============================================================================
function initLogSession(logFileName as string, level as integer, optional reset as boolean) as boolean

	dim success as boolean
	dim sfa as object
	dim logPath as string
	dim tmpLogPath as string
	dim pathIndex as long

	dMaxLogSize = INT_DEFAULT_MAX_LOG_SIZE
	bIsLogSessionInit = true
	on local error goto errInit
	logPath = addUrlSepIfNeeded(_getStoragePath()) &amp; STR_LOG_DIR
	sfa = createUnoService(&quot;com.sun.star.ucb.SimpleFileAccess&quot;)
	if (not sfa.exists(logPath)) then
		sfa.createFolder(logPath)
	end if
	tmpLogPath = logPath
	do while (sfa.exists(logPath) and (not sfa.isFolder(logPath)))
		logPath = tmpLogPath + pathIndex
		pathIndex = pathIndex + 1
	loop
	sLogFile = addUrlSepIfNeeded(logPath) &amp; logFileName
	if (isMissing(reset)) then
		reset = false
	end if
	iFile = freeFile()
	if (fileExists(sLogFile) and (not reset)) then
		open sLogFile for append as #iFile
	else
		open sLogFile for output as #iFile
	end if
	_setLogLevel(level)
	goto finally:
	errInit:
		closeLogSession()
	finally:

	initLogSession = bIsLogSessionInit

end function

&apos;==============================================================================
&apos;	sub: closeLogSession
&apos;		Termine la session et libère les ressources
&apos;
&apos;==============================================================================
sub closeLogSession()

	on local error resume next
	close #iFile
	sLogFile = &quot;&quot;
	iLogLevel = 0
	bIsLogSessionInit = false
	
end sub

&apos;==============================================================================
&apos;	function: logError
&apos;		Ajoute une erreur dans le fichier de log si le niveau de log 
&apos;		est suffisant.
&apos;
&apos;	Paramètres:
&apos;		sMsg - le message d&apos;erreur à reporter
&apos;
&apos;==============================================================================
sub logError(sMsg as String)

	if iLogLevel &gt;= INT_LEVEL_ERROR then
		_writeToFile(&quot;[&quot; &amp; Date &amp; &quot; &quot; &amp; Time &amp; &quot;] ERROR &gt;&gt;&gt; &quot; &amp; sMsg)
	end if
	
end sub

&apos;==============================================================================
&apos;	function: logWarning
&apos;		Ajoute un warning dans le fichier de log si le niveau de log 
&apos;		est suffisant.
&apos;
&apos;	Paramètres:
&apos;		sMsg - le message du warning à reporter
&apos;
&apos;==============================================================================
sub logWarning(sMsg as String)

	if iLogLevel &gt;= INT_LEVEL_WARNING then
		_writeToFile(&quot;[&quot; &amp; Date &amp; &quot; &quot; &amp; Time &amp; &quot;] WARNING &gt;&gt;&gt; &quot; &amp; sMsg)
	end if
	
end sub

&apos;==============================================================================
&apos;	function: logDebug
&apos;		Ajoute une trace de debug dans le fichier de log si le niveau de log 
&apos;		est suffisant.
&apos;
&apos;	Paramètres:
&apos;		sMsg - la trace de debug à reporter
&apos;
&apos;==============================================================================
sub logDebug(sMsg as String)

	if iLogLevel &gt;= INT_LEVEL_DEBUG then
		_writeToFile(&quot;[&quot; &amp; Date &amp; &quot; &quot; &amp; Time &amp; &quot;] DEBUG &gt;&gt;&gt; &quot; &amp; sMsg)
	end if
	
end sub

&apos;==============================================================================
&apos;	function: setLogSizeRotationTrigger
&apos;		Défini la taille maximale du fichier de log en octets.
&apos;		Au delà de cette taille, une copie du fichier est effectué et
&apos;		le fichier actuel est vidé.
&apos;		Une taille de O désactive la rotation. 
&apos;
&apos;	Paramètres:
&apos;		dSize - taille maximale du fichier de log en octets
&apos;
&apos;==============================================================================
sub setLogSizeRotationTrigger(dSize as double)

	if (dSize &lt; 0) then
		dSize = 0
	end if
	dMaxLogSize = dSize

end sub

&apos;==============================================================================
&apos;	function: getLogPath
&apos;
&apos;		Retourne le chemin du fichier de log de la session courrante ou bien
&apos;		une chaine de caractère vide si la session n&apos;a pas été initialisée. 
&apos;
&apos;==============================================================================
function getLogPath() as string

	getLogPath = sLogFile

end function


&apos;=============================================================================================================================
&apos;PRIVATE FUNCTIONS
&apos;=============================================================================================================================

&apos;==============================================================================
&apos;	sub: -setLogLevel
&apos;		Change le niveau de log minimal a ecrire dans le fichier.
&apos;
&apos;	paramètres:
&apos;		level - Niveau de log parmi INT_LEVEL_NONE, INT_LEVEL_ERROR, INT_LEVEL_WARNING, INT_LEVEL_DEBUG
&apos;
&apos;==============================================================================
sub _setLogLevel(level as Integer)

	if level &lt; INT_LEVEL_NONE then level = INT_LEVEL_NONE
	if level &gt; INT_LEVEL_DEBUG then level = INT_LEVEL_DEBUG
	iLogLevel = level
	
end sub

&apos;==============================================================================
&apos;	function: _writeToFile
&apos;		Ecrit une ligne dans le fichier de log. Ne fait rien si le chemin 
&apos;		vers le fichier de log n&apos;est pas encore specifie
&apos;		if an error occurs nothing is done, so it&apos;s safe to call this 
&apos;		method if no session has been initialised.
&apos;
&apos;	paramètres:
&apos;		message à ecrite dans le fichier de log
&apos;
&apos;	*Cette fonction est a usage interne uniquement*
&apos;
&apos;==============================================================================
sub _writeToFile(sMsg as String)

	if (bIsLogSessionInit) then
		_rotateIfNeeded()
		on local error resume next
		print #iFile, (sMsg &amp; chr(10))
	end if

end sub

&apos;saves the current log file and creates a new one.
sub _rotateIfNeeded()

	dim const backupSuffix = &quot;0&quot;
	dim sfa as object
	
	&apos;if an error occurs, log rotation is aborted
	on local error goto unexpErr
	sfa = createUnoService(&quot;com.sun.star.ucb.SimpleFileAccess&quot;)
	&apos;if the max size of log file is 0, rotation is deactivated
	if ((dMaxLogSize &gt; 0) and (sfa.getSize(sLogFile) &gt;= dMaxLogSize)) then
		&apos;closes the file if it was previously opened
		close #iFile
		&apos;copies the current log file
		sfa.copy(sLogFile, (sLogFile &amp; &quot;0&quot;))
		&apos;resets the log file
		iFile = freeFile()
		open sLogFile for output as #iFile
		&apos;keeps a trace in the new file
		print #iFile, (STR_ROTATION_STAMP &amp; chr(10))
	end if
	unexpErr:

end sub

&apos;==============================================================================
&apos;	function: _getStoragePath
&apos;		Retourne le chemin correspondant à la variable d&apos;environnement
&apos;		&quot;$(userpath)/store&quot;
&apos;		Could raise a com.sun.star.container.NoSuchElementException, com.sun.star.uno.Exception
&apos;
&apos;	*Cette fonction est a usage interne uniquement*
&apos;
&apos;==============================================================================
function _getStoragePath() as string

	dim param(0) as new com.sun.star.beans.PropertyValue

	param(0).name = &quot;nodepath&quot;
	param(0).value = &quot;/org.openoffice.Office.Common/Path/Current&quot;
	
	_getStoragePath = createUnoService(&quot;com.sun.star.util.PathSubstitution&quot;).substituteVariables(_
					  createUnoService(&quot;com.sun.star.configuration.ConfigurationProvider&quot;).createInstanceWithArguments(_
					  																	   &quot;com.sun.star.configuration.ConfigurationUpdateAccess&quot;, _
					  																	   param).getByName(&quot;Storage&quot;), true)

end function

</script:module>