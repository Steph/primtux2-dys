set etapes 1
set niveaux {0 1 3 5}
::1
set niveau 1
set ope {{2 4} {1 4} {2 4} {1 4}}
set interope {{1 7 1} {1 6 1} {1 7 1} {1 6 1}}
set ope1 [expr int(rand()*[lindex [lindex $ope 0] 1]) + [lindex [lindex $ope 0] 0]]
set ope2 [expr int(rand()*[lindex [lindex $ope 1] 1]) + [lindex [lindex $ope 1] 0]]
set ope3 [expr int(rand()*[lindex [lindex $ope 2] 1]) + [lindex [lindex $ope 2] 0]]
set ope4 [expr int(rand()*[lindex [lindex $ope 3] 1]) + [lindex [lindex $ope 3] 0]]
set volatil 0
set operations {{0+0}}
set enonce "Les billes.\nTim et Tom mettent ensemble leurs billes.\nTim a $ope1 sacs de 10 billes et $ope2 billes.\nTom a $ope3 sacs de 10 billes et $ope4 billes.\nCombien cela fait-il de billes en tout?"
set cible {{5 4 {} source0} {5 4 {} source1}}
set intervalcible 60
set taillerect 45
set orgy 40
set orgxorig 50
set orgsourcey 100
set orgsourcexorig 600
set source {sac_billes.gif bille.gif}
set orient 0
set labelcible {{Sacs de billes} Billes}
set quadri 0
set reponse [list [list {1} [list {Tim et Tom ont en tout} [expr ($ope1 + $ope3)*10 + ($ope2+$ope4)] {billes.}]]]
set ensembles [list [expr ($ope1+$ope3)] [expr $ope2+$ope4]]
set canvash 300
set c1height 160
set opnonautorise {}
::

