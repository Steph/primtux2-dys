#!/bin/sh
#menus.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
#**************************************************************************
#  File  : $$$
#  Author  : davidlucardi@aol.com
#  Modifier: 
#  Date    : 
#  Licence : GNU/GPL Version 2 ou plus
# 
#  Description:
#  ------------
# 
#  @version    
#  @author     David Lucardi
#  @modifier   
#  @project    Le terrier
#  @copyright  Eric Seigne 
# 
#  *************************************************************************



global sysFont types oldmarque listexo dirty
variable substexte
variable msel1
set substexte 1
set msel1 1
set dirty 0

set types {
    {{Fichiers texte, aller} {.txt .alr}}
}
set oldmarque "rien"

#t est un objet text
#w le nom a donnner au menu-frame
#p le nom de la toplevel parente
proc menu_frame {w t} {
global listexo
    menubutton $w.file -text [mc {Fichier}] -menu $w.file.m -pady 7
    menubutton $w.edit -text [mc {Edition}] -menu $w.edit.m -pady 7
    menubutton $w.marquer -text [mc {Marquer pour}] -menu $w.marquer.m -pady 7
    bind .menu.marquer <1> "updatemenus"

#le menu fichier
    menu $w.file.m -background white -tearoff 0
    $w.file.m add command -label "[mc {Nouveau}] (Crtl-n)" -command "nouveau $t"
    $w.file.m add command -label "[mc {Ouvrir}] (Crtl-o)" -command "charge $t none"
    $w.file.m add command -label "[mc {Enregistrer}] (Crtl-w)" -command "sauve $t"
    $w.file.m add command -label "[mc {Enregistrer Sous}]" -command "sauvesous $t"
    $w.file.m add separator
    $w.file.m add command -label "[mc {Quitter}] (Crtl-q)" -command "destroy ."

bind . <Control-n> "nouveau $t"
bind . <Control-o> "charge $t none"
bind . <Control-w> "sauve $t"
bind . <Control-q> "destroy ."
bind . <Control-c> "tk_textCopy $t"
bind . <Control-v> "tk_textPaste $t"
bind . <Control-x> "tk_textCut $t"
#le menu d'�dition
    menu $w.edit.m -background white -tearoff 0
    $w.edit.m add command -label [mc {Copier (Ctrl-c)}] -command "tk_textCopy $t"
    $w.edit.m add command -label [mc {Couper (Ctrl-x)}] -command "tk_textCut $t"
    $w.edit.m add command -label [mc {Coller (Ctrl-v)}] -command "tk_textPaste $t"
#le menu marquage
    menu $w.marquer.m -background white -tearoff 0 
    $w.marquer.m add radio -label [lindex [lindex $listexo 0] 1] -variable marque -value green -background green -command "coloremarque $t"
    $w.marquer.m add radio -label [lindex [lindex $listexo 1] 1] -variable marque -value blue -background blue -command "coloremarque $t"
    $w.marquer.m add radio -label [lindex [lindex $listexo 2] 1] -variable marque -value pink -background pink -command "coloremarque $t"
    $w.marquer.m add radio -label [lindex [lindex $listexo 3] 1] -variable marque -value orange -background orange -command "coloremarque $t"
    $w.marquer.m add radio -label [lindex [lindex $listexo 4] 1] -variable marque -value grey -background grey -command "coloremarque $t"
    $w.marquer.m add radio -label [lindex [lindex $listexo 5] 1] -variable marque -value yellow -background yellow -command "coloremarque $t"
    $w.marquer.m add radio -label [lindex [lindex $listexo 6] 1] -variable marque -value purple -background purple -command "coloremarque $t"
    $w.marquer.m add radio -label [lindex [lindex $listexo 7] 1] -variable marque -value bisque1 -background bisque1 -command "coloremarque $t"
    $w.marquer.m add radio -label [lindex [lindex $listexo 8] 1] -variable marque -value bisque2 -background bisque2 -command "coloremarque $t"
    $w.marquer.m add radio -label [lindex [lindex $listexo 9] 1] -variable marque -value bisque3 -background bisque3 -command "coloremarque $t"
    $w.marquer.m add radio -label [lindex [lindex $listexo 10] 1] -variable marque -value bisque4 -background bisque4 -command "coloremarque $t"
    $w.marquer.m add radio -label [lindex [lindex $listexo 13] 1] -variable marque -value red -background red -command "coloremarque $t"

    $w.marquer.m add radio -label [mc {Rien}] -variable marque -value rien -background white -command "coloremarque $t"
    pack $w.file -side left
    pack $w.edit -side left
    pack $w.marquer -side left
    $w.marquer.m invoke 12
    return $w
}





proc coloremarque {t} {
variable marque 
variable msel1
#variable substexte
#variable sel3

global sysFont listexo oldmarque
set ind [lsearch {green blue pink orange grey yellow purple bisque1 bisque2 bisque3 bisque4 none none red} $oldmarque]
if {$ind != -1} {
#set type [lindex [lindex $listexo $ind] 0]
set listexo [lreplace $listexo $ind $ind \173[lindex [lindex $listexo $ind] 0]\175\040\173[string map {\" ""} [.bframe.entitre get]]\175\040\173[string map {\" ""} [.bframe.enfaire get]]\175\040$msel1\040[lindex [lindex $listexo $ind] 4]]
}
catch {destroy .bframe}
frame .bframe -height 40
pack .bframe -side bottom -fill both
label .bframe.consigne
grid .bframe.consigne -row 0 -column 0 -columnspan 3

###############bidouillage pour contourner ce qui para�t �tre un bogue tcl/tk
set texte [$t get 1.0 "end - 1 chars"]
set texte [string map {\" \\" \[ \\[ \] \\] \{ \\{ \} \\}} $texte]
append tmp "\$t insert end \"$texte\"\n"
    foreach tag [$t tag names] {
        if {[$t tag ranges $tag] != ""} {
	  append tmp "\$t tag add $tag [$t tag ranges $tag]\n"
	  }
    }
    foreach tg {green blue pink orange grey yellow purple bisque1 bisque2 bisque3 bisque4 red} {
    $t tag delete $tg
    }
$t delete 0.0 end
eval $tmp
######################fin bidouillage
#if {$marque == "grey"} {
#   checkbutton .bframe.checksubst -text [mc {Substituer au texte}] -variable substexte -relief flat -activebackground grey
#   grid .bframe.checksubst -row 3 -column 2
#   }

if {$marque != "rien"} {
.text tag configure $marque -background $marque
}

grid .bframe.consigne -row 1 -column 0 
set ind [lsearch {green blue pink orange grey yellow purple bisque1 bisque2 bisque3 bisque4 none none red} $marque]
if {$ind != -1} {
#set sel3 [lindex [lindex $listexo $ind] 4]
#checkbutton .bframe.sel3 -text [mc {Exercice autoris�}] -variable sel3 -relief flat
#grid .bframe.sel3 -row 1 -column 0 -sticky w -padx 1c
grid .bframe.consigne -row 1 -column 1 
label .bframe.titre -text [mc {Titre de l'exercice}]
grid .bframe.titre -row 2 -column 0 -sticky w -padx 1c
entry .bframe.entitre 
grid .bframe.entitre -row 3 -column 0 -sticky w -padx 1c
label .bframe.faire -text [mc {Consigne}]
grid .bframe.faire -row 2 -column 1 -sticky w
entry .bframe.enfaire -width 35
grid .bframe.enfaire -row 3 -column 1 -sticky w 
.bframe.entitre insert end [lindex [lindex $listexo $ind] 1]
.bframe.enfaire insert end [lindex [lindex $listexo $ind] 2]
#.bframe.consigne configure -text "Type de l'exercice : [lindex [lindex $listexo $ind] 0]"
}
if {[string match bisque1 $marque]== 1 || [string match bisque2 $marque]== 1} {
label .bframe.men1 -text [mc {Type de l'exercice}]
grid .bframe.men1 -row 1 -column 2 -sticky w -padx 1c
radiobutton .bframe.sel1 -text [mc {Reconnaitre}] -variable msel1 -relief flat -value 1
grid .bframe.sel1 -row 2 -column 2 -sticky w -padx 1c
radiobutton .bframe.sel2 -text [mc {Completer}] -variable msel1 -relief flat -value 2
grid .bframe.sel2 -row 3 -column 2 -sticky w -padx 1c
.bframe.sel[lindex [lindex $listexo $ind] 3] select
}

set oldmarque $marque
}

proc nouveau {t} {
global categorie listexo tabaide tablongchamp tablistevariable tabstartdirect tablecture_mot tablecture_mot_cache dirty
if {$dirty == 1} {
set answer [tk_messageBox -message [mc {Enregistrer les modifications?}] -type yesno -icon info]
if {$answer == "yes"} {
sauve $t
}
}
set listexo \173closure\040[mc {Closure}]\040[mc {Complete}]\0401\0401\175\040\173reconstitution\040[mc {Reconstitution}]\040[mc {Complete}]\0401\0401\175\040\173phrase\040\173[mc {Phrases melangees}]\175\040\173[mc {Clique sur une phrase et deplace-la en cliquant sur les fleches}]\175\0401\0401\175\040\173mot\040\173[mc {Mots melanges}]\175\040\173[mc {Remets les mots en ordre en les deplacant sur les traits roses.}]\175\0401\0401\175\040\173faute\040\173[mc {Texte a corriger}]\175\040\173[mc {Clique sur les mots mal ecrits, reecris-les et appuie sur entree.}]\175\0401\0401\175\040\173espace\040\173[mc {Phrases sans espaces}]\175\040\173[mc {Clique pour separer les mots.}]\175\0401\0401\175\040\173incomplet\040\173[mc {Phrases incompletes}]\175\040\173[mc {Complete}]\175\0401\0401\175\040\173\173[mc {Exercice supplementaire}]\175\040\173[mc {Exercice supplementaire 1}]\175\040\173[mc {Exercice 1}]\175\0401\0400\175\040\173\173[mc {Exercice supplementaire}]\175\040\173[mc {Exercice supplementaire 2}]\175\040\173[mc {Exercice 2}]\175\0401\0400\175\040\173\173[mc {Flash}]\175\040\173[mc {Flash}]\175\040\173[mc {Clique sur le mot convenable}]\175\0401\0401\175\040\173\173[mc {Rapido}]\175\040\173[mc {Rapido}]\175\040\173[mc {Clique sur le mot convenable}]\175\0401\0401\175\040\173ponctuation1\040[mc {Ponctuation1}]\040\173[mc {Complete avec le signe de ponctuation convenable et appuie sur la touche entree pour valider.}]\175\0401\0401\175\040\173ponctuation2\040[mc {Ponctuation2}]\040\173[mc {Clique pour retablir la ponctuation et complete avec le signe convenable.}]\175\0401\0401\175\040\173Dictee\040[mc {Dict�e}]\040\173[mc {Compl�te et appuie sur le bouton pouce.}]\175\0401\0401\175
#set listexo \173closure\040[mc {Closure}]\040[mc {Complete}]\0401\0401\175\040\173reconstitution\040[mc {Reconstitution}]\040[mc {Complete}]\0401\0401\175\040\173phrase\040\173[mc {Phrases melangees}]\175\040\173[mc {Clique sur une phrase et deplace-la en cliquant sur les fleches}]\0401\0401\175\0401\0401\175\040\173mot\040\173[mc {Mots melanges}]\175\040\173[mc {Remets les mots en ordre en les deplacant sur les traits roses.}]\0401\0401\175\0401\0401\175\040\173faute\040\173[mc {Texte a corriger}]\175\040\173[mc {Clique sur les mots mal ecrits, reecris-les et appuie sur entree.}]\0401\0401\175\0401\0401\175\040\173espace\040\173[mc {Phrases sans espaces}]\175\040\173[mc {Clique pour separer les mots.}]\0401\0401\175\0401\0401\175\040\173incomplet\040\173[mc {Phrases incompletes}]\175\040\173[mc {Complete}]\0401\0401\175\0401\0401\175\040\173\173[mc {Exercice supplementaire}]\175\040\173[mc {Exercice supplementaire 1}]\175\040\173[mc {Exercice 1}]\0401\0401\175\0401\0400\175\040\173\173[mc {Exercice supplementaire}]\175\040\173[mc {Exercice supplementaire 2}]\175\040\173[mc {Exercice 2}]\0401\0401\175\0401\0400\175\040\173\173[mc {Exercice supplementaire}]\175\040\173[mc {Exercice supplementaire 3}]\175\040\173[mc {Exercice 3}]\0401\0401\175\0401\0400\175\040\173\173[mc {Exercice supplementaire}]\175\040\173[mc {Exercice supplementaire 4}]\175\040\173[mc {Exercice 4}]\0401\0401\175\0401\0400\175\040\173ponctuation1\040[mc {Ponctuation1}]\040\173[mc {Complete avec le signe de ponctuation convenable et appuie sur la touche entree pour valider.}]\0401\0401\175\0401\0401\175\040\173ponctuation2\040[mc {Ponctuation2}]\040\173[mc {Clique pour retablir la ponctuation et complete avec le signe convenable.}]\0401\0401\175\0401\0401\175

foreach i {closure reconstitution phrase mot faute espace incomplet bisque1 bisque2 flash rapido ponctuation1 ponctuation2 dictee} {
set tabaide($i) 2
set tablongchamp($i) 1
set tablistevariable($i) 1
set tabstartdirect($i) 1
set tablecture_mot($i) 1
set tablecture_mot_cache($i) 1
}
$t delete 1.0 end
set categorie ""
wm title [winfo toplevel $t] "Nouveau"
}

proc charge {t fil} {
global sysFont types tableaumots tableauindices tableauindices2 listexo categorie Home plateforme tabaide tablongchamp tablistevariable tabstartdirect initrep textelu tablecture_mot tablecture_mot_cache dirty
variable substexte
if {$dirty == 1} {
set answer [tk_messageBox -message [mc {Enregistrer les modifications?}] -type yesno -icon info]
if {$answer == "yes"} {
sauve $t
}
}

set dirty 0
catch {.menu.marquer.m invoke 12}

set listexo \173closure\040[mc {Closure}]\040[mc {Complete}]\0401\0401\175\040\173reconstitution\040[mc {Reconstitution}]\040[mc {Complete}]\0401\0401\175\040\173phrase\040\173[mc {Phrases melangees}]\175\040\173[mc {Clique sur une phrase et deplace-la en cliquant sur les fleches}]\175\0401\0401\175\040\173mot\040\173[mc {Mots melanges}]\175\040\173[mc {Remets les mots en ordre en les deplacant sur les traits roses.}]\175\0401\0401\175\040\173faute\040\173[mc {Texte a corriger}]\175\040\173[mc {Clique sur les mots mal ecrits, reecris-les et appuie sur entree.}]\175\0401\0401\175\040\173espace\040\173[mc {Phrases sans espaces}]\175\040\173[mc {Clique pour separer les mots.}]\175\0401\0401\175\040\173incomplet\040\173[mc {Phrases incompletes}]\175\040\173[mc {Complete}]\175\0401\0401\175\040\173\173[mc {Exercice supplementaire}]\175\040\173[mc {Exercice supplementaire 1}]\175\040\173[mc {Exercice 1}]\175\0401\0400\175\040\173\173[mc {Exercice supplementaire}]\175\040\173[mc {Exercice supplementaire 2}]\175\040\173[mc {Exercice 2}]\175\0401\0400\175\040\173\173[mc {Flash}]\175\040\173[mc {Flash}]\175\040\173[mc {Clique sur le mot convenable}]\175\0401\0401\175\040\173\173[mc {Rapido}]\175\040\173[mc {Rapido}]\175\040\173[mc {Clique sur le mot convenable}]\175\0401\0401\175\040\173ponctuation1\040[mc {Ponctuation1}]\040\173[mc {Complete avec le signe de ponctuation convenable et appuie sur la touche entree pour valider.}]\175\0401\0401\175\040\173ponctuation2\040[mc {Ponctuation2}]\040\173[mc {Clique pour retablir la ponctuation et complete avec le signe convenable.}]\175\0401\0401\175\040\173Dictee\040[mc {Dict�e}]\040\173[mc {Compl�te et appuie sur le bouton pouce.}]\175\0401\0401\175

#set listexo \173closure\040[mc {Closure}]\040[mc {Complete}]\0401\0401\175\040\173reconstitution\040[mc {Reconstitution}]\040[mc {Complete}]\0401\0401\175\040\173phrase\040\173[mc {Phrases melangees}]\175\040\173[mc {Clique sur une phrase et deplace-la en cliquant sur les fleches}]\0401\0401\175\0401\0401\175\040\173mot\040\173[mc {Mots melanges}]\175\040\173[mc {Remets les mots en ordre en les deplacant sur les traits roses.}]\0401\0401\175\0401\0401\175\040\173faute\040\173[mc {Texte a corriger}]\175\040\173[mc {Clique sur les mots mal ecrits, reecris-les et appuie sur entree.}]\0401\0401\175\0401\0401\175\040\173espace\040\173[mc {Phrases sans espaces}]\175\040\173[mc {Clique pour separer les mots.}]\0401\0401\175\0401\0401\175\040\173incomplet\040\173[mc {Phrases incompletes}]\175\040\173[mc {Complete}]\0401\0401\175\0401\0401\175\040\173\173[mc {Exercice supplementaire}]\175\040\173[mc {Exercice supplementaire 1}]\175\040\173[mc {Exercice 1}]\0401\0401\175\0401\0400\175\040\173\173[mc {Exercice supplementaire}]\175\040\173[mc {Exercice supplementaire 2}]\175\040\173[mc {Exercice 2}]\0401\0401\175\0401\0400\175\040\173\173[mc {Exercice supplementaire}]\175\040\173[mc {Exercice supplementaire 3}]\175\040\173[mc {Exercice 3}]\0401\0401\175\0401\0400\175\040\173\173[mc {Exercice supplementaire}]\175\040\173[mc {Exercice supplementaire 4}]\175\040\173[mc {Exercice 4}]\0401\0401\175\0401\0400\175\040\173ponctuation1\040[mc {Ponctuation1}]\040\173[mc {Complete avec le signe de ponctuation convenable et appuie sur la touche entree pour valider.}]\0401\0401\175\0401\0401\175\040\173ponctuation2\040[mc {Ponctuation2}]\040\173[mc {Clique pour retablir la ponctuation et complete avec le signe convenable.}]\0401\0401\175\0401\0401\175

foreach i {closure reconstitution phrase mot faute espace incomplet bisque1 bisque2 flash rapido ponctuation1 ponctuation2 dictee} {
set tabaide($i) 2
set tablongchamp($i) 1
set tablistevariable($i) 1
set tabstartdirect($i) 1
set tablecture_mot($i) 1
set tablecture_mot_cache($i) 1
}
if {[file tail $fil] == "Au choix" || [file tail $fil] == "none" || [file tail $fil] == ""} {
if {$plateforme == "windows"} {wm iconify .}
set file [tk_getOpenFile -initialdir [file join $initrep] -filetypes $types]
if {$plateforme == "windows"} {wm deiconify .}

} else {
set file $fil
}
set tampon ""
    if {$file != ""} {
    $t delete 1.0 end
        if [string match *.alr $file] {
	  source $file
        set auto 1
	  } else {
	  set f [open $file]
	      while {![eof $f]} {
	      #$t insert end [read $f 10000]
		set tampon [concat $tampon [read $f 10000]]
	      }
      set tampon [string map {\� \" \� \"} $tampon]
	regsub -all \040+ $tampon \040 tampon
	regsub -all {[�]} $tampon "..." tampon 
	regsub -all {[�]} $tampon "'" tampon 
	regsub -all {[-]} $tampon "-" tampon 
	regsub -all {[�]} $tampon "oe" tampon
	regsub -all {(\040\.)} $tampon "." tampon 
	regsub -all {(\040\!)} $tampon "!" tampon 
	regsub -all {(\040\?)} $tampon "?" tampon 
	regsub -all {(\040,)} $tampon "," tampon 
	regsub -all {(\040;)} $tampon ";" tampon 
	#regsub -all {[�]} $tampon "" tampon
	#regsub -all {[�]} $tampon "" tampon
	$t insert end $tampon

        set auto 0
        }
    wm title [winfo toplevel $t] "$file"
    set categorie [file tail $file]
    set initrep [file dirname $file]
    } else {
    return 2
    }
set textelu [.text  get 1.0 "end - 1 chars"]

return $auto
}

proc sauvesous {t} {
global initrep categorie
set types {    {{Fichiers aller}            {.alr}        }}
catch {set name [tk_getSaveFile -filetypes $types -initialdir [file join $initrep] -initialfile $categorie]}
if {$name != ""} {
set categorie $name
sauve $t
}
}

proc sauve {t} {
global sysFont basedir tableaumots tableauindices tableauindices2 listexo Home tabaide tablongchamp tablistevariable tabstartdirect initrep categorie tablecture_mot tablecture_mot_cache dirty
variable msel1
#variable sel3
variable marque
variable substexte
set dirty 0
set ext .alr
set ind [lsearch {green blue pink orange grey yellow purple bisque1 bisque2 bisque3 bisque4 none none red} $marque]
if {$ind != -1} {
#set type [lindex [lindex $listexo $ind] 0]
set listexo [lreplace $listexo $ind $ind \173[lindex [lindex $listexo $ind] 0]\175\040\173[string map {\" ""} [.bframe.entitre get]]\175\040\173[string map {\" ""} [.bframe.enfaire get]]\175\040$msel1\040[lindex [lindex $listexo $ind] 4]\040[lindex [lindex $listexo $ind] 5]\040[lindex [lindex $listexo $ind] 6]]
}

	if {$categorie ==""} {
	set types {    {{Fichiers aller}            {.alr}        }}
	catch {set name [tk_getSaveFile -filetypes $types -initialdir [file join $initrep] -initialfile $categorie]}
	} else {
	set name $categorie
	}
regsub -all {(\.txt)} $name "" name
regsub -all {(\.alr)} $name "" name
    if {$name != ""} {
    set name $name$ext
    set texte [$t get 1.0 "end - 1 chars"]
    set texte [string map {\� \" \� \"} $texte]
    set texte [string map {\" \\" \[ "" \] "" \{ "" \} ""} $texte]
	regsub -all \040+ $texte \040 texte
	regsub -all {[�]} $texte "..." texte 
	regsub -all {[�]} $texte "'" texte 
	regsub -all {[-]} $texte "-" texte 
	regsub -all {[�]} $texte "oe" texte
	regsub -all {(\040\.)} $texte "." texte 
	regsub -all {(\040\!)} $texte "!" texte 
	regsub -all {(\040\?)} $texte "?" texte  
	regsub -all {(\040,)} $texte "," texte 
	regsub -all {(\040;)} $texte ";" texte 

	#regsub -all {[�]} $texte "" texte
	#regsub -all {[�]} $texte "" texte
	wm title [winfo toplevel $t] "$name"


    append tmp "\$t insert end \"$texte\"\n"
        foreach tag [$t tag names] {
            if {[$t tag ranges $tag] != ""} {
	      append tmp "\$t tag add $tag [$t tag ranges $tag]\n"
	      }
        }
    catch {
    set list [array get tableaumots]
    for {set i 0} {$i < [llength $list]} {incr i 2} { 
    append tmp "set tableaumots([string map {" " \\040 \042 \\042 \n \\n} [lindex $list $i]]) [string map {" " \\040 \042 \\042 \n \\n} [lindex $list [expr $i + 1]]]\n"

    }
    }
    catch {
    set list [array get tableauindices]
    for {set i 0} {$i < [llength $list]} {incr i 2} {  
    append tmp "set tableauindices([string map {" " \\040 \042 \\042 \n \\n} [lindex $list $i]]) [string map {" " \\040 \042 \\042 \n \\n} [lindex $list [expr $i + 1]]]\n"
    }
    }
    catch {
    set list [array get tableauindices2]
    for {set i 0} {$i < [llength $list]} {incr i 2} {  
    append tmp "set tableauindices2([string map {" " \\040 \042 \\042 \n \\n} [lindex $list $i]]) [string map {" " \\040 \042 \\042 \n \\n} [lindex $list [expr $i + 1]]]\n"
    }
    }

    append tmp "set substexte $substexte \n"
    append tmp "set listexo \[lreplace listexo 0 end $listexo\]\n"
    append tmp "array set tabaide \{[array get tabaide]\}\n"
    append tmp "array set tablongchamp \{[array get tablongchamp]\}\n"
    append tmp "array set tablistevariable \{[array get tablistevariable]\}\n"
    append tmp "array set tabstartdirect \{[array get tabstartdirect]\}\n"
    append tmp "array set tablecture_mot \{[array get tablecture_mot]\}\n"
    append tmp "array set tablecture_mot_cache \{[array get tablecture_mot_cache]\}\n"

    set f [open [file join $initrep $name] "w"]  
    puts $f $tmp
    close $f
    set categorie [file tail $name]
    #set initrep [file dirname $name]
    }
}


proc updatemenus {} {
global sysFont listexo
for {set i 0} {$i <= 10} {incr i} {
.menu.marquer.m entryconfigure $i -label [lindex [lindex $listexo $i] 1]
}
}





