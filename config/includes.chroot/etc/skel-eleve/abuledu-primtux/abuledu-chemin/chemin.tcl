#!/bin/sh
# chemin.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 Andr� Connes <andre.connes@toulouse.iufm.fr>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#**************************************************************************
#  File  : chemin.tcl
#  Author  : Andr� Connes <andre.connes@toulouse.iufm.fr>
#  Modifier:
#  Date    : 19/05/2004
#  Licence : GNU/GPL Version 2 ou plus
#
#  Description:
#  ------------
#
#  @version    $Id: chemin.tcl,v 1.20 2006/03/27 13:13:15 abuledu_andre Exp $
#  @author     Andr� Connes
#  @modifier
#  @project    Le terrier
#  @copyright  Andr� Connes
#
#***********************************************************************
global sysFont glob

source chemin.conf
source msg.tcl
source aider.tcl
source fonts.tcl

#if { [catch {package require Iwidgets}] == 1} {
#  tk_messageBox -message "Il est n�cessaire d'installer le package Iwidgets"
#}

# gestion de l'espace de l'utilisateur
  if {![file exists [file join  $glob(home_chemin)]]} {
        catch { file mkdir [file join $glob(home_chemin)]}
  }
  if {![file exists [file join  $glob(home_reglages)]]} {
        catch { file mkdir [file join $glob(home_reglages)] }
  }
  #
  # langue par defaut
  #
  if {![file exists [file join  $glob(home_reglages) lang.conf]]} {
        set f [open [file join $glob(home_reglages) lang.conf] "w"]
        puts $f "fr"
        close $f
  }
  set f [open [file join $glob(home_reglages) lang.conf] "r"]
  gets $f lang
  close $f
  ::msgcat::mclocale $lang
  ::msgcat::mcload [file join [file dirname [info script]] msgs]
  #
  # couleurs par defaut du fond et des cases
  #
  if {![file exists [file join  $glob(home_reglages) couleurs.conf]]} {
        set f [open [file join $glob(home_reglages) couleurs.conf] "w"]
        puts $f "NonDaltonien"
        close $f
  }
  #
  # Repertoire de travail par defaut
  #
  if {![file exists [file join  $glob(home_reglages) dir_exos.conf]]} {
        set f [open [file join $glob(home_reglages) dir_exos.conf] "w"]
        puts $f "Commun"
        close $f
  }
  #
  # internationalisation
  #
  catch { file mkdir [file join $glob(home_msgs)] }
  # astuce suivante : ajoute automatiquement toute nouvelle langue chez l'utilisateur
  foreach f [glob [file join msgs *.msg]] {
        catch { file copy [file join $f] [file join $glob(home_msgs)]}
  }
  if {![file exists [file join  $glob(trace_dir)]]} {
        file mkdir [file join $glob(trace_dir)]
  }
  catch { file mkdir [file join $glob(home_chemin) grilles] }

wm resizable . 0 0
wm geometry . [expr int([winfo vrootwidth .]*0.89)]x[expr int([winfo vrootheight .]*0.8)]+0+0
. configure -background blue
wm title . "[mc {title_m}]"

################################################

proc init_dir_exos {} {
  global glob
  catch {set f [open [file join $glob(home_reglages) dir_exos.conf] "r"]}
  set glob(dir_exos) [gets $f]
  close $f
}

# changer le repertoire de travail
proc change_dir_exos {} {
  global glob dir_exos
  catch {set f [open [file join $glob(home_reglages) dir_exos.conf] "w"]}
  puts $f $dir_exos
  close $f
}

# changer les couleurs
proc change_couleurs {} {
  global glob vcouleurs
  catch {set f [open [file join $glob(home_reglages) couleurs.conf] "w"]}
  puts $f $vcouleurs
  close $f
}

proc setlang {lang} {
  global env glob
  set env(LANG) $lang
  catch {set f [open [file join $glob(home_reglages) lang.conf] "w"]}
  puts $f [file rootname [file tail $lang]]
  close $f

  ::msgcat::mclocale $lang
  ::msgcat::mcload [file join [file dirname [info script]] msgs]

  main_loop
}

proc lanceappli {appli niveau} {
        global glob
        set appli [file join $appli]
        exec $glob(wish) $appli $niveau &
}

proc my_delfile { } {
  global glob
  set tchemin {
        {{Tous les fichiers} *}
  }
  set fname [tk_getOpenFile -title [mc Selection] \
        -initialdir [file join $glob(home_chemin) grilles] \
        -filetypes $tchemin]
  file delete $fname
}

proc setwindowsusername {} {
    global user
    catch {destroy .utilisateur}
    toplevel .utilisateur -background grey -width 250 -height 100
    wm geometry .utilisateur +50+50
    frame .utilisateur.frame -background grey -width 250 -height 100
    pack .utilisateur.frame -side top
    label .utilisateur.frame.labobj -font {Helvetica 10} -text "[mc {Ton nom}] ?" -background grey
    pack .utilisateur.frame.labobj -side top
    entry .utilisateur.frame.entobj -font {Helvetica 10} -width 10
    pack .utilisateur.frame.entobj -side top
    button .utilisateur.frame.ok -background gray75 -text "Ok" -command "verifnom"
    pack .utilisateur.frame.ok -side top -pady 10
}

  proc verifnom {} {
   global glob
   set nom [string tolower [string trim [string map {\040 ""} [.utilisateur.frame.entobj get]]]]
    if {$nom ==""} {
        set nom eleve
    }
    # sauver le r�glage du nom
    catch {set f [open [file join $glob(home_chemin) reglages trace_user] "w"]}
    set glob(trace_user) "$glob(trace_dir)/$nom"
    puts $f $glob(trace_user)
    close $f
    catch {destroy .utilisateur}
}

##################################################################

proc main_loop {} {
  global c sysFont glob

  #########################
  #      Barre de menus   #
  #########################

  # Creation du menu utilise comme barre de menu
  #---------------------------------------------
  catch {destroy .menu}
  catch {destroy .frame}

  menu .menu -tearoff 0

  # Creation du menu Fichier
  menu .menu.fichier -tearoff 0
  .menu add cascade -label [mc {Fichier}] -menu .menu.fichier
#  .menu.fichier add command -label [mc {Editer Case}] -command "lanceappli editeur_case.tcl 0"
#  .menu.fichier add command -label [mc {Supprimer Case}] -command my_delfile
#  .menu.fichier add separator
  .menu.fichier add command -label [mc {Bilans}] -command "lanceappli bilan.tcl 0"
  .menu.fichier add separator
  .menu.fichier add command -label [mc {Quitter}] -command exit

  # Creation du menu Action
  menu .menu.action -tearoff 0
  .menu add cascade -label [mc {Activites}] -menu .menu.action
  .menu.action add cascade -label [mc {Cases}] -menu .menu.action.cases
  menu .menu.action.cases -tearoff 0
          .menu.action.cases add command -label [mc Decodage] \
                -command "lanceappli chemin_case_decode.tcl 0"
        .menu.action.cases add command -label [mc Codage_immediat]  \
                -command "lanceappli chemin_case_code_immediat.tcl 0"
        .menu.action.cases add command -label [mc Codage_differe]  \
                -command "lanceappli chemin_case_code_differe.tcl 0"
        .menu.action.cases add command -label [mc Cache]  \
                -command "lanceappli chemin_case_cache.tcl 0"
        .menu.action.cases add separator
        .menu.action.cases add command -label [mc De]  \
                -command "lanceappli chemin_case_un_de.tcl 0"
        .menu.action.cases add command -label [mc DeuxDes]  \
                -command "lanceappli chemin_case_deux_des.tcl 0"
        .menu.action.cases add command -label [mc Chiffre]  \
                -command "lanceappli chemin_case_chiffre.tcl 0"

  .menu.action add cascade -label [mc {Noeuds}] -menu .menu.action.noeud
  menu .menu.action.noeud -tearoff 0
          .menu.action.noeud add command -label [mc Decodage] \
                -command "lanceappli chemin_noeud_decode.tcl 0"
        .menu.action.noeud add command -label [mc Codage_immediat]  \
                -command "lanceappli chemin_noeud_code_immediat.tcl 0"
        .menu.action.noeud add command -label [mc Codage_differe]  \
                -command "lanceappli chemin_noeud_code_differe.tcl 0"
        .menu.action.noeud add command -label [mc Cache]  \
                -command "lanceappli chemin_noeud_cache.tcl 0"
        .menu.action.noeud add separator
        .menu.action.noeud add command -label [mc De]  \
                -command "lanceappli chemin_noeud_un_de.tcl 0"
        .menu.action.noeud add command -label [mc DeuxDes]  \
                -command "lanceappli chemin_noeud_deux_des.tcl 0"
        .menu.action.noeud add command -label [mc Chiffre]  \
                -command "lanceappli chemin_noeud_chiffre.tcl 0"


  .menu.action add cascade -label [mc {Route}] -menu .menu.action.route
  menu .menu.action.route -tearoff 0
          .menu.action.route add command -label [mc Decodage] \
                -command "lanceappli chemin_route_decode.tcl 0"
        .menu.action.route add command -label [mc Codage_immediat]  \
                -command "lanceappli chemin_route_code_immediat.tcl 0"
        .menu.action.route add separator
        .menu.action.route add command -label [mc De]  \
                -command "lanceappli chemin_route_un_de.tcl 0"
        .menu.action.route add command -label [mc DeuxDes]  \
                -command "lanceappli chemin_route_deux_des.tcl 0"
        .menu.action.route add command -label [mc Chiffre]  \
                -command "lanceappli chemin_route_chiffre.tcl 0"

  # Creation du menu Options
  menu .menu.options -tearoff 0
  .menu add cascade -label [mc {Options}] -menu .menu.options

  menu .menu.options.lang -tearoff 0
  .menu.options add cascade -label "[mc {Langue}]" -menu .menu.options.lang

  foreach i [glob [file join  $glob(home_msgs) *.msg]] {
    set langue [string map {.msg "" } [file tail $i]]
    .menu.options.lang add radio -label $langue -variable langue -command "setlang $langue"
  }

  menu .menu.options.wdir -tearoff 0
#  .menu.options add cascade -label [mc {Repertoire de travail}] -menu .menu.options.wdir
#  .menu.options.wdir add radio -label [mc {Commun}] -variable dir_exos -value Commun -command "change_dir_exos"
#  .menu.options.wdir add radio -label [mc {Individuel}] -variable dir_exos -value Individuel -command "change_dir_exos"

  menu .menu.options.couleurs -tearoff 0
  .menu.options add cascade -label [mc {Choix des couleurs}] -menu .menu.options.couleurs
  .menu.options.couleurs add radio -label [mc {Daltonien}] -variable vcouleurs -value Daltonien -command "change_couleurs"
  .menu.options.couleurs add radio -label [mc {Non Daltonien}] -variable vcouleurs -value NonDaltonien -command "change_couleurs"

  
  # Nom de l'utilisateur sous windows
  if {$glob(platform) == "windows"} {
    .menu add command -label [mc {Utilisateur}] -command "setwindowsusername"
  }

  # Creation du menu Aide
  menu .menu.aide -tearoff 0
    .menu add cascade -state normal \
	-label [mc Aide] -menu .menu.aide
  set l_langues [glob  [file join [pwd] aides aide.*.html]]
  foreach langue $l_langues {
    set lang [lindex [split $langue "."] end-1]
    .menu.aide add command -label "[mc Aide] $lang" -command "aider $lang"
  }
  .menu.aide add command -label [mc {A propos ...}] -command "source apropos.tcl"

  . configure -menu .menu

  # creation du frame
  ###################
  . configure -background blue
  frame .frame -background blue -height $glob(height) -width $glob(width)
  pack .frame -side top -fill both -expand yes

  # creation du canvas
  ####################

  set c .frame.c
  canvas $c -width $glob(width) -height $glob(height) -bg blue -highlightbackground blue
  pack $c -expand true

  button $c.case \
  	-image [image create photo -file [file join sysdata abuledu-case.png]] \
  	-borderwidth 5 -cursor heart \
  	-command "lanceappli choix_action.tcl chemin_case"
  grid $c.case -column 0 -row 0 -sticky e -padx 5 -pady 5  	
  button $c.noeud \
  	-image [image create photo -file [file join sysdata abuledu-noeud.png]] \
  	-borderwidth 5 -cursor heart \
  	-command "lanceappli choix_action.tcl chemin_noeud"
  grid $c.noeud -column 1 -row 0 -sticky e -padx 5 -pady 5  	
  button $c.route \
  	-image [image create photo -file [file join sysdata abuledu-route.png]] \
  	-borderwidth 5 -cursor heart \
  	-command "lanceappli choix_action.tcl chemin_route"
  grid $c.route -column 1 -row 1 -sticky e -padx 5 -pady 5  	
  button $c.quitte \
  	-image [image create photo -file [file join sysdata quitter.gif]] \
  	-borderwidth 5 -cursor heart \
  	-command exit
  grid $c.quitte -column 2 -row 1 -sticky e -padx 5 -pady 5  	


  set myimage [image create photo -file sysdata/background.gif]
  label $c.imagedisplayer -image $myimage -background blue
  grid $c.imagedisplayer -column 2 -row 0 -sticky e

#  label $c.nimages -bg blue
#  grid $c.nimages -column 1 -row 2

} ;# fin de main_loop

########################################################################
#                           programme principal                        #
########################################################################

  bind . <Control-q> {exit}

  # Nom de l'utilisateur par d�faut sous windows
  if {$glob(platform) == "windows"} {
     set nom eleve
     catch {set nom $env(USER)}
    # sauver le r�glage du nom
    catch {set f [open [file join $glob(home_chemin) reglages trace_user] "w"]}
    puts $f "$glob(trace_dir)/$nom"
    close $f
  }

main_loop

