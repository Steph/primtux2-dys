#!/bin/sh
#bilan.tcl
#\
exec wish "$0" ${1+"$@"}
#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#**************************************************************************
#  File  : $$$
#  Author  : jlsendral@free.fr
#  Modifier: 
#  Date    : 24/04/2002
#  Licence : GNU/GPL Version 2 ou plus
#
#  Description:
#  ------------
#
#  @version
#  @author     Jean-Louis Sendral
#  @modifier
#  @project    Le terrier
#  @copyright  Eric Seigne 24/04/2002
#
#  *************************************************************************
set basedir [file dir $argv0]
cd $basedir
##global sysFont
source path.tcl
source msg.tcl
##source fonts.tcl
##initlog  $tcl_platform(platform) $nom_elev
set  TRACEDIR  [pwd]
set basedir [pwd]
cd $TRACEDIR
set classe ""
# cd $dir_des_bilans (les classes = les groupes ?)
wm geometry . +0+0
wm title . [mc {bilan_elev}]

. configure  -height 480 -width 660 -background #808080
set font {Arial 12}
menu .menu -tearoff 0
global classe elev
# Creation du menu Classes
menu .menu.class -tearoff 0
.menu add cascade -label [mc {Classes}] -menu .menu.class
proc traite1 { ele cla } {
capturebilan $ele $cla
}
proc traite {class} {
global liste_noms elev
vide_eleve
capturenoms $class
catch {
##.menu.eleves add radio -label "Aucun" -variable elev -command "capturebilan $elev $class "
foreach elev $liste_noms  {
.menu.eleves  add radio -label  $elev  -variable elev -command "capturebilan $elev $class"
}
}
}
catch {
foreach i [lsort [glob -type f -dir [file join $basedir data]  *.bil  ]] {
.menu.class add radio -label [file tail $i ] -variable classe -command "traite [file tail $i]"
}
}
proc verif_pass  {} {
global w2 compt_mp flag_pass
set  mot [${w2}.n_m get ] ;##mot de passe bon???
if { $mot == "abuledu" } {
set flag_pass 1
return 
###	destroy ${w2}
	} else {
tk_messageBox -message  "[mc {Mot de passe incorrect}]" -icon error -type ok -title "[mc {Validation mot de passe} ]"
if {$compt_mp > 2 } { destroy w2 .}
incr compt_mp
##exit
${w2}.n_m delete 0 end
grab $w2
focus -force ${w2}.n_m
return
}
}
proc saisie_password {} {
global w2 compt_mp flag_pass
set compt_mp 0 ; set flag_pass 0
catch {destroy .top2 } 
set w2 [toplevel .top2]
bind $w2 <Destroy> "set flag_pass 2"
wm geometry $w2 250x150+100+0
wm resizable $w2 no no
wm title $w2 [mc {Password}]
catch {grab ${w2}}
lower  . ${w2}
raise ${w2} .
focus -force  ${w2}
label ${w2}.text_m -text [ mc {Mot de passe ?}] -font {arial 10}  -bg red
place ${w2}.text_m -x 35 -y 50
entry ${w2}.n_m -width 6 -font {arial 10} -justify center -show "*"
place ${w2}.n_m -x 110 -y 50
focus ${w2}.n_m
bind ${w2}.n_m <Return> "  verif_pass  "
bind ${w2}.n_m <KP_Enter> "  verif_pass  "
tkwait variable flag_pass
if {$flag_pass == 1} { catch {destroy ${w2} } } else {
if {$flag_pass == 2} { catch {destroy  .  }}
}
}
proc suppression { elev_classe } {
global classe elev basedir tcl_platform
###eleve doit d'abord �tre choisie ou classe
##les effacer du menu .menu.eleves delete 0 endset nom_elev $tcl_platform(user)
##set nom_classe [lindex [exec  id -G -n ] 1]
##class.bil est au premier qui la cr�e set env(USERNAME) $nom
###prof ? ou mot de passe
##saisie_password


	switch $elev_classe {
		classe {
			if { ![info exists classe ] || $classe == ""} {return}
		set ind_cl [.menu.class index $classe  ]
		file delete -force [file join $basedir data ${classe}]
		.menu.class delete $ind_cl
		set classe ""
		}
		eleve {
			if { ![info exists elev ] || $elev == ""} { exit}
##			set test [ expr $tcl_platform(user) != $elev  &&  $tcl_platform(platform) == "unix" ] 
			if { $tcl_platform(user) != $elev  &&  $tcl_platform(platform) == "unix" || ![info exists elev ] || $elev == "" } {
##message d'erreur que pour unix
			set user_prov $tcl_platform(user)
			tk_messageBox -message  "[format [mc {%1$s n'existe pas ou %2$s n'a pas le droit d'effacer %3$s.}] $elev $user_prov ${elev}]"\
			-icon error -type ok -title "[mc {Effacer Bilans ?} ]"
			return }
##effacer l'�l�ve + son html eleve.bilan.html if file  exists
##droits sur ces fichiers ????
		if { [file exists [file join $basedir data ${elev}.bilan.html] ]} {
		file delete -force [file join $basedir data ${elev}.bilan.html]
		}
		set f [open [file join $basedir data  $classe ] "r"]
		set f1 [open [file join  $basedir data classe_prov] "w+"]
			while { [gets $f ligne] >=0 } {
              if {  [lindex [split $ligne ":"] 1]  != $elev } {
				puts $f1 $ligne
				}
		} 
		close $f 
		close $f1
		set f [open [file join $basedir data  $classe ] "w+"]
		set f1 [open [file join  $basedir data classe_prov] "r"]
		while { [gets $f1 ligne] >=0  } { 
				puts $f $ligne
			}
	
		close $f 
		close $f1
##classe_prov appartient � celui qui le cr�e !!
		file delete  [file join  $basedir data classe_prov  ]
		set ind_el [.menu.eleves index $elev ] ; .menu.eleves delete $ind_el
		set elev "" ; catch { .text delete @0,0 end; destroy .text.sauv_html }
				}
		}

}

##.menu.class add command -label [mc {edit_scena_ment_appr}
proc main {} {
global compt_mp
menu .menu.eleves -tearoff 0
.menu add cascade -label "[mc {Eleves}]"  -menu .menu.eleves
menu .menu.fich -tearoff 0 
.menu add cascade -label "[mc {Fichiers}]"  -menu .menu.fich
##que si prof ???? pb win ???demander coonfirmation!!! ou mot de passe ?
saisie_password
if { $compt_mp == "" } {exit}
##$compt_mp == 0
catch {.menu.fich add command -label "[mc {Effacer les bilans de l'�l�ve choisi }  ]" -command "suppression eleve"
.menu.fich add command -label "[mc {Effacer la classe choisie }]" -command "suppression classe"

.menu.fich add command -label "[mc {Quitter}]" -command "exit"
. configure -menu .menu
text .text -yscrollcommand ".scroll set"  -xscrollcommand ".scroll1 set" \
-setgrid true -width 65 -height 30   -background bisque -font   {Helvetica  11}
##scrollbar .frame2.scroll2  -orient horizontal  -command ".frame2.text xview"
##$sysFont(l)
scrollbar .scroll -command ".text yview"
scrollbar .scroll1 -orient horizontal  -command ".text xview"
pack .scroll -side right -fill y
pack .scroll1 -side bottom -fill x
pack .text -expand yes -fill both
}

 }
 main

proc vide_eleve {} {
.menu.eleves delete 0 end
##menu .menu.eleves -tearoff 0
##.menu add cascade -label "[mc {Eleves}]"  -menu .menu.eleves
}
proc dans { el liste } {
foreach el_gen $liste {
if { $el_gen == $el } {
return 1
}
}
return 0
}
proc capturenoms { classe}  {
global basedir file liste_noms .text
if { $classe == "" } { return }
set file [open [file join $basedir data $classe ] "r" ]
set liste_noms {}
while {[gets $file ligne] >=0} {
 set liste_noms [linsert $liste_noms 0 [lindex [split $ligne ":"] 1]]

 }
close $file
set liste_noms [lsort $liste_noms ]
set liste_sans_doublons {}
foreach nom $liste_noms {
if { ! [dans $nom $liste_sans_doublons] } {
set liste_sans_doublons [linsert $liste_sans_doublons 0 $nom ]
}
}
set liste_noms $liste_sans_doublons

}


###.menu.eleves add command -label [mc {edit_scena_ment_appr} ]

### compter les Fais rr ...dans fichier .bil#################
#########################################
proc compte { liste  el} {
if { $liste == "" } {return 0} else {
		if { [lindex $liste 0 ] == $el  }  {
				return [expr 1 + [compte [lrange $liste 1 end]  $el]]  }  else  {
				return [compte  [lrange $liste 1 end] $el] 
				}
		}
}
proc capturebilan { eleve classe } {
global file nom_elev f basedir liste_noms tab_bilan tcl_platform
wm title . "[mc {bilan_elev}] : $eleve de [file rootname $classe]"
set f [open [file join $basedir data $classe ] "r" ]

set bilan {} ; set  liste_exos {}
##set f [open $file "r" ]
.text tag configure green -foreground green
.text tag configure green1 -foreground green -underline yes
.text tag configure red -foreground red
.text tag configure blue -foreground blue
.text tag configure white -foreground white
.text tag configure white1 -foreground white -underline yes -font {arial 14}
.text tag configure white2 -foreground white -underline yes
.text tag configure yellow -foreground yellow
.text tag configure orange -foreground orange
.text tag configure black1  -foreground  black -underline yes
.text tag configure red1  -foreground red -underline yes -font {hevetica  16}
.text delete 1.0 end
##.text insert end "$eleve $classe $f :" green
##.text insert end \n
set indice 1
while { ! [eof $f] } {
set ligne [gets  $f ]
## catch {set ligne2  [gets  $f ]} remplir bien tab(0,nbre)faire � part la gestion des doublons!!  
##.text insert end  "liste  $liste_noms" green
##.text insert end "sligne :[split $ligne : ]\n" red ; .text insert end " nom : [lindex [split $ligne : ] 1 ] el:  $eleve  \n" blue
if { [lsearch -exact  [split $ligne ":" ]  $eleve  ] >= 0 } {
## lsearch -exact [split $ligne ":" ] {$nom_elev} ] >= 0
##.text insert end "bool :[ lsearch  [split $ligne : ]  $eleve ]\n" yellow
##set bilan [ concat  [lrange [split $ligne ":" ] 1 4] $bilan ]
set ligne [split $ligne : ]
          set  date [lindex $ligne 0 ]  ;    set  nom  [lindex $ligne 1 ]
        set  nom_exo   [lindex $ligne 2 ]  ;     set  type_laby   [lindex $ligne 3 ] ; set car_laby  [lindex $ligne 4 ]
 ##le 4 :nr Tout Fais pour normal c pour cache  rr ou av td ...pour relatif   
##pour debug
set  liste_t_f {} ; set  liste_er_ms {} ; set  liste_c  {} ; set nbre_rr 0 ;  set liste_rr {} ; set liste {}
 set liste_r_t_f {}
        switch $car_laby 	 {
			nr {
			 set liste_er_ms {}  
			set nbre_nr [compte $ligne  "nr"] ; set tab_bilan($indice,nbre_rec) $nbre_nr
			for {set i 1 ; set j 0} { $i <= $nbre_nr } {incr i ; incr j 5 }  {
			set liste_er_ms_p  [linsert  $liste_er_ms \
			end [concat	[lindex $ligne [expr 5 + $j] ] [lindex $ligne  [expr 5 +1 + $j]] ] ]
			set  liste_er_ms   ${liste_er_ms_p}
			
			
							}
			}
			Tout  -  Fais {
			set nbre_tf  [expr [compte $ligne "Tout" ] + [compte $ligne "Fais"] ] ; set tab_bilan($indice,nbre_rec) $nbre_tf
			set liste_t_f {} ; set car_l_p $car_laby
			for {set i 1  ; set j 0} { $i <= $nbre_tf } {incr i  ; incr j 6} {
				if {  $car_l_p == "Tout" }  { 
				set liste_t_f_p   [linsert $liste_t_f end [list "[mc {Tout}]" [lrange  $ligne [expr 4 + $j + 1]  [expr 4 + $j + 5] ] ] ]
				} else {
				set liste_t_f_p   [linsert $liste_t_f end [list "[mc {Fais}]" [lrange  $ligne [expr 4 + $j + 1]  [expr 4 + $j + 5] ] ] ]
									}
				set liste_t_f  $liste_t_f_p
				set car_l_p  [lindex  $ligne [ expr $j + 10 ]   ]
							}
			
			
			}
			c  {
			
			set nbre_c  [compte $ligne  "c" ]
			set liste_c  {} ; set tab_bilan($indice,nbre_rec) $nbre_c
			for {set i 1 ; set j 0 } { $i <= $nbre_c } {incr i  ; incr j 5} {
			set liste_c_p  [linsert  $liste_c \
			end [concat	[lindex $ligne [expr 5 + $j] ] [lindex $ligne  [expr 5 +1 + $j]] ] ]
			set  liste_c   ${liste_c_p}
							
							
							}
			
			
			}
			rr -  "av_td" - "av_tg" - "re_tg" - "re_td" {
			set nbre_rr [expr  [compte $ligne  "rr" ]  + [compte $ligne "av_td"] + [compte $ligne  "av_tg"]\
				+ [compte $ligne "re_td"] + [compte  $ligne "re_tg" ] ]
			set liste_rr {} ; set car_laby_p  $car_laby ; ; set tab_bilan($indice,nbre_rec) $nbre_rr
			for {set i 1 ; set j 0  } { $i <= $nbre_rr } {incr i  ; incr j 5} { 
					if {  $car_laby_p == "rr" } {
					set liste_rr_p [ linsert  $liste_rr end  [list "rr" [lrange $ligne [expr 5 + $j] [expr 5 + $j + 2] ] ] ]
					 
					}
					if {  $car_laby_p == "av_td" } {
					set liste_rr_p [linsert $liste_rr end  [list "av_td" [lrange $ligne [expr 5 + $j] [expr 5  + $j + 2] ] ] ]
					 set liste $liste_rr_p 
					}		
					if {  $car_laby_p == "av_tg" } {
					set liste_rr_p [linsert  $liste_rr  end [list "av_tg" [lrange $ligne [expr 5+ $j] [expr 5 + $j + 2] ] ] ]
					}
					if {  $car_laby_p == "re_tg" } {
					set liste_rr_p [linsert $liste_rr end  [list "re_tg" [lrange $ligne [expr 5 + $j] [expr 5 + $j + 2] ] ] ]
					}		
					if {  $car_laby_p == "re_td" } {
					set liste_rr_p [linsert  $liste_rr  end  [list "re_tg" [lrange $ligne [expr 5 + $j] [expr 5 + $j + 2] ] ] ]
					}		
					set car_laby_p  [lindex $ligne [expr $j + 4 + 5 ]]	
					set liste_rr  $liste_rr_p
							}
			
					}
			"av_td_Tout" - "av_td_Fais" - "re_td_Tout" - "re_td_Fais" - "av_tg_Tout" - "av_tg_Fais" - "re_tg_Fais" - "re_tg_Tout"  {
			set nbre_r_t_f  [expr  [compte $ligne  "av_td_Tout" ] +  [compte $ligne  "av_td_Fais" ] + [compte $ligne  "re_td_Tout" ] +\
							[compte $ligne  "re_td_Fais" ] + [compte $ligne  "av_tg_Tout" ] + [compte $ligne  "av_tg_Fais" ] +\
							[compte $ligne  "re_tg_Fais" ] + [compte $ligne  "re_tg_Tout" ] ]
			set liste_r_t_f {} ; set car_laby_p  $car_laby ; ; set tab_bilan($indice,nbre_rec) $nbre_r_t_f
##			tk_messageBox  -message "[mc {nbre rep}] $nbre_r_t_f " -type ok -icon info   -title Imprimer?
			set car_l_p $car_laby
			for {set i 1  ; set j 0} { $i <= $nbre_r_t_f } {incr i  ; incr j 6} {
				if {  $car_l_p == "av_td_Tout" }  { 
				set liste_r_t_f_p [linsert $liste_r_t_f end [list "av_td_Tout" [lrange  $ligne [expr 4 + $j + 1]  [expr 4 + $j + 5] ] ] ]
					}
				if {  $car_l_p == "av_td_Fais" }  { 
				set liste_r_t_f_p [linsert $liste_r_t_f end [list "av_td_Fais" [lrange  $ligne [expr 4 + $j + 1]  [expr 4 + $j + 5] ] ] ]
					}
				if {  $car_l_p == "re_td_Tout" }  { 
				set liste_r_t_f_p [linsert $liste_r_t_f end [list "re_td_Tout" [lrange  $ligne [expr 4 + $j + 1]  [expr 4 + $j + 5] ] ] ]
					}
				if {  $car_l_p == "re_td_Fais" }  { 
				set liste_r_t_f_p [linsert $liste_r_t_f end [list "re_td_Fais" [lrange  $ligne [expr 4 + $j + 1]  [expr 4 + $j + 5] ] ] ]
					}
					if {  $car_l_p == "av_tg_Tout" }  { 
				set liste_r_t_f_p [linsert $liste_r_t_f end [list "av_tg_Tout" [lrange  $ligne [expr 4 + $j + 1]  [expr 4 + $j + 5] ] ] ]
					}
				if {  $car_l_p == "av_tg_Fais" }  { 
				set liste_r_t_f_p [linsert $liste_r_t_f end [list "av_tg_Fais" [lrange  $ligne [expr 4 + $j + 1]  [expr 4 + $j + 5] ] ] ]
					}
				if {  $car_l_p == "re_tg_Tout" }  { 
				set liste_r_t_f_p [linsert $liste_r_t_f end [list "re_tg_Tout" [lrange  $ligne [expr 4 + $j + 1]  [expr 4 + $j + 5] ] ] ]
					}
				if {  $car_l_p == "re_tg_Fais" }  { 
				set liste_r_t_f_p [linsert $liste_r_t_f end [list "re_tg_Fais" [lrange  $ligne [expr 4 + $j + 1]  [expr 4 + $j + 5] ] ] ]
					}
					set liste_r_t_f  $liste_r_t_f_p
				set car_l_p  [lindex  $ligne [ expr $j + 10 ]   ]	
						
						}
##			tk_messageBox  -message "[mc {nbre rep}] $liste_r_t_f " -type ok -icon info  -title Imprimer
				}
	}
	if { $indice == 1} {
        .text insert end  "[mc {Bilans pour}]" black1 ;   .text insert end  " $eleve"  red1 ;    .text insert end  "[mc {de la classe}]  $classe\n"   black
		}
		.text insert end " le ${date} pour ${nom_exo} [mc {type labyrinthe :}] ${type_laby}\n" 
##affichaage des bilans		
		if { $liste_er_ms  != {}  } {
		.text insert end "[mc {Il y a eu}] [llength $liste_er_ms] [mc {r�p�tition(s)}] :\n" blue
		set  j 1 ; ##pour les rec et r�cup�rer les valeurs
			  foreach el $liste_er_ms {
		marge ; marge ;   .text insert end "[lindex  $el  0] �chec(s) et [ lindex  $el  1] [mc {r�ussite(s) en}]\
						[lindex $el  2]  min et  [lindex $el 3]sec\n"  red
						 set tab_bilan($indice,$j,echec) [lindex  $el  0] ;  set tab_bilan($indice,$j,reussite) [lindex  $el  1] 
						   set tab_bilan($indice,$j,min) [lindex  $el  2] ;  set tab_bilan($indice,$j,sec) [lindex  $el  3] 
					 set  tab_bilan($indice,$j,liste) {} ; set  tab_bilan($indice,$j,car) "normal"
					 incr j
					 }
		  }

		 if { $liste_c != {}  } {
		 .text insert end "[mc {Il y a eu}] [llength $liste_c] [mc {r�p�tition(s)}] :\n" blue
		 set j 1
		   foreach el $liste_c {
		marge ; marge ;   .text insert end " [lindex $el 0] �chec(s) et [lindex $el 1] [mc {r�ussite(s) en}]\
						 [lindex $el  2 ]  min et  [lindex $el 3] sec\n" red 
						set tab_bilan($indice,$j,echec) [lindex  $el  0] ;  set tab_bilan($indice,$j,reussite) [lindex  $el  1] 
						 set tab_bilan($indice,$j,min) [lindex  $el  2]  ;  set tab_bilan($indice,$j,sec) [lindex  $el  3]  
						set  tab_bilan($indice,$j,liste) {} ; set  tab_bilan($indice,$j,car) "cach�"
						 incr j
						 
					  }
		 
		 }
	  if { $liste_t_f  != {} }  {
		   .text insert end "Il y a eu [llength $liste_t_f] r�p�tition(s) :\n" blue
##debug	   .text insert end $liste_t_f black
		    set j 1
		    foreach el $liste_t_f {
		    set car [lindex [lindex $el 0]  0 ] 
				    if { $car == "Tout" } {
		marge ; marge ;   .text insert end "[mc {Avec}] $car :\n" black1
		marge ; marge ;  .text insert end  "[mc {Avec}] [lindex [ lindex [lindex $el  1 ] 1] 0] �chec(s) et [lindex  [lindex  [lindex $el 1]  1  ] 1] [mc {r�ussite(s) pour cet essai.}]\n   \
				  [mc {Dur�e de tous les essais pr�c�dents de}] $car :	en [lindex [ lindex [lindex $el  1 ] 2] 0] min et [lindex [ lindex [lindex $el  1 ] 2] 1]  sec\n" red
			marge ; marge ; .text insert  end  "[mc {Avec comme suite de liste d'ordres utilis�s}] : [lindex [lindex $el 1] 4] [mc {(une par tentative)}]\n \
			[mc {et Listes corrig�es}] [lindex [lindex $el 1] 0]\n" red
			marge ; marge ; .text insert  end "[mc {et comme mauvaises listes}]  :[lindex [lindex $el 1] 3]\n"  red
					  set tab_bilan($indice,$j,echec) [lindex [ lindex [lindex $el  1 ] 1] 0] ;  set tab_bilan($indice,$j,reussite) [lindex  [lindex  [lindex $el 1]  1  ] 1]
					 set tab_bilan($indice,$j,min)  [lindex [ lindex [lindex $el  1 ] 2] 0]  ;  set tab_bilan($indice,$j,sec)   [lindex [ lindex [lindex $el  1 ] 2] 1]
					set  tab_bilan($indice,$j,liste)  [lindex [lindex $el 1] 0]
					set  tab_bilan($indice,$j,car) "Ant Tout"
					set  tab_bilan($indice,$j,liste_bad) [lindex [lindex $el 1] 3]
					set  tab_bilan($indice,$j,liste_eff) [lindex [lindex $el 1] 4]  	   
						   }
		  
			   if { $car == "Fais" } {
				marge ; marge ;   .text insert end  "[mc {Avec}] $car : \n" black1
				marge ; marge ; .text insert end "[mc {Avec}] [lindex [ lindex [lindex $el  1 ] 1] 0] �chec(s) et [lindex  [lindex  [lindex $el 1]  1  ] 1] [mc {r�ussite(s) pour cet essai.}]\n \
				[mc {Dur�e  de tous les essais pr�c�dents de}] $car : [lindex [ lindex [lindex $el  1 ] 2] 0] min et [lindex [ lindex [lindex $el  1 ] 2] 1]  sec\n"  red
				marge ; marge ; .text insert  end  "[mc {Avec comme suite de liste d'ordres utilis�s}] : [lindex [lindex $el 1] 4] [mc {(une par tentative)}]\n \
				[mc {et Listes corrig�es}] [lindex [lindex $el 1] 0]\n" red
				marge ; marge ; .text insert  end "[mc {et comme mauvaises listes}] :[lindex [lindex $el 1] 3]\n "  red
					   set tab_bilan($indice,$j,echec) [lindex [ lindex [lindex $el  1 ] 1] 0] ;  set tab_bilan($indice,$j,reussite) [lindex  [lindex  [lindex $el 1]  1  ] 1]
					 set tab_bilan($indice,$j,min)  [lindex [ lindex [lindex $el  1 ] 2] 0]  ;  set tab_bilan($indice,$j,sec)   [lindex [ lindex [lindex $el  1 ] 2] 1]
					set  tab_bilan($indice,$j,liste) [lindex [lindex $el 1] 0]
					set  tab_bilan($indice,$j,liste_bad) [lindex [lindex $el 1] 3]
					set  tab_bilan($indice,$j,liste_eff) [lindex [lindex $el 1] 4]
					  set  tab_bilan($indice,$j,car) "Ant Fais" 
					   
					   
					   }
		  incr j
		  }
			}
		 
		 if { $liste_r_t_f  != {} }  {
		   .text insert end "[mc {Il y a eu}] [llength $liste_r_t_f] [mc {r�p�tition(s)}] :\n" blue
###debug	   .text insert end $liste_r_t_f black
		    set j 1
		    foreach el $liste_r_t_f {
			
		    set car [lindex [lindex $el 0]  0 ] 
##debug			tk_messageBox  -message "[mc {nbre rep}] $car " -type ok -icon info  -title Imprimer
				    if { $car == "av_tg_Tout" } {
		 marge ; marge ;   .text insert end "[mc {Avec}] $car :\n" black1
		marge ; marge ;  .text insert end  "[mc {Avec}] [lindex [ lindex [lindex $el  1 ] 1] 0] �chec(s) et [lindex  [lindex  [lindex $el 1]  1  ] 1] [mc {r�ussite(s) pour cet essai.}]\n   \
				  [mc {Dur�e de tous les essais pr�c�dents de}] $car :	en [lindex [ lindex [lindex $el  1 ] 2] 0] min et [lindex [ lindex [lindex $el  1 ] 2] 1]  sec\n" red
			marge ; marge ; .text insert  end  "[mc {Avec comme suite de liste d'ordres utilis�s}] : [lindex [lindex $el 1] 4] [mc {(une par tentative)}]\n \
			[mc {et Listes corrig�es}] [lindex [lindex $el 1] 0]\n" red
			
			marge ; marge ; .text insert  end "[mc {et comme mauvaises listes}]  :[lindex [lindex $el 1] 3]\n"  red
					  set tab_bilan($indice,$j,echec) [lindex [ lindex [lindex $el  1 ] 1] 0] ;  set tab_bilan($indice,$j,reussite) [lindex  [lindex  [lindex $el 1]  1  ] 1]
					 set tab_bilan($indice,$j,min)  [lindex [ lindex [lindex $el  1 ] 2] 0]  ;  set tab_bilan($indice,$j,sec)   [lindex [ lindex [lindex $el  1 ] 2] 1]
					set  tab_bilan($indice,$j,liste)  [lindex [lindex $el 1] 0]
					set  tab_bilan($indice,$j,car) "Ant av_tg_Tout"
					set  tab_bilan($indice,$j,liste_bad) [lindex [lindex $el 1] 3]
					set  tab_bilan($indice,$j,liste_eff) [lindex [lindex $el 1] 4]
		 
		 }
		 	    if { $car == "av_tg_Fais" } {
				
				marge ; marge ;   .text insert end "Avec $car :\n" black1
		marge ; marge ;  .text insert end  "Avec [lindex [ lindex [lindex $el  1 ] 1] 0] �chec(s) et [lindex  [lindex  [lindex $el 1]  1  ] 1] r�ussite(s) pour cet essai.\n   \
				  Dur�e de tous les essais pr�c�dents de $car :	en [lindex [ lindex [lindex $el  1 ] 2] 0] min et [lindex [ lindex [lindex $el  1 ] 2] 1]  sec\n" red
			marge ; marge ; .text insert  end  "Avec comme suite de liste d'ordres utilis�s : [lindex [lindex $el 1] 4] (une par tentaive)\n \
			et Listes corrig�es [lindex [lindex $el 1] 0]\n" red
				
				
				
		  marge ; marge ; .text insert  end "et comme mauvaises listes  :[lindex [lindex $el 1] 3]\n"  red
					  set tab_bilan($indice,$j,echec) [lindex [ lindex [lindex $el  1 ] 1] 0] ;  set tab_bilan($indice,$j,reussite) [lindex  [lindex  [lindex $el 1]  1  ] 1]
					 set tab_bilan($indice,$j,min)  [lindex [ lindex [lindex $el  1 ] 2] 0]  ;  set tab_bilan($indice,$j,sec)   [lindex [ lindex [lindex $el  1 ] 2] 1]
					set  tab_bilan($indice,$j,liste)  [lindex [lindex $el 1] 0]
					set  tab_bilan($indice,$j,car) "Ant av_tg_Fais"
					set  tab_bilan($indice,$j,liste_bad) [lindex [lindex $el 1] 3]
					set  tab_bilan($indice,$j,liste_eff) [lindex [lindex $el 1] 4]
		  
		  
		  
		  }
		 	    if { $car == "re_tg_Tout" } {
				marge ; marge ;   .text insert end "Avec $car :\n" black1
		marge ; marge ;  .text insert end  "Avec [lindex [ lindex [lindex $el  1 ] 1] 0] �chec(s) et [lindex  [lindex  [lindex $el 1]  1  ] 1] r�ussite(s) pour cet essai.\n   \
				  Dur�e de tous les essais pr�c�dents de $car :	en [lindex [ lindex [lindex $el  1 ] 2] 0] min et [lindex [ lindex [lindex $el  1 ] 2] 1]  sec\n" red
			marge ; marge ; .text insert  end  "Avec comme suite de liste d'ordres utilis�s : [lindex [lindex $el 1] 4] (une par tentative)\n \
			et Listes corrig�es [lindex [lindex $el 1] 0]\n" red
				
				
		 marge ; marge ; .text insert  end "et comme mauvaises listes  :[lindex [lindex $el 1] 3]\n"  red
					  set tab_bilan($indice,$j,echec) [lindex [ lindex [lindex $el  1 ] 1] 0] ;  set tab_bilan($indice,$j,reussite) [lindex  [lindex  [lindex $el 1]  1  ] 1]
					 set tab_bilan($indice,$j,min)  [lindex [ lindex [lindex $el  1 ] 2] 0]  ;  set tab_bilan($indice,$j,sec)   [lindex [ lindex [lindex $el  1 ] 2] 1]
					set  tab_bilan($indice,$j,liste)  [lindex [lindex $el 1] 0]
					set  tab_bilan($indice,$j,car) "Ant re_tg_Tout"
					set  tab_bilan($indice,$j,liste_bad) [lindex [lindex $el 1] 3]
					set  tab_bilan($indice,$j,liste_eff) [lindex [lindex $el 1] 4]
		 
		 }
		 	    if { $car == "re_tg_Fais" } {
			marge ; marge ;   .text insert end "Avec $car :\n" black1
		marge ; marge ;  .text insert end  "Avec [lindex [ lindex [lindex $el  1 ] 1] 0] �chec(s) et [lindex  [lindex  [lindex $el 1]  1  ] 1] r�ussite(s) pour cet essai.\n   \
				  Dur�e de tous les essais pr�c�dents de $car :	en [lindex [ lindex [lindex $el  1 ] 2] 0] min et [lindex [ lindex [lindex $el  1 ] 2] 1]  sec\n" red
			marge ; marge ; .text insert  end  "Avec comme suite de liste d'ordres utilis�s : [lindex [lindex $el 1] 4] (une par tentative)\n \
			et Listes corrig�es [lindex [lindex $el 1] 0]\n" red	
		 
		 marge ; marge ; .text insert  end "et comme mauvaises listes  :[lindex [lindex $el 1] 3]\n"  red
					  set tab_bilan($indice,$j,echec) [lindex [ lindex [lindex $el  1 ] 1] 0] ;  set tab_bilan($indice,$j,reussite) [lindex  [lindex  [lindex $el 1]  1  ] 1]
					 set tab_bilan($indice,$j,min)  [lindex [ lindex [lindex $el  1 ] 2] 0]  ;  set tab_bilan($indice,$j,sec)   [lindex [ lindex [lindex $el  1 ] 2] 1]
					set  tab_bilan($indice,$j,liste)  [lindex [lindex $el 1] 0]
					set  tab_bilan($indice,$j,car) "Ant re_tg_Fais"
					set  tab_bilan($indice,$j,liste_bad) [lindex [lindex $el 1] 3]
					set  tab_bilan($indice,$j,liste_eff) [lindex [lindex $el 1] 4]
		 }
		 
		 		    if { $car == "av_td_Tout" } {
			marge ; marge ;   .text insert end "Avec $car :\n" black1
		marge ; marge ;  .text insert end  "Avec [lindex [ lindex [lindex $el  1 ] 1] 0] �chec(s) et [lindex  [lindex  [lindex $el 1]  1  ] 1] r�ussite(s) pour cet essai.\n   \
				  Dur�e de tous les essais pr�c�dents de $car :	en [lindex [ lindex [lindex $el  1 ] 2] 0] min et [lindex [ lindex [lindex $el  1 ] 2] 1]  sec\n" red
			marge ; marge ; .text insert  end  "Avec comme suite de liste d'ordres utilis�s : [lindex [lindex $el 1] 4] (une par tentative)\n \
			et Listes corrig�es [lindex [lindex $el 1] 0]\n" red		
					
		 marge ; marge ; .text insert  end "et comme mauvaises listes  :[lindex [lindex $el 1] 3]\n"  red
					  set tab_bilan($indice,$j,echec) [lindex [ lindex [lindex $el  1 ] 1] 0] ;  set tab_bilan($indice,$j,reussite) [lindex  [lindex  [lindex $el 1]  1  ] 1]
					 set tab_bilan($indice,$j,min)  [lindex [ lindex [lindex $el  1 ] 2] 0]  ;  set tab_bilan($indice,$j,sec)   [lindex [ lindex [lindex $el  1 ] 2] 1]
					set  tab_bilan($indice,$j,liste)  [lindex [lindex $el 1] 0]
					set  tab_bilan($indice,$j,car) "Ant av_td_Tout"
					set  tab_bilan($indice,$j,liste_bad) [lindex [lindex $el 1] 3]
					set  tab_bilan($indice,$j,liste_eff) [lindex [lindex $el 1] 4]
		 
		 }
		 	    if { $car == "av_td_Fais" } {
			marge ; marge ;   .text insert end "Avec $car :\n" black1
		marge ; marge ;  .text insert end  "Avec [lindex [ lindex [lindex $el  1 ] 1] 0] �chec(s) et [lindex  [lindex  [lindex $el 1]  1  ] 1] r�ussite(s) pour cet essai.\n   \
				  Dur�e de tous les essais pr�c�dents de $car :	en [lindex [ lindex [lindex $el  1 ] 2] 0] min et [lindex [ lindex [lindex $el  1 ] 2] 1]  sec\n" red
			marge ; marge ; .text insert  end  "Avec comme suite de liste d'ordres utilis�s : [lindex [lindex $el 1] 4] (une par tentative)\n \
			et Listes corrig�es [lindex [lindex $el 1] 0]\n" red	
		 marge ; marge ; .text insert  end "et comme mauvaises listes  :[lindex [lindex $el 1] 3]\n"  red
					  set tab_bilan($indice,$j,echec) [lindex [ lindex [lindex $el  1 ] 1] 0] ;  set tab_bilan($indice,$j,reussite) [lindex  [lindex  [lindex $el 1]  1  ] 1]
					 set tab_bilan($indice,$j,min)  [lindex [ lindex [lindex $el  1 ] 2] 0]  ;  set tab_bilan($indice,$j,sec)   [lindex [ lindex [lindex $el  1 ] 2] 1]
					set  tab_bilan($indice,$j,liste)  [lindex [lindex $el 1] 0]
					set  tab_bilan($indice,$j,car) "Ant av_td_Fais"
					set  tab_bilan($indice,$j,liste_bad) [lindex [lindex $el 1] 3]
					set  tab_bilan($indice,$j,liste_eff) [lindex [lindex $el 1] 4]
		 
		 }
		 	    if { $car == "re_td_Tout" } {
				marge ; marge ;   .text insert end "Avec $car :\n" black1
		marge ; marge ;  .text insert end  "Avec [lindex [ lindex [lindex $el  1 ] 1] 0] �chec(s) et [lindex  [lindex  [lindex $el 1]  1  ] 1] r�ussite(s) pour cet essai.\n   \
				  Dur�e de tous les essais pr�c�dents de $car :	en [lindex [ lindex [lindex $el  1 ] 2] 0] min et [lindex [ lindex [lindex $el  1 ] 2] 1]  sec\n" red
			marge ; marge ; .text insert  end  "Avec comme suite de liste d'ordres utilis�s : [lindex [lindex $el 1] 4] (une par tentative)\n \
			et Listes corrig�es [lindex [lindex $el 1] 0]\n" red
				
				
		 marge ; marge ; .text insert  end "et comme mauvaises listes  :[lindex [lindex $el 1] 3]\n"  red
					  set tab_bilan($indice,$j,echec) [lindex [ lindex [lindex $el  1 ] 1] 0] ;  set tab_bilan($indice,$j,reussite) [lindex  [lindex  [lindex $el 1]  1  ] 1]
					 set tab_bilan($indice,$j,min)  [lindex [ lindex [lindex $el  1 ] 2] 0]  ;  set tab_bilan($indice,$j,sec)   [lindex [ lindex [lindex $el  1 ] 2] 1]
					set  tab_bilan($indice,$j,liste)  [lindex [lindex $el 1] 0]
					set  tab_bilan($indice,$j,car) "Ant re_td_Tout"
					set  tab_bilan($indice,$j,liste_bad) [lindex [lindex $el 1] 3]
					set  tab_bilan($indice,$j,liste_eff) [lindex [lindex $el 1] 4]
		 
		 }
		 	    if { $car == "re_td_Fais" } {
			marge ; marge ;   .text insert end "Avec $car :\n" black1
		marge ; marge ;  .text insert end  "Avec [lindex [ lindex [lindex $el  1 ] 1] 0] �chec(s) et [lindex  [lindex  [lindex $el 1]  1  ] 1] r�ussite(s) pour cet essai.\n   \
				  Dur�e de tous les essais pr�c�dents de $car :	en [lindex [ lindex [lindex $el  1 ] 2] 0] min et [lindex [ lindex [lindex $el  1 ] 2] 1]  sec\n" red
			marge ; marge ; .text insert  end  "Avec comme suite de liste d'ordres utilis�s : [lindex [lindex $el 1] 4] (une par tentative)\n \
			et Listes corrig�es [lindex [lindex $el 1] 0]\n" red	
		 marge ; marge ; .text insert  end "et comme mauvaises listes  :[lindex [lindex $el 1] 3]\n"  red
					  set tab_bilan($indice,$j,echec) [lindex [ lindex [lindex $el  1 ] 1] 0] ;  set tab_bilan($indice,$j,reussite) [lindex  [lindex  [lindex $el 1]  1  ] 1]
					 set tab_bilan($indice,$j,min)  [lindex [ lindex [lindex $el  1 ] 2] 0]  ;  set tab_bilan($indice,$j,sec)   [lindex [ lindex [lindex $el  1 ] 2] 1]
					set  tab_bilan($indice,$j,liste)  [lindex [lindex $el 1] 0]
					set  tab_bilan($indice,$j,car) "Ant re_td_Fais"
					set  tab_bilan($indice,$j,liste_bad) [lindex [lindex $el 1] 3]
					set  tab_bilan($indice,$j,liste_eff) [lindex [lindex $el 1] 4]
		 }
		 
		 
		 
		 incr j
		 
		 }
##fin foreach		 
		 }
##fin if liste_r_t_f		 
		 
		 if { $liste_rr != {} }  {
		  .text insert end "Il y a eu [llength $liste_rr] r�p�tition(s) :\n" blue
##debug		  .text insert end $liste_rr black
			set j 1
		      foreach el $liste_rr {
			        set car [lindex [lindex $el 0]  0 ] 
			         if { $car == "rr" } {
		 marge ; marge ;   .text insert end "Avec $car :     " black1
	.text insert end " [lindex [ lindex [lindex $el  1 ] 0] 0] �chec(s) et [lindex  [lindex  [lindex $el 1]  0  ] 1] r�ussite(s)\
		en [lindex [ lindex [lindex $el  1 ] 1] 0] min et [lindex [ lindex [lindex $el  1 ] 1] 1]  sec.\n"  red
		    set tab_bilan($indice,$j,echec) [lindex [ lindex [lindex $el  1 ] 0] 0] ;  set tab_bilan($indice,$j,reussite) [lindex  [lindex  [lindex $el 1]  0  ] 1]
					 set tab_bilan($indice,$j,min)  [lindex [ lindex [lindex $el  1 ] 1] 0]  ;  set tab_bilan($indice,$j,sec)   [lindex [ lindex [lindex $el  1 ] 1] 1]
					set  tab_bilan($indice,$j,liste)  {}
					  set  tab_bilan($indice,$j,car) "Relatif " 
		 
		 
		 }
		   if { $car == "av_tg" } {
		 marge ; marge ;   .text insert end "Avec $car : " black1
		.text insert end  "[lindex [ lindex [lindex $el  1 ] 0] 0] �chec(s) et [lindex  [lindex  [lindex $el 1]  0  ] 1] r�ussite(s)\
		en [lindex [ lindex [lindex $el  1 ] 1] 0] min et [lindex [ lindex [lindex $el  1 ] 1] 1]  sec.\n"  red
		 set tab_bilan($indice,$j,echec) [lindex [ lindex [lindex $el  1 ] 0] 0] ;  set tab_bilan($indice,$j,reussite) [lindex  [lindex  [lindex $el 1]  0  ] 1]
					 set tab_bilan($indice,$j,min)  [lindex [ lindex [lindex $el  1 ] 1] 0]  ;  set tab_bilan($indice,$j,sec)   [lindex [ lindex [lindex $el  1 ] 1] 1]
					set  tab_bilan($indice,$j,liste)  {}
					  set  tab_bilan($indice,$j,car) "Rel AV TG" 
		 
		 }
		   if { $car == "av_td" } {
		 marge ; marge ;   .text insert end "Avec $car : " black1
		 .text insert end  "[lindex [ lindex [lindex $el  1 ] 0] 0] �chec(s) et [lindex  [lindex  [lindex $el 1]  0  ] 1]  r�ussite(s)\
		en [lindex [ lindex [lindex $el  1 ] 1] 0] min et [lindex [ lindex [lindex $el  1 ] 1] 1]  sec.\n"  red
		 set tab_bilan($indice,$j,echec) [lindex [ lindex [lindex $el  1 ] 0] 0] ;  set tab_bilan($indice,$j,reussite) [lindex  [lindex  [lindex $el 1]  0  ] 1]
					 set tab_bilan($indice,$j,min)  [lindex [ lindex [lindex $el  1 ] 1] 0]  ;  set tab_bilan($indice,$j,sec)   [lindex [ lindex [lindex $el  1 ] 1] 1]
					set  tab_bilan($indice,$j,liste)  {}
					  set  tab_bilan($indice,$j,car) "Rel AV TD" 
		 
		 }
		   if { $car == "re_td" } {
		 marge ; marge ;   .text insert end "Avec $car : " black1
		 .text insert end  "[lindex [ lindex [lindex $el  1 ] 0] 0] �chec(s) et [lindex  [lindex  [lindex $el 1]  0  ] 1]  r�ussite(s)\
		en [lindex [ lindex [lindex $el  1 ] 1] 0] min et [lindex [ lindex [lindex $el  1 ] 1] 1]  sec.\n"  red
		 set tab_bilan($indice,$j,echec) [lindex [ lindex [lindex $el  1 ] 0] 0] ;  set tab_bilan($indice,$j,reussite) [lindex  [lindex  [lindex $el 1]  0  ] 1]
					 set tab_bilan($indice,$j,min)  [lindex [ lindex [lindex $el  1 ] 1] 0]  ;  set tab_bilan($indice,$j,sec)   [lindex [ lindex [lindex $el  1 ] 1] 1]
					set  tab_bilan($indice,$j,liste)  {}
					  set  tab_bilan($indice,$j,car) "Rel TD" 
		 
		 }
		   if { $car == "re_tg" } {
		 marge ; marge ;   .text insert end "Avec $car : " black1
		 .text insert end  " [lindex [ lindex [lindex $el  1 ] 0] 0] �chec(s) et [lindex  [lindex  [lindex $el 1]  0  ] 1] r�ussite(s)\n\
		en [lindex [ lindex [lindex $el  1 ] 1] 0] min et [lindex [ lindex [lindex $el  1 ] 1] 1]  sec\n."  red
		 set tab_bilan($indice,$j,echec) [lindex [ lindex [lindex $el  1 ] 0] 0] ;  set tab_bilan($indice,$j,reussite) [lindex  [lindex  [lindex $el 1]  0  ] 1]
					 set tab_bilan($indice,$j,min)  [lindex [ lindex [lindex $el  1 ] 1] 0]  ;  set tab_bilan($indice,$j,sec)   [lindex [ lindex [lindex $el  1 ] 1] 1]
					set  tab_bilan($indice,$j,liste)  {}
					  set  tab_bilan($indice,$j,car) "Rel RE TG" 
		 }
	  	  
		  incr j
		  }
	  }
	
	.text insert end  ""  white
			     .text insert end "=======================================================\n"  black

set liste_exos [linsert  $liste_exos end $nom_exo]
set tab_bilan($indice,type_car) "$type_laby $car_laby" ; set tab_bilan($indice,date) $date
incr indice
} ; ## fin pour un eleve une ligne donc un exo remplir html ?? remplir tableautab_bilan) pb pour cat_laby
##set liste_exos [linsert  $liste_exos end $nom_exo]
##set tab_bilan($indice,date) [lindex $ligne 0]  ; set tab_bilan($indice,typ_car) "$type_laby $car_laby##selon listes exos
} ; ## fin du fichier .bil ie toute les ligne d'un �l�ve donc tous les exos
set tab_bilan(0,nom) $eleve ; set tab_bilan(0,classe) $classe ; set tab_bilan(0,nbre_l) $indice
set tab_bilan(0,nom_exo) $nom_exo
##.text insert end "[array get tab_bilan] \net $liste_exos "
.text see @0,0
catch {destroy .text.sauv_html}
 button .text.sauv_html -text "Afficher Bilan_html de $eleve" -command "sauver_bilan_html "
## if { $eleve != $tcl_platform(user)  } {.text.sauv_html configure -state disabled } 
 .text window create end  -window .text.sauv_html  -align top -padx 30
 ## .text.sauv_html configure  -state disabled


}
proc marge {} {
.text insert end  "   "
}
proc sauver_bilan_html { } {
global w2 eleve basedir nom_exo tab_bilan progaide tcl_platform
set fnom [file join $basedir data $tab_bilan(0,nom).bilan]
  set  f  [open $fnom.html w]
   puts $f "<html>\n<head>\n<title>[mc {Bilan de}] $tab_bilan(0,nom)</title>\n</head><body>\n"
  puts $f "<h2> [mc {Synth�se pour }] : $tab_bilan(0,nom) [mc {de la classe}] $tab_bilan(0,classe) </h2>"
  puts $f  "[mc {Ant pour Anticiper, Rel pour relatif, idem pour un m�me travail r�p�t�  par Recommencer.}] <br><br>"
  puts $f "<table border='1' cellspacing='0'>\n<tr>\n<Caption>[mc {Tableau bilan pour le logiciel Labyrinthe.}]</Caption>"
  puts $f ""
 puts $f "<th>l'execice :</th><th>[mc {date}]</th>"
    puts $f "<th>[mc {Type car}]</th><th>[mc {�checs}]</th><th>r�ussites</th><th>[mc {dur�e}]</th><th>[mc {Liste d'actions effectives (pour Ant)}]</th><th>[mc {Listes corrig�es (pour Ant)}]</th><th>[mc {D�but des listes erron�es (arr�t � la 1�re erreur)}]</th> </tr>"
  for { set i 1 }  { $i <  $tab_bilan(0,nbre_l)   }  { incr i  }  {
  ##�liminer les doublons..
       puts $f "<tr>"
	       for {set k 1} { $k <= $tab_bilan($i,nbre_rec) } { incr k} {
       if { $k == 1} {  puts $f "<td>$tab_bilan(0,nom_exo)</td>" } else {     puts $f "<td> idem</td>" }
       if { $k == 1} {  puts $f "<td align='center'>$tab_bilan($i,date)</td>"} else {     puts $f "<td> idem</td>" }
        puts $f "<td align='center'>$tab_bilan($i,$k,car)</td>"
        puts $f "<td align='center'>$tab_bilan($i,$k,echec)</td>"
	  puts $f "<td align='center'>$tab_bilan($i,$k,reussite)</td>"
	  set minutes  $tab_bilan($i,$k,min) ; set secondes  $tab_bilan($i,$k,sec) 
        puts $f "<td align='center'>$minutes min $secondes  sec</td>"
		set p $tab_bilan($i,type_car)
		if { $p == "normal Tout" || $p == "normal Fais" || $p == "relatif av_td_Fais" || $p == "relatif av_td_Tout" \
		|| $p == "relatif re_td_Fais" || $p == "relatif re_td_Tout" || $p == "relatif re_tg_Fais" || $p == "relatif re_tg_Tout" \
		|| $p == "relatif av_tg_Fais" || $p == "relatif av_tg_Tout"} {
		puts $f "<td align='center'>$tab_bilan($i,$k,liste_eff)</td>" 
		} 
		if { $p == "normal Tout" || $p == "normal Fais" || $p == "relatif av_td_Fais" || $p == "relatif av_td_Tout" \
		|| $p == "relatif re_td_Fais" || $p == "relatif re_td_Tout" || $p == "relatif re_tg_Fais" || $p == "relatif re_tg_Tout" \
		|| $p == "relatif av_tg_Fais" || $p == "relatif av_tg_Tout" } {
		 if  { $tab_bilan($i,$k,liste) != ""  } { set contenu_l $tab_bilan($i,$k,liste)} else { set contenu_l "..." }
		puts $f "<td align='center'>$contenu_l</td>" 
		} else { puts $f "<td align='center'> ....</td>" }
		if { $p == "normal Tout" || $p == "normal Fais" || $p == "relatif av_td_Fais" || $p == "relatif av_td_Tout" \
		|| $p == "relatif re_td_Fais" || $p == "relatif re_td_Tout" || $p == "relatif re_tg_Fais" || $p == "relatif re_tg_Tout" \
		|| $p == "relatif av_tg_Fais" || $p == "relatif av_tg_Tout"
			} {
			if { $tab_bilan($i,$k,liste_bad) != "" }  { set contenu_b $tab_bilan($i,$k,liste_bad) } else { set contenu_b "..." }
		puts $f "<td align='center'>$contenu_b </td>" } else {
		puts $f "<td align='center'> ... </td>" }
	 puts $f "</tr>"
	}
       
	
    }
    puts $f "</table>\n</body>\n</html>"
   close $f
  #fin bilan.html
   if {$tcl_platform(platform) == "windows"} {
  set fichier [file attributes "$fnom.html" -shortname]
  } else {
  set fichier "$fnom.html"
  }
  catch { exec $progaide file:$fichier & }

}
#
