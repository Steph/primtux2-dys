global sysFont msound tsound
set tsound 0
if {[catch {package require snack}]} {
set msound 0
} else {
set msound 1
snack::sound s
}

proc opensound {c} {
global sysFont curson tabson msound
set curson ""

if {[lsearch -regexp [$c gettags cible] uident*] !=-1} {
set idt [lindex [$c gettags cible] [lsearch -regexp [$c gettags cible] uident*]]
} else {
return
}
toplevel .wsound -background #ffff80
wm title .wsound "Choisir un son"
wm transient .wsound .
frame .wsound.frame -background #ffff80 -width 400 -height 300
pack .wsound.frame -side left
listbox .wsound.frame.list -yscrollcommand ".wsound.frame.scroll set"
scrollbar .wsound.frame.scroll -command ".wsound.frame.list yview"
place .wsound.frame.list -height 130 -width 130 -x 60 -y 50
place .wsound.frame.scroll -x 180 -y 50 -height 130
label .wsound.frame.son -background #ffff80 -text "Choisir un son"
place .wsound.frame.son -x 70 -y 30
button .wsound.frame.ok -text "Ok"  -command "setson $idt"
place .wsound.frame.ok -x 70 -y 250
button .wsound.frame.annul -text "Annuler" -command {destroy .wsound}
place .wsound.frame.annul -x 130 -y 250


#snack::sound s
label .wsound.frame.enr -text "Ecouter" -background #ffff80
place .wsound.frame.enr -x 240 -y 30
snack::createIcons
button .wsound.frame.b1 -bitmap snackPlay -command Play -activebackground grey
#button .wsound.frame.b2 -bitmap snackPause -command Pause -activebackground grey
button .wsound.frame.b3 -bitmap snackStop -command Stop -activebackground grey
#button .wsound.frame.b4 -bitmap snackRecord -command Record -fg red -activebackground grey

place .wsound.frame.b1 -x 250 -y 70
#place .wsound.frame.b2 -x 275 -y 70
place .wsound.frame.b3 -x 300 -y 70
#place .wsound.frame.b4 -x 325 -y 70
entry .wsound.frame.text -width 17
place .wsound.frame.text -x 240 -y 120
catch {.wsound.frame.text insert end $tabson($idt)}
peuplelistson
bind .wsound.frame.list <ButtonRelease-1> "changeson %x %y"
}

##################################################################"
#procÚdures pour le son


proc soundEnter {c idt} {

$c configure -cursor hand1
s stop
set ext .wav
set idt [lindex [split $idt .] 0]

    catch {
      s configure -file [file join sons $idt$ext]
      s play
 
      #set ::op p
   } 
}

proc soundLeave {c} {

$c configure -cursor left_ptr

s stop
}

proc Record {} {
global sysFont curson tsound
set tsound 1
set curson [.wsound.frame.text get]
	if {$curson !=""} {
		if {[file exists [file join sons $curson]]} {
   		set tsound 0
    			if {[tk_messageBox -message "Voulez-vous remplacer $curson ?" -type yesno -icon info] == "no"} {
    			return
			}




		}
   			set ext .wav
   			set son [lindex [split $curson .] 0]

			s stop
			set ::op s
   			if {$tsound ==1} {
			#bell

			s write [file join sons $son$ext]
   			update
			after 200
			}



			s configure -file [file join sons $son$ext]
			s record
			set ::op r

			catch {
			.wsound.frame.b1 configure -relief raised
			.wsound.frame.b4 configure -relief groove
			}
		

		
	} else {
	set answer [tk_messageBox -message "Erreur, veuillez donner un nom de fichier" -type ok -icon info]
	}
	peuplelistson
}

proc Play {} {
global sysFont curson
set curson [.wsound.frame.text get]

if {$curson !=""} {
    set ext .wav
    set son [lindex [split $curson .] 0]
    s stop
    set ::op s
  if {[file exists [file join sons $son$ext]]} {
    catch {
      s configure -file [file join sons $son$ext]
      s play -command Stop
      set ::op p

    .wsound.frame.b1 configure -relief groove
    .wsound.frame.b4 configure -relief raised
    .wsound.frame.b2 configure -relief raised
    }
    }
    } else {
    set answer [tk_messageBox -message "Erreur" -type ok -icon info]
    }
}

proc Stop {} {

after 200
catch {

s stop
snack::sound s
 .wsound.frame.b1 configure -relief raised
 .wsound.frame.b4 configure -relief raised
 .wsound.frame.b2 configure -relief raised

}
}

proc Pause {} {
global sysFont s
 s pause
  if {$::op != "s"} {
     if {[.wsound.frame.b2 cget -relief] == "raised"} {
	 .wsound.frame.b2 configure -relief groove
       } else {
	 .wsound.frame.b2 configure -relief raised
       }
  }
}

proc peuplelistson {} {
   .wsound.frame.list delete 0 end
   set ext .wav
   catch {foreach i [lsort [glob [file join sons *$ext]]] {
     .wsound.frame.list insert end [file tail $i]
     }
   }
}

proc changeson {x y} {
	global sysFont curson
	set curson [.wsound.frame.list get @$x,$y]
	.wsound.frame.text delete 0 end
	.wsound.frame.text insert end $curson

}

proc setson {idt} {
global sysFont tabson curson
set tabson($idt) $curson
destroy .wsound
}
