############################################################################
# Copyright (C) 2002 David Lucardi
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : fichier.php
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 26/04/2002
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version    $Id: associe4.tcl,v 1.4 2006/05/21 10:15:29 david Exp $
# @author     David Lucardi
# @project
# @copyright  David Lucardi 26/04/2002
#
#
#########################################################################
#!/bin/sh
#associe4.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

###########################################################
#etiqarr :tableau pour recevoir les syllabes
#imgarr : tableau pour recevoir les images
#font2 : police de caract�re
#nb : nombre total d'item du jeu en cours
#nbreu : index de l'item du jeu en cours
#listdata : donn�es du jeu, dans une liste
#bgl, bgn : couleurs utilis�es
# longlist1 : nombre de syllabes du mot
#nbreu index de l'item en cours
#nbessai nombre de tentatives sur l'item en cours
#listeval :liste pour collecter les informations pour les fiches bilan
#categorie : variable pour la categorie
#user : variable pour le nom de l'utilisateur
#flag : d�tection de r�entrance dans le survol des objets, pour la gestion du son
#niveau : gestion du passage au niveau de difficult� sup�rieur 

#variables
source fonts.tcl
source path.tcl
source msg.tcl
source eval.tcl


global etiqarr imgarr font2 nb listdata longlist1 nbreu bgl nbessai listeval categorie user flag niveau repbasecat Home sound baseHome serie
set flag 0
set bgn #ffff80
set bgl #ff80c0
set nbessai 0
set niveau 0
set catedefaut ""
set font2 ""
set son 1
set arg [lindex $argv 0]
set serie [lindex $argv 1]

set ident $tcl_platform(user)
set plateforme $tcl_platform(platform)
initlog $plateforme $ident
inithome

#interface
. configure -background $bgn
set c .frame.c
frame .frame -width 640 -height 380 -background $bgn
pack .frame -side top -fill both -expand yes
wm geometry . +0+0
canvas $c -width 640 -height 380 -background $bgn -highlightbackground $bgn
pack $c
frame .bframe -background $bgn
pack .bframe -side top 
for {set i 1} {$i < 9} {incr i 1} {
label .bframe.lab$i -background #ffff80 -width 4
grid .bframe.lab$i -column [expr $i -1] -row 1 -sticky e
}

label .bframe.lab22 -background #ffff80 -text [mc {Place la bonne syllabe sur la ligne rose.}]
grid .bframe.lab22 -column $i -padx 1 -row 1
button .bframe.b1 -image [image create photo imagavant -file [file join sysdata avant.gif] ] -background #ff80c0 -command "setniveauprec $c"
grid .bframe.b1 -column [expr $i + 1] -padx 1 -row 1 -sticky w

button .bframe.b2 -image [image create photo imagbut -file [file join sysdata again2.gif] ] -background #ff80c0
grid .bframe.b2 -column [expr $i + 2] -padx 1  -row 1 -sticky w
button .bframe.b3 -image [image create photo suitebut -file [file join sysdata suite.gif]] -background #ff80c0 -command "quitte"
grid .bframe.b3 -column [expr $i + 3] -padx 1  -row 1 -sticky w

image create photo pbien -file [file join sysdata pbien.gif] 
image create photo ppass -file [file join sysdata ppass.gif]
image create photo pmal -file [file join sysdata pmal.gif]
image create photo pneutre -file [file join sysdata pneutre.gif]

#ouverture du fichier de configuration pour r�cup�rer des variables
catch {set f [open [file join $baseHome reglages associations.conf] "r"]
set catedefaut [gets $f]
set font2 [gets $f]
set son [gets $f]
set tmp [gets $f]
set tmp [gets $f]
set repbasecat [gets $f]
close $f}

set ext .cat
if {$catedefaut != "none" && $catedefaut != "" && $serie !="0"} {
set catedefaut [string map {.cat ""} $catedefaut]$serie$ext
}

if {$font2 == "none" || $font2 == ""} {
set font2 {Arial 20 bold}

} else {
set font2 \173$font2\175\04020\040bold
}

#d�tection possibilit� son (variable sound, et param�tre d'activation du son : son)
#if {[catch {package require snack}] || $son == 0} {
#set sound 0
#} else {
#set sound 1
#snack::sound s
#}

catch {
set f [open [file join $baseHome reglages son.conf] "r"]
set sson [gets $f]
close $f
set sound [expr $sound && [lindex $sson 2]]
}

proc setniveauprec {c} {
global listeval categorie serie repbasecat listdata Home
if {$serie > 0} {
incr serie -1
set ext .cat
set file $categorie
for {set i 1} {$i <= [string length $file]} {incr i 1} {
if {[string match {[0-9]} [string index $file end]] == 1} {
set  file [string range $file 0 [expr [string length $file] -2] ]
}
}
if {$serie != 0} {
set file $file$serie$ext
} else {
set file $file$ext
}

set f [open [file join $Home categorie $repbasecat $file] "r"]
set listdata [gets $f]
close $f
set listdata [string map {\* ""} $listdata]

set listeval \173[mc {Remettre les syllabes dans l'ordre - }]\175\040$file
wm title . "[mc {Remettre les syllabes dans l'ordre - }]$file"

main $c
}
}

proc init {c} {
global catedefaut listdata categorie Home font2 repbasecat getcat sound listeval iwish

#on propose la boite de dialogue ouvrir un fichier, s'il n'y a pas de cat�gorie par d�faut depuis le fichier de conf
	if {$catedefaut == "none" || $catedefaut == ""} {
	opencat
	catch {destroy .opencate}
	set ext .cat
	set file $getcat$ext
 	} else {
	set file $catedefaut
	}
wm title . "[mc {Remettre les syllabes dans l'ordre - }]$file" 

	.bframe.b2 configure -command "again $c"
	#.bframe.b3 configure -command "suite $c"
if {[catch { set f [open [file join $Home categorie $repbasecat $file] "r" ] }] } {
 
      set answer [tk_messageBox -message [mc {Erreur de fichier.}] -type ok -icon info] 
	exec $iwish associations.tcl &
      exit
}

if {[catch {set listdata [gets $f]}]} {
     set answer [tk_messageBox -message [mc {Erreur de fichier.}] -type ok -icon info]
     close $f
	exec $iwish associations.tcl &
     exit
     } else {
     close $f
     }


set categorie [lindex [split [lindex [split $file /] end] .] 0]
set listeval \173[mc {Remettre les syllabes dans l'ordre - }]\175\040$categorie

catch {
set ext ".dat"
set f [open [file join $Home categorie $repbasecat $categorie$ext] "r" ] 
set sson [lindex [lindex [gets $f] 2] 4]
set sound [expr $sound && $sson]
close $f
}
main $c
}


#proc�dures
###################################################
#Quand on appuie sur le bouton recommence 
###############################################""
proc again {c} {
global listeval user categorie
set listeval \173[mc {Remettre les syllabes dans l'ordre - }]\175\040$categorie
#enregistreval
main $c
}



#############################""""""
#rebouclage
proc main {c} {
# score : calcul du pourcentage de r�ussite de l'�l�ve
global etiqarr imgarr nb listdata nbreu nbessai score categorie listeval
set nbreu 0
set nbessai 0
set score 0

set listdata [string map {\* ""} $listdata]

#informations pour les fiches bilan
#set tmp [mc {Remettre les syllabes dans l'ordre - }]
#set listeval \173$tmp\175\040$categorie

#on �limine de la liste les �l�ments qui n'ont qu'une syllabe, qui sont vides
set leng [llength $listdata]
for {set i [expr $leng - 1 ]} {$i >= 0} {incr i -1} {
  if {[llength [lindex [lindex $listdata $i ] 3]] <= 1} {
     set listdata [lreplace $listdata $i $i]
     }
  }
set leng [llength $listdata]
if {$leng < 1} {
  set answer [tk_messageBox -message [mc {Pas de donnees valides pour ce jeu.}] -type ok -icon info] 
  exec $iwish associations.tcl &
  exit
  }

#on m�lange la liste
for {set i 1} {$i <= $leng} {incr i 1} {
  set t1 [expr int(rand()*$leng)]
  set t2 [expr int(rand()*$leng)]
  set tmp [lindex $listdata $t1]
  set listdata [lreplace $listdata $t1 $t1 [lindex $listdata $t2]]
  set listdata [lreplace $listdata $t2 $t2 $tmp]
  }
#on remplit les tableaux charg�s de m�moriser les images et leurs �tiquettes
set nb $leng
if {$nb > 7} {
set nb 8}

#barre de progression
for {set i 1} {$i <= $nb} {incr i 1} {
.bframe.lab$i configure -image pneutre -width 30
}

for {set i 1} {$i <= $nb} {incr i 1} {
set imgarr($i) [lindex [lindex $listdata [expr $i - 1] ] 0]
set etiqarr($i) [lindex [lindex $listdata [expr $i - 1] ] 1]
}
#On place l'image et ses �tiquettes de syllabes sur le canevas
place $c

}

##################################################
#placement des images, textes sur le canevas
#################################################
proc place {c} {
# erreur : variable pour d�terminer � quel moment on peut afficher l'aide
global etiqarr imgarr font2 nb listdata longlist1 nbreu bgl ale erreur Home
set erreur 0
#on efface tout et on place les �l�ments sur le canevas
$c delete all
image create photo ok0 -file [file join sysdata ok0.gif] 
image create photo ok1 -file [file join sysdata ok1.gif]
$c create image 440 180 -image ok0 -tags verif
$c addtag highlight withtag verif
image create photo kimage1 -file [file join $Home images $imgarr([expr $nbreu +1])] -width 130 -height 130

#longlist1 sert � r�cup�rer les syllabes du mot � afficher
set list1 [lindex [lindex $listdata $nbreu ] 3]
set longlist1 [llength $list1]

#on m�lange les syllabes
for {set i 0} {$i < $longlist1} {incr i 1} {
  set ale($i) $i
  }
for {set i 1} {$i <= $longlist1} {incr i 1} {
  set t1 [expr int(rand()*$longlist1)]
  set t2 [expr int(rand()*$longlist1)]
  set temp $ale($t1)
  set ale($t1) $ale($t2)
  set ale($t2) $temp
  }

#on d�termine la longueur de la +longue syllabe, pour la dimension des traits
set max 0
for {set i 0} {$i < $longlist1} {incr i 1} {
  if {[string length [lindex $list1 $i]] > $max} { 
   set max [string length [lindex $list1 $i]] 
   }
  }


#on place les syllabes et les traits
for {set i 0} {$i < $longlist1} {incr i 1} {
  $c create text [expr 200 + $ale($i)*50] 350 -text [lindex $list1 $i] -anchor n -tags source$i -font $font2 
  $c create text [expr 180 - ([lindex $font2 1])*int($max/2) + $i*$max*([lindex $font2 1])] 220 -text [string repeat "_" $max] -anchor n -tags cible$i -font $font2 -fill $bgl
  $c addtag drag withtag source$i
  }

set xpos 200
set ypos 150
#on place l'iamge
$c create image $xpos $ypos -image kimage1 -tags img
}

#########################################################""
##lancement des op�rations
init $c

#gestion des �v�nements
$c bind verif <Any-Enter> "pushEnter $c"
$c bind verif <Any-Leave> "pushLeave $c"
$c bind verif <ButtonRelease-1> "verif $c"
#bind . <Destroy> "quitte"
$c bind drag <ButtonRelease-1> "itemStopDrag $c %x %y"
$c bind drag <1> "itemStartDrag $c %x %y"
$c bind drag <B1-Motion> "itemDrag $c %x %y"
if {$sound == 1} {
$c bind img <Any-Enter> "soundEnter $c"
$c bind img <Any-Leave> "soundLeave $c"
}




proc soundEnter {c} {
global imgarr flag nbreu etiqarr repbasecat Home
if {$flag == 0 } {
    set ext .wav
    set son [lindex [split $imgarr([expr $nbreu +1]) .] 0]
    enterstart [file join $Home sons $son$ext]
   }
}

proc soundLeave {c} {
enterstop
}


proc majbilan {} {
global bonnereponse nbreu listeval score nbessai
        switch $nbessai {
                1 {.bframe.lab$nbreu configure -image pbien -width 30
                lappend listeval 1\040\173$bonnereponse\175
                incr score 10
                }
                2 {.bframe.lab$nbreu configure -image ppass -width 30
                lappend listeval 2\040\173$bonnereponse\175
                incr score 5
                }
                default {.bframe.lab$nbreu configure -image pmal -width 30
                lappend listeval 3\040\173$bonnereponse\175
                }
              }

}
##############################################################"
# v�rification, lorsque l'on appuie sur le bouton v�rifier
################################################################
proc verif {c} {
global longlist1 nbreu nb listdata ale erreur font2 nbessai listeval user niveau categorie score bonnereponse repbasecat Home serie
variable repert

set compt 0
incr nbessai

#on concat�ne les syllabes plac�es dans le string reponse
set reponse ""
for {set j 0} {$j < $longlist1} {incr j 1} {
   set ciblecoord [$c bbox [$c find withtag cible$j]]
   foreach i [$c find overlapping [lindex $ciblecoord 0] [lindex $ciblecoord 1] [lindex $ciblecoord 2] [lindex $ciblecoord 3]] {
# on renvoie les syllabes mal plac�es � leur place

     if {[lsearch [$c gettags $i] cible$j] == -1} {

         if { [$c itemcget $i -text] != [lindex [lindex [lindex $listdata $nbreu ] 3] $j] } { 
              set numsource [string index [lindex [$c gettags $i] 0] end]
              $c coords $i [expr 200 + $ale($numsource)*50] 350
              }
        if {[lsearch [$c gettags $i] source$j] != -1} {            
        }
        set reponse $reponse[$c itemcget $i -text]
        }
   }
}
set bonnereponse ""
for {set k 0} {$k < [llength [lindex [lindex $listdata $nbreu ] 3] ]} {incr k 1} {
set bonnereponse $bonnereponse[lindex [lindex [lindex $listdata $nbreu] 3] $k]
}

#on compare avec la chaine initiale
if {[string compare $reponse $bonnereponse] == 0} {
   $c itemconf drag -tag dead
   incr nbreu
   $c delete withtag verif
   if {$nbreu==$nb} {
     image create photo figure -file [file join sysdata bien.gif]
     $c create image 440 100 -image figure -tags figure
     update
     after 2000
     $c delete all

### on met � jour les images de la barre de progression
     majbilan     
##########################################################"""""""
#on calcule le pourcentage de r�ussite
           set score [expr $score*10/$nb]
           set pourcent %
           $c create text 200 100 -text "[mc {Score : }] $score\040$pourcent" -font {Arial 24}
		lappend listeval \040bilan\040$serie\04013\040$repbasecat\040$repert\040$score
		enregistreval

            if {$score >= 75} { 
            update
            after 2000
#on d�termine le fichier de cat�gorie suivant � ouvrir
            set file $categorie
            set niveau ""
            for {set i 1} {$i <= [string length $file]} {incr i 1} {
               if {[string match {[0-9]} [string index $file end]] == 1} {
                 set niveau [string index $file end]$niveau
                 set  file [string range $file 0 [expr [string length $file] -2] ]
                }
            }
            if {$niveau == ""} {
            set niveau 0
            }
            incr niveau
            set ext .cat
            set file $file$niveau$ext
            if {[catch {set f [open [file join $Home categorie $repbasecat $file] "r"]} ]} {
            $c create text 200 200 -text [mc {C'est fini!}] -font {Arial 24}
            } else {
            set listdata [gets $f]
            close $f
            #enregistreval
            set categorie [lindex [split [lindex [split $file /] end] .] 0]
		wm title . "[mc {Remettre les syllabes dans l'ordre - }]$file" 
		#lappend listeval "4 \{[mc {Remettre les syllabes dans l'ordre - }] $categorie\}"
		for {set i 1} {$i < 9} {incr i 1} {
		.bframe.lab$i configure -image "" -width 1
		}
		incr serie
		set listeval \173[mc {Remettre les syllabes dans l'ordre - }]\175\040$categorie	
            main $c
            }
            } else {
            #s'il n'y a pas d'autre niveau de difficult� 
            $c create text 200 200 -text [mc {C'est fini!}] -font {Arial 24}
            }
################################################################
     } else {
#si c'est r�ussi, on continue jusqu'� la fin du jeu
	image create photo figure -file [file join sysdata bien.gif]
	$c create image 440 120 -image figure -tags figure
### on met � jour les images de la barre de progression
      majbilan
      set nbessai 0
      update
      after 2000
      place $c
     }
#s'il faut afficher l'aide
} else {
if {[incr erreur] ==2} {
$c create text 440 60 -text $bonnereponse -font $font2
}
image create photo figure -file [file join sysdata mal.gif]
$c create image 440 100 -image figure -tags figure
}
}


###########################################################"
proc pushEnter {c} {
#$c itemconf highlight -outline blue
$c itemconf highlight -image ok1

}

##########################################################
proc pushLeave {c} {
#$c itemconf highlight -outline red
$c itemconf highlight -image ok0

}
          
          

################################################################"
proc itemStartDrag {c x y} {
    global lastX lastY sourcecoord flag
    set flag 1
    catch {$c delete withtag figure}
    set sourcecoord [$c coords current]
    set lastX [$c canvasx $x]
    set lastY [$c canvasy $y]
    $c raise current
    }

#################################################################
proc itemStopDrag {c x y} {
global lastX lastY sourcecoord nb listdata flag
    set flag 0
    set strcible cible
    set strsource [lindex [$c gettags current] 0]
    set ciblecoord [$c bbox [$c find withtag $strcible]]
    set coord [$c bbox current]
    if {[llength [$c find overlapping [lindex $coord 0] [lindex $coord 1] [lindex $coord 2] [lindex $coord 3]]] > 2 || [lindex $coord 0] > 620 || [lindex $coord 1] > 360} {
    $c coords current [lindex $sourcecoord 0] [lindex $sourcecoord 1]
    return
    }
    
    foreach i [$c find overlapping [lindex $coord 0] [lindex $coord 1] [lindex $coord 2] [lindex $coord 3]] {
         if {[lsearch -regexp [$c gettags $i] cible*] != -1} {
            $c coords current [$c coords $i]
            }
    }
}

#########################################################"
proc itemDrag {c x y} {
    global lastX lastY
    set x [$c canvasx $x]
    set y [$c canvasy $y]
    $c move current [expr $x-$lastX] [expr $y-$lastY]
    set lastX $x
    set lastY $y
}

proc suite {c} {
global catedefaut
enregistreval
set catedefaut ""
init $c
}



















