############################################################################
# Copyright (C) 2002 David Lucardi
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : fichier.php
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 26/04/2002
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version    $Id: associations.tcl,v 1.9 2006/05/21 10:15:29 david Exp $
# @author     David Lucardi
# @project
# @copyright  David Lucardi 26/04/2002
#
#
#########################################################################
#!/bin/sh
#associations.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

global basedir Homeconf plateforme sound
set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)

source fonts.tcl
source path.tcl
source msg.tcl

init $plateforme
inithome
initlog $plateforme $ident
changehome

#wm resizable . 0 0
#on place la fenetre en haut � gauche
wm geometry . +0+0
. configure -background #ffff80

###########################################
proc recherchescorecouleur {} {
global listeval score frcolor serie seriemax Home
variable demarre
variable repert
variable repertcat
set ext .cat
	for {set x 0} {$x<19} {incr x} {
	set score($x) ""
	set frcolor($x) white
	set serie($x) 0
	set seriemax($x) 1
	}

	for {set x [llength $listeval]} {$x>-1} {incr x -1} {
	set it [lindex $listeval $x]
		if {[lindex [lindex $it end] 0] == "bilan"} {
		set bil [lindex $it end]
		set seriebil [lindex $bil 1]
		set catbil [lindex $it 1]
		set numexobil [lindex $bil 2]
		set repertbil [lindex $bil 3]
		set dossierbil [lindex $bil 4]
		set scorebil [lindex $bil 5]
			if {$score($numexobil) == "" && $repertbil == $repertcat && $dossierbil == $repert && (($seriebil==0 && $catbil==$demarre) || ($seriebil!=0 && $catbil=="$demarre$seriebil"))} {
set tmp 1
set file $demarre$tmp$ext
while { [file exists [file join $Home categorie $repertcat $file]]} {
incr tmp
set file $demarre$tmp$ext
}
set seriemax($numexobil) $tmp		

			set score($numexobil) "$scorebil %"
			set serie($numexobil) $seriebil
			if {$scorebil < 50} {set frcolor($numexobil) red}
			if {$scorebil >= 50 && $scorebil < 75} {set frcolor($numexobil) green}
			if {$scorebil  >= 75} {set frcolor($numexobil) yellow}
			}
		}
	}
}



proc interface {} {

global Home basedir sysFont progaide plateforme listacti sound baseHome prof abuledu user listeval score frcolor serie iwish seriemax
variable repert
variable repertcat
variable demarre
variable police


set listacti 1\0401\0401\0401\0401\0401\0401\0401\0401\0401\0401\0401\0401\0401\0401\0401\0401\0401\0401
set f [open [file join $baseHome reglages associations.conf] "r"]
set tmp [gets $f]
set police [gets $f]
close $f


if {$tmp == "none"} {
set demarre "Aucune"
} else {
set ext .dat
set demarre [string map {.cat ""} $tmp]
if {[catch {set f [open [file join $Home categorie $repertcat $demarre$ext] "r"]}] != 1} {
set listacti [lindex [gets $f] 1]
}
}

wm title . "Associations - [mc {Categorie}] : $demarre - [mc {Utilisateur}] : [string map {.log \040} [file tail $user]]"

set listacti [string map {0 disabled 1 normal} $listacti]
catch {destroy .menu}
catch {destroy .wleft}
catch {destroy .wcenter}
catch {destroy .wright}

menu .menu -tearoff 0

# Creation du menu Fichier
menu .menu.fichier -tearoff 0
.menu add cascade -label [mc {Fichier}] -menu .menu.fichier

if {($abuledu == 0) || ($repert==0) || ($prof==1)} {
.menu.fichier add command -label [mc {Editeur}] -command "exec $iwish editeur.tcl"
} else {
.menu.fichier add command -label [mc {Editeur}] -command "exec $iwish editeur.tcl" -state disabled
}
.menu.fichier add command -label [mc {Gestion}] -command "exec $iwish gestion.tcl"
.menu.fichier add command -label [mc {Imprimer le suivi}] -command "imprimesuivi"

.menu.fichier add sep
.menu.fichier add command -label [mc {Quitter  Ctrl-Q}] -command exit


menu .menu.repbase -tearoff 0 
.menu add cascade -label [mc {Dossier de categories}] -menu .menu.repbase
catch {
foreach i [lsort [glob [file join $Home categorie cat_*]]] {
.menu.repbase add radio -label [lindex [ split [file tail $i] _ ] 1] -variable repertcat -value [file tail $i] -command "changerepbasecat [file tail $i] \n interface"
}
}



menu .menu.action -tearoff 0
.menu add cascade -label [mc {Activites}] -menu .menu.action
menu .menu.action.m1 -tearoff 0

catch {
.menu.action add radio -label "Aucune" -variable demarre -command "changecatedefaut"
foreach i [lsort [glob [file join $Home categorie $repertcat *.cat]]] {
if {([string first non_ [file tail [lindex [split [lindex [split $i /] end] .] 0]]] != 0) && ([string match {[0-9]} [file tail [string index [lindex [split [lindex [split $i /] end] .] 0] end ]]] != 1)} {
.menu.action add radio -label [file tail [lindex [split [lindex [split $i /] end] .] 0]] -variable demarre -command "changecatedefaut"
}
}
}

menu .menu.option -tearoff 0
.menu add cascade -label [mc {Options}] -menu .menu.option

menu .menu.option.police -tearoff 0 

.menu.option add cascade -label [mc {Police}] -menu .menu.option.police


set listedebonnespolices {Arial {Arial Black} courier {Cataneo BT} helvetica {Lucida Handwriting} times {Times New Roman}}
foreach i [lsort [font families]] {
      if {[lsearch $listedebonnespolices $i] != -1} {
.menu.option.police add radio -label $i -variable police -command "setpolice"

     }   
    }


set ext .msg
menu .menu.option.lang -tearoff 0 
.menu.option add cascade -label "[mc {Langue}]" -menu .menu.option.lang

foreach i [glob [file join  $basedir msgs *$ext]] {
set langue [string map {.msg ""} [file tail $i]]
.menu.option.lang add radio -label $langue -variable langue -command "setlang $langue"
}



###########################

menu .menu.option.repwork -tearoff 0 
.menu.option add cascade -label [mc {Repertoire de travail}] -menu .menu.option.repwork 
.menu.option.repwork add radio -label [mc {Individuel}] -variable repert -value 0 -command "changeinterface"
.menu.option.repwork add radio -label [mc {Commun}] -variable repert -value 1 -command "changeinterface"



#if {$plateforme == "windows"} {
.menu add command -label [mc {Utilisateur}] -command "setwindowsusername"
#}

menu .menu.aide -tearoff 0
.menu add cascade -label [mc {Aide}] -menu .menu.aide
	if {[file exists [file join $baseHome reglages lang.conf]] == 1} {
	set f [open [file join $baseHome reglages lang.conf] "r"]
  	gets $f lang
  	close $f
	set fich "_index.htm"
	set fich $lang$fich
	set fichier [file join [pwd] aide $fich]
	} else {
	set fichier [file join [pwd] aide fr_index.htm]
	}
  


if {[file exists $fichier] != 1} {
set fichier [file join [pwd] aide fr_index.htm]
}
.menu.aide add command -label [mc {Aide}] -command "exec $progaide file:$fichier &"
#.menu.aide add command -label [mc {Aide}] -command "exec $progaide \042$fichier\042 &"
.menu.aide add command -label [mc {A propos ...}] -command "source apropos.tcl"

. configure -menu .menu
###############################################################"
set listeval ""
catch {
set f [open [file join $user] "r" ]
while {![eof $f]} {
lappend listeval [gets $f]                          
}
close $f
}
recherchescorecouleur
#######################################################################"
. configure -background blue
frame .wleft -background blue -height 300 -width 200
pack .wleft -side left
if {$sound != 0} {
label .wleft.a1 -text [mc {Entendre}] -background #ff9f80 -font $sysFont(s)
grid .wleft.a1 -row 0 -column 0 -sticky news
}
label .wleft.a2 -text [mc {Voir}] -background #ffff80 -font $sysFont(s)
grid .wleft.a2 -row 0 -column 1 -sticky news
label .wleft.a3 -text [mc {Reconnaitre}] -background #ff9f80 -font $sysFont(s)
grid .wleft.a3 -row 0 -column 2 -columnspan 2 -sticky news
label .wleft.a4 -text [mc {Combiner}] -background #ffff80 -font $sysFont(s)
grid .wleft.a4 -row 0 -column 4 -sticky news
label .wleft.a5 -text [mc {Ecrire}] -background #ff9f80 -font $sysFont(s)
grid .wleft.a5 -row 0 -column 5 -sticky news
if {$sound != 0} {

button .wleft.b11 -image [image create photo -file sysdata/b17.jpg] -background $frcolor(0) -command "exec $iwish associe15.tcl 1 $serie(0) &;exit" -bd 4 -height 90 -width 90
grid .wleft.b11 -row 1 -column 0 -sticky news -padx 3 -pady 3
.wleft.b11 configure -state [lindex $listacti 0]
set ltext ""
if {$score(0) != ""} {set ltext "serie [expr $serie(0) +1]/$seriemax(0)"}
#if {$serie(0) != 0} {set ltext "serie $serie(0)"}
label .wleft.l11 -text "$ltext $score(0)" -foreground $frcolor(0) -background blue -font $sysFont(s)
grid .wleft.l11 -row 2 -column 0 -sticky news

button .wleft.b12 -image [image create photo -file sysdata/b18.jpg] -background $frcolor(1) -command "exec $iwish associe16.tcl 1 $serie(1) &;exit" -bd 4 -height 90 -width 90
grid .wleft.b12 -row 3 -column 0 -sticky news -padx 3 -pady 3
.wleft.b12 configure -state [lindex $listacti 1]
set ltext ""
if {$score(1) != ""} {set ltext "serie [expr $serie(1) +1]/$seriemax(1)"}
#if {$serie(1) != 0} {set ltext "serie $serie(1)"}
label .wleft.l12 -text "$ltext $score(1)" -foreground $frcolor(1) -background blue -font $sysFont(s)
grid .wleft.l12 -row 4 -column 0 -sticky news

button .wleft.b13 -image [image create photo -file sysdata/b19.jpg] -background $frcolor(2) -command "exec $iwish associe17.tcl 1 $serie(2) &;exit" -bd 4 -height 90 -width 90
grid .wleft.b13 -row 5 -column 0 -sticky news -padx 3 -pady 3
.wleft.b13 configure -state [lindex $listacti 2]
set ltext ""
if {$score(2) != ""} {set ltext "serie [expr $serie(2) +1]/$seriemax(2)"}
#if {$serie(2) != 0} {set ltext "serie $serie(2)"}
label .wleft.l13 -text "$ltext $score(2)" -foreground $frcolor(2) -background blue -font $sysFont(s)
grid .wleft.l13 -row 6 -column 0 -sticky news
}

button .wleft.b21 -image [image create photo -file sysdata/b7.jpg] -background $frcolor(3) -command "exec $iwish associe10.tcl 1 $serie(3) &;exit" -bd 4 -height 90 -width 90
grid .wleft.b21 -row 1 -column 1 -sticky news -padx 3 -pady 3
.wleft.b21 configure -state [lindex $listacti 3]
set ltext ""
if {$score(3) != ""} {set ltext "serie [expr $serie(3) +1]/$seriemax(3)"}
#if {$serie(3) != 0} {set ltext "serie $serie(3)"}
label .wleft.l21 -text "$ltext $score(3)" -foreground $frcolor(3) -background blue -font $sysFont(s)
grid .wleft.l21 -row 2 -column 1 -sticky news

button .wleft.b22 -image [image create photo -file sysdata/b8.jpg] -background $frcolor(4) -command "exec $iwish associe13.tcl 1 $serie(4) &;exit" -bd 4 -height 90 -width 90
grid .wleft.b22 -row 3 -column 1 -sticky news -padx 3 -pady 3
.wleft.b22 configure -state [lindex $listacti 4]
set ltext ""
if {$score(4) != ""} {set ltext "serie [expr $serie(4) +1]/$seriemax(4)"}
#if {$serie(4) != 0} {set ltext "serie $serie(4)"}
label .wleft.l22 -text "$ltext $score(4)" -foreground $frcolor(4) -background blue -font $sysFont(s)
grid .wleft.l22 -row 4 -column 1 -sticky news

button .wleft.b23 -image [image create photo -file sysdata/b13.jpg] -background $frcolor(5) -command "exec $iwish associe9.tcl 1 $serie(5) &;exit" -bd 4 -height 90 -width 90
grid .wleft.b23 -row 5 -column 1 -sticky news -padx 3 -pady 3
.wleft.b23 configure -state [lindex $listacti 5]
set ltext ""
if {$score(5) != ""} {set ltext "serie [expr $serie(5) +1]/$seriemax(5)"}
#if {$serie(5) != 0} {set ltext "serie $serie(5)"}
label .wleft.l23 -text "$ltext $score(5)" -foreground $frcolor(5) -background blue -font $sysFont(s)
grid .wleft.l23 -row 6 -column 1 -sticky news

button .wleft.b31 -image [image create photo -file sysdata/b1.jpg] -background $frcolor(6) -command "exec $iwish associe1.tcl 1 $serie(6) &; exit" -bd 4 -height 90 -width 90
grid .wleft.b31 -row 1 -column 2 -sticky news -padx 3 -pady 3
.wleft.b31 configure -state [lindex $listacti 6]
set ltext ""
if {$score(6) != ""} {set ltext "serie [expr $serie(6) +1]/$seriemax(6)"}
#if {$serie(6) != 0} {set ltext "serie $serie(6)"}
label .wleft.l31 -text "$ltext $score(6)" -foreground $frcolor(6) -background blue -font $sysFont(s)
grid .wleft.l31 -row 2 -column 2 -sticky news

button .wleft.b32 -image [image create photo -file sysdata/b2.jpg] -background $frcolor(7) -command "exec $iwish associe1.tcl 2 $serie(7) &; exit" -bd 4 -height 90 -width 90
grid .wleft.b32 -row 3 -column 2 -sticky news -padx 3 -pady 3
.wleft.b32 configure -state [lindex $listacti 7]
set ltext ""
if {$score(7) != ""} {set ltext "serie [expr $serie(7) +1]/$seriemax(7)"}
#if {$serie(7) != 0} {set ltext "serie $serie(7)"}
label .wleft.l32 -text "$ltext $score(7)" -foreground $frcolor(7) -background blue -font $sysFont(s)
grid .wleft.l32 -row 4 -column 2 -sticky news

button .wleft.b33 -image [image create photo -file sysdata/b3.jpg] -background $frcolor(8) -command "exec $iwish associe2.tcl 1 $serie(8) &; exit" -bd 4 -height 90 -width 90
grid .wleft.b33 -row 5 -column 2 -sticky news -padx 3 -pady 3
.wleft.b33 configure -state [lindex $listacti 8]
set ltext ""
if {$score(8) != ""} {set ltext "serie [expr $serie(8) +1]/$seriemax(8)"}
#if {$serie(8) != 0} {set ltext "serie $serie(8)"}
label .wleft.l33 -text "$ltext $score(8)" -foreground $frcolor(8) -background blue -font $sysFont(s)
grid .wleft.l33 -row 6 -column 2 -sticky news

button .wleft.b34 -image [image create photo -file sysdata/b4.jpg] -background $frcolor(9) -command "exec $iwish associe2.tcl 2 $serie(9) &; exit" -bd 4 -height 90 -width 90
grid .wleft.b34 -row 7 -column 2 -sticky news -padx 3 -pady 3
.wleft.b34 configure -state [lindex $listacti 9]
set ltext ""
if {$score(9) != ""} {set ltext "serie [expr $serie(9) +1]/$seriemax(9)"}
#if {$serie(9) != 0} {set ltext "serie $serie(9)"}
label .wleft.l34 -text "$ltext $score(9)" -foreground $frcolor(9) -background blue -font $sysFont(s)
grid .wleft.l34 -row 8 -column 2 -sticky news

button .wleft.b41 -image [image create photo -file sysdata/b9.jpg] -background $frcolor(10) -command "exec $iwish associe3.tcl 1 $serie(10) &; exit" -bd 4 -height 90 -width 90
grid .wleft.b41 -row 1 -column 3 -sticky news -padx 3 -pady 3
.wleft.b41 configure -state [lindex $listacti 10]
set ltext ""
if {$score(10) != ""} {set ltext "serie [expr $serie(10) +1]/$seriemax(10)"}
#if {$serie(10) != 0} {set ltext "serie $serie(10)"}
label .wleft.l41 -text "$ltext $score(10)" -foreground $frcolor(10) -background blue -font $sysFont(s)
grid .wleft.l41 -row 2 -column 3 -sticky news

button .wleft.b42 -image [image create photo -file sysdata/b14.jpg] -background $frcolor(11) -command "exec $iwish associe11.tcl 1 $serie(11) &; exit" -bd 4 -height 90 -width 90
grid .wleft.b42 -row 3 -column 3 -sticky news -padx 3 -pady 3
.wleft.b42 configure -state [lindex $listacti 11]
set ltext ""
if {$score(11) != ""} {set ltext "serie [expr $serie(11) +1]/$seriemax(11)"}
#if {$serie(11) != 0} {set ltext "serie $serie(11)"}
label .wleft.l42 -text "$ltext $score(11)" -foreground $frcolor(11) -background blue -font $sysFont(s)
grid .wleft.l42 -row 4 -column 3 -sticky news

if {$sound != 0} {
button .wleft.b43 -image [image create photo -file sysdata/b16.jpg] -background $frcolor(12) -command "exec $iwish associe12.tcl 1 $serie(12) &; exit" -bd 4 -height 90 -width 90
grid .wleft.b43 -row 5 -column 3 -sticky news -padx 3 -pady 3
.wleft.b43 configure -state [lindex $listacti 12]
set ltext ""
if {$score(12) != ""} {set ltext "serie [expr $serie(12) +1]/$seriemax(12)"}
#if {$serie(12) != 0} {set ltext "serie $serie(12)"}
label .wleft.l43 -text "$ltext $score(12)" -foreground $frcolor(12) -background blue -font $sysFont(s)
grid .wleft.l43 -row 6 -column 3 -sticky news
}

button .wleft.b51 -image [image create photo -file sysdata/b5.jpg] -background $frcolor(13) -command "exec $iwish associe4.tcl 1 $serie(13) &; exit" -bd 4 -height 90 -width 90
grid .wleft.b51 -row 1 -column 4 -sticky news -padx 3 -pady 3
.wleft.b51 configure -state [lindex $listacti 13]
set ltext ""
if {$score(13) != ""} {set ltext "serie [expr $serie(13) +1]/$seriemax(13)"}
#if {$serie(13) != 0} {set ltext "serie $serie(13)"}
label .wleft.l51 -text "$ltext $score(13)" -foreground $frcolor(13) -background blue -font $sysFont(s)
grid .wleft.l51 -row 2 -column 4 -sticky news

button .wleft.b52 -image [image create photo -file sysdata/b6.jpg] -background $frcolor(14) -command "exec $iwish associe14.tcl 1 $serie(14) &; exit" -bd 4 -height 90 -width 90
grid .wleft.b52 -row 3 -column 4 -sticky news -padx 3 -pady 3
.wleft.b52 configure -state [lindex $listacti 14]
set ltext ""
if {$score(14) != ""} {set ltext "serie [expr $serie(14) +1]/$seriemax(14)"}
#if {$serie(14) != 0} {set ltext "serie $serie(14)"}
label .wleft.l52 -text "$ltext $score(14)" -foreground $frcolor(14) -background blue -font $sysFont(s)
grid .wleft.l52 -row 4 -column 4 -sticky news

button .wleft.b53 -image [image create photo -file sysdata/b10.jpg] -background $frcolor(15) -command "exec $iwish associe5.tcl 1 $serie(15) &; exit" -bd 4 -height 90 -width 90
grid .wleft.b53 -row 5 -column 4 -sticky news -padx 3 -pady 3
.wleft.b53 configure -state [lindex $listacti 15]
set ltext ""
#if {$serie(15) != 0} {set ltext "serie $serie(15)"}
if {$score(15) != ""} {set ltext "serie [expr $serie(15) +1]/$seriemax(15)"}
label .wleft.l53 -text "$ltext $score(15)" -foreground $frcolor(15) -background blue -font $sysFont(s)
grid .wleft.l53 -row 6 -column 4 -sticky news

button .wleft.b54 -image [image create photo -file sysdata/b11.jpg] -background $frcolor(16) -command "exec $iwish associe6.tcl 1 $serie(16) &; exit" -bd 4 -height 90 -width 90
grid .wleft.b54 -row 7 -column 4 -sticky news -padx 3 -pady 3
.wleft.b54 configure -state [lindex $listacti 16]
set ltext ""
if {$score(16) != ""} {set ltext "serie [expr $serie(16) +1]/$seriemax(16)"}
#if {$serie(16) != 0} {set ltext "serie $serie(16)"}
label .wleft.l54 -text "$ltext $score(16)" -foreground $frcolor(16) -background blue -font $sysFont(s)
grid .wleft.l54 -row 8 -column 4 -sticky news

button .wleft.b61 -image [image create photo -file sysdata/b12.jpg] -background $frcolor(17) -command "exec $iwish associe7.tcl 1 $serie(17) &; exit" -bd 4 -height 90 -width 90
grid .wleft.b61 -row 1 -column 5 -sticky news -padx 3 -pady 3
.wleft.b61 configure -state [lindex $listacti 17]
set ltext ""
if {$score(17) != ""} {set ltext "serie [expr $serie(17) +1]/$seriemax(17)"}
#if {$serie(17) != 0} {set ltext "serie $serie(17)"}
label .wleft.l61 -text "$ltext $score(17)" -foreground $frcolor(17) -background blue -font $sysFont(s)
grid .wleft.l61 -row 2 -column 5 -sticky news

if {$sound != 0} {
button .wleft.b62 -image [image create photo -file sysdata/b15.jpg] -background $frcolor(18) -command "exec $iwish associe8.tcl 1 $serie(18) &; exit" -bd 4 -height 90 -width 90
grid .wleft.b62 -row 3 -column 5 -sticky news -padx 3 -pady 3
.wleft.b62 configure -state [lindex $listacti 18]
set ltext ""
if {$score(18) != ""} {set ltext "serie [expr $serie(18) +1]/$seriemax(18)"}
#if {$serie(18) != 0} {set ltext "serie $serie(18)"}
label .wleft.l62 -text "$ltext $score(18)" -foreground $frcolor(18) -background blue -font $sysFont(s)
grid .wleft.l62 -row 4 -column 5 -sticky news

}

#set myimage [image create photo -file sysdata/background.gif]
#label .wcenter.imagedisplayer -image $myimage -background blue
#pack .wcenter.imagedisplayer -side top

}


############
# Bindings #
############
bind . <Control-q> {exit}


proc changeinterface {} {
global categorie repbasecat baseHome Home
variable demarre
variable repertcat
changehome
set demarre "Aucune"
set categorie $demarre
set repbasecat "cat_francais"
set repertcat $repbasecat
set f [open [file join $baseHome reglages associations.conf] "r"]
set tmp1 [gets $f]
set tmp2 [gets $f]
set tmp3 [gets $f]
set tmp4 [gets $f]
set tmp5 [gets $f]
set tmp6 [gets $f]
close $f
set f [open [file join $baseHome reglages associations.conf] "w"]
puts $f $categorie
puts $f $tmp2
puts $f $tmp3
puts $f $tmp4
puts $f $tmp5
puts $f $repbasecat
close $f
if {![file isdirectory [file join $Home categorie $repbasecat]]} {
	file mkdir [file join $Home categorie $repbasecat]
	}
changecatedefaut
#interface
}


proc lanceappli {appli niveau serie} {
global iwish
set appli [file join $appli]
exec $iwish $appli $niveau $serie &
exit
}

interface

proc setlang {lang} {
global env plateforme baseHome
set env(LANG) $lang
set f [open [file join $baseHome reglages lang.conf] "w"]
puts $f $lang
close $f

::msgcat::mclocale $lang
::msgcat::mcload [file join [file dirname [info script]] msgs]
interface
}

proc changecatedefaut {} {
global categorie Home baseHome
variable demarre
set ext .cat
set categorie $demarre
if {$categorie == "Aucune"} {
set cattmp "none"
} else {
set cattmp $categorie$ext
}
set f [open [file join $baseHome reglages associations.conf] "r"]
set tmp1 [gets $f]
set tmp2 [gets $f]
set tmp3 [gets $f]
set tmp4 [gets $f]
set tmp5 [gets $f]
set tmp6 [gets $f]
close $f
set f [open [file join $baseHome reglages associations.conf] "w"]
puts $f $cattmp
puts $f $tmp2
puts $f $tmp3
puts $f $tmp4
puts $f $tmp5
puts $f $tmp6
close $f

interface
}


proc setpolice {} {
global categorie Home baseHome
variable police
set f [open [file join $baseHome reglages associations.conf] "r"]
set tmp1 [gets $f]
set tmp2 [gets $f]
set tmp3 [gets $f]
set tmp4 [gets $f]
set tmp5 [gets $f]
set tmp6 [gets $f]
close $f
set f [open [file join $baseHome reglages associations.conf] "w"]
puts $f $tmp1
puts $f $police
puts $f $tmp3
puts $f $tmp4
puts $f $tmp5
puts $f $tmp6
close $f

}

proc imprimesuivi {} {
global progaide LogHome user score listacti serie seriemax
variable demarre
variable repertcat
#[lindex $listacti 0]
set listexo {{J'entends, je n'entends pas - } {J'entends (Debut-Milieu-Fin) - } {J'entends (Quelle syllabe?) - } {Discrimination visuelle, lettre - } {Discrimination visuelle, lettre (niveau 2) - } {Discrimination visuelle, phoneme ou mot - } {Une image, plusieurs etiquettes - } {Une image, plusieurs etiquettes - } {Plusieurs images, une etiquette - } {Plusieurs images, une etiquette - } {4 images, 4 etiquettes - } {Mots voisins (support image) - } {Mots voisins (support mot entendu) - } {Remettre les syllabes dans l'ordre - } {Remettre les syllabes dans l'ordre - } {Remettre les lettres dans l'ordre - } {Retrouver la fin des mots - } {Dictee visuelle - } {Dictee - }}
set g [open [file join $LogHome tmp1.htm] "w" ] 
puts $g  "<html><body>"
puts $g  "<p align = center><B>Fiche de [string map {.log \040} [file tail $user]]</B></p>"
puts $g  "<p><B>[mc {Categorie}] : $demarre - [mc {Dossier}] : $repertcat</B></p>"


for {set i 0} {$i < 19} {incr i} {
set texte [mc [lindex $listexo $i]]
if {[lindex $listacti $i] !=0} {
#if {$serie($i) !=0} {set texte "$texte serie $serie($i) : "}
if {$score($i) !=""} {set texte "$texte serie [expr $serie($i)+ 1]/$seriemax($i) : $score($i)"}
#set texte "$texte$score($i)"
}

puts $g  "<p>$texte</p>"
}

close $g
set fichier [file join [pwd] $LogHome tmp1.htm]
exec $progaide $fichier &
}

