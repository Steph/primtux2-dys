#!/bin/sh
#labyrinthes.tcl
#\
exec wish "$0" ${1+"$@"}
#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#**************************************************************************
#  File  : $$$
#  Author  : jlsendral@free.fr
#  Modifier: 
#  Date    : 24/04/2002
#  Licence : GNU/GPL Version 2 ou plus
#
#  Description:
#  ------------
#  @version
#  @author     Jean-Louis Sendral
#  @modifier   
#  @project    Le terrier
#  @copyright  Eric Seigne 24/04/2002
#
#  *************************************************************************



global sysFont rep_travail
source labyrinthe.conf
#source i18n
source msg.tcl
##source fonts.tcl
source path.tcl
set plateforme $tcl_platform(platform) ; set ident $tcl_platform(user)
init $plateforme
#initlog $plateforme  $ident
set basedir [pwd]
cd $basedir
set rep_travail $DOSSIER_EXOS ; ###rep par d�faut
set f [open [file join $Home reglages reglage.conf] "r"  ]
##set init_laby {1 2 {1 4 2   3}}
##et sir�glage vide ???
set init_laby [gets $f] ;#init du labyrinthe.
if {$init_laby == ""} { set init_laby aucun } 
set rep_travail  [gets $f]
if {$rep_travail == ""} { set rep_travail $DOSSIER_EXOS }
##traite_laby $init_laby
##choix_repert $rep_travail commun
close $f
##set init_laby "aucun"
##wm geometry . +0+0
##. configure -background blue -height 460 -width 560
##wm resizable . 0 0
##wm title . [mc {Labyrinthes_ relatifs_caches}] 
##. configure -background #ffff80

proc choix_repert { dossier type} {
global rep_travail DOSSIER_EXOS Home laby nom_laby init_laby
if { $type == "exemple"  || $dossier == $DOSSIER_EXOS || \
$dossier == [file join $Home data ]}  {
set rep_travail $dossier
#return
}
set rep_travail $dossier
## catch  { set dir  [tk_chooseDirectory  -initialdir $dossier  -title "[mc {Choisir le r�pertoire de travail}]" ]
##  set rep_travail $dir  }
set liste_laby {}
catch {
foreach i [lsort [glob -type f -dir [file join $rep_travail ]  *.lab  ]] {
set liste_laby [linsert $liste_laby end [file tail $i ] ]
}
}
##set liste_laby {a b c}
.menu.choix delete 0 end
.menu.choix  add radio -label  "aucun"  -variable laby -command "traite_laby  aucun " 
foreach laby $liste_laby  {
.menu.choix  add radio -label  $laby  -variable laby -command "traite_laby  $laby"
}

set laby $init_laby
##.menu.choix invoke [.menu.choix index [file tail $init_laby]]
##les s�quences

set liste_seq {}
catch {
foreach i [lsort [glob -type f -dir [file join $rep_travail ]  *.seq  ]] {
set liste_seq [linsert $liste_seq end [file tail $i ] ]
}
}
##tk_messageBox -message "liste : $liste_seq" -title "liste seqs" -icon info -type ok 
.menu.sequences delete 0 end
foreach seq $liste_seq  {
.menu.sequences  add radio -label  $seq  -variable seq -command "traite_seq  $seq"
}

}
proc traite_laby { lab_c} {
global rep_travail basedir init_laby Home
if { $lab_c == "aucun" } {
set init_laby aucun
cd $basedir
set f [open [file join $Home reglages reglage.conf] "w+"  ]
puts $f "aucun"
puts $f $rep_travail
close $f 

return }
##cd $rep_travail modifs dues � wish84
##set f [open [file join $lab_c] "r"  ]
##set init_laby {1 2 {1 4 2   3}}
##set init_laby [gets $f] ;#init du labyrinthe.
set init_laby  $lab_c
##close $f
cd $basedir
set f [open [file join $Home reglages reglage.conf] "w+"  ]
puts $f $lab_c
puts $f  $rep_travail
close $f
##.menu.choix  add radio -label $init_laby -variable laby1 -command ""
}


proc interface {} {
global Home DOSSIER_EXOS basedir rep_travail init_laby choix_rep
wm geometry . +0+0
. configure -background blue 
##-height 460 -width 560
##wm resizable . 0 0
wm title . [mc {Labyrinthes normaux  relatifs cach�s}] 

catch {destroy .menu}
catch {destroy .frame}
global tcl_platform basedir progaide
set font {Arial 16}

########################
# 	Barre de menus	#
#-----------------------#

# Creation du menu utilise comme barre de menu:
menu .menu -tearoff 0

# Creation du menu Fichier
menu .menu.fichier -tearoff 0
.menu add cascade -label [mc {Activit�s}] -menu .menu.fichier
.menu.fichier add command -label [mc {Labyrinthe} ] -command {lanceappli laby.tcl   $init_laby   "normal" $rep_travail 0 }
.menu.fichier add command -label [ mc {Labyrinthe_relatif}] -command {lanceappli laby.tcl  $init_laby "relatif" $rep_travail 0 }
.menu.fichier add command -label [ mc {Labyrinthe_cache}] -command {lanceappli laby.tcl  $init_laby  "cache"  $rep_travail 0}
.menu.fichier add sep
.menu.fichier add command -label [ mc {Cr�er un labyrinthe}] -command {\
 exec $iwish laby.tcl aucun normal "[file join  $rep_travail]" 0  ;\
 choix_repert [ file  join $rep_travail ] Commun }
##tk_messageBox  -message "[mc {test cr��r}]"    -icon info  -type ok -title "Autre d�part, autre arriv�e ?"
## exec $iwish $appli $lab $type $rep $bouton_ant&
 .menu.fichier add sep

 .menu.fichier add command -label [ mc {Editer une s�quence}] -command {exec $iwish sequences.tcl $rep_travail  ; choix_repert [file join $rep_travail ] Commun }
 
 
 .menu.fichier add sep
.menu.fichier add command -label [ mc {Bilan par Classe}] -command {exec $iwish bilan.tcl & }
.menu.fichier add sep
.menu.fichier add command -label [ mc {quitter}] -command exit
.menu add cascade -label [mc {Labyrinthes}] -menu .menu.choix
##.menu add cascade -label [mc {Aides}] -menu .menu.aides
set ext .msg
##choix des r�pertoires travail
menu .menu.choix -tearoff 0
menu .menu.sequences -tearoff 0
.menu add cascade -label [mc {S�quences}] -menu .menu.sequences


menu .menu.option -tearoff 0
.menu add cascade -label [mc {Reglages}] -menu .menu.option
set ext .msg
##choix des r�pertoires travail
menu .menu.option.repert -tearoff 0
.menu.option add cascade -label "[mc {R�pertoires}]" -menu .menu.option.repert
menu .menu.option.repert.ssmenu_perso -tearoff 0
.menu.option.repert add cascade -label "[mc {Personnel}]" -menu .menu.option.repert.ssmenu_perso
##.menu.option.repert.ssmenu_perso add cascade -label [mc {perso}] -menu .menu.option.repert.ssmenu_perso.rep

### -command "choix_repert [file join $Hom] personnel "
.menu.option.repert.ssmenu_perso  add radio -label "[mc {Personnel} ]" -variable choix_rep -value [file join $Home data] \
-command  { choix_repert "[file join  $Home data ]"  personnel }
catch {
foreach i [lsort [glob -type d -dir [file join $Home data ] *  ]] {
.menu.option.repert.ssmenu_perso  add radio -label [file tail $i] -variable choix_rep -value $i \
-command { choix_repert  $choix_rep  personnel }
}
}

menu .menu.option.repert.ssmenu_commun -tearoff 0
.menu.option.repert add cascade -label "[mc {Commun}]"  -menu .menu.option.repert.ssmenu_commun
.menu.option.repert.ssmenu_commun  add radio -label "[mc {Commun}]" -variable choix_rep  \
 -command { choix_repert $DOSSIER_EXOS  commun }

 catch {
foreach i [lsort [glob -type d -dir [file join $DOSSIER_EXOS  ] *  ]] {
.menu.option.repert.ssmenu_commun  add radio -label [file tail $i] -variable choix_rep -value $i \
-command   { choix_repert $choix_rep  commun }
}



}
##.menu.option.repert.ssmenu_commun invoke 0
##set choix_rep "Commun"
menu .menu.option.repert.ssmenu_exemple -tearoff 0
.menu.option.repert add cascade  -label "[mc {Exemple}]" -menu .menu.option.repert.ssmenu_exemple
.menu.option.repert.ssmenu_exemple  add radio -label "[mc {Exemple}]" -variable choix_rep  \
-command { choix_repert $basedir exemple}
##-value $basedir
##set choix_rep "Commun"
##-command {choix_repert 
 ##"$basedir"  exemple }
set choix_rep $rep_travail
menu .menu.option.lang -tearoff 0
.menu.option add cascade -label "[mc {Langue}]" -menu .menu.option.lang
foreach i [glob [file join  $basedir msgs *$ext]] {
set langue [string map {.msg ""} [file tail $i]]
	if { $langue == "fr" } {
.menu.option.lang add radio -label $langue -variable langue -command "setlang $langue" }  else {
 .menu.option.lang add radio -label $langue -variable langue -command "setlang $langue"
}
}
.menu add cascade -label [mc {Aides}] -menu .menu.aides
menu .menu.aides -tearoff 0
cd $basedir
if { $tcl_platform(platform)  != "unix" } {
.menu.aides add command -label "[mc {Aide }]" -command {exec ./$progaide file:[pwd]/aide.html  & }
} else { 
.menu.aides add command -label "[mc {Aide }]" -command {exec $progaide aide.html  & }
}
##menu .menu.option.repert.ssmenu_perso -tearoff 0
.menu.aides add command  -label "[mc {A_propos}]" -command { source apropos.tcl }
##. configure -menu .menu
set normal "-background {} -relief flat"
##lister les fichiers .lab du r�pertoire de travail

##basedir � changer en rep_travail

##. configure -menu .menu

################
# Image de fond #
#################
###cocher le dossier pris par le bouton !!!
##lance appli laby.tcl   $init_laby   "normal" $rep_travail
##frame . -background blue 
##-height 400 -width 300
##pack .frame -expand true
frame .gauche -background blue -height 320 -width 220
frame .centre -background blue -height 320 -width 220
frame .droite -background blue -height 320 -width 220


##pour lancer depuis les boutons
proc lanceappli1 {type} {
global DOSSIER_EXOS basedir iwish
switch $type {
			"cache"  { lanceappli laby.tcl [ choix_fich $type ] $type [file join $DOSSIER_EXOS pour_cache_ant] 0}

			"normal_ant" { lanceappli laby.tcl [ choix_fich $type ] "normal" [file join $DOSSIER_EXOS pour_cache_ant] 1}
			
			"relatif" { lanceappli laby.tcl [ choix_fich $type ] $type  $DOSSIER_EXOS 0}
			
			"relatif_contr"  {lanceappli laby.tcl [ choix_fich $type ] "relatif"  $DOSSIER_EXOS 1}
				
			"normal" {	lanceappli laby.tcl [ choix_fich $type ] $type  $DOSSIER_EXOS 0 }
			"parcours" {
			set liste_laby {}
			foreach i [lsort [glob -type f -dir [file join $basedir ]  *.lab  ]] {
			set liste_laby [linsert $liste_laby end [file tail $i ] ]
			}
			set compt 0
			set len_liste [llength $liste_laby]
			if { $len_liste == 0} { set compt 2}
			
			destroy .
			
			foreach exo  $liste_laby {
			if { $compt == 1 } {break }
			exec $iwish laby.tcl $exo normal $basedir 0 
			exec $iwish laby.tcl $exo relatif $basedir 0 
			exec $iwish laby.tcl $exo cache $basedir 0 
			exec $iwish laby.tcl $exo normal $basedir 1 
			exec $iwish laby.tcl $exo relatif $basedir 1 
			incr compt
			 
							}
			
				
		exec $iwish labyrinthes.tcl
				}
}
}
proc choix_fich {type} {
global DOSSIER_EXOS
set liste_c ""
switch $type {
		"cache" - "normal_ant" { set repert_choisi [file join $DOSSIER_EXOS pour_cache_ant ]  }
		"relatif"  {set repert_choisi  $DOSSIER_EXOS }
		"relatif_contr" {set repert_choisi  $DOSSIER_EXOS }
		"normal" { set repert_choisi $DOSSIER_EXOS}	
			}
 if { [catch {
foreach i [lsort [glob -type f -dir [file join $repert_choisi ]  *.lab  ]] {
set liste_c  [linsert $liste_c end [file tail $i ] ]
}
}] == 1 } {tk_messageBox -message "[format [mc {R�pertoire %1$s inexistant ou fichier manquant ?}] $repert_choisi ]" -title "[mc {Abandon}]" -icon info -type ok ; exit}

return  [lindex  $liste_c [expr int(rand()*[llength $liste_c]) ] ]	
}


pack .gauche -side left
set image [image create photo -file images/labyrinthes_creer.gif]
button .gauche.labA3 -image $image -text [mc {Cr�er}] -background blue -font {Helvetica 14 normal} -pady 6 -justify left -foreground black -height 128 -width 130
bind .gauche.labA3 <1> {lanceappli laby.tcl aucun normal "[file join  $Home data ]" 0 }
pack .gauche.labA3 -side top -anchor w

set image [image create photo -file images/labyrinthes_normal.gif]
button .gauche.labA1 -image $image -text [mc {Normal}] -background blue -font {Helvetica 14 normal} -pady 6 -justify left -foreground black -height 128 -width 130
pack .gauche.labA1 -side top -anchor w
bind .gauche.labA1 <1> "lanceappli1 normal "

set image [image create photo -file images/labyrinthes_ant.gif]
button .gauche.labA2 -image $image -text [mc {Anticiper}] -background blue -font {Helvetica 14 normal} -pady 6 -justify left -foreground black -height 128 -width 130
pack .gauche.labA2 -side top -anchor w
bind .gauche.labA2 <1> "lanceappli1 normal_ant  "
##invoke ant ???


pack .centre -side left 
set image [image create photo -file images/background1.gif]
label .centre.imagedisplayer -image $image -background blue

set image [image create photo -file images/labyrinthes_parcours.gif]
button .centre.labA1 -image $image -text [mc {Parcours}] -background blue -font {Helvetica 14 normal} -pady 6 -justify left -foreground black -height 128 -width 130
pack .centre.labA1 -side top -anchor w
bind .centre.labA1 <1> "lanceappli1 parcours "

set image [image create photo -file images/sortie.gif]
button .centre.labA2 -image $image -text [mc {Quitter}] -background blue -font {Helvetica 14 normal} -pady 6 -justify left -foreground black -height 120 -width 120
pack .centre.labA1 -side top -anchor center
pack .centre.imagedisplayer -side top  
pack .centre.labA2 -side top -anchor center
bind .centre.labA2 <1> { exit}


pack .droite -side left
set image [image create photo -file images/labyrinthes_cache.gif]
button .droite.labA2 -image $image -text [mc {Labyrinthe Cach�}] -background blue -font {Helvetica 14 normal} -pady 6 -justify left -foreground black -height 128 -width 130
pack .droite.labA2 -side top -anchor center
bind .droite.labA2 <1> "lanceappli1 cache "

set image [image create photo -file images/labyrinthes_relatif.gif]
button .droite.labA3 -image $image -text [mc {Labyrinthe relatif}] -background blue -font {Helvetica 14 normal} -pady 6 -justify left -foreground black -height 128 -width 130
pack .droite.labA3 -side top -anchor center
bind .droite.labA3 <1> "lanceappli1 relatif "
set image [image create photo -file images/labyrinthes_contr.gif]
button .droite.labA4 -image $image -text [mc {Labyrinthe relatif_contr}] -background blue -font {Helvetica 14 normal} -pady 6 -justify left -foreground black -height 128 -width 130
pack .droite.labA4 -side top -anchor center
bind .droite.labA4 <1> "lanceappli1 relatif_contr "


###grid .frame.imagedisplayer -column 1 -row 0 -sticky e
}
############
# Bindings #
############
bind . <Control-q> {exit}
#######choix du repertoire travail et mise � jour listes labyrinthes

##choix_repert "$basedir"  exemple
. configure -menu .menu



proc traite_seq {seq} {
global rep_travail basedir init_laby Home iwish
set f [open [file join $rep_travail $seq ] "r"  ]
set data [ gets $f ] 
close $f
##nom_laby type (r n c) ant 0 ou 1 ou rien cont 0 ou 1 ou rien
destroy .
foreach exo2 $data {
set exo1 [lindex $exo2 0] ; set exo [split $exo1 ":"]
set nom [lindex $exo 0] ; set type [lindex $exo 1] ; set ant [lindex $exo 2 ] ; set contr [lindex $exo 3 ]
if { $type == "n" && $ant == "o" } { set type "n_a" }
if { $type == "r" && $ant == "o" && $contr == "o"} { set type "r_c_a" }
if { $type == "r" && $ant == "n" && $contr == "o" } { set type "r_c" }

switch $type {
		"n" {
##		destroy .
		exec $iwish laby.tcl $nom normal  $rep_travail 0 
		exec $iwish labyrinthes.tcl
		}
		"n_a" {
		exec $iwish laby.tcl $nom "normal" $rep_travail 1
		}

		"r" {
		exec $iwish laby.tcl $nom relatif $rep_travail 0
		}
			
		"r_c_a"  {
		exec $iwish laby.tcl $nom "relatif"  $rep_travail 1
		}
		
		"r_c" {
		exec $iwish laby.tcl $nom "relatif"  $rep_travail 0
		}
		
		"c" {
		exec $iwish laby.tcl $nom cache $rep_travail 0
		}
}

}
exec $iwish labyrinthes.tcl
}
proc lanceappli {appli lab type rep bouton_ant} {
    global iwish
	destroy .
    exec $iwish $appli $lab $type $rep $bouton_ant  
	exec $iwish labyrinthes.tcl
}
proc setlang {lang} {
global env plateforme
set env(LANG) $lang
::msgcat::mclocale $lang
::msgcat::mcload [file join [file dirname [info script]] msgs]
##interface
}
set langue "fr"
setlang fr
interface
##traite_laby $init_laby
choix_repert $rep_travail commun
set choix_rep $rep_travail
##.menu.choix  add radio -label  "aucun"  -variable laby -command "traite_laby  aucun "
##choix_repert [ file  join $rep_travail ] Commun
#traite_laby $init_laby
#traite_laby aucun ; ##pour que au d�marrage de labyrinthes aucun actif sinon pb de r�pertoire
##interface
##bind . <FocusIn> {choix_repert $rep_travail Commun }
##.menu.choix invoke [.menu.choix index $init_laby] 
#modifier wish pour PC windaube








