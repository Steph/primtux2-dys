#pico.py
# -*- coding: utf-8
#A part of NonVisual Desktop Access (NVDA)
#Copyright (C) 2010 Aleksey Sadovoy <lex@progger.ru>
#This file is covered by the GNU General Public License.
#See the file COPYING for more details.

import os
import sys
import codecs
import commands
import Tkinter as tk
import tkFileDialog
import shutil

import speechDictHandler    

BASE_PATH = os.path.abspath(os.path.dirname(sys.argv[0]))
os.chdir(BASE_PATH)
pf = open(os.path.join(BASE_PATH,"../config.txt"), 'r')
voice_file =pf.readline().strip('\r\n').decode('iso-8859-1').encode('utf-8','replace')
rate=pf.readline().strip('\n')
pf.flush()
pf.close()


pf = open(os.path.join(BASE_PATH,"../speak.txt"), 'r')
data =pf.read().decode('iso-8859-1').encode('utf-8','replace')
pf.flush()
pf.close()
#data=data.replace(chr(39),"")


if __name__ == "__main__":
	#args = [unicode(x, "iso-8859-1") for x in sys.argv]
	args = sys.argv[1:]

speechDictHandler.initialize(voice_file)
data = speechDictHandler.processText(data)
data = '"%s"'%(data)
commandline ="pico2wave -l '%s' -w temp.wav %s"%(voice_file,data)
os.popen(commandline)
os.popen("aplay temp.wav")

if len(sys.argv[1:])>=1 and args[0] =='1' :
	inputfile = "temp.wav"
	outputfile = "temp.mp3"
	commandline = "lame -h %s %s"%(inputfile,outputfile)
	os.popen(commandline)
	root = tk.Tk()
	root.withdraw()
	destfile=""
	homedir = os.path.expanduser('~')
	destfile = tkFileDialog.asksaveasfilename(filetypes = [('MP3','*.mp3')], initialfile='extrait.mp3' ,title='Enregistrer sous', defaultextension='.mp3', initialdir=homedir)
	sourcefile = outputfile
	if destfile != "":
		shutil.copy2(sourcefile, destfile)







