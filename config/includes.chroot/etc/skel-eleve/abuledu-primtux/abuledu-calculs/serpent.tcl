#!/bin/sh
#calcul_sur_bande.tcl
#\
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#**************************************************************************
#  File  : $$$
#  Author  : jlsendral@free.fr
#  Date    : 01/02/2005
#  Licence : GNU/GPL Version 2 ou plus
#
#  Description:
#  ------------
#
#  @version 0.1
#  @author     Jean-Louis Sendral. 
#  @project    Le terrier
#  @copyright  Eric Seigne 24/04/2002
#
#  ##############################################################
source bande.conf ; ##calculs.conf ???
source msg.tcl
source path.tcl
set  basedir [pwd]
######################Recceuil des parametres dans le fichier .ban ##########################
## A deux ou contre ordi, qui ? l'un est le connect� l'autre ? li ? col ? zone d�part (pas),
## limite des pi�ges(nbre , espacement) � mettre dans des toplevel
##tests
##set pas 5 ; set nom_autre "jean"; ###ou ordi contrainte 1 pour nbre de pieges 2 pour ecart , 3 pour dist au d�part > 3*$pas, 4 aucune.

#############Tracer le trame du quadrillage#########
########################################
proc fais_grille { } {
global init tab pas
 for {set y 0} {$y <=  $init(nbrow) } {incr y} {
		 for {set x 0} {$x < $init(nbcol) } {incr x} {
		 set ne_x  [expr $x * $init(wlen) ] ; set ne_y [expr $y * $init(hlen) ]
		set tab(rect${y}_${x})	[ .frame_middle.can create rectangle  $ne_x $ne_y   [expr $ne_x + $init(wlen)]  [expr $ne_y  +$init(hlen) ]  -tags rect${y}_${x} -outline red]
						 }
 }
}
###Pour  choisir en premier de poser les pi�ges ou le d�part et ainsi 
##pouvoir lancer l'activit� par poser_pieges ou poser_depart
#############################################################################
proc choix {pose} {
global premier nom_autre cont pas
 .frame_middle.left.liste_pieges configure -state normal
 .frame_middle.right.liste_pieges configure -state normal
switch $pose {
			pose_piege {
			.frame_middle.left.liste_pieges  configure -state normal
			 .frame_middle.left.nom_pp configure  -text "[format [mc {Le poseur des pi�ges est : %1$s.Liste des pi�ges:}] $premier ]" 
			 .frame_middle.right.nom_sa  configure -text "[format [mc {Le choix du d�part est fait par %1$s.D�part:}] $nom_autre ]" -background red
			.frame_middle.right.liste_pieges  configure -state disabled
			.frame_up.pd configure -state disabled
			.frame_up.pp configure -state disabled
			focus .frame_middle.left.liste_pieges
			label .frame_middle.left.valide -text "[mc {liste � valider}]"
			 place  .frame_middle.left.valide -x 15 -y 115
			  set cont(1)  "nombre de pieges( <=  $pas)"  
			if { $premier != "ordinateur" }  {
			  set cont(1)  "nbre de pieges(<=  $pas)" 
			.frame_up.cont1 configure -text $cont(1)
								}
						}
			pose_depart {
			.frame_middle.right.liste_pieges  configure -state normal
			 .frame_middle.left.nom_pp configure  -text "[format [mc {Le poseur des pi�ges est : %1$s.Liste des pi�ges:}] $nom_autre ]" 
			.frame_middle.right.nom_sa  configure  -text "[format [mc {Le choix du d�part est fait par %1$s.D�part:}] ${premier} ]" 
			.frame_middle.left.liste_pieges   configure -state disabled			
			focus .frame_middle.right.liste_pieges
			label .frame_middle.right.valide -text "[mc {d�part � valider}]"
			place  .frame_middle.right.valide -x 15 -y 115
			.frame_up.pp configure -state disabled
			.frame_up.pd configure -state disabled
			if { $premier != "ordinateur" } {
			set cont(1)  "nbre de pieges(>=[expr $pas /2])"
			 .frame_up.cont1 configure -text $cont(1) }
			}
					
			}
##bind  .frame_middle.left.liste_pieges  <Return> " poser_pieges [  .frame_middle.left.liste_pieges get] "
##bind .frame_middle.left.liste_pieges  <KP_Enter> "poser_pieges [  .frame_middle.left.liste_pieges get]  "
##bind  .frame_middle.right.liste_pieges  <Return> "choix_depart  "   
##bind .frame_middle.right.liste_pieges  <KP_Enter> "choix_depart   [  .frame_middle.right.liste_pieges get] "
			
					}

####Pour Recommencer tout (noms, ..) ou autre_essai  (essais limit�s) avec les m�mes donn�es de d�part#####
#########################################################################					
proc recommencer {} {
global nom_autre tab_cont
###remettre � noir depart pieges enlever oh ou bien,vider listes; recommencer disabled, destroy les sauter
## ne conserve pas ordinateur qd on clique sur poser ..
##pour ordi nom_autre  � ordinaateur
catch {destroy .frame_middle.right.lancer_a  .frame_middle.right.lancer  .frame_middle.left.lancer_a  .frame_middle.left.lancer_a}
catch {destroy .frame_middle.right.valide .frame_middle.left.valide}
.frame_middle.right.liste_pieges delete 0 end ;.frame_middle.left.liste_pieges delete 0 end ;
  .frame_up.encore configure -state normal
### set .. &;destroy
  catch { destroy .frame .frame_up .frame_up .frame_middle .frame_down .frame_bottom}
if { $nom_autre == "ordinateur"} { set nom_autre "ordinateur"}
 
for {set but 1} {$but <=3 } { incr but } {
set  tab_cont($but) 0 
	      }
 main
  }
proc autre_essai {} {
global choix depart pas arrivee tab_n max_essais nbre_essais  liste_pieges font_case max_n
##set reste_essais [expr $max_essais - $nbre_essais ]
##.frame_middle.right.re_essai configure  -text "Autre Essai ?\n  Plus que $reste_essais essai(s)."
.frame_middle.can delete  img
if { $nbre_essais == $max_essais} {
tk_messageBox -message "[format [mc {Trop d'essais: %1$s Tu dois anticiper tes choix.Tu peux recommencer}] $nbre_essais]" -title "[mc {Message Nombre essais}]" -icon info -type ok
return
}
incr nbre_essais
for {set i $depart } { $i <= [expr $arrivee + $pas] && $i <= $max_n } { incr i $pas } {
				.frame_middle.can  itemconfigure $tab_n(text_$i)  -fill black -font $font_case
				}
.frame_middle.can  itemconfigure $tab_n(text_${arrivee})  -fill black -text $arrivee  -font $font_case
		foreach i [ .frame_middle.left.liste_pieges get ]  {
		.frame_middle.can  itemconfigure $tab_n(text_$i)  -fill black -font $font_case
			}	
catch {destroy .frame_middle.right.lancer_a  .frame_middle.right.lancer  .frame_middle.left.lancer_a  .frame_middle.left.lancer}
catch {destroy .frame_middle.right.valide .frame_middle.left.valide   .frame_middle.right.bond_t .frame_middle.right.bond}
.frame_middle.right.liste_pieges delete 0 end ;.frame_middle.left.liste_pieges delete 0 end ;
  .frame_up.encore configure -state normal
.frame_up.pp configure -state normal 
.frame_up.pd configure -state normal 
.frame_middle.right.re_essai  configure -state disabled
bind  .frame_middle.left.liste_pieges  <Return> " poser_pieges"
bind .frame_middle.left.liste_pieges  <KP_Enter> "poser_pieges "
bind  .frame_middle.right.liste_pieges  <Return> "choix_depart  "   
bind .frame_middle.right.liste_pieges  <KP_Enter> "choix_depart "
##mette � noir les atteints  depart pas arrivee

switch $choix {
		pose_piege {
		.frame_up.pp invoke
		.frame_middle.left.liste_pieges configure -state normal
		 .frame_middle.right.liste_pieges configure -state normal
		 .frame_middle.left.liste_pieges delete 0 end
		.frame_middle.left.liste_pieges insert end $liste_pieges ; ##les redessinner
		.frame_middle.left.liste_pieges configure -state disabled
		poser_pieges
		 focus -force .frame_middle.right.liste_pieges 
		
		}
		pose_depart {
		.frame_middle.right.liste_pieges  configure -state normal
		.frame_up.pd invoke ; .frame_middle.right.liste_pieges delete 0 end
		.frame_middle.right.liste_pieges 	insert end $depart ; ###effacer � valider et mettre pieges � valider
	 .frame_middle.left.liste_pieges configure -state normal ; choix_depart
		focus -force .frame_middle.left.liste_pieges 
		}
}
return
}
  
 ####Por savoir qui commencera (le premier) : nom de l'invit�. Choix du pas. ########"#
 ##########################################################
  proc init_qui_deb_pas { } {
  global nom_autre premier pas nom_elev w2 argv
proc retiens_nom { } {
global nom_autre  w2
set  nom_autre  [${w2}.nom_autre get ]
if { $nom_autre == {}} {
${w2}.nom_autre  delete 0 end
focus ${w2}.nom_autre
} else {
bind ${w2}.nom_autre <Return> ""
bind ${w2}.nom_autre <KP_Enter> ""
${w2}.nom_autre configure -state disabled ; update
set nom_autre  $nom_autre ; retiens_prem
}
}
proc retiens_pas { } {
global nom_elev nom_autre  choix_prem pas w2 premier

if { [ ${w2}.choix_pas  get] == {}  || ![ regexp {((^[1-9][0-9]{0,1}$)|(^[sS]{1}[1-9]{1}$))} [ ${w2}.choix_pas  get]  ] } { 
tk_messageBox -message "[mc {Pas non conforme}]" -title "[mc {Message Contrainte donn�es}]" -icon info -type ok
 ${w2}.choix_pas delete 0 end ; focus -force ${w2}.choix_pas ; return} else {
if { $nom_autre != "ordinateur" } {
		if { [regexp {^[1-9][0-9]{0,1}$} [ ${w2}.choix_pas  get] ]  } {
			if {  [${w2}.choix_pas  get]  > 10 } {set pas 6} else { set pas [ ${w2}.choix_pas  get]  }
						}  else {  set pas [ ${w2}.choix_pas  get]  }
			
						}  else {
			if { [regexp {^[sS]{1}[1-9]{1}$} [ ${w2}.choix_pas  get] ]  } { set pas 5 }  else { set pas [ ${w2}.choix_pas  get]  }
			}
##set retour [ list $nom_autre $premier $pas $choix_prem  ]
##return $retour
 destroy ${w2}
}
}

proc retiens_premier {} {
global nom_elev nom_autre w2 choix_prem pas premier
set premier $choix_prem
 ${w2}.elev configure -state disabled
 ${w2}.autre configure -state disabled
 if { $nom_autre !=  "ordinateur" } {
		label ${w2}.pas  -text "[mc {Choisir le pas (<= 10)Ou une situation pr�d�finie(s1, ..s9)}]" -font {arial 11} 
					} else {
				label ${w2}.pas  -text "[mc {Choix du pas (<= 10)}]" -font {arial 11} 	
					}
place ${w2}.pas -x 5 -y 170
entry ${w2}.choix_pas -width 2 -justify center
place ${w2}.choix_pas -x 240 -y 175
focus -force  ${w2}.choix_pas 
##set pas ""
###set pas [ ${w2}.choix_pas  get]
bind ${w2}.choix_pas <Return> "retiens_pas"
bind ${w2}.choix_pas <KP_Enter> "retiens_pas"
##destroy ${w2}
}
proc retiens_prem {} {
global nom_elev nom_autre w2 choix_prem premier
label ${w2}.text_prem -text "[format [mc {Choisir le nom du premier � agir: %1$s ou %2$s}]  $nom_elev ${nom_autre}].\n[mc {Il choisira de saisir le d�part ou les pi�ges.}]" -font {arial 11} 
place ${w2}.text_prem -x 10 -y 100 
radiobutton ${w2}.elev -text  "choisir $nom_elev" -variable choix_prem  -value $nom_elev -foreground red -command  "set premier $nom_elev ;  retiens_premier"
radiobutton ${w2}.autre -text  "choisir $nom_autre" -variable choix_prem  -value $nom_autre -foreground  red -command "set premier $nom_autre ;  retiens_premier"
place ${w2}.elev -x  60     -y 140
place ${w2}.autre  -x  210    -y 140
if { $nom_autre == "ordinateur" } { ${w2}.autre invoke}
}
 catch {destroy .top2 }
set w2 [toplevel .top2]
wm geometry $w2 380x250+0+0 
wm resizable $w2 no no
wm title $w2 "[mc {Choix de l'autre eleve,  du premier, du pas}]"
 
 label ${w2}.text_autre -text "[mc {Saisir le nom  de l'autre �l�ve.Vous pouvez Saisir ordinateur pour travailler avec lui.}]\
		  \n[mc {Penser � valider toutes les r�ponses par Entr�e}]" -font {arial 11}
place ${w2}.text_autre -x 10 -y 10 
entry ${w2}.nom_autre -width 20 -font {arial 10} -justify center
place ${w2}.nom_autre -x 100 -y 70
focus ${w2}.nom_autre
bind ${w2}.nom_autre <Return> "retiens_nom"
bind ${w2}.nom_autre <KP_Enter> "retiens_nom "
if { [lindex $argv 1] == 1 } {
set nom_autre "ordinateur" ; set choix_prem $nom_autre ; ${w2}.nom_autre insert end $nom_autre ; retiens_prem  ; set pas 5 ;\
 ${w2}.choix_pas insert end $pas ; retiens_pas ; return
}

 ##grab ${w2}
raise  ${w2}  .
  } 
  ##fin init_qui_deb_pas
 ################################################################# 
  
 ############Programme principal 
  proc main {} {
global init tcl_platform choix pas tab tab_n premier nom_autre nom_elev choix_prem param w2 max_essais max_n nbre_essais max_nombre
global font_piege font_case progaide predef pas_back cont basedir tab_nxy
if { $tcl_platform(platform) == "unix" } {
set nom_elev $tcl_platform(user)
set nom_classe [lindex [exec  id -G -n ] 1]
set font_mess1 {helvetica 8 bold} ;  set font_case_jeu {helvetica 18 bold}
set font_case {helvetica 14 bold} ; set font_piege { helvetica 18 bold}
set font_mess2 {helvetica 10 bold}
} else {
 set nom_elev eleve
 set nom_classe classe
 set font_case {helvetica 22} ; set font_piege { helvetica 30 bold}
 set font_mess1 {helvetica 10 bold}
}
##max pour pi�ges
##set max_nombre 89
##set nom_autre ""
initlog $tcl_platform(platform) $nom_elev
### $nom_autre $premier $pas $choix_prem]nom_elev nom_autre  choix_prem pas w2 premier
 init_qui_deb_pas ; catch { tkwait window ${w2} } ; ##intercepter pas pour les standards dans predef
if { [regexp {^\d+$} $pas ] } { set pas $pas ; set pas_back 5} else {set pas_back $pas ; set pas [lindex $predef([ string index $pas 1 ]) 0]  }
if { $premier == $nom_elev } { set nom_autre $nom_autre } else { set  nom_autre $nom_elev }
###contre l'ordinateur n'a de sens que si c'est l'ordi qui choissit en premier.
##if  $nom_autre == "ordinateur"  
## set premier ordinateur ; set nom_autre $nom_elev
  ##set contraint_npiege 1 ; set contraint_espacement 1 ; ###ou 2 (pas plus de 2 cons�cutifs) 3 (pas de contr)
  set max_essais [expr $pas / 2] ; set nbre_essais 1
 if { [expr $init(nbrow)  % 2] !=0 } {set mult [expr [expr $init(nbrow) / 2] +1] ; set reste [expr $init(nbrow) / 2] }  else  {
							set mult [expr $init(nbrow) / 2]  ;  set reste [expr [expr $init(nbrow) / 2]  - 1] }
set max_n [  expr [ expr $mult * $init(nbcol) ] +  $reste  ]
 frame .frame
##toplevel .frame -background grey
. configure -width [ expr $init(width)+ 50 ] -height $init(height)
wm title . "Calculs sur  bande. Vers la division euclidienne.(Cf Ermel Cycle 2)"
wm geometry  . +0+0  
wm resizable . 1 1
catch {destroy .frame_up .frame_up  .frame_middle .frame_down}
##set init(nbrow) 12 ; set init(nbcol) 14
set largeur [expr $init(width) +50 ] ; set hauteur  $init(height)
set init(wlen)   [ expr [expr $largeur - 290]  /  $init(nbcol) ] 
set init(hlen)  [ expr [expr $hauteur - 160]  /  [expr $init(nbrow)  + 1]] 
#if { $init(wlen)   <=  $init(hlen) }  { set  init(hlen)  $init(wlen) } else {  set init(wlen) $init(hlen) }
#set init(wlen) [expr $init(wlen) + 1 ] ; ###pour adapter taille quadr
frame .frame_up  -borderwidth 2 -height 90 -width $largeur -background blue 
##frame .frame_up1  -borderwidth 2 -height 20 -width [expr $largeur -100 -100] -background blue
label .frame_up.nom -text "[mc {Le choix de}] $premier :" -bg bisque -font {helvetica 10 bold}
radiobutton .frame_up.pp -text  "[mc {poser les  pi�ges}]" -variable choix  -value pose_piege -foreground red -command  "choix pose_piege"
radiobutton .frame_up.pd -text  "[mc {choisir le  d�part}]" -variable choix  -value pose_depart -foreground  red -command "choix pose_depart"
label .frame_up.pas -text "[mc {Les sauts seront de :}] $pas"  -bg bisque  -font {helvetica 14 bold}
button .frame_up.quit -text "[mc {Quitter}]" -command exit
frame .frame_middle -width  $largeur  -height [expr $hauteur -90 - 70] 
##frame .frame_bottom -width 400 -height 50 -background black
frame .frame_middle.left -width [expr 100 + 30] -height [expr $hauteur -90 - 70]  ;##-background blue
 label .frame_middle.left.nom_pp  -text "[mc {Poseur des pi�ges(ou)Liste des pi�ges}]" -background bisque -font $font_mess1
entry .frame_middle.left.liste_pieges -width 25 -bd 3 -highlightcolor red -highlightthickness 3 -font $font_mess1; ###70=30+40 90 -90 - 70-40
canvas .frame_middle.can  -height [expr $hauteur -90 - 70] -width [expr $largeur - 200] -background orange 
frame .frame_middle.right -width [expr 100 +40+60]  -height [expr $hauteur -90 - 70] -background grey
label .frame_middle.right.nom_sa  -text "[mc {Le choix du d�part est fait par (ou)Liste des cases atteintes  :}]" -background bisque -justify center -font $font_mess1
entry .frame_middle.right.liste_pieges -width 25 -bd 3 -highlightcolor red -highlightthickness 3 -font $font_mess1 -xscrollcommand ".frame_middle.right.scroll1 set"
scrollbar .frame_middle.right.scroll1 -command ".frame_middle.right.liste_pieges xview" -orient horizontal
.frame_middle.left.liste_pieges configure -state disabled
 .frame_middle.right.liste_pieges configure -state disabled
button .frame_middle.right.re_essai -text "[format [mc {Autre Essai ? Au plus %1$s}] $max_essais ]" -command autre_essai
frame .frame_down  -height 70 -background bisque1 
message .frame_down.message -text "[format [mc {Travail :Il s'agit de se d�placer par bond de %1$s � partir d'un d�part}] $pas ]\
 [format [mc {et ce �vitant des pi�ges.D�part et pi�ges choisis par %1$s et %2$s}] $premier $nom_autre]" -font {helvetica 11} -aspect 500 -bg bisque
label .frame_down.pour_avis -text  "[mc {Remarques :}]"
text .frame_down.avis -relief sunken -height 4 -width 50
.frame_down.avis insert end  "[mc {Pourquoi es-tu s�r de ton choix de pi�ges ou de d�part :}]\n"

place  .frame_up  -x 0 -y 0
place .frame_middle  -x 0 -y 90
place .frame_down  -x 0 -y  [expr $hauteur - 70]
if { $tcl_platform(platform) != "unix" } {
place .frame_middle.right.re_essai  -x 20 -y 250
} else {
place .frame_middle.right.re_essai  -x 20 -y 250

}
.frame_middle.right.re_essai configure  -state disabled
##checkbutton  .frame_up.cont0 -variable tab_cont(0) -text "cont(0)"
##grid    .frame_up.nom -row 0 -column 0 ; grid .frame_up.pp -row 0 -column 1
##grid .frame_up.pd  -row 0 -column 2 ; grid   .frame_up.pas .frame_up.quit   -row 0 -column 3
##pack .frame_up -side top
label .frame_up.choix  -text "[mc {Contraintes sur les pi�ges (la 1�re d�pend du choix de d�part):}]" -bg bisque -font {helvetica 10 bold}
place  .frame_up.choix -x 0 -y 30
place .frame_up.nom -x 0 -y 0
place   .frame_up.pp  -x 190  -y  0
place  .frame_up.pd     -x 320 -y  0
place .frame_up.pas     -x 460 -y 0
place  .frame_up.quit     -x [expr $largeur -80] -y 30 
place .frame_middle.left  -x  0 -y 0
place  .frame_middle.can  -x [expr 100 +30]  -y 0
place  .frame_middle.right   -x [expr   $largeur - 100 - 40 - 25 ]   -y 0
place   .frame_middle.left.nom_pp -x 0 -y 10
place .frame_middle.left.liste_pieges   -x 0 -y 65
place .frame_middle.right.nom_sa   -x 0 -y 10
place .frame_middle.right.liste_pieges  -x 0  -y 65
place .frame_middle.right.scroll1 -x 0 -y 90 -width 170
if {$premier == "ordinateur" }  {
 set has [expr int(2*rand())] 
 set depart [expr int(${pas}*rand())] 
   if { $has == 0} {set choix "pose_depart" ; .frame_up.pd  invoke} else { set choix "pose_piege" ;  .frame_up.pp invoke} 
  }  else  {
   set choix "" }
if {$premier == "ordinateur" && $choix == "pose_depart" } {
 set depart [expr int(${pas}*rand())] ; .frame_middle.right.liste_pieges insert end $depart
 focus -force .frame_middle.right.liste_pieges
 } 
if {$premier == "ordinateur" && $choix =="pose_piege" } {
 set n_pieges $pas ; ###set liste_p {} affiner le choix 
 for {set i 1} {$i <= $pas } {incr i} {
 set piege [expr int( $max_n*rand())] ;  ###set $liste_p [linsert $liste_p end " $piege "]
 if { $piege <= $pas }  { set i [expr $i -1] ; continue }
 .frame_middle.left.liste_pieges insert end " $piege"
 }
 focus -force .frame_middle.left.liste_pieges
 } 
 
 set cont(1) "[mc {� venir!}]"
 set cont(2)   "[mc {�cart entre pieges}](> $pas)" ; set cont(3)  "[mc {distance au d�part}](3*$pas)" 
for {set but 1} {$but <=3 } { incr but } {
checkbutton  .frame_up.cont$but -variable  tab_cont($but) -onvalue 1 -text "$cont($but)" -padx 1 -foreground red
place  .frame_up.cont$but  -y  60  -x  [expr  170 * [expr $but -1]  + 0 ]
if { $premier == "ordinateur"  && $choix  == "pose_piege"} {  .frame_up.cont$but configure -state disabled }
  }
 button .frame_up.encore -text "[mc {Recommencer}]" -command "recommencer"
 place .frame_up.encore  -x [expr $largeur -200] -y 30
 .frame_up.encore configure -state disabled  
  button .frame_up.aide -text [mc {Aide}]
switch $tcl_platform(platform)  {
	"windows"  {
     bind .frame_up.aide <1>  "exec $progaide ${basedir}/aide/calculs_sur_bandes.html & "
		}
	"unix" {	bind .frame_up.aide <1> "exec $progaide [pwd]/aide/calculs_sur_bandes.html & "
	        }
	    }

place  .frame_up.aide -x [expr $largeur -60] -y 60
 
pack  .frame_down.message  -side left
pack .frame_down.avis  -side right
## .frame_down.pour_avis -side left
###la bande num�rique
fais_grille
for {set y 0  } {$y < $init(nbrow) } {incr y } {
		set reste [expr $y % 4] ; set quot [expr $y / 4]  ; set m [expr $y / 2] ; set k 0 ; set k_deux 0
		 for {set x 0} {$x < $init(nbcol) } {incr x} {
			switch $reste  { 
				 0 { 
				
				  set i [expr [ expr $m * $init(nbcol) ] + $m + $k ]
				 set ne_x  [expr $x * $init(wlen) ] ; set ne_y [expr $y * $init(hlen) ] 
				set tab_n(text_$i)  [ .frame_middle.can  create text     [expr $ne_x + ($init(wlen) /2)]  [expr $ne_y  +$init(hlen) /2 ]    -text $i -font $font_case  -fill black -tags tag_${i}  ]    
				set tab_nxy($i) [list $x  $y]
				.frame_middle.can  itemconfigure	 $tab(rect${y}_${x})   -fill bisque1 
				 incr k
				  if { $x == [expr $init(nbcol) -1] } {
				  incr i
					set tab_n(text_$i)  [ .frame_middle.can  create text     [expr $ne_x + ($init(wlen) /2)]  [expr $ne_y  +$init(hlen) /2  +$init(hlen)]    -text $i -font $font_case -fill black   -tags tag_${i}  ]   
					set tab_nxy($i) [list $x  $y]
					incr y 
				
				.frame_middle.can  itemconfigure	 $tab(rect${y}_${x})   -fill bisque1
				set k 0 
				}
				  }
				 
				 2 { 			 
				 set j  [expr [ expr $m * $init(nbcol) ] + $m  +$init(nbcol) - 1 -  $k_deux ]
				   set ne_x  [expr $x * $init(wlen) ] ; set ne_y [expr $y * $init(hlen) ] 
				set tab_n(text_$j)  [ .frame_middle.can  create text     [expr $ne_x + ($init(wlen) /2)]  [expr $ne_y  +$init(hlen) /2 ]    -text $j -font $font_case -fill black -tags tag_${j}  ]    
				 .frame_middle.can  itemconfigure	 $tab(rect${y}_${x})   -fill bisque1
				 set tab_nxy($j) [list $x  $y]
				 incr k_deux
				 if { $x == 0 } {
				  incr j
				set tab_n(text_$j)  [ .frame_middle.can  create text     [expr $ne_x + ($init(wlen) /2)]  [expr $ne_y  +$init(hlen) /2  +$init(hlen)]    -text $j -font $font_case -fill black -tags tag_${j}  ]   
				set z [expr $y +1] 
				set tab_nxy($j) [list $x  $y]
				.frame_middle.can  itemconfigure	 $tab(rect${z}_${x})  -fill bisque1
				set k_deux 1 
				}
				 
				 
				 }
				 default {}
				 }
		 
		 
		 
		 }
 
}
#if { $i >= $j }  { set dernier_n [ expr $init(nbcol) + $i] } else {set dernier_n [ expr $init(nbcol) +$j] }
zone_depart $pas
###recup du standart ppiege ou choix d�part pour contraintes : set tab_cont($but) lindex $predef(i) 2 3 4
if {$premier != "ordinateur" &&  ![regexp {^\d+$} $pas_back ]  } { 

##set pas_back "s8"
##.frame_down.avis insert end $pas_back
set liste_contr {} ; set liste1 {} ; set liste2 {} 
.frame_up.[ lindex $predef([ string index $pas_back 1 ])  1]  invoke 
set tab_cont(1) [lindex   $predef([ string index $pas_back 1 ])       2]
set tab_cont(2) [lindex   $predef([ string index $pas_back 1 ])       3]
set tab_cont(3) [lindex   $predef([ string index $pas_back 1 ])       4]
if { $tab_cont(1) == 1} { set liste1 [ linsert  $liste_contr end  [list $cont(1)]  ]  ; .frame_up.cont1 invoke ;  set liste_contr $liste1}
if { $tab_cont(2) == 1}	{	 .frame_up.cont2   invoke  ; set liste2 [ linsert $liste1 end [list $cont(2)]] ; set liste_contr $liste2}
if { $tab_cont(3) == 1}	{ .frame_up.cont3 invoke ; set liste_contr [ linsert $liste2 end [list $cont(3)]]   }
 ##.frame_middle.left.liste_pieges insert end $tab_cont(2)
 set message_p ""
 for {set i 0} {$i <  [llength $liste_contr]} { incr i} {
	 set message_p  "$message_p[lindex $liste_contr $i]\n"
 }
if { $message_p  == "" } { set message_p "[mc {Pas de contrainte}]"}
 ##debug
 ##.frame_middle.left.liste_pieges insert end $message_p
tk_messageBox -message "[format [mc {Donn�es  standard de %1$s :Pas: %2$s Contrainte(s) pi�ge(s): %3$s}] $pas_back $pas $message_p]" -title "[mc {Message Contrainte pi�ges}]" -icon info -type ok
 }
### $predef([ string index $pas 1 ])  1] 

##.frame_up.pp invoke
bind  .frame_middle.left.liste_pieges  <Return> " poser_pieges"
bind .frame_middle.left.liste_pieges  <KP_Enter> "poser_pieges "
bind  .frame_middle.right.liste_pieges  <Return> "choix_depart  "   
bind .frame_middle.right.liste_pieges  <KP_Enter> "choix_depart "
## pack .frame_middle.left.valide
 ##pack  .frame_middle.right.valide
## .frame_middle.can create rectangle  0 0 20  20 -outline red
 ##pack .frame_middle -fill both -side left
##pack .frame_right -fill both -side left
##pack .frame_down -fill both -anchor w -side bottom
} ; ##fin main
############################################
###########Marquer la zone desd�parts possibles########
###########################################
proc zone_depart { pas} {
global tab
for {set k 0} { $k < $pas} { incr k } {
.frame_middle.can  itemconfigure	 $tab(rect0_${k})   -fill  brown
}
}
##set cont(1)  nbre_pieges ; set cont(2)   "�cart" ; set cont(3)  "dist d�part" ; set cont(4) "aucune"
#########traiter contraintes auxilliaire#######
proc traite_cont2 {liste} {
global pas
for {set ind 0} { $ind <=  [ expr [llength $liste ] -2 ] } { incr ind } {
			if { [expr [lindex $liste [expr $ind + 1]] -  [lindex $liste $ind] ]  <  $pas }  {
			return 1 ; break
			}
			}
		return 0
			}
####Traiter les contraintes sur la liste des pi�ges#################
###################################################

proc traite_contrainte { liste } {
global nom_autre premier tab_cont pas depart liste_inter choix max_nombre max_n
##controler les elements pieges : nbrs trop grands, pas nombre.
foreach i $liste  { 
				if { $i > $max_n ||  ![regexp {(^[0-9]+$)} $i ]  } { 
							set a_effacer 1
				tk_messageBox -message "[format [mc {Nombre > %1$s ou non conforme}] $max_n ]" -title "[mc {Message Contrainte}]" -icon info -type ok		
					return 1		}
								}

set liste_inter [lsort -integer $liste] ; set a_effacer 0


if { $tab_cont(1)  == 1  &&  $choix == "pose_depart" && [llength $liste ] <  [expr $pas  / 2] } {
tk_messageBox -message "[mc {Contrainte non respect�e:Pas assez de pi�ges !!!Retaper les nombres}]" -title "[mc {Message Contrainte}]" -icon info -type ok
set a_effacer 1 
}

if { $liste == {}  ||  [lindex $liste 0]   < $pas  } {
 tk_messageBox -message "[mc {Contrainte non respect�e Liste vide ou Premier pi�ge mal plac� Retaper les nombres}]" -title "[mc {Message Contrainte}]" -icon info -type ok
set a_effacer 1 }
## $premier == "ordinateur" && $choix == "pose_piege" 
 ##set a_effacer 1

if { $tab_cont(1)  == 1 && [ llength $liste]  >  $pas  && $choix == "pose_piege"} {
			set a_effacer 1
	 tk_messageBox -message "[mc {Contrainte non respect�e :Trop de  pi�ges.Retaper les nombres}]" -title "[mc {Message Contrainte}]" -icon info -type ok
 set a_effacer 1		
				}
	
if { $tab_cont(2) == 1 &&  [traite_cont2 $liste_inter] }  {
	 tk_messageBox -message "[mc {Contrainte non respect�e Pi�ges trop proches !Retaper les nombres}]" -title "[mc {Message Contrainte}]" -icon info -type ok	
 		set a_effacer 1
				}
if { $choix == "pose_depart"	} { set dep $depart} else { set dep $pas}
if { $tab_cont(3)  == 1    &&  [ expr [lindex $liste 0] - $dep ]  <= [expr 3 *$pas]  } {
 tk_messageBox -message "[mc {Contrainte non respect�e:Premier pi�ge trop pr�s du d�part Retaper les nombres}]" -title "[mc {Message Contrainte}]" -icon info -type ok
			set a_effacer 1
				}
if { $a_effacer == 1} { return 1 } else { return $liste_inter }

}
###########Contr�le de la liste des pi�ges en  premier ou deuxi�me tenant compte des contraintes#############
##########################################################
proc poser_pieges {} {
global tab_n premier choix font_piege
set res [traite_contrainte [.frame_middle.left.liste_pieges get ]]
if {$res  == 1 } {
 ##tk_messageBox -message "[mc {Contrainte non respect�e\n Retaper les nombres}]" -title "[mc {Message Contrainte}]" -icon info -type ok 
.frame_middle.left.liste_pieges delete 0 end 
return
  } else { set liste_traitee $res}
foreach i $liste_traitee  {
				.frame_middle.can  itemconfigure $tab_n(text_$i)  -fill red -font $font_piege
				}
				##set choix  pose_pieges
	 switch  $choix {
###controle entree � faire
		 pose_piege {
				.frame_middle.right.liste_pieges configure  -state normal 
				focus -force .frame_middle.right.liste_pieges
				.frame_middle.left.liste_pieges configure  -state disabled
				label .frame_middle.right.valide -text "[mc {d�part � valider}]"
				place  .frame_middle.right.valide  -x 10  -y 115
				 catch {destroy .frame_middle.left.valide }
				}
								
				pose_depart {
				catch {destroy .frame_middle.left.valide }
				 button .frame_middle.left.lancer_a  -text   "[mc {Sauter seul}]"    -command "sauter_a" -bg green
				place  .frame_middle.left.lancer_a -x 20  -y 130
				 button .frame_middle.left.lancer -text  "[mc {Sauter par bond}]" -command "sauter" -bg green
				place .frame_middle.left.lancer  -x 10 -y 170
				}
				}


 
}
#########Contr�le du choix du d�part en premier ou second choix.##################
##############################################################
proc choix_depart { } {
 global tab_n depart premier choix pas   font_piege 
 ###controle entree � faire mettre un drapeau d�part canvas coords
 ###catch destroy .frame_middle.left.valide
 set depart  [lindex [.frame_middle.right.liste_pieges get ] 0]
 if { ![regexp {^\d+$} $depart] || $depart >=  $pas || $depart == {} } {
 .frame_middle.right.liste_pieges delete 0 end     
 tk_messageBox -message "[format [mc {Le d�part %1$s ne convient pas car : %1$s >= %2$s ou n'est pas donn�}] $depart $pas ]" -title "[mc {Message d�part}]" -icon info -type ok 
 return}
  .frame_middle.can  itemconfigure $tab_n(text_$depart)  -fill blue -font $font_piege
  ## clignote  $tab_n(text_$depart) 1
 ##sauter si en second (choix = pp) sinon passer � liste  pieges
 switch  $choix {
			 pose_piege {
			 catch {destroy .frame_middle.right.valide }
			  .frame_middle.right.nom_sa configure -text "\n\n [mc {Cases atteintes :}]"
			  ##Cases atteintes
			 button .frame_middle.right.lancer_a -text    "[mc {Sauter seul}]"    -command  " sauter_a" -bg green
			place .frame_middle.right.lancer_a -x 20 -y 130
			button .frame_middle.right.lancer -text  "[mc {Sauter par bond}]" -command  "sauter" -bg green
			place .frame_middle.right.lancer -x 10  -y 160
			
			 }
			 pose_depart {
			  .frame_middle.right.nom_sa configure -text "\n\n[mc {Cases atteintes :}]"
			 .frame_middle.left.liste_pieges configure  -state normal
			 focus -force .frame_middle.left.liste_pieges
			 label .frame_middle.left.valide -text "[mc {liste pi�ges � valider}]"
				place  .frame_middle.left.valide -x 0 -y 115
			.frame_middle.right.liste_pieges configure  -state disabled	
				 catch {destroy .frame_middle.right.valide }
			 }
 }
 
  }
proc bind_inactif { } {
 bind  .frame_middle.left.liste_pieges  <Return> {}
bind .frame_middle.left.liste_pieges  <KP_Enter> {}
##.frame_middle.left.liste_pieges configure -state disabled
bind  .frame_middle.right.liste_pieges  <Return> {}  
bind .frame_middle.right.liste_pieges  <KP_Enter> {}
}
############Pour activer les sauts en automatique ou control�,  � partir du d�part et terminer l'activit� :
#### tomber dans un pi�ge ou les �viter : ##################
############################################

  proc sauter_a  { args}  {
  global tab_n pas depart premier nom_autre choix neo_depart arrivee max_essais nbre_essais 
 global  liste_pieges font_piege max_n basedir init tab_nxy
   ##debug  .frame_middle.right.liste_pieges insert end $args
  catch {.frame_middle.left.lancer configure -state disabled}
  catch {.frame_middle.right.lancer configure -state disabled}
  set  liste_ordonnee [ lsort -integer [ .frame_middle.left.liste_pieges get ]  ] ; set liste_pieges $liste_ordonnee
 if { $args == {} } {set saut_faits 0 ; set nbre_sauts 1
 set prem_saut [expr $depart + $pas] ; set inter $prem_saut  
 } else {
 set inter [expr [ lindex [lindex $args 0] 1]+ $pas]  ; set nbre_sauts [expr [lindex [lindex $args 0]  0] ] ; set saut_faits 0 
 }
 bind_inactif
  	 while { $inter <  $max_n  &&  $saut_faits  <  $nbre_sauts } {
 ###debug  .frame_middle.right.liste_pieges insert 0 " a $nbre_sauts b $neo_depart c $inter d"
  if { [lsearch $liste_ordonnee $inter ] != -1}  {         ;### inter ds pi�ge
  set arrivee $inter ;   set quot1  [expr $inter / $pas] ; .frame_middle.right.liste_pieges insert end " $arrivee"
      set x [expr [lindex $tab_nxy($inter) 0]  * $init(wlen) ]  ;  set y [expr [lindex $tab_nxy($inter) 1]  * $init(hlen) + [expr 2 *$init(hlen) ] ]
        if { $x == 0}  {set x $init(wlen) }  ;  if {  [lindex $tab_nxy($inter) 0]  == [ expr $init(nbcol)  - 1] }  {set x [expr $x - $init(wlen) ]} 
	if {  [lindex $tab_nxy($inter) 1] == [expr  $init(nbrow) -1 ]}  {set y [expr $y - $init(hlen) ] }
     if  { $choix == "pose_piege" }  { 
       .frame_middle.can create image  $x $y      -image [image create photo -file [file join  $basedir images mal.png]]  -tags img
   set message2 "[format [mc {Pi�ge %1$s atteint,car}] $inter ] $inter =  $quot1 * $pas + $depart\n[format [mc {Le d�part %1$s a �t� mal choisi par %2$s}] $depart $nom_autre ]" }  else {
 .frame_middle.can create image  $x $y      -image [image create photo -file [file join  $basedir images mal.png]]  -tags img
 set message2 "[format [mc {Pi�ge %1$s atteint,car}] $inter ] $inter =  $quot1 * $pas + $depart\n[mc {La liste des pi�ges :}]\n[ .frame_middle.left.liste_pieges  get]\n[mc {mal choisie par}] $nom_autre " 
.frame_middle.right.re_essai  configure -state normal
 }
##   .frame_middle.can  itemconfigure $tab_n(text_$inter)  -fill brown -text  $message1 -font {helvetica 28 bold} ; bell
  tk_messageBox -message $message2 -title "[mc {Message fin}]" -icon info -type ok 
  set reste_essais [expr $max_essais - $nbre_essais ]
.frame_middle.right.re_essai configure  -text "[format [mc {Autre Essai ? Plus que %1$s essai(s)}] $reste_essais ]"
   break
  }
 if { $inter > [lindex $liste_ordonnee  end ]  || [expr $inter + $pas]  >= $max_n } {
 if { [expr $inter + $pas] >= $max_n } {
  .frame_middle.right.liste_pieges insert end " $inter"
   .frame_middle.can  itemconfigure $tab_n(text_$inter)  -fill blue  -font $font_piege
  .frame_middle.right.liste_pieges insert end " [expr $inter + $pas]"
 set inter $max_n
  }
 set  der [expr  [lindex $liste_ordonnee  end ]  + 1] ; set arrivee $der
   if { [expr $inter + $pas] < $max_n } {
  .frame_middle.right.liste_pieges insert end " $inter"
  .frame_middle.can  itemconfigure $tab_n(text_$inter)  -fill blue  -font $font_piege
  }
   set x [expr [lindex $tab_nxy($inter) 0]  * $init(wlen) ] ;  set y [expr [lindex $tab_nxy($inter) 1]  * $init(hlen) + [expr 2 *$init(hlen) ] ]
    if { $x == 0} {set x $init(wlen) } ;  if {  [lindex $tab_nxy($inter) 0]  == [ expr $init(nbcol)  - 1]}  {set x [expr $x - $init(wlen) ]} 
	if {  [lindex $tab_nxy($inter) 1] == [ expr $init(nbrow) -1]}  {set y [expr $y - $init(hlen) ] }
    if  { $choix == "pose_piege" }  { 
 .frame_middle.can create image  $x $y      -image [image create photo -file [file join  $basedir images bien.png]]  -tags img
 set message2 "[mc {Aucun des pi�ges n'est atteint}][format [mc {Le d�part %1$s a �t� bien choisi par %2$s}] $depart $nom_autre ]" } else {
  .frame_middle.can create image  $x $y      -image [image create photo -file [file join  $basedir images bien.png]]  -tags img
 set message2 "[mc {Aucun des pi�ges n' est atteint.La liste de pi�ges:}] [ .frame_middle.left.liste_pieges  get]\n[mc {a �t� bien choisie par}] $nom_autre." 
.frame_middle.right.re_essai  configure -state normal
 }
 ##.frame_middle.can  itemconfigure $tab_n(text_$der)  -fill green -text $message1 -font {helvetica 28 bold} ; bell ; bell
  tk_messageBox -message $message2 -title "[mc {Message fin}]" -icon info -type ok 
   set reste_essais [expr $max_essais - $nbre_essais ]
.frame_middle.right.re_essai configure  -text "[format [mc {Autre Essai ? Plus que %1$s essai(s)}] $reste_essais ]"
  break
  }
  .frame_middle.can  itemconfigure $tab_n(text_$inter)  -fill yellow  -font $font_piege
  .frame_middle.right.liste_pieges configure -state normal
  .frame_middle.right.liste_pieges insert end " $inter"
  clignote   $tab_n(text_$inter) 3
  incr inter $pas 
  if { $args != {}  } { incr saut_faits }
  }
  if { $args != {}  } {
  .frame_middle.right.bond  delete 0 end ; focus -force .frame_middle.right.bond   
  set neo_depart [expr $neo_depart + (${nbre_sauts}*$pas)  ] }
  .frame_up.encore configure -state normal
  .frame_middle.right.re_essai configure -state normal
   set date [clock format [clock seconds] -format "%H%M%d%b"]
 ### set bilan "$date:$premier:$pas:$depart:"
  ##m�moriser pour bilan nom_elev nom_autre contrainte pas depart auto ou pas liste pieges echec ou pas
  ## bouton recommencer avec m�me pas et choisir le demarrage : nom_autre d�part ou nom_autre liste pieges
  }
 proc sauter { } {
 global tab_n pas depart choix  neo_depart
  bind_inactif
  set neo_depart $depart
 ## eleve tape des nombres dep+4+4+4 ou dep+2*4 ou n (�param�trer ??)
 .frame_middle.right.re_essai configure -state disabled
  catch {.frame_middle.right.lancer_a configure -state disabled}
 catch {.frame_middle.left.lancer_a configure -state disabled }
catch { .frame_middle.left.lancer configure -state disabled }
  label .frame_middle.right.bond_t -text "[format [mc {Donne les nombres sauts sous la forme %1$s+ %1$s+.. ou nx%1$s ou n}] ${pas} ]" 
  entry .frame_middle.right.bond -width 25 -bd 3 -highlightcolor red -highlightthickness 3 -font {helvetica 13 bold}
  place .frame_middle.right.bond_t -x 0 -y 160
  place .frame_middle.right.bond  -x 0 -y 210
 bind   .frame_middle.right.bond <Return> "traiter_bonds"
 bind   .frame_middle.right.bond <KP_Enter> "traiter_bonds"
 focus -force .frame_middle.right.bond
  ##m�moriser pour bilan nom_elev autre contrainte pas depart auto ou pas liste pieges echec ou pas
 ## bouton ^[0-9]+$)recommenceravec m�me pas et choisir le demarrage : autre d�part ou autre liste pieges
 }
  proc traiter_bonds  { } {
  global pas depart neo_depart max_n
  .frame_middle.right.re_essai  configure -state disabled
   set rep  [.frame_middle.right.bond  get]  ; set a $pas
   set format "\{^ *$a\( *\\+ *$a\)* *\$\}" ; set format1 "\{^\\d\($a\)\*\$\}"
   set format2 "\{^ *\\d *x *$a *\$\}" ; set format3 "\{^ *\\d+ *\$\}"
 if { ![regexp [lindex $format 0]  $rep ]  &&  ![regexp [lindex $format2 0]  $rep ] && ![regexp [lindex $format3 0] $rep] } {
 tk_messageBox -message "[format [mc {Bond non conforme:n'est pas comme %1$s+%1$s+.. ou nx%1$s ou n}] ${pas} ]"  -title "[mc {Message contrainte}]" -icon info -type ok 
   .frame_middle.right.bond delete 0 end
   return}
 
     if { $rep == {}  } { return }
   regsub {x}  $rep "*" var
   set calcul [expr $var]
  if { [ expr $calcul % $pas ] != 0  || $calcul  > $max_n } { 
  .frame_middle.right.bond delete 0 end ; focus -force .frame_middle.right.bond 
  tk_messageBox -message "[format [mc {%1$s n'est pas un multiple de %2$s ou est > %3$s}] $calcul $pas $max_n ]"  -title "[mc {Message contrainte}]" -icon info -type ok 
  return}
  ### rep est multiple de 5 sous quelle forme ? peu impore rep / pas comme traite_listep
##  set neo_depart $depart
###que si automatique  
 ### .frame_middle.right.bond configure -state disabled
 set nbre_pas  [expr $calcul  /  $pas]
  set liste_sauts {} ; set args {}
  for {set i 1} {$i <= $nbre_pas } {incr i} {
  set liste_sauts  [linsert $liste_sauts end  [expr $neo_depart + $i * $pas] ]
   }
  set args  [linsert $args 0  $nbre_pas] ; set args  [linsert $args end $neo_depart]
   sauter_a   $args
  return
  }
############################################## 
  
proc clignote {  qui nclic} {
global tab_n font_case
 for  {set cl 1} {$cl <= $nclic} {incr cl} {
after 200
.frame_middle.can itemconfigure $qui   -font $font_case  -fill yellow 
 update
after 200
 .frame_middle.can itemconfigure  $qui -font  $font_case  -fill blue 
update
}

}
main


