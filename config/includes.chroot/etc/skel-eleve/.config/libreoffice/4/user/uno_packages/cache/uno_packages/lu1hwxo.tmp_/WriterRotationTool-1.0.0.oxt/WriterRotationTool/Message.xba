<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE script:module PUBLIC "-//OpenOffice.org//DTD OfficeDocument 1.0//EN" "module.dtd">
<script:module xmlns:script="http://openoffice.org/2000/script" script:name="Message" script:language="StarBasic">REM  *****  BASIC  *****

&apos;**************************************************
&apos;AUTHOR		: T. VATAIRE
&apos;DATE		: 12/10/09
&apos;VERSION	: 0.1.2
&apos;**************************************************
&apos;This module contains all functions relating to Message objects
&apos;**************************************************
&apos;licence : LGPL v3.0
&apos;**************************************************

Option Explicit


&apos;------------------------------------
&apos;Private members
&apos;------------------------------------

type struct_Message
	content as string
	messageType as integer
	errNum as integer
	errLine as integer
	sysMess as string
end type


&apos;-------------------------------------
&apos;Public constants
&apos;-------------------------------------

&apos;constant allowing to display a message without icon
public const INT_MESSAGE_TYPE_VOID = 0
&apos;constant allowing to display a message with an &quot;error&quot; icon
public const INT_MESSAGE_TYPE_ERROR = 16
&apos;constant allowing to display a message with a &quot;warning&quot; icon
public const INT_MESSAGE_TYPE_WARNING = 48
&apos;constant allowing to display a message with an &quot;info&quot; icon
public const INT_MESSAGE_TYPE_INFO = 64


&apos;-------------------------------------
&apos;Private functions
&apos;-------------------------------------

&apos;checks the validity of a message
function _message_isValidContent(content as string) as boolean
	_message_isValidContent = (len(content) &gt; 0)
end function

&apos;checks for the validity of the type of message (error, warning, information...)
function _message_isValidType(mType as integer) as boolean
	_message_isValidType = ((mType = INT_MESSAGE_TYPE_ERROR) or _
						   (mType = INT_MESSAGE_TYPE_WARNING) or _
						   (mType = INT_MESSAGE_TYPE_INFO) or _
						   (mType = INT_MESSAGE_TYPE_VOID))
end function

&apos;-------------------------------------
&apos;Public functions
&apos;-------------------------------------

&apos;creates a new message object
&apos;param content			: string			: the content of the message to be displayed at screen.
&apos;param messageType		: integer			: the type of the message. must be one of the INT_MESSAGE_TYPE_* constants.
&apos;param showSystemError	: boolean			: allows to display system informations in the content of the message when an error is intercepted. default is false.
&apos;return					: struct_Message	: the newly created message
function message_new(content as string, messageType as integer, optional showSystemError as boolean) as struct_Message

	dim result as object

	if (not _message_isValidContent(content)) then
		content = &quot;message_new : Invalid &lt;content&gt; parameter. Unable to display original message.&quot;
		messageType = INT_MESSAGE_TYPE_ERROR
		showSystemError = false
	end if
	if (not _message_isValidType(messageType)) then
		messageType = INT_MESSAGE_TYPE_VOID
	end if
	if (isMissing(showSystemError)) then
		showSystemError = false
	end if
	result = createObject(&quot;struct_Message&quot;)
	result.content = content
	result.messageType = messageType
	if (showSystemError) then
		result.errNum = err
		result.errLine = erl
		result.sysMess = error
	end if
	
	message_new = result
	
end function

&apos;checks for the validity of a message.
&apos;param message	: struct_Message	: the message to be checked
&apos;return 		: boolean			: true if the message is a valid message, false otherwise
function message_isValid(message as struct_Message) as boolean

	dim success as boolean
	
	success = (not isnull(message))
	if (success) then
		on local error goto errSystem
		&apos;raises an error if &apos;content&apos; and &apos;messageType&apos; properties don&apos;t exist
		success = (_message_isValidContent(message.content) and _message_isValidType(message.messageType))
		if (success) then
			&apos;raises an error if &apos;errNum&apos;, &apos;errLine&apos; or &apos;sysMess&apos; properties don&apos;t exist
			message.errNum
			message.errLine
			message.sysMess
		end if
		goto finally
		errSystem:
			success = false
		finally:
	end if
	
	message_isValid = success

end function

&apos;Returns a string representation of a message.
&apos;the result is the concatenation of the message content, of a separator and of system error message if it exists.
&apos;param message		: struct_Message	: the message to convert.
&apos;param separator	: string			: the separator inserted between the message content and the system error message.
&apos;return				: string			: the string representation of the message or an empty string if an error occured.
function message_formatToString(message as struct_Message, separator as string) as string

	dim result as string

	if (message_isValid(message)) then
		result = message.content
		if (message.errNum &lt;&gt; 0) then
			result = result &amp; separator &amp; &quot;Error &quot; &amp; message.errNum &amp; &quot; : &quot; &amp; message.sysMess &amp; &quot; (line : &quot; &amp; message.errLine &amp; &quot;).&quot;
		end if
	end if
	
	message_formatToString = result

end function

</script:module>