#!/bin/sh
#pluie.tcl   ESSAI DE RECONSTRUCTION D'UN Logiciel IPT (auteur ?)
#\
exec wish "$0" ${1+"$@"}
###########################################################################
#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#**************************************************************************
#  File  : $$$
#  Author  : jlsendral@free.fr
#  Modifier: 
#  Date    : 24/04/2002
#  Licence : GNU/GPL Version 2 ou plus
#
#  Description:
#  ------------
#
#  @version
#  @author     Jean-Louis Sendral
#  @modifier 
#  @project    Le terrier
#  @copyright  Eric Seigne 24/04/2002
#
#  *************************************************************************
set basedir [file dir $argv0]
cd $basedir
 source calculs.conf
source i18n
source msg.tcl
source fonts.tcl
 source path.tcl
#bind . <F1> "showaide { }"
set basedir [pwd] ; cd $basedir
 set basedir [pwd]
set fich [lindex $argv 0 ]
set DOSSIER_EXOS1  [lindex $argv 5]
##cd  $DOSSIER_EXOS1
set f [open [file join $DOSSIER_EXOS1 $fich] "r"  ]
set listdata [gets $f] ;#init de la liste des couples.
#set fich $f
#"set fich [file tail $f]
close $f
proc vieux_fichier {expr} {
set e1 [regsub -all  {\/}  $expr ":" var]
set e2 [regsub -all  {\*} $var "x" var1 ]
set e3 [regsub -all  {\.} $var1 "," var2 ]
return $var2
}
 set listcouples [ vieux_fichier [lindex $listdata 1] ]
if { $listcouples  == {} } {
 tk_messageBox -message [mc {mess_list3}] -title "Message mauvaise liste" -icon error -type ok
exit  }
foreach el $listcouples {
 if { [ regexp {^[0-9]*\,[0-9]*$} $el ]  } {
 tk_messageBox -message [mc {liste avec d�cimaux ?} ] -title "[mc {Message mauvaise liste}] " -icon error -type ok
 exit
 }
 }
foreach el $listcouples {
if { [ regexp {.*[\+|\-|x|\:]+.*[\+|\-|x|\:]+.*} $el  ] } {
 tk_messageBox -message [mc {expressions trop longues ?}] -title "[mc {Message mauvaise liste}]" -icon error -type ok
exit  
 }
 }
 ### if { ![ regexp {^[0-9]*\.?[0-9]*$} $el ]  

 global sysFont
##source msg.tcl
##source fonts.tcl
##source path.tcl
 proc choix_bg { } {
set l_coul [list maroon2 lavender orange orange1 sienna orchid3 lightblue violet]
set taille [llength $l_coul]
set ind [expr int(${taille}*rand()) ]
return [lindex  $l_coul  $ind]
}
 
 set bgcolor [choix_bg]
# #808080
set bgl #ff80c0
wm geometry . +0+0
. configure -background $bgcolor
##wm resizable . 0 0
wm  title . "[mc {calculs approch�s}]-Sous GPL [lindex $argv 0 ] "
set date [clock format [clock seconds] -format "%H%M%D"]

frame .frame -width 640 -height 640 -background bisque
##808080


#bframe pour les calculs (bframe.res)
frame .bframe -background $bgcolor -relief raised -bd 3 -height 6
pack .bframe -side bottom -fill both
label .bframe.lab_consigne -background $bgcolor -text [mc {mess_calc_appr}]
grid .bframe.lab_consigne -padx 20 -column 1 -row 1
button .bframe.but_calcul -text [mc {lancer_calc}] \
-command ".bframe.but_calcul configure -state disable ; calcul"
grid .bframe.but_calcul -column 3 -row 1 -padx 10
entry .bframe.res -width 18 -justify center -textvariable rep  -font {Arial 20}
grid .bframe.res -column 2 -row 1 ; 
#essai de bind control� par return, mais ne sert � rien pour l'instant"
#bind .bframe.res <Keypress-Return>  [set rep1 [.bframe.res get]] ;#[set rep1 $rep]

#mframe pour le nom et le score et les tests de deboggage !!
frame .mframe -background $bgcolor -relief raised -bd 3
pack .mframe -side bottom -fill both 
#entry .mframe.ent_nom -width 20
#grid .mframe.ent_nom -column 1 -row 1 -sticky e
#label .mframe.lab_nom  -text "est   ton nom " -background #ffff80  -anchor e
#grid .mframe.lab_nom -column 2 -row 1
set nom_elev  [lindex $argv 1 ] ;#init nom �l�ve et nom clase
set nom_classe [lindex $argv 2 ]
initlog  $tcl_platform(platform) $nom_elev
label .mframe.lab_nom  -text "[mc {bonjour}] $nom_elev" \
         -background $bgcolor -anchor e
  #AC# column 1 par 5
  grid .mframe.lab_nom -column 1 -row 1
#cd $basedir
set oper [ vieux_fichier [lindex  $listdata 0] ]
##set oper [lindex  $listdata 0]
if { $oper == "x" } { set oper_c "*" } 	elseif { $oper == ":" } {set oper_c "/" } else {set oper_c $oper }
proc change_oper {expr} {
set e1 [regsub -all  {:}  $expr "\/" var]
set e2 [regsub -all  {x} $var "\*" var1 ]
set e3 [regsub -all  {,} $var1 "\." var2 ]
return $var2
}
set n_lig [lindex  $listdata 2]
#set n_col [lindex  $listdata 3]
set n_col 1 
if { $n_lig > 17 } {
set n_lig 17
}
proc traite_expr { expr} {
if { [string last + $expr] != -1  || [string last - $expr] != -1  \
 || [string last x $expr] != -1  || [string last \: $expr] != -1 } {
return "\(${expr}\)"
} else {
return $expr
}
} 
proc traite_expr1 {expr} {
 if { [string last "\(" $expr ] == -1  } {
 return $expr
    } else {
	return [expr $expr]
	}
}

proc reviens_oper {expr} { 
 set e1 [regsub -all  {\.}  $expr "," var]
 return $var
 }
##.bframe.res insert end [reviens_oper 3.25 ]
proc choix_n_v { } {
set ind [expr int(4*rand()) ]
return [lindex  [ list  x y z t] $ind]
##pb x *
}
set nom_v [ choix_n_v ]
proc choix_cote { } {
set ind [expr int( 2*rand())]
return [lindex  [ list  g d ]  $ind]
}
set nom_c  [ choix_cote ]
##set nom_c  "d"

#hframe pour la tomb�e des expressions.  par grid
frame .hframe -background $bgcolor
pack .hframe -side bottom -fill both
set j 1
for {set i 1} {$i <= $n_lig} {incr i 1} {
           label .hframe.lab_bilan$i$j -text "_________"  -background $bgcolor  -font {Arial 20}
        grid .hframe.lab_bilan$i$j -column $j -row $i
    }

label .hframe.rep -text "____________\n____________" -font {Arial 10}  ; place .hframe.rep  -x 0 -y 200
message .hframe.rep_bon -text "                 \n               " -background $bgcolor 
grid .hframe.rep_bon -column 3 -row 1 -padx 10 


#grid .hframe.rep -column [expr $n_col + 4] -row 6 ;# essai pour les bad calculs
set icone_b	[image create photo -file [file join  $basedir images bien.png]   ]
##label .hframe.bien -image $icone_b -background $bgcolor
set icone_m	[image create photo -file [file join  $basedir images mal.png]   ]

#"set listdata {+ {1 2 5 3 14 5 7 8 4 5 1 5 14 5 23 5 4 15 8 9 0 3 4 5  78 1}}
#init des couples de nombre en jeu, de l'op�ration, des couples au d�part
#des bons calculs et des mauvais

## set oper [lindex $listdata 0]
set ndepart [expr int($n_col / 2) ]
#set ndepart 1; #< nbre colonnes -1  
set listcouples [ vieux_fichier [lindex $listdata 1] ]
set uu [llength $listcouples]
set nbrecouples [expr $uu /2]
### set nx [traite_expr [lindex [lrange $listcouples 0 1] 0] ]
	set nx   [lindex [lrange $listcouples 0 1]  0 ] 
        set ny  [lindex [lrange $listcouples 0 1]  1] 
	set equat   [expr [change_oper [traite_expr ${nx}]${oper}[traite_expr ${ny}] ] ]
##set nom_c "g"
	switch  $nom_c {
		"g"  { set ny1 [traite_expr $ny ]
	  if  { [regexp {^([0-9]+)([\+|\-|x|\:]){1}([0-9]+)$}  $nx tout prem deux  trois ]  }  {
      set nx1   [traite_expr "${prem}${deux}${nom_v}" ]; set trois_rep $trois
           }  else  { set nx1 $nom_v  ; set trois_rep $nx }
          .hframe.lab_bilan11  configure -text "${nx1}${oper}${ny1}=[reviens_oper $equat]"  -background $bgcolor -font {Arial 20}	
###		 .hframe.lab_bilan21  configure -text "$
		}
		"d" { set nx1 [traite_expr $nx ]
		   if { [ regexp {^([0-9]+)([\+|\-|x|\:]){1}([0-9]+)$}  $ny tout prem deux trois  ]  }   {
	  	  set ny1  [traite_expr "${prem}${deux}${nom_v}"] ; set trois_rep $trois  
           }  else  { set ny1 $nom_v ; set trois_rep $ny }
          .hframe.lab_bilan11  configure -text "${nx1}${oper}${ny1}=[reviens_oper $equat]"  -background $bgcolor -font {Arial 20}	
			}
			}
	
   set ii [expr ($i /2) + 1]
 
 set listcouples [lrange $listcouples 2 end]
set nbrecouplesrestant [expr $nbrecouples - 1 ]
set indexcol 2
#attention � ne pas d�passer $depart"
set score 0 ; set echec 0 ; set reussite 0 ; set mrep 0 ; set bonneliste {} ; set echecliste {} ;
set tempo [lindex $argv 3] ; set drapeau 1 ; set drapeau_bon 0 ; set bon_lig 0   ; set troptard {}
if { ![regexp {^[0-9]+$} $tempo ] } {
set tempo 1000  } 

proc memorise {} {
global input
set input [.bframe.res get]
.bframe.res delete 0 end
}
proc solution { op b_lis t_lis e_lis app nom fich date } {
global env tcl_platform w2
catch {destroy .top2 }
set w2 [toplevel .top2]
wm geometry $w2 +250+0
wm title $w2 [mc {bil_bon_calc_app}]
###grab ${w2}
text ${w2}.text -yscrollcommand "${w2}.scroll set" -setgrid true -width 60 -height 15 \
 -wrap word -background black -font   {Helvetica  12}
scrollbar ${w2}.scroll -command "${w2}.text yview"
pack ${w2}.scroll -side right -fill y
pack ${w2}.text -expand yes -fill both
${w2}.text tag configure green -foreground green
${w2}.text tag configure red -foreground red
${w2}.text tag configure yellow -foreground yellow
${w2}.text tag configure normal -foreground black
${w2}.text tag configure white1 -foreground white    -font  {helvetica 16}
${w2}.text tag configure white -foreground white -underline yes
proc marge {} {
global w2
${w2}.text insert end  "   "
}
  ${w2}.text insert end  "   [format [mc {le %1$s, exercice : %2$s}] $date $fich ]\n"     red
${w2}.text insert end   "[mc {resum}] $nom  :\n"    white1
marge ; ${w2}.text insert end  "[mc {Calcul d'�quations}]  :\n"   white
foreach cal $b_lis {
marge
${w2}.text insert end  " [mc {Ton equation}] : [lindex $cal 0]  [mc {donne bien}] [lindex $cal 1] \n" green
}
${w2}.text insert end "\n"
marge ; ${w2}.text insert end  "[mc {Mauvais calculs} ] :\n"   white
foreach cal $e_lis {
 marge ;  ${w2}.text insert end "[mc {ton_idee}]  [lindex $cal 1] ne r�soud pas : [lindex $cal 0] \n" red 
 }
${w2}.text insert end "\n"
marge ; ${w2}.text insert end  "[mc {trop_tard}]\n"   white
foreach cal $t_lis {
${w2}.text insert end "  $cal\n" yellow
}
 if  {  $tcl_platform(platform)  == "unix"  }  {
###after 1000
set rep [tk_messageBox  -message "[mc {imprim}]" -type yesno -icon question  -default no -title Imprimer? ]
 if {  $rep  == "yes"  }  {
### set  t    [ ${w2}.text  get  0.0  end  ]
 exec  lpr   <<  [ ${w2}.text  get  0.1  end  ]
 ###  $b_lis  $e_lis
 ### ${w2}.text  get  0 end  ]
 }
 }
raise ${w2} .frame
}
proc non_doublons { liste} {
if { $liste == {} } {
return $liste
} else {
if { [lsearch [lrange $liste 1 end] [lindex $liste 0]] == -1 } {
    return [ list [lindex $liste 0] [ non_doublons [lrange $liste 1 end]] ]
   } else {
     return [non_doublons [lrange $liste 1 end]]
    }   
}
}




bind .bframe.res <Return> "memorise"
bind .bframe.res <KP_Enter> "memorise"
set input 100000  ; set avreu 0
##############################################################################################
# programme principal avec usage pseudo recursif. R�le de after pas tr�s clair encore
# A l'arr�t : travail sauv� ds un fichier qui doit exister
##############################################################################################
proc calcul {} {
global fich tempo nom_elev n_col n_lig listcouples nbrecouples score echec avreu reussite  approx  nom_v nom_v bgcolor \
mrep nbrecouplesrestant indexcol oper input bonneliste echecliste date drapeau bon_lig bon_col drapeau_bon   troptard equat
global icone_b icone_m trois_rep
focus -force .bframe
catch { destroy  .hframe.bien .hframe.mal }

set approx 5
        if  { ( $echec == $nbrecouples ) || \
( $reussite >= $nbrecouples ) || \
( [expr $echec + $reussite] >= $nbrecouples )} {
#focus -force .mframe.ent_bilan
##$nom_elev, [mc {reussite}] $reussite calcul(s)." 
label .mframe.lab_bilan  -text \
"[format [mc {%1$s Tu as reussi � %2$s calcul(s)}] $nom_elev $reussite]" -background $bgcolor  -anchor e
    grid .mframe.lab_bilan -column 5 -row 1 -padx 10
    set mrep_approx  $echecliste
 button .bframe.sol -text [mc {R�sum� du travail ?}] -foreground yellow \
-command "solution $oper {$bonneliste} {$troptard}  {$echecliste} $approx $nom_elev [file tail $fich]  $date"
 grid .bframe.sol -column 4 -row 1

    sauver $date $nom_elev $fich $oper $bonneliste $troptard  $mrep_approx
 button .bframe.bis -text [mc {Recommencer ? }] -foreground blue  -command "recommencer "
grid .bframe.bis -column 5 -row 1
 .bframe.but_calcul configure -text [mc {quitter}] -state active -command { exit }
return 1
        }
          .hframe.lab_bilan${n_lig}1 configure -text "_________" -background  $bgcolor  -font {Arial 20}

        if  { ($nbrecouplesrestant != 0 ) && ($indexcol <= $n_col && $drapeau )  } {
            set acal [ lrange $listcouples 0 1 ]
            set listcouples [lrange $listcouples 2 end ]
            set nx [lindex $acal 0]  ; set ny [lindex $acal 1] 
			set equat [expr [change_oper [traite_expr ${nx}]${oper}[traite_expr ${ny}] ] ]
			##	set equat [expr [change_oper (${nx})${oper}(${ny})] ] 
##			set equat [expr ${nx}${oper}${ny} ]  
	    	    switch  $nom_c {
		"g"  { set ny1  [traite_expr $ny ]
	  if  { [regexp {^([0-9]+)([\+|\-|x|\:]){1}([0-9]+)$}  $nx tout prem deux  trois ]  }  {
##$trois est la bonne r�ponse
      set nx1   [traite_expr "${prem}${deux}${nom_v}" ] ; set trois_rep $trois
           }  else  { set nx1 $nom_v ; set trois_rep $nx}
##nx1 bonne rep		   
          .hframe.lab_bilan11  configure -text "${nx1}${oper}${ny1}=[reviens_oper $equat]"  -background $bgcolor -font {Arial 20}	
###		 .hframe.lab_bilan21  configure -text "$
		}
		
			
		"d" { set nx1 [traite_expr $nx ]
		   if { [ regexp {^([0-9]+)([\+|\-|x|\:]){1}([0-9]+)$}  $ny tout prem deux trois  ]  }   {
	  	  set ny1  [traite_expr "${prem}${deux}${nom_v}" ]  ; set trois_rep $trois
           }  else  { set ny1 $nom_v ; set trois_rep $ny}
          .hframe.lab_bilan11  configure -text "${nx1}${oper}${ny1}=[reviens_oper $equat]"  -background  $bgcolor  -font {Arial 20}	
			}		
			
					}
##	    set trois_rep $trois
	   	  
		if { ${bon_lig} != 1 && $drapeau_bon == 1 } {
.hframe.lab_bilan1${indexcol}  configure -text "$nx1$oper$ny1=[reviens_oper $equat]"  -background $bgcolor -font {Arial 20}
} else {
.hframe.lab_bilan1${indexcol}  configure -text "$nx1$oper$ny1=[reviens_oper $equat]"  -background $bgcolor -font {Arial 20}
}
           set indexcol 1  
            set nbrecouplesrestant [expr $nbrecouplesrestant - 1]
		set drapeau 0
} ;
if { $drapeau_bon == 1} {
if { ${bon_lig} !=1 } {
.hframe.lab_bilan${bon_lig}${bon_col}  configure -text "_________" -background  $bgcolor -font {Arial 20}
#set drapeau_bon 0
#} else {
#.hframe.lab_bilan${bon_lig}${bon_col}  configure -text "BON" -background  $bgcolor -font {Arial 20}
set drapeau_bon 0
}
}

set avreu $reussite ; avancer  ;
after $tempo  calcul     
 }
 proc change_oper {expr} {
set e1 [regsub -all  {:}  $expr "\/" var]
set e2 [regsub -all  {x} $var "\*" var1 ]
set e3 [regsub -all  {,} $var1 "\." var2 ]
return $var2
}

### gestion des calculs de l'avan��e...#################
###########################################
proc avancer {} {
global rep1 n_lig n_col bon_lig bon_col drapeau_bon listcouples nbrecouplesrestant oper approx avreu reussite echec 
global  mrep score bonneliste echecliste  troptard  indexcol input equat  nom_v nom_c bgcolor icone_b icone_m oper_c trois_rep
focus  .bframe.res ; set drapeau_bon 0
set rep1 $input  
### [.bframe.res get]
if  {![ regexp {^[0-9]*\.?[0-9]*$} $rep1 ] == 1 || [ regexp {(^0[8-9]+$)} $rep1 ] || $rep1 == "." || $rep1 == {} } {
        set input 100000 ; set rep1 $input
}
# receuil de la r�ponse de l'�l�ve. La dur�e de la frappe d�pend de after 1500
# pour chaque case jusqu'� la ligne n_lig-1 faire :
for  {set col 1 } {$col <= $n_col } {incr col } {
    for  {set lig [expr $n_lig - 1]} {$lig > 0 } {incr lig -1} {
        set val  [.hframe.lab_bilan${lig}${col} cget -text] ;# ce qu'il y a dans la case!
##	 set nom_c "g"
	
		 if {   [ regexp {^([x|y|z|t]+)[\+|\-|x|\:]\(?([0-9]+[\+|\-|x|\:]*[0-9]*)\)?=([0-9]+)$} $val tout prem deux  trois]  ||  \
[ regexp {^\(?([0-9]+)([\+|\-|x|\:])[x|y|z|t]+\)?[\+|\-|x|\:]\(?([0-9]+[\+|\-|x|\:]*[0-9]*)\)?\=[0-9]+$} $val tout prem deux  trois]  || \
 [ regexp {^\(?([0-9]+[\+|\-|x|\:]*[0-9]*)\)?[\+|\-|x|\:]+([x|y|z|t]+)=([0-9]+)$}   $val tout prem deux trois  ]  ||  [regexp {^\(?([0-9]+[\+|\-|x|\:]*[0-9]*)\)?[\+|\-|x|\:]\(?([0-9]+)([\+|\-|x|\:])([x|y|z|t]+)\)?=[0-9]+$}  $val tout prem deux trois quatre ] }  {																				 
 ##  .hframe.lab_bilan51  configure -text  "$val"
		if { $nom_c  == "g" }  {
	                     if { [regexp {^[x|y|z|t]$}  $prem ]  }  { 
				  set nx  $prem  ; set var $rep1 
				  set ny  [ traite_expr1 $deux ]  }  else  {
###	      .hframe.lab_bilan51  configure -text "ds switchg"
		     set nx  [expr [change_oper ${prem} ] [change_oper ${deux} ] ${rep1}] ; set var $nx
		    set ny  [ traite_expr1 $trois]  
										      }
					    }  elseif  { $nom_c == "d" } {
###.hframe.lab_bilan51  configure -text "ds	d"		    
		    if { [regexp {^[x|y|z|t]$}  $deux ]  }  { 
		    set ny $rep1 ; set nx [traite_expr1 $prem] ; set var $nx
		    } else { set var [traite_expr1 $prem] 
		set ny [ expr [change_oper	${deux} ] [change_oper ${trois}] {$rep1} ]
				}

					   }
## set var 12 
 ##hframe.lab_bilan71  configure -text " coucou $var $oper $ny $equat"
 ##si $trois est rep c'est bon
	    if  { $trois_rep ==  $rep1  }   { ; #bonne r�ponse.
		##[expr [change_oper ${var} ]${oper_c} [change_oper ${ny}] ]
           set drapeau_bon 1 
##	    .bframe.res delete 0 end  ;# on efface la bonne r�ponse de l'input.

label .hframe.bien -image $icone_b -background $bgcolor
grid .hframe.bien -row ${lig} -column 3 -rowspan 4
            incr score ; incr reussite ; #" .mframe.bil insert end $reussite
set bonneliste [linsert  $bonneliste end [list $val  $rep1 ]]
##set bonneliste [ linsert  $bonneliste end $tout]
set bon_lig $lig ; set bon_col $col
.hframe.rep configure -text "____________\n____________"
set input 100000 ; set rep1 $input
 if { $bon_lig == 1 } {
.hframe.lab_bilan2${col}  configure -text "[mc {Bon calcul}]" -background #ffffff -font {Arial 20} -foreground black
clignote "[mc {Bon calcul}]"  2   ${col}  2
.hframe.lab_bilan${lig}${col}  configure -text "_________" -background  $bgcolor  -font {Arial 20} -foreground black
} else {
.hframe.lab_bilan${lig}${col}  configure -text "[mc {Bon calcul}]" -background #ffffff -font {Arial 20} -foreground black
clignote "[mc {Bon calcul}]" ${lig}   ${col}  2
.hframe.lab_bilan${lig}${col}  configure -text "_________" -background  $bgcolor -font {Arial 20} -foreground black
}
if  { ($nbrecouplesrestant != 0) && ([string compare .hframe.lab_bilan1${col} "_________" ] != 0 ) } {
    set nx   [lindex [lrange $listcouples 0 1]  0 ] 
    set ny   [lindex [lrange $listcouples 0 1]  1] 
        set listcouples [lrange $listcouples 2 end ]
		
		set equat [expr [change_oper (${nx})${oper}(${ny})] ] 
		
 #      set equat [expr [change_oper "${nx}${oper}${ny}"] ]  
       set nom_v [ choix_n_v ]
      switch  $nom_c {
		"g"  { set ny1 [traite_expr $ny ]
	  if  { [regexp {^([0-9]+)([\+|\-|x|\:]){1}([0-9]+)$}  $nx tout prem deux  trois ]  }  {
      set nx1   "\(${prem}${deux}${nom_v}\)" ; set trois_rep $trois
 ##     .hframe.lab_bilan31  configure -text  "2x $nx1"
           }  else  {
##	   .hframe.lab_bilan71  configure -text  "x"
	   set nx1 $nom_v ; set trois_rep $nx
	   }
          .hframe.lab_bilan11  configure -text "${nx1}${oper}${ny1}=[reviens_oper $equat]"  -background  $bgcolor -font {Arial 20}	
##		 .hframe.lab_bilan81  configure -text "${nx1}${oper}${ny1}"
		}

		"d" { set nx1 [traite_expr $nx ]
		   if { [ regexp {^([0-9]+)([\+|\-|x|\:])([0-9]+)$}  $ny tout prem deux trois  ]  }   {
	  	  set ny1  [traite_expr "${prem}${deux}${nom_v}" ]  ; set trois_rep $trois
           }  else  { set ny1 $nom_v ; set trois_rep $ny }
          .hframe.lab_bilan11  configure -text "${nx1}${oper}${ny1}=$equat"  -background  $bgcolor  -font {Arial 20}	
			}
			}

 ## .hframe.lab_bilan11  configure -text "${nom_v}${oper}${ny1}=$equat"  -background $bgcolor -font {Arial 20}
    set nbrecouplesrestant [expr $nbrecouplesrestant - 1]
    }
  break
#".hframe.lab_bilan${lig}${col}  configure -text "___ " -background #808080 -font {Arial 14}
}  else {
 #mauvaise r�ponse, on fait descendre! l'expression.
    set k [expr $lig + 1]
   .hframe.lab_bilan${k}${col} configure -text "${val}" -background $bgcolor -font {Arial 20}
.hframe.lab_bilan${lig}${col}  configure -text "_________" -background  $bgcolor -font {Arial 20}
.hframe.rep configure -text "[mc {bad calcul}]\n____________"  -font {Arial 10} -background  red
   # on efface la mauvaise r�ponse de l'input !.
   if { $input  == ""} {
   label .hframe.mal -image $icone_m -background $bgcolor
   grid .hframe.mal -column 1 -row ${lig} -rowspan 4
   }
   if { $input  != 100000 } {
   label .hframe.mal -image $icone_m -background $bgcolor
   grid .hframe.mal -column 3 -row ${lig} -rowspan 4
    set echecliste [ linsert $echecliste end [list ${val} $rep1]]
incr mrep ; set  input 100000 ; set rep1 $input
 }
}
##fin regexp de g swith
##fin avant derni�re ligne cas g
##fin if =g
}
##il reste la derni�re ligne : les trop tard !
for  {set t 1} {$t <= $n_col} {incr t} {
    set val [.hframe.lab_bilan${n_lig}${t} cget -text]
 if {   [ regexp {^([x|y|z|t]+)[\+|\-|x|\:]\(?([0-9]+[\+|\-|x|\:]*[0-9]*)\)?=([0-9]+)$} $val tout prem deux  trois]  ||  \
[ regexp {^\(?([0-9]+([\+|\-|x|\:])[x|y|z|t]+\))?[\+|\-|x|\:]\(?([0-9]+[\+|\-|x|\:]*[0-9]*)\)?\=[0-9]+$} $val tout prem deux  trois]  || \
  [ regexp {^\(?([0-9]+[\+|\-|x|\:]*[0-9]*)\)?[\+|\-|x|\:]+([x|y|z|t]+)=([0-9]+)$}   $val tout prem deux trois  ]  ||  [regexp {^\(?([0-9]+[\+|\-|x|\:]*[0-9]*)\)?[\+|\-|x|\:]\(?([0-9]+)([\+|\-|x|\:])([x|y|z|t]+)\)?=[0-9]+$}  $val tout prem deux trois quatre ]}  {
            .hframe.lab_bilan${n_lig}${t} configure -text [mc {trop_tard1}] -background bisque1 -font {Arial 20}
        	    ##ffffff
catch {	label .hframe.mal -image $icone_m -background $bgcolor ;\
  grid .hframe.mal -column 3 -row  [expr ${lig} -1 ] -rowspan 3	}	
				
            incr echec
#.hframe.lab_bilan${n_lig}${t} configure -text [mc {Perdu}] -background #ffffff -font {Arial 20}
    set nx  $prem
    set ny  $deux
 set troptard [ linsert $troptard end $tout ]

     if  { $nbrecouplesrestant != 0  } {   ;#&& ($t != $indexcol)
     
set ny   [lindex [lrange $listcouples 0 1]  1] 
set nx   [lindex [lrange $listcouples 0 1]  0] 
        set listcouples [lrange $listcouples 2 end ] ; set nom_v [ choix_n_v ]
		
		set equat [expr [change_oper (${nx})${oper}(${ny})] ] 
  ##     set equat [expr  [change_oper ${nx}] ${oper_c} [change_oper ${ny} ]]  
      switch  $nom_c {
		"g"  { set ny1 [traite_expr $ny ]
	  if  { [regexp {^([0-9]+)([\+|\-|x|\:]){1}([0-9]+)$}  $nx tout prem deux  trois ]  }  {
      set nx1   "\(${prem}${deux}${nom_v}\)" ; set trois_rep $trois

 ##        .hframe.lab_bilan41  configure -text "bas bon"
           }  else  { set nx1 $nom_v ; set trois_rep $nx }
          .hframe.lab_bilan11  configure -text "${nx1}${oper}${ny1}=[reviens_oper $equat]"  -background $bgcolor -font {Arial 20}	
###		 .hframe.lab_bilan21  configure -text "$
		}
		"d" { set nx1 [traite_expr $nx ]
		   if { [ regexp {^([0-9])+([\+|\-|x|\:]){1}([0-9]+)$}  $ny tout prem deux trois  ]  }   {
	  	  set ny1  [traite_expr "${prem}${deux}${nom_v}" ]   ; set trois_rep $trois
           }  else  { set ny1 $nom_v ; set trois_rep $ny}
         .hframe.lab_bilan11  configure -text "${nx1}${oper}${ny1}=[reviens_oper $equat]"  -background  $bgcolor -font {Arial 20}	
			}
			}
    
 ##   .hframe.lab_bilan1${t}  configure -text "${nx1}${oper}${ny1}=$equat"  -background  $bgcolor -font {Arial 20}
    set nbrecouplesrestant [expr $nbrecouplesrestant - 1]
	}
}
} ;##fin derni�re ligne
}
#".mframe.bil insert 0 [list "$reussite u" $echec] ;# pour tester
# si le nbe de reussites est inchang� c'est une mauvaise r�ponse sinon bonne r�ponse.
#essai de mentionner un mauvais calcul : non concluant !
if  { $avreu == $reussite } { 
  ##     .hframe.rep configure -text "${mrep} [mc {mauvais calcul}](s)\n____________" -font {Arial 10} -background red
          } else  {
    .hframe.rep configure -text "____________\n____________"
}
return 1
} 
}
####fin colonne
 ##fin avancer

proc clignote {texte lig col  nclic} { ; #after supprim� car semble pos� des pbs de // avec l'autre after
#"   if  { $nclic ==1 } {.hframe.lab_bilan${lig}${col}  configure -text "______"  -background #ffffff -font {Arial 10} ; return 1}
global bgcolor
for  {set cl 1} {$cl <= $nclic} {incr cl} {
after 200
.hframe.lab_bilan${lig}${col}  configure -text "$texte"  -background  $bgcolor -font {Arial 20} -foreground green
update
after 200
.hframe.lab_bilan${lig}${col}  configure -text "$texte"  -background $bgcolor -font {Arial 20} -foreground red
update
update
}
}
# sauver les r�sultats
proc sauver {dat nom fic oper listereussite troptard mrep_approx } { ; # � s�curiser
global savingFile nom_classe TRACEDIR LogHome  user choix_lieu approx DOSSIER_EXOS1 basedir
set TRACEDIR $LogHome
cd $TRACEDIR
#set types {
#    {"Cat�gories"		{.txt}	}
#}
#catch {set file [tk_getSaveFile -filetypes $types]}
#set f [open [file join $file] "a"]
#set travail  "date : $dat nom : $nom fichier : $fic $oper bon : $listereussite echec : $listechec  mauvais cal : $badcal"
set basefile [file tail $fic]
set ou [lrange [ split $DOSSIER_EXOS1 "/" ]  end-1 end]
if { [lsearch -exact $ou "C:" ]  >= 0 } {
set ou [ lreplace $ou  [lsearch -exact $ou "C:" ] [lsearch -exact $ou "C:"  ] "C" ]
}
## "$dat&$nom&{[file tail ${fic}] de $ou}&$oper&{$liste_rep}&$listetard&$listechec&un"
set savingFile [file join $basedir data  ${nom_classe}.bil ]
set travail  "$dat&$nom&{[file tail ${fic}] de $ou}&$oper&{$listereussite}&$troptard&$mrep_approx&eq"
catch {set f [open [file join $savingFile] "a"]}
puts $f $travail
close $f
#set savingFile ${nom_classe}.bil
#  catch {set f [open [file join $savingFile] "a"]}
#  set travail  "&$dat&$nom&${basefile} de $choix_lieu &$oper&{$listereussite}&$troptard&$mrep_approx&5
#puts $f $travail
#close $f
set ou [lindex [ split $DOSSIER_EXOS1 "/" ]  end]
catch {set flog  [open [file join $user] "a+"]}


set log "$dat&$nom&{[file tail ${fic}] de $ou}&$oper&{$listereussite}&$troptard&$mrep_approx&eq"
##set log "$dat&$nom&{[file tail ${fic}] de $ou }&$oper&$listereussite&$troptard&$mrep_approx&eq"
puts  $flog  $log
close $flog
###exec   /usr/bin/leterrier_log  --message=$log   --logfile=$user
}
 proc recommencer {} {
 global basedir DOSSIER_EXOS1 argv  iwish nom_elev nom_classe tempo
 set exo [lindex $argv 0 ]  ; destroy .
 exec  $iwish  [file join $basedir pluie_eq.tcl ] [file join $DOSSIER_EXOS1 $exo ] $nom_elev $nom_classe $tempo  $DOSSIER_EXOS1 &
}








