set etapes 1
set niveaux {1 2 4 5}
::1
set niveau 2
set ope {{10 20} {30 20}}
set interope {{0 40 1} {10 60 1}}
set scaleb {0 60 1}
set ope1 [expr int(rand()*[lindex [lindex $ope 0] 1]) + [lindex [lindex $ope 0] 0]]
set ope2 [expr int(rand()*[lindex [lindex $ope 1] 1]) + [lindex [lindex $ope 1] 0]]
set operations [list [list [expr $ope1 ]+[expr $ope2 - $ope1]] [list [expr $ope2 - $ope1]+[expr $ope1 ]] [list [expr $ope2]-[expr $ope1]]]
set enonce "Distances.\nJe suis � la borne rouge.\nClamville est � [expr $ope1] km.\nFortville est � [expr $ope2] km.\nQuelle est la distance entre Clamville et Fortville?"
set reponse [list [list {1} [list {La distance est de} [expr $ope2 - $ope1] km.]]]
set resultdessin [expr $ope2]
set dpt $ope1
set source {{0}}
set opnonautorise {0 1}
set canvash 140
set c1height 50
set placearriv $ope2
::