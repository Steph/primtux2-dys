############################################################################
# Copyright (C) 2002 David Lucardi
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# Author  : jean louis sendral
#           mailto: jlsendral@free.fr
# Date    : 26/04/2004
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version
# @author    jean louis sendral
# @project
# @copyright  jean louis sendral 26/04/2004
#
#
#########################################################################
#!/bin/sh
#bilan.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}


set c .frame.c
set bgn #ffff80
set bgl #ff80c0
source msg.tcl
source fonts.tcl
  global env
 source path.tcl
set plateforme $tcl_platform(platform) ; set ident $tcl_platform(user)
init $plateforme
initlog $plateforme  $ident
if { $tcl_platform(platform)  == "unix" } {
if {[catch { set f [open [file join ${user}  ] "r"  ] }] }  {
   set answer [tk_messageBox -message [mc {Erreur de fichier}] -type ok -icon info]
   exit
   }
 }
 ## set f  [ open  [file join $user  ]  "r" ]
. configure -background black -width 640 -height 480
wm geometry . +0+0
 if { $tcl_platform(platform) == "windows" } {
set types {
	{"Cat�gories"		{.log}	}
         }
catch {set file [tk_getOpenFile -initialdir $LogHome -filetypes $types]}
if {[catch { set f [open [file join $file] "r" ] }] } {
     set answer [tk_messageBox -message [mc {err_fich}] -type ok -icon info]
     exit
     }
}

wm title . "Bilan\040 de [lindex [split [lindex [split $user /] end] .] 0]"
text .text -yscrollcommand ".scroll set" -setgrid true -width 60 -height 20 -wrap word -background black -font   {Helvetica  12}
##$sysFont(l)
scrollbar .scroll -command ".text yview"
pack .scroll -side right -fill y
pack .text -expand yes -fill both
proc change_oper {expr} {
set e1 [regsub -all  {:}  $expr "\/" var]
set e2 [regsub -all  {x} $var "\*" var1 ]
set e3 [regsub -all  {,} $var1 "\." var2 ]
return $var2
}
proc traite_un {el l_bc l_mc l_be} {
global nbre_f mauvais_cal_expr nbre_b bon_calcul liste_r
global mauv_calc_p bon_calc_p bon_calc_expr  nbre_t
##set mauv_calc_p {} ; set bon_calc_p {}
if { $el == {} } {
 set bon_calcul $l_bc ; set mauvais_cal_expr $l_mc
 .text insert  end "[mc {bon_expr_p} ]: $bon_calc_p \n" green
.text insert  end "[mc {mauv_expr_p}] : $mauv_calc_p \n" red
##set liste_r  [linsert $liste_r 0 [lindex $el 0 ] ]
.text insert  end "[mc {retard}] [lindex $l_bc end ]\n " yellow
.text insert  end "--------------------------------------------------------------------\n" blue
return
}
##le premier est le calcul � faire
##les autres sauf dernier sont les calculs ou expressions faux
## set mauvais_cal_expr [ concat $mauvais_cal_expr [lrange $el 1 end-1 ]]
set l_bc [linsert $l_bc end [lindex $el 0] ]
set bon_calc_p [linsert $bon_calc_p end [lindex $el 0] ]
 set l_mc [ concat $l_mc [lrange $el 1 end-1 ]]
set mauv_calc_p [ concat $mauv_calc_p [lrange $el 1 end-1 ]]
set nbre_f [llength $l_mc ] ; set nbre_b [llength $l_bc ]
##si le dernier est {} c'est trop tard
##si le dernier nombre est �gal au premier c'est bon calcul sinon liste et on recommence
 if {[regexp {^[0-9]*\,?[0-9]+$} [lindex $el end] ]  && [expr [change_oper [lindex $el end]]] == [expr [change_oper [lindex $el 0 ] ]] }  {
##  set bon_calcul [linsert  $bon_calcul  end  [lindex $el end] ]
  set l_bc [linsert  $l_bc  end  [lindex $el end] ] ; set bon_calc_p [linsert  $bon_calc_p  end  [lindex $el end] ]
 set l_bc [concat   $l_be $l_bc ] ; set bon_calc_p [concat $l_be  $bon_calc_p ]
 incr nbre_b ; set bon_calcul $l_bc ; set mauvais_cal_expr $l_mc
## .text insert  end "[mc {essai bo ma}]  $bon_calcul \n $mauvais_cal_expr " white
  .text insert  end "[mc {bon_expr_p} ]: $bon_calc_p \n" green
.text insert  end "[mc {mauv_expr_p}] : $mauv_calc_p \n" red
.text insert  end "----------------------------------------------------------------------\n" blue
## .text insert  end "[mc {bon_c_de}] [lindex $el 0]\n" green ; return
   } else {
    if { [lindex $el end ] != {}  } {
 ## set bon_calcul [linsert $bon_calcul  end  [lindex  [lindex $el  end] 0] ]
##  set l_be [linsert  $l_be  end [lindex [lindex $el end ]  0] ] ; incr nbre_b
## .text insert  end "[mc {bon_expr_pp} ]: $l_be \n" green
traite_un  [ lindex $el end] $l_bc  $l_mc  $l_be
} else { incr nbre_t ;  traite_un  [ lindex $el end] $l_bc  $l_mc  $l_be }
}
return
}

proc termine {} {
global  liste_calculs_init nbre_f nbre_b bon_calcul  nbre_b   mauvais_cal_expr nbre_t
set nbre_bon [expr $nbre_b - [llength $liste_calculs_init ]]
.text insert  end "[mc {stat} ]\n"   white2
.text insert  end  "[ mc {soit_b_calculs}] $nbre_bon " green
.text insert  end  "[ mc {soit_m_calculs}] $nbre_f " red
.text insert end "[mc {soit_trop_tard} ] $nbre_t\n" yellow
##$liste_calculs_init \n $bon_calcul \n $mauvais_cal_expr \n $nbre_f   $nbre_b \n" white
}


proc traite_liste1 { l_rep } {
global liste_calculs_init nbre_f mauvais_cal_expr bon_calcul  liste_r  nbre_f nbre_b bon_calcul_expr 
global mauv_calc_p bon_calc_p  bon_expr_p nbre_t
 if  { $l_rep == {}  } { termine ; return }
.text insert  end  "[mc {le_calc_de}] [lindex [lindex $l_rep 0] 0 ] \n" white1
##${w2}.text insert end  "[lindex  [lindex $l_rep 0] 0]" green1
set liste_calculs_init [linsert $liste_calculs_init  end [lindex [lindex $l_rep 0] 0 ] ]
set mauv_calc_p {} ; set bon_calc_p {} ; set  bon_expr_p {}
traite_un  [lindex $l_rep 0] $bon_calcul $mauvais_cal_expr $bon_calcul_expr
traite_liste1 [ lrange $l_rep 1 end ]
}
##pour d�marrer le processus recursif et pr�senter
proc traite_liste { l_rep1 } {
global liste_calculs_init nbre_f nbre_b mauvais_cal_expr bon_calcul  set liste_r bon_calcul_expr nbre_t 
.text insert  end "[mc {la_liste_history}] :\n $l_rep1\n\n" white
set  liste_calculs_init {} ;  set nbre_f 0 ; set nbre_b 0 ;  set nbre_t  0
set  mauvais_cal_expr {} ;  set  bon_calcul  {}  ; set  bon_calcul_expr {}  ; set liste_r {}
traite_liste1 $l_rep1
##.text insert  end "[mc  {Soit :} ]\n" white1
##.text insert  end "[mc {bon_calc_expr}] : $bon_calcul \n " green
##.text insert  end "[mc {mauv_calc_expr}] : $mauvais_cal_expr \n" red
##.text insert  end "[mc {retard}] : $liste_r \n" yellow
##.text insert end  "\n---------------------------------------------------------------------------------------\n" blue

}
.text tag configure green -foreground green
.text tag configure red -foreground red
.text tag configure yellow -foreground yellow
.text tag configure normal -foreground black
.text tag configure white -foreground white
.text tag configure white1 -foreground white -underline yes
.text tag configure white2  -foreground white -underline yes -font {helvetica 16}
      while {![eof $f]}  {
	  ##resultats de l'�l�ve
           set listeval [gets $f]
	   if { $listeval == "" } {
	   continue
	   }
	     set liste_plate [split $listeval & ]
          set  date [lindex $liste_plate 0 ]  ;    set  nom  [lindex $liste_plate 1 ]
	  wm title . "Bilan de $nom"
        set  nom_exo   [lindex $liste_plate 2 ]  ;     set  oper   [lindex $liste_plate 3 ]
          set  bonne_liste   [lindex $liste_plate 4 ]
         set  liste_troptard    [lindex $liste_plate 5 ]  ;  set  mrep_ou_echecs [lindex $liste_plate 6 ]
         set logiciel [lindex $liste_plate 7 ] 
         if { $logiciel == "ap" } {
         set approx [lindex  $mrep_ou_echecs 0] ;  set n_mrep [lindex   $mrep_ou_echecs 1]
         set message " [ format [mc {avec une approximation de : %1$s et liste d'erreurs  de %2$s}] $approx ${n_mrep}]"
         }
         if { $logiciel == "pl" } {
         set message "[mc {nbre_max_err}] $mrep_ou_echecs"
        }
       if { $logiciel == "un" } {
         set message "[mc {list_err}]\n  $mrep_ou_echecs"
        	} 
	  if { $logiciel == "eq" } {
         set message "[mc {Mauvais calculs :}]\n  $mrep_ou_echecs"
        	} 
		
		           .text insert end  "${date} par  ${nom} pour l'exercice : ${nom_exo}. Op�ration : ${oper}\n"  white1
     if { $logiciel == "un" } {
 ###   set bonne_liste { {11+11 2 {2*11 22}} {15+5 5 20} }
 ## r�sultats
     traite_liste $bonne_liste
	 } else  {
   .text insert end  "[mc {bon_calc}] ou bonnes �quations :\n ${bonne_liste}\n"     green
             .text insert end " [mc {trop_tard}]\n ${liste_troptard}\n" yellow
	     .text insert end "$message\n"  red
		 }
	 .text insert end "============================================================\n"  white
	.text insert end "============================================================\n"  white
	 }
	 


close $f













